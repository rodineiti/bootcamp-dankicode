<?php
ini_set("display_errors", 1);
ini_set("error_reporting", E_ALL);
ini_set('xdebug.overload_var_dump', 1);

session_start();

$timezone = date_default_timezone_set("America/Sao_Paulo");

require __DIR__ . "/vendor/autoload.php";

try {
    require __DIR__ . '/routes/web.php';
    require __DIR__ . '/routes/projeto01.php';
    require __DIR__ . '/routes/projeto02.php';
    require __DIR__ . '/routes/projeto05.php';
    require __DIR__ . '/routes/projeto06.php';
    require __DIR__ . '/routes/projeto07.php';
    require __DIR__ . '/routes/projeto08.php';
    require __DIR__ . '/routes/admin.php';
} catch (\Exception $exception) {
    echo $exception->getMessage();
}

resolve();