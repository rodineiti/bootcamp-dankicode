$(function() {
    $('[name=value_min]').maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
    $('[name=value_max]').maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

    function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    }

    sendRequest();

    $("#search_imovel").keyup(delay(function (event) {
        if (this.value !== '' && this.value.length > 3) {
            sendRequest(this.value);
        }
    }, 500));

    function sendRequest(title = '') {
        $('form').ajaxSubmit({
            data: {'name': title, 'identifier': 'searchsite'},
            success: function (data) {
                $('.lista-imoveis .container').html(data.html);
            }
        });
    }
});