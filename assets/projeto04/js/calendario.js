$(function() {
    $('td[dia]').click(function(event) {
        $('td').removeAttr('bgcolor');
        $(this).attr('bgcolor', '#CCC');
        var novoDia = $(this).attr('dia').split('-');
        var novoDia = `${novoDia[2]}/${novoDia[1]}/${novoDia[0]}`;
        trocarDatas($(this).attr('dia'), novoDia);
        getTarefas($(this).attr('dia'));
    });

    function trocarDatas(nFormato, formatado){
        $('input[name=date]').attr('value', nFormato);
        $('form h2').html('Adicionar Tarefa para: ' + formatado);
        $('.box-tarefas .card-title').html('Adicionar Tarefa para: ' + formatado);
    }

    $('form').submit(function(event) {
        event.preventDefault();
        aplicarEvento();
    });

    function aplicarEvento(data) {
        var title = $('input[name=title]');
        var date = $('input[name=date]');
        if (title.val() != '' && date.val() != '') {
            $.ajax({
                url: baseUrl + '/calendar/store',
                dataType: 'json',
                type: 'POST',
                data: {'title': title.val(), 'date': date.val(), 'ac': 'Adicionar'}
            })
                .done(function(data) {
                    if (!data.error) {
                        $('.box-tarefas').prepend('<div class="box-alert msg-success"><i class="fa fa-check"></i> '+data.message+'</div>');
                        let template = `
					<div class="box-tarefas-single">
						<p>${data.title}</p>
					</div>
					`;
                        $('#content').append(template);
                        $('form')[0].reset();
                        setInterval(function() {
                            $('.box-alert').fadeOut();
                        }, 2000);
                    } else {
                        $('.box-tarefas').prepend('<div class="box-alert msg-error"><i class="fa fa-check"></i> '+data.message+'</div>');
                    }
                })
                .fail(function() {
                    console.log('fail');
                });
        } else {
            alert('Informe a mensagem antes de enviar');
            $('textarea').focus();
        }
    }

    function getTarefas(date) {
        $('#content').html('');
        if (date != '') {
            $.ajax({
                url: baseUrl + '/calendar/last',
                dataType: 'json',
                type: 'POST',
                data: {'date': date, 'ac': 'Pegar'}
            })
                .done(function(data) {
                    if (!data.error) {
                        data.todos.map((item, key) => {
                            let template = `
						<div class="box-tarefas-single">
							<p>${item.title}</p>
						</div>
						`;
                            $('#content').append(template);
                        });
                    } else {
                        $('.box-content').prepend('<div class="box-alert msg-error"><i class="fa fa-check"></i> Erro ao pegar mensagens</div>');
                    }
                })
                .fail(function() {
                    console.log('fail');
                });
        } else {
            alert('Informe a mensagem antes de enviar');
            $('textarea').focus();
        }
    }
});