$(function() {

    $('#content').animate({scrollTop: $('#content').get(0).scrollHeight});

    $('textarea').keyup(function(event) {
        // console.log(event);
        // // if (event.keyCode == 13 || event.which == 13) {
        // // 	sendChat();
        // // }
    });

    $('form').submit(function(event) {
        event.preventDefault();
        postChat();
    });

    function postChat() {
        var message = $('textarea');
        if (message.val() != '') {
            $.ajax({
                url: baseUrl + '/chat/store',
                dataType: 'json',
                type: 'POST',
                data: {'message': message.val(), 'ac': 'Enviar'}
            })
                .done(function(data) {
                    if (!data.error) {
                        $('.box-content').prepend('<div class="box-alert msg-success"><i class="fa fa-check"></i> Mensagem Enviada com sucesso</div>');
                        let template = `
					<div class="mensagem-chat">
						<span>${data.user}</span>
						<p>${data.message}</p>
					</div>
					`;
                        $('#content').append(template);
                        $('#content').animate({scrollTop: $('#content').get(0).scrollHeight});
                        $('form')[0].reset();
                        setInterval(function() {
                            $('.box-alert').fadeOut();
                        }, 2000);
                    } else {
                        $('.box-content').prepend('<div class="box-alert msg-error"><i class="fa fa-check"></i> Erro ao enviar mensagem</div>');
                    }
                })
                .fail(function() {
                    console.log('fail');
                });
        } else {
            alert('Informe a mensagem antes de enviar');
            $('textarea').focus();
        }
    }

    function getChat() {
        var lastId = $('lastId').attr('val');
        $.ajax({
            url: baseUrl + '/chat/last',
            dataType: 'json',
            type: 'POST',
            data: {'lastId': lastId, 'ac': 'Pegar'}
        })
            .done(function(data) {
                if (!data.error) {
                    console.log(data);
                    data.chats.map((item, key) => {
                        let template = `
					<div class="mensagem-chat">
						<span>${item.id} - ${item.user.name}</span>
						<p>${item.message}</p>
					</div>
					`;
                        $('#content').append(template);
                        $('#content').animate({scrollTop: $('#content').get(0).scrollHeight});
                    });
                } else {
                    $('.box-content').prepend('<div class="box-alert msg-error"><i class="fa fa-check"></i> Erro ao pegar mensagens</div>');
                }
            })
            .fail(function() {
                console.log('fail');
            });
    }

    setInterval(function(){
        getChat();
    }, 5000);
});