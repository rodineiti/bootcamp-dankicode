$(function() {
    $('a.btn-pagamento').click(function(event) {
        event.preventDefault();
        $.ajax({
            url: baseUrl,
            dataType: 'json',
            type: 'POST',
            data: {'ac': 'Pagar'}
        })
            .done(function(data) {
                if (data.status) {
                    var isOpenLightBox = PagSeguroLightbox({
                        code: data.code[0]
                    }, {
                        success: function(transactionCode) {
                            console.log('realizado transação');
                        },
                        abort: function() {
                            console.log('fechando janela');
                        }
                    });
                } else {
                    alert('Errou');
                }
            })
            .fail(function() {
                console.log('fail');
            })
            .always(function() {
                console.log('alwais');
            });
    });
});