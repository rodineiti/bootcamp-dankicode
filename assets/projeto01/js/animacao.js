$(function() {
	var atual = -1;
	var max = $('.box-especialidade').length - 1;
	var timer;
	var animacaoDelay = 2;

	function executarAnimacao() {
		$('.box-especialidade').hide();
		timer = setInterval(logica, animacaoDelay * 1000);
		function logica () {
			atual++;
			if (atual > max) {
				clearInterval(timer);
				return false;
			}

			$('.box-especialidade').eq(atual).fadeIn();
		}
	}

	executarAnimacao();
});