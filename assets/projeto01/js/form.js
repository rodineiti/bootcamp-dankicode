$(function() {
    $('body').on('submit', '#form', function(event) {
		event.preventDefault();
		if (validate()) {
			$.ajax({
                url: include_path + 'ajax/send.php',
                dataType: 'json',
                type: 'POST',
                data: $(this).serialize(),
                beforeSend: function( xhr ) {
    				$('.overlay-loading').fadeIn();
  				}
            })
            .done(function(data) {
            	if (data.status) {
            		$('.msg-success').slideToggle();
            		setTimeout(function() {
            			$('.msg-success').fadeOut();
            		}, 3000);
            	} else {
            		$('.msg-error').slideToggle();
            		setTimeout(function() {
            			$('.msg-error').fadeOut();
            		}, 3000);
            	}
            	$('.overlay-loading').fadeOut();
            })
            .fail(function() {
			    $('.overlay-loading').fadeOut();
			    $('.msg-error').slideToggle();
        		setTimeout(function() {
        			$('.msg-error').fadeOut();
        		}, 3000);
			})
			.always(function() {
			    $('.overlay-loading').fadeOut();
			});
		} else {
			alert('Campos vazios não permitidos');
		}
	});

	function validate() {
		if ($('input[name=name]').val() === '' || $('input[name=email]').val() === '' || $('input[name=phone]').val() === '' || $('input[name=subject]').val() === '') {
			return false;
		} else {
			return true;
		}
	}
});