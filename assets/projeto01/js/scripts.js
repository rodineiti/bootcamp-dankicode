$(function() {
	$('nav.mobile i').click(function(event) {
		$('nav.mobile').find('ul').slideToggle();
		if ($(this).hasClass('fa fa-bars')) {
			$(this).removeClass('fa fa-bars');
			$(this).addClass('fa fa-times');
		} else {
			$(this).removeClass('fa fa-times');
			$(this).addClass('fa fa-bars');
		}
	});

	if ($('target').length > 0) {
		let el = `#${$('target').attr('target')}`;
		$('html,body').animate({'scrollTop':$(el).offset().top}, 2000)
	}

	// carregaDinamico();

	function carregaDinamico() {
		$('[realtime]').click(function(event) {
			var page = $(this).attr('realtime');
			$('.container-principal').hide();
			$('.container-principal').load(include_path+'pages/'+page+'.php');

			setTimeout(function() {
				initialize();
				var content = '<p style="color:black;font-size:13px;padding:10px 0;">Custom Address</p>';
				addMarker(-34.397, 150.644, '', content, true);
				
				map.panTo({lat:-23.550520,lng:-46.633309});
				map.setZoom(15);
			}, 2000);

			$('.container-principal').fadeIn(1000);

			return false;
		});
	}
});