$(function() {
	var map;
	function initialize() {
		var mapProps = {
			center: new google.maps.LatLng(-34.397,150.644),
			scrollWheel: false,
			zoom: 8,
			MapTypeId: google.maps.MapTypeId.ROADMAP
		}
		map = new google.maps.Map(document.getElementById('map'), mapProps);
	}

	function addMarker(lat, lng, icon, content, click) {
		var latLng = { lat: lat, lng: lng };

		var marker = new google.maps.Marker({
			position: latLng,
			map:map,
			icon:icon
		});

		var infoWindow = new google.maps.InfoWindow({
			content:content,
			maxWidth: 200,
			pixelOffset: new google.maps.Size(0,20),
		});
		
		if (click) {
			google.maps.event.addListener(marker, 'click', function() {
				infoWindow.open(map, marker);
			});
		} else {
			infoWindow.open(map, marker);
		}	
	}

	initialize();
	var content = '<p style="color:black;font-size:13px;padding:10px 0;">Custom Address</p>';
	addMarker(-34.397, 150.644, '', content, true);

	setTimeout(function() {
		map.panTo({lat:-23.550520,lng:-46.633309});
		map.setZoom(15);
	}, 2000);
});