$(function() {
	var current = 0;
	var max = $('.banner-single').length - 1;

	function changeSlide() {
		setInterval(function() {
			$('.banner-single').eq(current).animate({'opacity':'0'}).fadeOut(2000);
			current++;
			if (current > max) {
				current = 0;
			}
			$('.banner-single').eq(current).animate({'opacity':'1'}).fadeIn(2000);
			$('.bullets span').removeClass('active-slide');
			$('.bullets span').eq(current).addClass('active-slide');
		}, 3000);
	}

	function initSlide() {
		$('.banner-single').hide();
		$('.banner-single').eq(0).show();
		for (var i = 0; i <= max; i++) {
			var content = $('.bullets').html();
			if (i == 0) {
                content+='<span class="active-slide"></span>';
            } else {
                content+='<span></span>';
            }
            $('.bullets').html(content);
		}
	}

	$('body').on('click', '.bullets span', function(event) {
		var self = $(this);
		$('.banner-single').eq(current).animate({'opacity':'0'}).fadeOut();
		current = self.index();
		$('.banner-single').eq(current).animate({'opacity':'1'}).fadeIn();
		$('.bullets span').removeClass('active-slide');
		self.addClass('active-slide');
	});

	initSlide();
	changeSlide();
});