$(function() {
	$( ".boxes" ).sortable({
		start: function() {
			var el = $(this);
			el.find('.box-single-wrapper > div:nth-of-type(1)').css({'border': '2px dashed #ccc'});
		},
		update: function(event, ui) {
			var data = $(this).sortable('serialize');
			var el = $(this);
			el.find('.box-single-wrapper > div:nth-of-type(1)').css({'border': '1px solid #ccc'});
			$.ajax({
				url: baseUrl + '/ajax/empreendimentos',
				type: 'POST',
				data: data,
			})
			.done(function(data) {
				console.log('atualizado');
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		},
		stop: function() {
			var el = $(this);
			el.find('.box-single-wrapper > div:nth-of-type(1)').css({'border': '1px solid #ccc'});
		}
	});
});