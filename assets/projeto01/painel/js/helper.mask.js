$(function() {
	$('[name=cpf]').mask('999.999.999-99');
	$('[name=cnpj]').mask('99.999.999/9999-99');
	$('[name=value]').maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

	$('[name=type]').change(function(event) {
		if ($(this).val() == 'PF') {
			$('[name=cpf]').parent().show();
			$('[name=cnpj]').parent().hide();
		} else {
			$('[name=cpf]').parent().hide();
			$('[name=cnpj]').parent().show();
		}
	});
});