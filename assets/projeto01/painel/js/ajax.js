$(function() {
	$('.ajax').ajaxForm({
		dataType: 'json',
		beforeSend: function() {
			$('.ajax').animate({'opacity': '0.6'});
			$('.ajax').find('input[type=submit]').attr('disabled', 'true');
		},
		success: function(data) {
			$('.ajax').animate({'opacity': '1'});
			$('.ajax').find('input[type=submit]').removeAttr('disabled');
			$('.box-alert').remove();
			if (data.status) {
				$('.ajax').prepend('<div class="box-alert msg-success"><i class="fa fa-check"></i> '+data.message+'</div>');
				if ($('.ajax')[0].hasAttribute('autalizar') == false) {
					$('.ajax')[0].reset();
				}
			} else {
				$('.ajax').prepend('<div class="box-alert msg-error"><i class="fa fa-check"></i> '+data.message+'</div>');
			}
		}
	});

	$('.btn.delete').click(function(event) {
		event.preventDefault();
		var el = $(this).parent().parent().parent();
		var item_id = $(this).attr('item_id');
		$.ajax({
			url: include_path + 'ajax/forms.php',
			type: 'POST',
			dataType: 'json',
			data: {id: item_id, ac: 'Deletar'},
		})
		.done(function(data) {
			if (data.status) {
				el.fadeOut();
				$('.boxes').prepend('<div class="box-alert msg-success"><i class="fa fa-check"></i> '+data.message+'</div>');
			} else {
				$('.boxes').prepend('<div class="box-alert msg-error"><i class="fa fa-check"></i> '+data.message+'</div>');
			}
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		return false;		
	});
});