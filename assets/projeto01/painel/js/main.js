$(function() {
	var open = true;
	var wd = $(window)[0].innerWidth;
	var targetMenuSize = (wd <= 400) ? 200 : 250;
	if (wd <= 768) {
		$('.menu').css({'width':'0','padding':'0'});
		open = false;
	}

	$('.menu-btn').click(function(event) {
		if (open) {
			$('.menu').animate({'width':0,'padding':0}, () => open = false);
			$('header,.content').css({'width':'100%'});
			$('header,.content').animate({'left':0}, () => open = false);
		} else {
			$('.menu').css({'display':'block'})
			$('.menu').animate({'width':targetMenuSize+'px','padding':'10px 0'}, () => open = true);
			if (wd > 768) {
				$('header,.content').css({'width':'calc(100% - 250px)'});
			}
			$('header,.content').animate({'left':targetMenuSize+'px'}, () => open = true);
		}
	});
	
	$(window).resize(function(event) {
		wd = $(window)[0].innerWidth;
		targetMenuSize = (wd <= 400) ? 200 : 250;
		if (wd <= 768) {
			$('.menu').css({'width':'0','padding':'0'});
			$('header,.content').css({'width':'100%','left':'0'});
		// } else {
		// 	$('header,.content').css({'width':'calc(100% - 250px)','left':'250px'});
		// 	$('.menu').css({'width':'250px','padding':'10px 0'});
			open = false;			
		}		
	});

	$('[formato=data]').mask('99/99/9999');

	$('[actionBtn=delete]').click(function(event) {
		if (confirm("Tem certeza que quer deletar?")) {
			return true;
		}
		return false;
	});
});