<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=SITE_NAME?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?=asset("projeto01/css/fontawesome/all.css")?>">
    <link rel="stylesheet" href="<?=asset("projeto02/css/styles.css")?>">
</head>
<body>
<header>
    <div class="container">
        <div class="logo">
            <a style="color:white;" href="<?= route("projeto02.home"); ?>">Portal Imobiliário</a>
        </div>
        <nav class="desktop">
            <ul>
                <?php foreach(getMenusProjeto02() as $key => $value): ?>
                    <li><a href="<?= route("projeto02.empreentimento.show", ["slug" => $value->id]); ?>"><?=$value->name?></a></li>
                <?php endforeach; ?>
            </ul>
        </nav>
        <div class="clear"></div>
    </div>
</header><!-- /header -->

<section class="search1">
    <div class="container">
        <h2>O que você procura?</h2>
        <input type="text" name="search_imovel" id="search_imovel" />
    </div>
</section>

<section class="search2">
    <div class="container">
        <form method="post" action="<?=route("projeto02.ajax.send"); ?>">
            <div class="form-group">
                <label>Área Mínima</label>
                <input type="number" min="0" max="100000" step="100" name="area_min" id="area_min" />
            </div>
            <div class="form-group">
                <label>Área Máxima</label>
                <input type="number" min="0" max="100000" step="100" name="area_max" id="area_max" />
            </div>
            <div class="form-group">
                <label>Preço Mínimo</label>
                <input type="text" name="value_min" id="value_min" />
            </div>
            <div class="form-group">
                <label>Preço Máximo</label>
                <input type="text" name="value_max" id="value_max" />
            </div>
            <div class="clear"></div>
        </form>
    </div>
</section>

<?php $this->viewTemplate($view, $data); ?>

<script>var baseUrl = '<?=BASE_URL?>projeto02/';</script>
<script src="<?=asset("js/jquery.min.js")?>"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="<?=asset("projeto01/js/constants.js")?>"></script>
<?php loadJSAdmin([
    'projeto01/painel/js/jquery.maskMoney.js',
    'projeto01/painel/js/jquery.form.min.js',
    'projeto02/js/script.js'
],'projeto02')?>
</body>
</html>