<section class="lista-imoveis">
    <div class="container">
        <h2 class="card-title">Listando <b><?=$item->imoveis()->count()?> imóveis</b> de <?=$item->name?></h2>
        <?php foreach($item->imoveis()->all() as $key => $value): ?>
            <?php $image = $value->images()->first(); ?>
            <div class="row-imoveis">
                <div class="r1">
                    <img src="<?=media('uploads/' . $image->image)?>" alt="img" />
                </div>
                <div class="r2">
                    <p><i class="fa fa-info"></i> Nome do Imóvel: <b><?=$value->name?></b></p>
                    <p><i class="fa fa-info"></i> Área do Imóvel: <b><?=$value->area?></b></p>
                    <p><i class="fa fa-info"></i> Valor do Imóvel: <b><?=$value->value?></b></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>