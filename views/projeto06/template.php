<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=SITE_NAME?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?=asset("projeto01/css/fontawesome/all.css")?>">
    <link rel="stylesheet" href="<?=asset("projeto06/css/style.css")?>">
</head>
<body>

<header>
    <div class="container">
        <div class="logo"><a href="<?=route('projeto06.shop')?>">Loja Virtual</a></div>
        <nav class="desktop">
            <ul>
                <li><a href="javascript:void(0)">Carrinho (<?=cart()->count()?>)</a></li>
                <li style="background: #1e88e0;"><a href="<?=route('projeto06.checkout')?>">Finalizar Compra</a></li>
            </ul>
        </nav>
        <div class="clear"></div>
    </div>
</header>

<?php if ($errors = flashMessage("errors")): ?>
    <div class="alert alert-<?=$errors["status"]?>">
        <?php if (is_array($errors["messages"])): ?>
            <ul>
                <?php foreach ($errors["messages"] as $error): ?>
                    <li><?=$error?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
        <?php if (is_string($errors["messages"])): ?>
            <?=$errors["messages"]?>
        <?php endif; ?>
    </div>
<?php endif; ?>

<?php $this->viewTemplate($view, $data); ?>

<script>var baseUrl = '<?=BASE_URL?>projeto06/';</script>
<script src="<?=asset("js/jquery.min.js")?>"></script>
<script src="<?=asset("projeto06/js/script.js")?>"></script>
</body>
</html>