<div class="chamada-escolher">
    <div class="container">
        <h2>Escolha seus produtos e compre agora!</h2>
    </div>
</div>

<div class="lista-items">
    <div class="container">
        <?php foreach ($products as $value): ?>
            <?php $image = $value->images()->first(); ?>
            <div class="produto-sigle-box">
                <img src="<?=media('uploads/'.$image->image)?>" alt="img">
                <p>Preço: R$ <?=$value->value?></p>
                <a href="<?=route('projeto06.addToCart', ["id" => $value->id])?>">Adicionar no carrinho</a>
            </div>
        <?php endforeach; ?>
    </div>
</div>