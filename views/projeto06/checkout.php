<div class="chamada-escolher">
    <div class="container">
        <h2>Feche seu pedido agora!</h2>
    </div>
</div>

<div class="tabela-pedidos">
    <div class="container">
        <h2>Carrinho:</h2>
        <table>
            <tr>
                <td>Nome do produto</td>
                <td>Quantidade</td>
                <td>Valor</td>
            </tr>
            <?php foreach(cart()->get() as $value): ?>
                <tr>
                    <td><?=$value['name']?></td>
                    <td><?=$value['qty']?></td>
                    <td><?=$value['value']?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<div class="finalizar-pedido">
    <div class="container">
        <h2>Total: R$ <?=cart()->total()?></h2>
        <div class="clear"></div>
        <a href="" class="btn-pagamento">Realizar Pagamento</a>
        <div class="clear"></div>
    </div>
</div>