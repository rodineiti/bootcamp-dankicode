<target target="<?=$target?>" />

<section class="banner-container">
    <?php foreach ($slides as $key => $value): ?>
        <div style="background-image: url('<?=media('uploads/' . $value->slide)?>');" class="banner-single"></div>
    <?php endforeach; ?>
    <div class="overlay"></div>
    <div class="container">
        <form method="post" id="form">
            <h2>Qual o seu melhor e-mail?</h2>
            <input type="email" name="email" required />
            <input type="submit" name="ac" value="Cadastrar">
            <input type="hidden" name="identifier" value="home">
        </form>
    </div><!-- container -->
    <div class="bullets"></div>
</section><!-- banner-container -->

<section id="sobre" class="descricao-autor">
    <div class="container">
        <div class="w100 left">
            <h2 class="text-center">
                <img src="<?=asset('projeto01/images/7.jpg')?>" alt="img" />
                <?=config_projeto01()->author?>
            </h2>
            <p><?=config_projeto01()->description?></p>
        </div>
        <div class="clear"></div>
    </div><!-- container -->
</section><!-- descricao-autor -->

<section id="especialidades" class="especialidades">
    <h2 class="title">Especialidades</h2>
    <div class="container">
        <div class="w33 left box-especialidade">
            <h3><i class="<?=config_projeto01()->icon1?>"></i></h3>
            <h4><?=config_projeto01()->title1?></h4>
            <p><?=config_projeto01()->description1?></p>
        </div><!-- box-especialidade -->
        <div class="w33 left box-especialidade">
            <h3><i class="<?=config_projeto01()->icon2?>"></i></h3>
            <h4><?=config_projeto01()->title2?></h4>
            <p><?=config_projeto01()->description2?></p>
        </div><!-- box-especialidade -->
        <div class="w33 left box-especialidade">
            <h3><i class="<?=config_projeto01()->icon3?>"></i></h3>
            <h4><?=config_projeto01()->title3?></h4>
            <p><?=config_projeto01()->description3?></p>
        </div><!-- box-especialidade -->
        <div class="clear"></div>
    </div><!-- container -->
</section><!-- especialidades -->

<section id="depoimentos" class="extras">
    <div class="container">
        <div class="w50 left container-depoimentos">
            <h2 class="title">Depoimentos</h2>
            <?php foreach ($depoimentos as $key => $value): ?>
                <div class="depoimento-sigle">
                    <div class="depoimento-descricao">
                        <?=$value->description?>
                    </div>
                    <p class="nome-autor">
                        <?=$value->name?>
                        <span style="font-weight: 300; font-size: 12px; font-style: italic;">
                            <?=date('d/m/Y',strtotime($value->created_at))?>
                        </span>
                    </p>
                </div>
            <?php endforeach; ?>
        </div>
        <div id="servicos" class="w50 left container-servicos">
            <h2 class="title">Serviços</h2>
            <div class="servicos">
                <ul>
                    <?php foreach ($servicos as $key => $value): ?>
                        <li><?=$value->name?> - <?=$value->description?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="clear"></div>
    </div><!-- container -->
</section><!-- extras -->