<section class="noticia-single">
    <div class="container">
        <header>
            <h1><i class="fa fa-calendar"></i> <?=$post->dateNews?> - <?=$post->title?></h1>
        </header><!-- /header -->
        <article>
            <p><?=$post->description?></p>
            <img src="<?=media('uploads/'.$post->image)?>" alt="img" />
        </article>
        <div class="container-error-login">
            <?php if(check("admins")): ?>
                <h2 class="postar-comentario">
                    Faça um comentário <i class="fa fa-comments"></i>
                </h2>
                <form method="post" action="<?=route('admin.comentarios.store')?>">
                    <input type="text" name="userName" value="<?=auth("admins")->name?>" disabled>
                    <textarea placeholder="Seu comentário" name="comment"></textarea>
                    <input type="hidden" name="news_id" value="<?=$post->id?>">
                    <input type="submit" name="ac" value="Comentar">
                </form>
                <h2 class="postar-comentario">
                    Comentários recentes
                </h2>
                <?php $comments = $post->comments()->all() ?? []; ?>
                <?php foreach($comments as $value): ?>
                    <div class="box-comment-noticia">
                        <h3><?=$value->user()->name?></h3>
                        <p><?=$value->comment?></p>
                        <form method="post" action="<?=route('admin.respostas.store')?>">
                            <input type="text" name="userName" value="<?=auth("admins")->name?>" disabled>
                            <textarea placeholder="Sua resposta" name="answer"></textarea>
                            <input type="hidden" name="comment_id" value="<?=$value->id?>">
                            <input type="hidden" name="news_id" value="<?=$post->id?>">
                            <input type="submit" name="ac_comment" value="Responder">
                        </form>
                    </div>
                    <h2 class="postar-comentario">Respostas do comentário</h2>
                    <?php $answers = $value->answers()->all() ?? []; ?>
                    <?php foreach($answers as $value2): ?>
                        <div class="box-comment-noticia">
                            <h3><?=$value2->user()->name?></h3>
                            <p><?=$value2->answer?></p>
                        </div>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            <?php else: ?>
                <p>Para comentar você precisa se logar. Para se logar <a href="<?=route('admin.login')?>">Clique aqui</a></p>
            <?php endif; ?>
        </div>
    </div>
</section>