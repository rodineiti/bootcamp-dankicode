<section class="header-noticias">
    <div class="container">
        <h2><i class="fa fa-home"></i></h2>
        <h2>Acompanhe as últimas <b>notícias do Portal</b></h2>
    </div>
</section><!-- header-noticias -->

<section class="container-portal">
    <div class="container">
        <div class="sidebar">
            <div class="box-content-sidebar">
                <h3><i class="fa fa-search"></i> Realizar uma busca</h3>
                <form method="get">
                    <input type="text" name="search" placeholder="O que procura?" required />
                    <input type="submit" name="ac" value="Pesquisar">
                </form>
            </div>
            <div class="box-content-sidebar">
                <h3><i class="fa fa-search"></i> Por Categoria</h3>
                <form>
                    <select name="slug" id="slug">
                        <option value="all">Todas as categorias</option>
                        <?php foreach ($categorias as $key => $value): ?>
                            <option
                                    value="<?=$value->slug?>"
                                    <?php if ($slug === $value->slug): ?>selected<?php endif; ?>><?=$value->title?></option>
                        <?php endforeach; ?>
                    </select>
                </form>
            </div>
            <div class="box-content-sidebar">
                <h3><i class="fa fa-user"></i> Sobre o autor</h3>
                <div class="autor-box-portal">
                    <div class="image-autor-portal text-center">
                        <img src="<?=asset('projeto01/images/7.jpg')?>" alt="img">
                    </div>
                    <div class="texto-autor-portal text-center">
                        <h3><?=config_projeto01()->author?></h3>
                        <p><?=substr(config_projeto01()->description, 0, 300).'...'?></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="conteudo-portal">
            <div class="header-conteudo-portal">
                <?php if ($slug !== 'all'): ?>
                    <h2>Visualizando Posts em <span><?=$categoria->title?></span></h2>
                <?php else: ?>
                    <h2>Visualizando todos os Posts</h2>
                <?php endif; ?>
            </div>
            <?php foreach ($noticias as $key => $value): ?>
                <div class="box-single-conteudo-portal">
                    <h2><?=date('d/m/Y', strtotime($value->dateNews))?>- <?=$value->title?></h2>
                    <p><?=substr(strip_tags($value->description), 0, 400).'...'?></p>
                    <a href="<?=route('projeto01.noticia_single', ['id' => $value->id])?>">Leia mais</a>
                </div>
            <?php endforeach; ?>

            <div class="paginator">
                <?php for($i = 1; $i <= $pages; $i++): ?>
                    <?php if ($i === $page): ?>
                        <a class="active-page"><?=$i?></a>
                    <?php else: ?>
                        <a href="<?=route('projeto01.noticias', ['slug' => $slug])?>?<?php
                        $pageArray = $_GET;
                        unset($pageArray["uri"]);
                        unset($pageArray["ac"]);
                        $pageArray["page"] = $i;
                        echo http_build_query($pageArray);
                        ?>"><?=$i?></a>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>

        </div>

        <div class="clear"></div>
        <div class="container">
</section>