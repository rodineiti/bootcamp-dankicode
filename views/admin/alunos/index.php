<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.alunos.create"); ?>" class="btn btn-primary mb-2">Adicionar</a>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Criado em</th>
                    <th scope="col">Opções</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $user): ?>
                    <tr>
                        <th scope="row"><?= $user->id ?></th>
                        <td><?= $user->name ?></td>
                        <td><?= $user->email ?></td>
                        <td><?= $user->created_at ?></td>
                        <td>
                            <a href="<?= route("admin.alunos.edit", ["id" => $user->id]); ?>" class="btn btn-info">Editar</a>
                            <a
                                    onclick="return confirm('Tem certeza que deseja deletar?')"
                                    href="<?= route("admin.alunos.destroy", ["id" => $user->id]); ?>"
                                    class="btn btn-danger">Deletar</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>