<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.noticias.index"); ?>" class="btn btn-info mb-2">Voltar</a>
            <h1>Adicionar notícia</h1>
            <form method="POST" action="<?= route("admin.noticias.store"); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="category_id">Categoria:</label>
                    <select name="category_id" id="category_id" class="form-control" required>
                        <option value="">Selecione</option>
                        <?php foreach ($categorias as $item): ?>
                            <option value="<?=$item->id?>"><?=$item->title?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Título:</label>
                    <input type="text" name="title" id="title" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="description">Descrição:</label>
                    <textarea name="description" id="description" class="form-control tinymce" rows="5" required></textarea>
                </div>
                <div class="form-group">
                    <label for="image">Imagem:</label>
                    <input type="file" name="image" id="image" class="form-control" required />
                </div>
                <input type="submit" value="Criar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>