<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.noticias.index"); ?>" class="btn btn-info mb-2">Voltar</a>
            <h1>Editar notícia</h1>
            <form method="POST" action="<?= route("admin.noticias.update", ["id" => $item->id]); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="category_id">Categoria:</label>
                    <select name="category_id" id="category_id" class="form-control" required>
                        <option value="">Selecione</option>
                        <?php foreach ($categorias as $category): ?>
                            <option value="<?=$category->id?>" <?php if ($category->id === $item->category_id): ?>selected<?php endif; ?>><?=$category->title?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Título:</label>
                    <input type="text" name="title" id="title" value="<?= $item->title?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="description">Descrição:</label>
                    <textarea name="description" id="description" class="form-control tinymce" rows="5" required><?= $item->description?></textarea>
                </div>
                <div class="form-group">
                    <label for="image">Imagem:</label>
                    <input type="file" name="image" id="image" class="form-control" />
                </div>
                <input type="submit" value="Editar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>