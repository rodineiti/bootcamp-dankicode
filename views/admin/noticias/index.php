<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.noticias.create"); ?>" class="btn btn-primary mb-2">Adicionar</a>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Título</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Image</th>
                    <th scope="col">Criado em</th>
                    <th scope="col">Opções</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($list as $item): ?>
                    <tr>
                        <th scope="row"><?= $item->id ?></th>
                        <td><?= $item->title ?></td>
                        <td><?= $item->category()->title ?></td>
                        <td>
                            <img width="100" class="img-fluid" src="<?=media('uploads/' . $item->image)?>" alt="<?=$item->title?>">
                        </td>
                        <td><?= $item->created_at ?></td>
                        <td>
                            <a href="<?= route("admin.noticias.edit", ["id" => $item->id]); ?>" class="btn btn-info">Editar</a>
                            <a onclick="return confirm('Deseja realmente deletar?');" href="<?= route("admin.noticias.destroy", ["id" => $item->id]); ?>" class="btn btn-danger">
                                Deletar
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>