<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a target="_blank" href="<?= route("admin.pagamentos.pdfFinance", ["status" => "pendente"]); ?>" class="btn btn-warning mb-2">Gerar PDF</a>
            <h1>Pagamentos pendentes</h1>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome do pagamento</th>
                    <th scope="col">Nome do Cliente</th>
                    <th scope="col">Parcela</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Vencimento</th>
                    <th scope="col">Marcar como pago</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($financesP as $finance): ?>
                    <tr>
                        <th scope="row"><?= $finance->id ?></th>
                        <td><?= $finance->name ?></td>
                        <td><?= $finance->client()->name ?></td>
                        <td><?= $finance->portion ?></td>
                        <td><?= str_price($finance->value) ?></td>
                        <td><?= $finance->expiration ?></td>
                        <td>
                            <a onclick="return confirm('Deseja realmente alterar para Pago?');"
                               href="<?= route("admin.pagamentos.updateStatusPayment", ["client_id" => $finance->client()->id, "finance_id" => $finance->id]); ?>"
                               class="btn btn-success">
                                Pagar
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <hr>
            <a target="_blank" href="<?= route("admin.pagamentos.pdfFinance", ["status" => "concluido"]); ?>" class="btn btn-warning mb-2">Gerar PDF</a>
            <h1>Pagamentos concluídos</h1>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome do pagamento</th>
                    <th scope="col">Nome do Cliente</th>
                    <th scope="col">Parcela</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Vencimento</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($financesC as $finance): ?>
                    <tr>
                        <th scope="row"><?= $finance->id ?></th>
                        <td><?= $finance->name ?></td>
                        <td><?= $finance->client()->name ?></td>
                        <td><?= $finance->portion ?></td>
                        <td><?= str_price($finance->value) ?></td>
                        <td><?= $finance->expiration ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>