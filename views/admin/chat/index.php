<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <div class="box-content">
                <h1><i class="fa fa-comments"></i> Chat Online</h1>
                <div class="box-chat-online" id="content">
                    <?php foreach ($chats as $key => $value): ?>
                        <?php $lastId = $value->id; ?>
                        <div class="mensagem-chat">
                            <span><?=$value->id?> - <?=$value->user()->name?></span>
                            <p><?=$value->message?></p>
                        </div>
                    <?php endforeach; ?>
                    <?php setInput('lastId', $lastId); ?>
                </div>
                <form method="post">
                    <textarea name="message"></textarea>
                    <input type="submit" name="ac" value="Enviar" />
                </form>
            </div>
        </div>
    </div>
</div>