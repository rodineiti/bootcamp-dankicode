<div class="card" style="min-height: 100%;">
    <div class="card-body">
        <div class="nav flex-column nav-pills" aria-orientation="vertical">
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/home"])?>" href="<?= route("admin.home");?>">
                Home
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/users","admin/users/create","admin/users/edit"])?>"
               href="<?= route("admin.users.index");?>">
                Usuários
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/depoimentos","admin/depoimentos/create","admin/depoimentos/edit"])?>"
               href="<?= route("admin.depoimentos.index");?>">
                Depoimentos
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/servicos","admin/servicos/create","admin/servicos/edit"])?>"
               href="<?= route("admin.servicos.index");?>">
                Serviços
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/slides","admin/slides/create","admin/slides/edit"])?>"
               href="<?= route("admin.slides.index");?>">
                Slides
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/config","admin/config/create","admin/config/edit"])?>"
               href="<?= route("admin.config.edit");?>">
                Configurações do site
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/clientes","admin/clientes/create","admin/clientes/edit"])?>"
               href="<?= route("admin.clientes.index");?>">
                Clientes
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/categorias","admin/categorias/create","admin/categorias/edit"])?>"
               href="<?= route("admin.categorias.index");?>">
                Categorias
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/noticias","admin/noticias/create","admin/noticias/edit"])?>"
               href="<?= route("admin.noticias.index");?>">
                Noticias
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/pagamentos","admin/pagamentos/create","admin/pagamentos/edit"])?>"
               href="<?= route("admin.pagamentos.index");?>">
                Lista de pagamentos
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/produtos","admin/produtos/create","admin/produtos/edit"])?>"
               href="<?= route("admin.produtos.index");?>">
                Produtos
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/empreendimentos","admin/empreendimentos/create","admin/empreendimentos/edit"])?>"
               href="<?= route("admin.empreendimentos.index");?>">
                Empreendimentos
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/chat"])?>"
               href="<?= route("admin.chat.index");?>">
                Chat
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/calendar"])?>"
               href="<?= route("admin.calendar.index");?>">
                Calendário
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/alunos","admin/alunos/create","admin/alunos/edit"])?>"
               href="<?= route("admin.alunos.index");?>">
                Alunos
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/modulos","admin/modulos/create","admin/modulos/edit"])?>"
               href="<?= route("admin.modulos.index");?>">
                Módulos
            </a>
            <a class="nav-link border mb-1 <?=setMenuActive(["admin/aulas","admin/aulas/create","admin/aulas/edit"])?>"
               href="<?= route("admin.aulas.index");?>">
                Aulas
            </a>
        </div>
    </div>
</div>