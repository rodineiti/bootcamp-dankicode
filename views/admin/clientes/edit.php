<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.clientes.index"); ?>" class="btn btn-info mb-2">Voltar</a>
            <h1>Editar cliente</h1>
            <form method="POST" action="<?= route("admin.clientes.update", ["id" => $item->id]); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome:</label>
                    <input type="text" name="name" id="name" class="form-control" value="<?=$item->name?>" required />
                </div>
                <div class="form-group">
                    <label for="email">E-mail:</label>
                    <input type="text" name="email" id="email" class="form-control" value="<?=$item->email?>" required />
                </div>
                <div class="form-group">
                    <label for="type">Tipo:</label>
                    <select name="type" id="type" class="form-control">
                        <option value="PF" <?php if ($item->type === "PF"): ?>selected<?php endif; ?>>Pessoa Física</option>
                        <option value="PJ" <?php if ($item->type === "PJ"): ?>selected<?php endif; ?>>Pessoa Jurídica</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="cpf_cnpj">CPF/CNPJ:</label>
                    <input type="text" name="cpf_cnpj" id="cpf_cnpj" class="form-control" value="<?=$item->cpf_cnpj?>" required />
                </div>
                <div class="form-group">
                    <label for="image">Imagem:</label>
                    <input type="file" name="image" id="image" class="form-control" />
                </div>
                <input type="submit" value="Editar" class="btn btn-primary" />
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-9">
            <hr>
            <h1>Adicionar pagamento</h1>
            <form method="POST" action="<?= route("admin.clientes.addPayment", ["id" => $item->id]); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome do pagamento:</label>
                    <input type="text" name="name" id="name" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="value">Valor:</label>
                    <input type="text" name="value" id="value" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="portion">Parcelas:</label>
                    <input type="number" name="portion" id="portion" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="interval">Intervalo:</label>
                    <input type="number" name="interval" id="interval" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="expiration">Data de vencimento:</label>
                    <input type="date" name="expiration" id="expiration" class="form-control" required />
                </div>
                <input type="submit" value="Adicionar" class="btn btn-primary" />
            </form>
            <hr>
            <h1>Pagamentos pendentes</h1>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome do pagamento</th>
                    <th scope="col">Parcela</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Vencimento</th>
                    <th scope="col">Marcar como pago</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($financesP as $finance): ?>
                    <tr>
                        <th scope="row"><?= $finance->id ?></th>
                        <td><?= $finance->name ?></td>
                        <td><?= $finance->portion ?></td>
                        <td><?= str_price($finance->value) ?></td>
                        <td><?= $finance->expiration ?></td>
                        <td>
                            <a onclick="return confirm('Deseja realmente alterar para Pago?');"
                               href="<?= route("admin.clientes.updateStatusPayment", ["id" => $item->id, "finance_id" => $finance->id]); ?>"
                               class="btn btn-success">
                                Pagar
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <hr>
            <h1>Pagamentos concluídos</h1>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome do pagamento</th>
                    <th scope="col">Parcela</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Vencimento</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($financesC as $finance): ?>
                    <tr>
                        <th scope="row"><?= $finance->id ?></th>
                        <td><?= $finance->name ?></td>
                        <td><?= $finance->portion ?></td>
                        <td><?= str_price($finance->value) ?></td>
                        <td><?= $finance->expiration ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>