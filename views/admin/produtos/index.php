<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.produtos.create"); ?>" class="btn btn-primary mb-2">Adicionar</a>

            <div class="row">
                <?php foreach ($list as $item): ?>
                <?php $image = $item->images()->first(); ?>
                    <div class="col-sm-4 mt-3">
                        <div class="card mb-3" style="max-width: 540px;min-height: 400px;">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <?php if ($image): ?>
                                    <img src="<?=media('uploads/' . $image->image)?>"
                                         class="card-img" alt="<?=$item->name?>">
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title">Nome: <?=$item->name?></h5>
                                        <p class="card-text">Descrição: <strong><?=str_limit_chars($item->description, 50)?></strong></p>
                                        <p class="card-text">Largura: <strong><?=$item->width?></strong></p>
                                        <p class="card-text">Altura: <strong><?=$item->height?></strong></p>
                                        <p class="card-text">Comprimento: <strong><?=$item->length?></strong></p>
                                        <p class="card-text">Peso: <strong><?=$item->weight?></strong></p>
                                        <p class="card-text">Valor: <strong><?=str_price($item->value)?></strong></p>
                                        <p class="card-text"><small class="text-muted"><?=date_fmt($item->created_at)?></small></p>
                                    </div>
                                    <form method="POST" action="<?= route("admin.produtos.updateQty", ["id" => $item->id]); ?>">
                                        <div class="form-row align-items-center">
                                            <div class="col-auto">
                                                <input type="number" name="qty" class="form-control mb-2" id="qty" value="<?=$item->qty?>" />
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" class="btn btn-primary mb-2">Atualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                    <hr>
                                    <div class="card-body">
                                        <a href="<?= route("admin.produtos.edit", ["id" => $item->id]); ?>" class="btn btn-info">Editar</a>
                                        <a onclick="return confirm('Deseja realmente deletar?');" href="<?= route("admin.produtos.destroy", ["id" => $item->id]); ?>" class="btn btn-danger">
                                            Deletar
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>