<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.produtos.index"); ?>" class="btn btn-info mb-2">Voltar</a>
            <h1>Adicionar produto</h1>
            <form method="POST" action="<?= route("admin.produtos.store"); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome do produto:</label>
                    <input type="text" name="name" id="name" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="description">Descrição:</label>
                    <textarea name="description" id="description" class="form-control ckeditor" rows="5" required></textarea>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-2">
                            <label for="width">Largura:</label>
                            <input type="number" name="width" id="width" class="form-control" value="0" required />
                        </div>
                        <div class="col-sm-2">
                            <label for="height">Altura:</label>
                            <input type="number" name="height" id="height" class="form-control" value="0" required />
                        </div>
                        <div class="col-sm-2">
                            <label for="length">Comprimento:</label>
                            <input type="number" name="length" id="length" class="form-control" value="0" required />
                        </div>
                        <div class="col-sm-2">
                            <label for="weight">Peso:</label>
                            <input type="number" name="weight" id="weight" class="form-control" value="0" required />
                        </div>
                        <div class="col-sm-2">
                            <label for="qty">Quantidade:</label>
                            <input type="number" name="qty" id="qty" class="form-control" value="0" required />
                        </div>
                        <div class="col-sm-2">
                            <label for="value">Valor:</label>
                            <input type="text" name="value" id="value" class="form-control" value="0" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="images">Imagems:</label>
                        <input type="file" name="images[]" multiple id="images"
                               accept="image/png, image/jpg, image/jpeg"
                               class="form-control" required />
                    </div>
                </div>
                <input type="submit" value="Criar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>