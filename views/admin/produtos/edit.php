<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.produtos.index"); ?>" class="btn btn-info mb-2">Voltar</a>
            <?php $images = $item->images()->all() ?? []; ?>
            <?php if (count($images)): ?>
            <div class="jumbotron jumbotron-fluid">
                <div class="container-fluid">
                    <div class="row">
                        <?php foreach ($images as $image): ?>
                            <div class="col-sm-3">
                                <div class="card" style="width: 18rem;">
                                    <img src="<?=media('uploads/'.$image->image)?>" class="card-img-top" alt="<?=$image->image?>">
                                    <div class="card-body">
                                        <a onclick="return confirm('Deseja realmente deletar?');"
                                           href="<?= route("admin.produtos.destroyImage", ["product_id" => $item->id, "id" => $image->id]); ?>"
                                           class="btn btn-danger">
                                            Deletar
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <h1>Editar produto: <?=$item->name?></h1>
            <form method="POST" action="<?= route("admin.produtos.update", ["id" => $item->id]); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome do produto:</label>
                    <input type="text" name="name" id="name" class="form-control" value="<?=$item->name?>" required />
                </div>
                <div class="form-group">
                    <label for="description">Descrição:</label>
                    <textarea name="description" id="description" class="form-control ckeditor" rows="5" required><?=$item->description?></textarea>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-2">
                            <label for="width">Largura:</label>
                            <input type="number" name="width" id="width" class="form-control" value="<?=$item->width?>" required />
                        </div>
                        <div class="col-sm-2">
                            <label for="height">Altura:</label>
                            <input type="number" name="height" id="height" class="form-control" value="<?=$item->height?>" required />
                        </div>
                        <div class="col-sm-2">
                            <label for="length">Comprimento:</label>
                            <input type="number" name="length" id="length" class="form-control" value="<?=$item->length?>" required />
                        </div>
                        <div class="col-sm-2">
                            <label for="weight">Peso:</label>
                            <input type="number" name="weight" id="weight" class="form-control" value="<?=$item->weight?>" required />
                        </div>
                        <div class="col-sm-2">
                            <label for="qty">Quantidade:</label>
                            <input type="number" name="qty" id="qty" class="form-control" value="<?=$item->qty?>" required />
                        </div>
                        <div class="col-sm-2">
                            <label for="value">Valor:</label>
                            <input type="text" name="value" id="value" class="form-control" value="<?=str_price($item->value)?>" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="images">Imagems:</label>
                        <input type="file" name="images[]" multiple id="images"
                               accept="image/png, image/jpg, image/jpeg"
                               class="form-control" />
                    </div>
                </div>
                <input type="submit" value="Editar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>