<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <h1>Editar Configurações do Site</h1>
            <form method="POST" action="<?= route("admin.config.update", ["id" => $item->id]); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="title">Título:</label>
                    <input type="text" name="title" id="title" value="<?= $item->title?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="author">Autor:</label>
                    <input type="text" name="author" id="author" value="<?= $item->author?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="description">Descrição:</label>
                    <textarea name="description" id="description" class="form-control tinymce" rows="5" required><?= $item->description?></textarea>
                </div>
                <hr>
                <div class="form-group">
                    <label for="icon1">Ícone fontawesome 1:</label>
                    <input type="text" name="icon1" id="icon1" value="<?= $item->icon1?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="title1">Título 1:</label>
                    <input type="text" name="title1" id="title1" value="<?= $item->title1?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="description1">Descrição 1:</label>
                    <textarea name="description1" id="description1" class="form-control tinymce" rows="5" required><?= $item->description1?></textarea>
                </div>
                <hr>
                <div class="form-group">
                    <label for="icon2">Ícone fontawesome 2:</label>
                    <input type="text" name="icon2" id="icon2" value="<?= $item->icon2?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="title2">Título 2:</label>
                    <input type="text" name="title2" id="title2" value="<?= $item->title2?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="description2">Descrição 2:</label>
                    <textarea name="description2" id="description2" class="form-control tinymce" rows="5" required><?= $item->description2?></textarea>
                </div>
                <hr>
                <div class="form-group">
                    <label for="icon3">Ícone fontawesome 3:</label>
                    <input type="text" name="icon3" id="icon3" value="<?= $item->icon3?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="title3">Título 3:</label>
                    <input type="text" name="title3" id="title3" value="<?= $item->title3?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="description3">Descrição 3:</label>
                    <textarea name="description3" id="description3" class="form-control tinymce" rows="5" required><?= $item->description3?></textarea>
                </div>
                <input type="submit" value="Editar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>