<style type="text/css">
	*{
		margin: 0;
		padding: 0;
		box-sizing: border-box;
	}
	h2 {
		background: #333;
		color: white;
		padding: 8px;
	}
	.box {
		width: 900px;
		margin: 0 auto;
	}
	table {
		width: 900px;
		margin-top: 15px;
		border-collapse: collapse;
	}
	table td {
		font-size: 14px;
		padding: 8px;
		border: 1px solid #ccc;
	}
</style>
<div class="box">
	<?php $nome = (isset($data['status']) && $data['status'] === 'concluido') ? 'Concluídos' : 'Pendentes'; ?>
	<h2>
		<i class="fa fa-check"></i> Pagamentos <?=$nome?>
	</h2>
	<?php
	$nome = ($nome == 'Pendentes') ? '0' : '1';
	?>
	<div class="wrapper-table">
		<table>
			<tr>
				<td style="font-weight: bold;">Id</td>
				<td style="font-weight: bold;">Nome do Pagamento</td>
				<td style="font-weight: bold;">Parcela</td>
				<td style="font-weight: bold;">Valor</td>
				<td style="font-weight: bold;">Vencimento</td>
				<?php if ($nome == '1'): ?>
					<td style="font-weight: bold;">Pago Em</td>
				<?php endif; ?>
			</tr>
			<?php foreach ($data['list'] as $key => $value): ?>
				<tr>
					<td><?=$value->id?></td>
					<td><?=$value->name?></td>
					<td><?=$value->portion?></td>
					<td><?=$value->value?></td>
					<td><?=date('d/m/Y', strtotime($value->expiration))?></td>
					<?php if ($nome == '1'): ?>
						<td><?=date('d/m/Y', strtotime($value->paid))?></td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</table>
	</div>
</div>