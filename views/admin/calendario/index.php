<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <div class="box-content">
                <h2>
                    <i class="fa fa-comments"></i>
                    Calendário e Agenda | <?=getMonthString((int)$mes-1)?>/<?=$ano?>
                </h2>
                <table class="table">
                    <tr>
                        <td>Domingo</td>
                        <td>Segunda</td>
                        <td>Terça</td>
                        <td>Quarta</td>
                        <td>Quinta</td>
                        <td>Sexta</td>
                        <td>Sábado</td>
                    </tr>
                    <?php

                    $n = 1;
                    $z = 0;
                    $numeroDias+=$diaInicialdoMes;

                    while($n <= $numeroDias){
                        if (($diaInicialdoMes == 7) && ($z != $diaInicialdoMes)) {
                            $z = 7;
                            $n = 8;
                        }
                        if ($n % 7 == 1) {
                            echo '<tr>';
                        }
                        if ($z >= $diaInicialdoMes) {
                            $dia = $n - $diaInicialdoMes;
                            if ($dia < 10) {
                                $dia = str_pad($dia, strlen($dia)+1, "0", STR_PAD_LEFT);
                            }
                            $atual = "$ano-$mes-$dia";
                            if ($atual != $dataAtual) {
                                echo '<td style="cursor: pointer;" dia="'.$atual.'">'.$dia.'</td>';
                            } else {
                                echo '<td style="cursor: pointer;" bgcolor="#CCC" dia="'.$atual.'">'.$dia.'</td>';
                            }
                        } else {
                            echo "<td></td>";
                            $z++;
                        }
                        if ($n % 7 == 0) {
                            echo '</tr>';
                        }
                        $n++;
                    }
                    ?>
                </table>
                <form method="post">
                    <h2>Adicionar tarefa para <?=date('d/m/Y', time())?></h2>
                    <input type="text" name="title">
                    <input type="hidden" name="date" value="<?=date('Y-m-d')?>">
                    <input type="submit" name="ac" value="Adicionar">
                </form>
                <div class="box-tarefas">
                    <h2 class="card-title">Tarefas do dia <?=date('d/m/Y', time())?></h2>
                    <div id="content">
                        <?php foreach ($tasks as $key => $value): ?>
                            <div class="box-tarefas-single">
                                <p><?=$value->title?></p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>