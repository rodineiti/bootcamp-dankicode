<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.aulas.index"); ?>" class="btn btn-info mb-2">Voltar</a>
            <h1>Editar aula</h1>
            <form method="POST" action="<?= route("admin.aulas.update", ["id" => $item->id]); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="module_id">Módulo:</label>
                    <select name="module_id" id="module_id" class="form-control" required>
                        <option value="">Selecione</option>
                        <?php foreach ($modules as $module): ?>
                            <option value="<?=$module->id?>" <?php if ($module->id === $item->module_id): ?>selected<?php endif; ?>><?=$module->name?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Título:</label>
                    <input type="text" name="name" id="name" value="<?= $item->name?>" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="link">Link:</label>
                    <input type="text" name="link" id="link" value="<?= $item->link?>" class="form-control" required />
                </div>
                <input type="submit" value="Editar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>