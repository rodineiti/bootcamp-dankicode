<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.aulas.create"); ?>" class="btn btn-primary mb-2">Adicionar</a>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Módulo</th>
                    <th scope="col">Título</th>
                    <th scope="col">Link</th>
                    <th scope="col">Criado em</th>
                    <th scope="col">Opções</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($list as $item): ?>
                    <tr>
                        <th scope="row"><?= $item->id ?></th>
                        <td><?= $item->module()->name ?></td>
                        <td><?= $item->name ?></td>
                        <td><?= $item->link ?></td>
                        <td><?= $item->created_at ?></td>
                        <td>
                            <a href="<?= route("admin.aulas.edit", ["id" => $item->id]); ?>" class="btn btn-info">Editar</a>
                            <a onclick="return confirm('Deseja realmente deletar?');" href="<?= route("admin.aulas.destroy", ["id" => $item->id]); ?>" class="btn btn-danger">
                                Deletar
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>