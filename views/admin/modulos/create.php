<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.modulos.index"); ?>" class="btn btn-info mb-2">Voltar</a>
            <h1>Adicionar módulo</h1>
            <form method="POST" action="<?= route("admin.modulos.store"); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Título:</label>
                    <input type="text" name="name" id="name" class="form-control" required />
                </div>
                <input type="submit" value="Criar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>