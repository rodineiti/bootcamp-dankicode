<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.empreendimentos.create"); ?>" class="btn btn-primary mb-2">Adicionar</a>

            <div class="row boxes">
                <?php foreach ($list as $item): ?>
                    <div class="col-sm-4 mt-3 box-single-wrapper">
                        <div class="card mb-3" style="max-width: 540px;min-height: 400px;">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <?php if ($item->image): ?>
                                    <img src="<?=media('uploads/' . $item->image)?>"
                                         class="card-img" alt="<?=$item->name?>">
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title">Nome: <strong><?=$item->name?></strong></h5>
                                        <p class="card-text">Valor: <strong><?=str_price($item->value)?></strong></p>
                                        <p class="card-text"><small class="text-muted"><?=date_fmt($item->created_at)?></small></p>
                                    </div>
                                    <hr>
                                    <div class="card-body">
                                        <a href="<?= route("admin.empreendimentos.show", ["id" => $item->id]); ?>" class="btn btn-primary">Visualizar</a>
                                        <a href="<?= route("admin.empreendimentos.edit", ["id" => $item->id]); ?>" class="btn btn-info">Editar</a>
                                        <a onclick="return confirm('Deseja realmente deletar?');" href="<?= route("admin.empreendimentos.destroy", ["id" => $item->id]); ?>" class="btn btn-danger">
                                            Deletar
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>