<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.empreendimentos.show", ["id" => $property->id]); ?>" class="btn btn-info mb-2">Voltar</a>
            <h1>Adicionar imóvel</h1>
            <form method="POST" action="<?= route("admin.imoveis.store"); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome do imóvel:</label>
                    <input type="text" name="name" id="name" class="form-control" required />
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="area">Área:</label>
                            <input type="number" name="area" id="area" class="form-control" value="0" required />
                        </div>
                        <div class="col-sm-6">
                            <label for="value">Valor:</label>
                            <input type="text" name="value" id="value" class="form-control" value="0" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="images">Imagems:</label>
                        <input type="file" name="images[]" multiple id="images"
                               accept="image/png, image/jpg, image/jpeg"
                               class="form-control" required />
                    </div>
                </div>
                <input type="hidden" name="property_id" value="<?=$property->id?>" />
                <input type="submit" value="Criar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>