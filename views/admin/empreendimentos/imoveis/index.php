<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.empreendimentos.index"); ?>" class="btn btn-info mb-2">Voltar</a>
            <div class="jumbotron jumbotron-fluid">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="card">
                                <div class="card-header">
                                    Imagem do empreendimento
                                </div>
                                <div class="card-body" style="width: 18rem;">
                                    <img src="<?=media('uploads/'.$item->image)?>" class="card-img-top" alt="<?=$item->image?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="card">
                                <div class="card-header">
                                    Informações do empreendimento
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title">Nome: <strong><?=$item->name?></strong></h5>
                                    <p class="card-text">Valor: <strong><?=str_price($item->value)?></strong></p>
                                    <p class="card-text">Tipo: <strong><?=$item->type === "C" ? "Casa" : "Apartamento" ?></strong></p>
                                    <p class="card-text"><small class="text-muted"><?=date_fmt($item->created_at)?></small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <h1>Imoveis</h1>
            <a href="<?= route("admin.imoveis.create", ["property_id" => $item->id]); ?>" class="btn btn-primary mb-2">Adicionar</a>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Área</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Opções</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($list as $value): ?>
                    <tr>
                        <th scope="row"><?= $value->id ?></th>
                        <td><?= $value->name ?></td>
                        <td><?= $value->area ?></td>
                        <td><?= str_price($value->value) ?></td>
                        <td>
                            <a href="<?= route("admin.imoveis.edit", ["id" => $value->id, "property_id" => $item->id]); ?>"
                               class="btn btn-info">Editar</a>
                            <a onclick="return confirm('Deseja realmente deletar?');"
                               href="<?= route("admin.imoveis.destroy", ["id" => $value->id, "property_id" => $item->id]); ?>"
                               class="btn btn-danger">
                                Deletar
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>