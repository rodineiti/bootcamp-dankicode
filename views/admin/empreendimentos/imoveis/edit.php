<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.empreendimentos.show", ["id" => $property->id]); ?>" class="btn btn-info mb-2">Voltar</a>
            <?php $images = $item->images()->all() ?? []; ?>
            <?php if (count($images)): ?>
            <div class="jumbotron jumbotron-fluid">
                <div class="container-fluid">
                    <div class="row">
                        <?php foreach ($images as $image): ?>
                            <div class="col-sm-3">
                                <div class="card" style="width: 18rem;">
                                    <img src="<?=media('uploads/'.$image->image)?>" class="card-img-top" alt="<?=$image->image?>">
                                    <div class="card-body">
                                        <a onclick="return confirm('Deseja realmente deletar?');"
                                           href="<?= route("admin.imoveis.destroyImage", ["home_id" => $item->id, "id" => $image->id, "property_id" => $property->id]); ?>"
                                           class="btn btn-danger">
                                            Deletar
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <h1>Editar imóvel: <?=$item->name?></h1>
            <form method="POST" action="<?= route("admin.imoveis.update", ["id" => $item->id]); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome do imóvel:</label>
                    <input type="text" name="name" id="name" class="form-control" value="<?=$item->name?>" required />
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="area">Área:</label>
                            <input type="number" name="area" id="area" class="form-control" value="<?=$item->area?>" required />
                        </div>
                        <div class="col-sm-6">
                            <label for="value">Valor:</label>
                            <input type="text" name="value" id="value" class="form-control" value="<?=str_price($item->value)?>" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="images">Imagems:</label>
                        <input type="file" name="images[]" multiple id="images"
                               accept="image/png, image/jpg, image/jpeg"
                               class="form-control" />
                    </div>
                </div>
                <input type="hidden" name="property_id" value="<?=$property->id?>" />
                <input type="submit" value="Editar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>