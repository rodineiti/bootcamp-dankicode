<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.empreendimentos.index"); ?>" class="btn btn-info mb-2">Voltar</a>
            <h1>Adicionar empreendimento</h1>
            <form method="POST" action="<?= route("admin.empreendimentos.store"); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome do empreendimento:</label>
                    <input type="text" name="name" id="name" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="description">Descrição:</label>
                    <textarea name="description" id="description" class="form-control ckeditor" rows="5" required></textarea>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="type">Tipo:</label>
                            <select name="type" id="type" class="form-control" required>
                                <option value="">Selecione</option>
                                <option value="A">Apartamento</option>
                                <option value="C">Casa</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label for="value">Valor:</label>
                            <input type="text" name="value" id="value" class="form-control" value="0" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image">Imagem:</label>
                        <input type="file" name="image" id="image"
                               accept="image/png, image/jpg, image/jpeg"
                               class="form-control" required />
                    </div>
                </div>
                <input type="submit" value="Criar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>