<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.empreendimentos.index"); ?>" class="btn btn-info mb-2">Voltar</a>
            <h1>Editar empreendimento: <?=$item->name?></h1>
            <form method="POST" action="<?= route("admin.empreendimentos.update", ["id" => $item->id]); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome do empreendimento:</label>
                    <input type="text" name="name" id="name" class="form-control" value="<?=$item->name?>" required />
                </div>
                <div class="form-group">
                    <label for="description">Descrição:</label>
                    <textarea name="description" id="description" class="form-control ckeditor" rows="5" required><?=$item->description?></textarea>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="type">Tipo:</label>
                            <select name="type" id="type" class="form-control" required>
                                <option value="" <?php if ($item->type === ""): ?>selected<?php endif; ?>>Selecione</option>
                                <option value="A" <?php if ($item->type === "A"): ?>selected<?php endif; ?>>Apartamento</option>
                                <option value="C" <?php if ($item->type === "C"): ?>selected<?php endif; ?>>Casa</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label for="value">Valor:</label>
                            <input type="text" name="value" id="value" class="form-control" value="<?=str_price($item->value)?>" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image">Imagem:</label>
                        <input type="file" name="image" id="image"
                               accept="image/png, image/jpg, image/jpeg"
                               class="form-control" />
                    </div>
                </div>
                <input type="submit" value="Editar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>