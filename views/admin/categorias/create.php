<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.categorias.index"); ?>" class="btn btn-info mb-2">Voltar</a>
            <h1>Adicionar categoria</h1>
            <form method="POST" action="<?= route("admin.categorias.store"); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="title">Título:</label>
                    <input type="text" name="title" id="title" class="form-control" required />
                </div>
                <input type="submit" value="Criar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>