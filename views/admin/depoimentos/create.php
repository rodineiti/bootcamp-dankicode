<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?=$this->view("admin/menu");?>
        </div>
        <div class="col-md-9">
            <a href="<?= route("admin.depoimentos.index"); ?>" class="btn btn-info mb-2">Voltar</a>
            <h1>Adicionar depoimento</h1>
            <form method="POST" action="<?= route("admin.depoimentos.store"); ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Nome do Usuário:</label>
                    <input type="text" name="name" id="name" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="description">Descrição:</label>
                    <textarea name="description" id="description" class="form-control tinymce" rows="5" required></textarea>
                </div>
                <input type="submit" value="Criar" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>