<div class="jumbotron">
    <h1 class="display-4">BEM VINDO AO <?=SITE_NAME?></h1>
    <p class="lead">Este é um template base para suas aplicações frontend e backend utilizando PHP, MYSQL e Bootstrap no padrão MVC,
        utilizando as boas práticas para criar suas aplicações de maneira organizada.</p>
    <hr class="my-4">
    <p>Está é a home do template, onde poderá utilizar para ser a página principal do seu site.</p>
    <?php if (!auth()): ?>
        <p>Logo abaixo você poderá acessar a parte Administrativa do template.</p>
        <div class="row">
            <div class="col-sm-4 mb-2">
                <div class="card">
                    <div class="card-header text-center">
                        <a class="btn btn-primary btn-lg" href="<?= route("admin.login"); ?>" role="button">Acessar Admin</a>
                    </div>
                    <div class="card-body">
                        <img
                            style="width: 100%;"
                            src="https://user-images.githubusercontent.com/25492122/102229319-685a5b00-3eca-11eb-86b1-423cf35e56e5.png"
                            alt="admin" />
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mb-2">
                <div class="card">
                    <div class="card-header text-center">
                        <a class="btn btn-primary btn-lg" href="<?= route("projeto01.home"); ?>" role="button">Projeto 01</a>
                    </div>
                    <div class="card-body">
                        <img
                                style="width: 100%;"
                                src="https://user-images.githubusercontent.com/25492122/102229091-24ffec80-3eca-11eb-95ce-38a28e8b09ae.png"
                                alt="admin" />
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mb-2">
                <div class="card">
                    <div class="card-header text-center">
                        <a class="btn btn-primary btn-lg" href="<?= route("projeto02.home"); ?>" role="button">Projeto 02</a>
                    </div>
                    <div class="card-body">
                        <img
                                style="width: 100%;"
                                src="https://user-images.githubusercontent.com/25492122/102229120-30531800-3eca-11eb-8e61-6080c0322945.png"
                                alt="admin" />
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mb-2">
                <div class="card">
                    <div class="card-header text-center">
                        <a class="btn btn-primary btn-lg" href="<?= route("projeto05.forum", ["slug" => "all"]); ?>" role="button">Projeto 05</a>
                    </div>
                    <div class="card-body">
                        <img
                                style="width: 100%;"
                                src="https://user-images.githubusercontent.com/25492122/102229176-3d700700-3eca-11eb-8adc-0d39869f777b.png"
                                alt="admin" />
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mb-2">
                <div class="card">
                    <div class="card-header text-center">
                        <a class="btn btn-primary btn-lg" href="<?= route("projeto06.shop"); ?>" role="button">Projeto 06</a>
                    </div>
                    <div class="card-body">
                        <img
                                style="width: 100%;"
                                src="https://user-images.githubusercontent.com/25492122/102229209-452fab80-3eca-11eb-989e-4c71ee02358f.png"
                                alt="admin" />
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mb-2">
                <div class="card">
                    <div class="card-header text-center">
                        <a class="btn btn-primary btn-lg" href="<?= route("projeto07.home"); ?>" role="button">Projeto 07</a>
                    </div>
                    <div class="card-body">
                        <img
                                style="width: 100%;"
                                src="https://user-images.githubusercontent.com/25492122/102229233-4d87e680-3eca-11eb-8ded-694eef4a5080.png"
                                alt="admin" />
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mb-2">
                <div class="card">
                    <div class="card-header text-center">
                        <a class="btn btn-primary btn-lg" href="<?= route("projeto08.home"); ?>" role="button">Projeto 08</a>
                    </div>
                    <div class="card-body">
                        <img
                                style="width: 100%;"
                                src="https://user-images.githubusercontent.com/25492122/102229268-58427b80-3eca-11eb-8196-ffb533db1bf9.png"
                                alt="admin" />
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>