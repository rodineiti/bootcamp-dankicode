<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=SITE_NAME?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?=asset("projeto01/css/fontawesome/all.css")?>">
    <link rel="stylesheet" href="<?=asset("projeto01/css/styles.css")?>">
</head>
<body>

<div class="overlay-loading">
    <img src="<?=asset('projeto01/images/loading.gif')?>" alt="loading">
</div><!-- overlay-loading -->
<div class="msg-success">Formulário enviado com sucesso!</div>
<div class="msg-error">Ocorreu um erro tentar enviar a mensagem, tente mais tarde!</div>

<header>
    <div class="container">
        <div class="logo left"><a href="/bootcamp-dankicode/projeto01">Logomarca</a></div><!-- logo -->
        <nav class="desktop right">
            <ul>
                <li><a href="<?= route("projeto01.home");?>">Home</a></li>
                <li><a href="<?= route("projeto01.sobre");?>">Sobre</a></li>
                <li><a href="<?= route("projeto01.especialidades");?>">Especialidades</a></li>
                <li><a href="<?= route("projeto01.servicos");?>">Serviços</a></li>
                <li><a href="<?= route("projeto01.noticias", ["slug" => "all"]);?>">Notícias</a></li>
                <li><a href="<?= route("projeto05.forum", ["slug" => "all"]);?>">Fórum</a></li>
                <li><a realtime="contato" href="<?= route("projeto01.contato");?>">Contato</a></li>
            </ul>
        </nav>
        <nav class="mobile right">
            <i class="fa fa-bars"></i>
            <ul>
                <li><a href="<?= route("projeto01.home");?>">Home</a></li>
                <li><a href="<?= route("projeto01.sobre");?>">Sobre</a></li>
                <li><a href="<?= route("projeto01.especialidades");?>">Especialidades</a></li>
                <li><a href="<?= route("projeto01.servicos");?>">Serviços</a></li>
                <li><a href="<?= route("projeto01.noticias", ["slug" => "all"]);?>">Notícias</a></li>
                <li><a href="<?= route("projeto01.contato");?>">Contato</a></li>
            </ul>
        </nav>
        <div class="clear"></div>
    </div><!-- container -->
</header><!-- /header -->

<?php if ($errors = flashMessage("errors")): ?>
    <div class="alert alert-<?=$errors["status"]?>">
        <?php if (is_array($errors["messages"])): ?>
            <ul>
                <?php foreach ($errors["messages"] as $error): ?>
                    <li><?=$error?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
        <?php if (is_string($errors["messages"])): ?>
            <?=$errors["messages"]?>
        <?php endif; ?>
    </div>
<?php endif; ?>

<?php $this->viewTemplate($view, $data); ?>

<footer>
    <div class="container">
        <p>Todos os direitos reservados</p>
    </div><!-- container -->
</footer>

<script>var baseUrl = '<?=BASE_URL?>projeto05/';</script>
<script src="<?=asset("js/jquery.min.js")?>"></script>
<script src="<?=asset("projeto01/js/constants.js")?>"></script>

<?php if (check_url_template(["projeto01/contato"])): ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnv7y3NuVB3H7-6XXXbtFUYOsvWql_d-8"></script>
    <script src="<?=asset("projeto01/js/map.js")?>"></script>
<?php endif; ?>

<?php if (check_url_template(["projeto01"])): ?>
    <script src="<?=asset("projeto01/js/slider.js")?>"></script>
<?php endif; ?>

<script src="<?=asset("projeto01/js/animacao.js")?>"></script>
<script src="<?=asset("projeto01/js/scripts.js")?>"></script>
<script src="<?=asset("projeto01/js/form.js")?>"></script>
<script>
    $(function() {
        $('select[name=slug]').change(function() {
            window.location.href=`${baseUrl}/${$(this).val()}`;
        });
    });
</script>
</body>
</html>