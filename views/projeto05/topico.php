<section class="noticia-single">
    <div class="container">
        <header>
            <h1><i class="fa fa-calendar"></i> <?=date_fmt($topic->created_at)?> - <?=$topic->title?></h1>
        </header><!-- /header -->
        <div class="container-error-login">
            <?php if(check("admins")): ?>
                <?php $posts = $topic->posts()->all() ?? []; ?>
                <?php foreach($posts as $value): ?>
                    <div class="box-comment-noticia">
                        <h3><?=$value->user()->name?>  - <i><?=date_fmt($value->created_at)?></i></h3>
                        <p><?=$value->message?></p>
                    </div>
                <?php endforeach; ?>
                <h2 class="postar-comentario">
                    Faça um postagem neste tópico <i class="fa fa-comments"></i>
                </h2>
                <form method="post" action="<?=route('projeto05.post.store')?>">
                    <textarea placeholder="Seu comentário" name="message"></textarea>
                    <input type="hidden" name="topic_id" value="<?=$topic->id?>">
                    <input type="submit" name="ac" value="Postar">
                </form>
            <?php else: ?>
                <p>Para comentar você precisa se logar. Para se logar <a href="<?=route('admin.login')?>">Clique aqui</a></p>
            <?php endif; ?>
        </div>
    </div>
</section>