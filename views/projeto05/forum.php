<section class="header-noticias">
    <div class="container">
        <h2><i class="fa fa-home"></i></h2>
        <h2>Acompanhe as últimas <b>postagens do Forum</b></h2>
    </div>
</section><!-- header-noticias -->

<section class="container-portal">
    <div class="container">
        <div class="sidebar">
            <div class="box-content-sidebar">
                <h3><i class="fa fa-search"></i> Realizar uma busca</h3>
                <form method="get">
                    <input type="text" name="search" placeholder="O que procura?" required />
                    <input type="submit" name="ac" value="Pesquisar">
                </form>
            </div>
            <div class="box-content-sidebar">
                <h3><i class="fa fa-search"></i> Por Forum</h3>
                <form>
                    <select name="slug" id="slug">
                        <option value="all">Todas os forums</option>
                        <?php foreach ($forums as $value): ?>
                            <option
                                    value="<?=$value->id?>"
                                    <?php if ($slug === $value->id): ?>selected<?php endif; ?>><?=$value->title?></option>
                        <?php endforeach; ?>
                    </select>
                </form>
            </div>
            <div class="box-content-sidebar">
                <h3><i class="fa fa-search"></i> Adicionar Forum</h3>
                <form method="post" action="<?=route('projeto05.forum.store')?>">
                    <input type="text" placeholder="Informe o título do fórum" name="title" value="" required />
                    <input type="submit" value="Adicionar" />
                </form>
            </div>
            <div class="box-content-sidebar">
                <h3><i class="fa fa-search"></i> Adicionar Tópico</h3>
                <form method="post" action="<?=route('projeto05.topic.store')?>">
                    <select name="forum_id" id="forum_id" required>
                        <option value="">Selecione</option>
                        <?php foreach ($forums as $value): ?>
                            <option value="<?=$value->id?>"><?=$value->title?></option>
                        <?php endforeach; ?>
                    </select>
                    <input type="text" placeholder="Informe o título do tópico" name="title" value="" required />
                    <input type="submit" value="Adicionar" />
                </form>
            </div>
        </div>

        <div class="conteudo-portal">
            <div class="header-conteudo-portal">
                <?php if ($slug !== 'all'): ?>
                    <h2>Visualizando Tópicos do Forum <span><?=$forum->title?></span></h2>
                <?php else: ?>
                    <h2>Visualizando todos os Tópicos</h2>
                <?php endif; ?>
            </div>
            <?php foreach ($topics as $value): ?>
                <div class="box-single-conteudo-portal">
                    <h2><?=date('d/m/Y', strtotime($value->created_at))?>- <?=$value->title?></h2>
                    <a href="<?=route('projeto05.single', ['id' => $value->id])?>">Ver tópico</a>
                </div>
            <?php endforeach; ?>

            <div class="paginator">
                <?php for($i = 1; $i <= $pages; $i++): ?>
                    <?php if ($i === $page): ?>
                        <a class="active-page"><?=$i?></a>
                    <?php else: ?>
                        <a href="<?=route('projeto05.forum', ['slug' => $slug])?>?<?php
                        $pageArray = $_GET;
                        unset($pageArray["uri"]);
                        unset($pageArray["ac"]);
                        $pageArray["page"] = $i;
                        echo http_build_query($pageArray);
                        ?>"><?=$i?></a>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>

        </div>

        <div class="clear"></div>
        <div class="container">
</section>