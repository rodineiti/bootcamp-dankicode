<div class="box-cadastro">
    <div class="container">
        <div class="box1">
            <img src="<?=asset('images/rede-social.png')?>" alt="img">
        </div>
        <div class="box2">
            <h2>Efetuar cadastro</h2>
            <form method="post" enctype="multipart/form-data">
                <input type="text" name="name" placeholder="Seu nome..." required />
                <input type="email" name="email" placeholder="Seu e-mail..." required />
                <input type="password" name="password" placeholder="Sua senha..." required />
                <p>Envie sua foto para perfil</p>
                <input type="file" name="image" required />
                <input type="submit" name="ac" value="Inscreva-se" />
            </form>
        </div>
        <div class="clear"></div>
    </div>
</div>