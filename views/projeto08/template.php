<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=SITE_NAME?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?=asset("projeto01/css/fontawesome/all.css")?>">
    <link rel="stylesheet" href="<?=asset("projeto08/css/style.css")?>">
</head>
<body>

<header>
    <div class="container">
        <div class="logo"><a href="<?=route('projeto08.home')?>">Rede Social</a></div>
        <div class="form-login">
            <?php if(!check()): ?>
                <form method="post" action="<?=route('projeto08.login')?>">
                    <input type="email" name="email" placeholder="Seu e-mail..." required />
                    <input type="password" name="password" placeholder="Sua senha..." required />
                    <input type="submit" name="acl" value="Entrar" />
                </form>
            <?php else: ?>
                <a class="btn-logout" href="<?=route('projeto08.solicitacoes')?>">Solicitações(<?=getSolicitacoes()->count()?>)</a>
                <a class="btn-logout" href="<?=route('projeto08.comunidade')?>">Comunidade</a>
                <a class="btn-logout" href="<?=route('projeto08.logout')?>">Sair</a>
            <?php endif; ?>
        </div>
        <div class="clear"></div>
    </div>
</header><!-- /header -->

<?php if ($errors = flashMessage("errors")): ?>
    <div class="alert alert-<?=$errors["status"]?>">
        <?php if (is_array($errors["messages"])): ?>
            <ul>
                <?php foreach ($errors["messages"] as $error): ?>
                    <li><?=$error?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
        <?php if (is_string($errors["messages"])): ?>
            <?=$errors["messages"]?>
        <?php endif; ?>
    </div>
<?php endif; ?>

<?php $this->viewTemplate($view, $data); ?>

<script>var baseUrl = '<?=BASE_URL?>projeto08/';</script>
<script src="<?=asset("js/jquery.min.js")?>"></script>
</body>
</html>