
<section class="comunidade">
    <div class="container">
        <div class="w100">
            <h2 class="title">Comunidade</h2>
            <?php foreach($members as $value): ?>
                <div class="membro-listagem">
                    <div class="box-imagem">
                        <div style="background-image: url('<?=media('avatars/'.$value->avatar)?>')" class="box-imagem-wrapper"></div>
                    </div>
                    <p><?=$value->name?></p>
                    <?php if(!$value->isAmigoPendente($value->id) && !$value->isAmigo($value->id)): ?>
                        <a href="<?=route('projeto08.adicionar', ['id' => $value->id])?>">Adicionar como amigo</a>
                    <?php else: ?>
                        <a style="background: red !important;"
                           href="<?=route('projeto08.desfazer', ['id' => $value->id])?>">
                            Desfazer <?=($value->isAmigoPendente($value->id)) ? 'Solicitação' : 'Amizade'?>
                        </a>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
            <div class="clear"></div>
        </div>
    </div>
</section>