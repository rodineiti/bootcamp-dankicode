<section class="comunidade">
    <div class="container">
        <div class="w100">
            <h2 class="title">Solicitações pendentes</h2>
            <?php foreach($solicitacoes as $value): ?>
                <div class="membro-listagem">
                    <div class="box-imagem">
                        <div style="background-image: url('<?=media('avatars/'.$value->userFrom()->avatar)?>')"
                             class="box-imagem-wrapper"></div>
                    </div>
                    <p><?=$value->name?></p>
                    <a href="<?=route('projeto08.aceitar', ['id_from' => $value->IDREQUEST])?>">Aceitar</a>
                    <a style="background: red !important;" href="<?=route('projeto08.recusar', ['id_from' => $value->IDREQUEST])?>">Recusar</a>
                </div>
            <?php endforeach; ?>
            <div class="clear"></div>
        </div>
    </div>
</section>