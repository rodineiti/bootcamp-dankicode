<section class="perfil-info">
    <div class="container">
        <div class="w30">
            <h2 class="title"><?=auth()->name?></h2>
            <div class="container-img">
                <img src="<?=asset('images/rede-social.png')?>" alt="">
            </div>
            <div class="lista-amigos">
                <h3>Minhas Amizades (<?=count($amigos)?>)</h3>
                <?php foreach($amigos as $value): ?>
                    <div class="img-single-amigo">
                        <div class="img-single-amigo-wrapper" style="background-image: url('<?=media('avatars/'.$value->avatar)?>')"></div>
                    </div>
                <?php endforeach; ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>
<section class="feed">
    <div class="container">
        <div class="w70">
            <h2 class="title">O que você está pensando...</h2>
            <form method="post" action="<?=route('projeto08.post')?>">
                <textarea name="post" placeholder="Digite aqui..."></textarea>
                <input type="submit" name="feed" value="Enviar" />
            </form>

            <?php foreach($posts as $value): ?>
                <div class="post-single">
                    <div class="img-user">
                        <img src="<?=media('avatars/'.$value->user()->avatar)?>" alt="img" />
                    </div>
                    <div class="post-user">
                        <p><?=$value->user()->name?></p>
                        <p><?=$value->post?></p>
                    </div>
                    <div class="clear"></div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>