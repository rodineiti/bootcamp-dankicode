<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=SITE_NAME?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?=asset("projeto01/css/fontawesome/all.css")?>">
    <link rel="stylesheet" href="<?=asset("projeto07/css/style.css")?>">
</head>
<body>

<header>
    <div class="container">
        <div class="logo"><a href="<?=route('projeto07.home')?>">Plataforma EAD</a></div>
        <nav class="desktop">
            <ul>
                <li><a href="">Conheça o curso</a></li>
                <li><a href="">Sobre</a></li>
                <li><a href="">Contato</a></li>
                <?php if (check("students")): ?>
                    <li><a href="#">Deslogar</a></li>
                <?php endif; ?>
            </ul>
        </nav>
        <div class="clear"></div>
    </div>
</header>

<?php if ($errors = flashMessage("errors")): ?>
    <div class="alert alert-<?=$errors["status"]?>">
        <?php if (is_array($errors["messages"])): ?>
            <ul>
                <?php foreach ($errors["messages"] as $error): ?>
                    <li><?=$error?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
        <?php if (is_string($errors["messages"])): ?>
            <?=$errors["messages"]?>
        <?php endif; ?>
    </div>
<?php endif; ?>

<?php $this->viewTemplate($view, $data); ?>

<script>var baseUrl = '<?=BASE_URL?>projeto07/';</script>
<script src="<?=asset("js/jquery.min.js")?>"></script>
<script src="<?=asset("projeto07/js/script.js")?>"></script>
</body>
</html>