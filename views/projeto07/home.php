
<div class="box container">
    <h3>Seja bem vindo <?=auth("students")->name?></h3>
    <br>
    <?php if ($access > 0): ?>
        <?php foreach($modules as $modulo): ?>
            <p class="nome-module"><?=$modulo->name?></p>
            <?php foreach($modulo->lessons()->all() ?? [] as $aula): ?>
                <p class="nome-aula">
                    <a href="<?=route('projeto07.aula', ['id' => $aula->id])?>"><?=$aula->name?></a>
                </p>
            <?php endforeach; ?>
        <?php endforeach; ?>
    <?php else: ?>
        <h3 class="deseja-estudar">Você ainda não é aluno, conheça o curso no video abaixo.</h3>
        <div class="apresentacao">
            <iframe src="https://www.youtube.com/embed/Gz3Q_c67ZH0"></iframe>
        </div>
        <a class="btn-chamada" href="<?=route('projeto07.comprar')?>">Comprar o Curso!</a>
    <?php endif; ?>
</div>