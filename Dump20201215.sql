-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: bootcamp_dankicode
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_forum_posts`
--

DROP TABLE IF EXISTS `admin_forum_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_forum_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_forum_posts`
--

LOCK TABLES `admin_forum_posts` WRITE;
/*!40000 ALTER TABLE `admin_forum_posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_forum_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  FULLTEXT KEY `name_email_FULLTEXT` (`name`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'admin','admin@admin.com','$2y$10$JxsGcZCAPf6pl8BvnppKtONxdVxkJW1pKjWzlr67oSBFS5/nD/oVm','2020-08-14 15:15:47','2020-08-14 15:15:47'),(2,'outro user','user@user.com','$2y$10$JxsGcZCAPf6pl8BvnppKtONxdVxkJW1pKjWzlr67oSBFS5/nD/oVm','2020-12-11 17:10:00','2020-12-11 17:10:00');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (3,'HTML','html',3,'2020-11-30 19:55:14','2020-11-30 19:55:14'),(4,'CSS','css',4,'2020-11-30 19:55:14','2020-11-30 19:55:14'),(6,'JAVASCRIPT','javascript',6,'2020-11-30 19:55:14','2020-12-01 20:45:00');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat`
--

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
INSERT INTO `chat` VALUES (1,1,'olá mundo','2020-12-11 20:00:37','2020-12-11 20:00:37'),(2,2,'blz','2020-12-11 20:00:37','2020-12-11 20:00:37'),(3,1,'sim','2020-12-11 20:00:37','2020-12-11 20:00:37'),(4,1,'olá','2020-12-11 20:00:37','2020-12-11 20:00:37'),(5,2,'diga','2020-12-11 20:00:37','2020-12-11 20:00:37'),(6,1,'teste api','2020-12-11 20:28:35',NULL),(7,1,'teste api 2','2020-12-11 20:29:55',NULL),(8,1,'teste api 3','2020-12-11 20:32:16',NULL),(9,2,'legal','2020-12-11 20:32:40',NULL),(10,2,'bem','2020-12-11 20:36:59',NULL);
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `cpf_cnpj` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (15,'Rodinei Teixeira','teste@teste.com.rod','PF','123.456.789-09','5d1bca94d4ec9.png','15','2020-12-01 20:12:42','2020-12-01 20:12:42'),(16,'Fulano de tal','rodineiguitar@gmail.com','PJ','28.726.608/0001-17','5d1bced7a9c9a.png','16','2020-12-01 20:12:42','2020-12-01 20:12:42');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,1,10,'comentário postando','2020-12-14 15:48:21','2020-12-14 15:48:21'),(2,1,10,'adicionando novo comentário','2020-12-14 15:48:21','2020-12-14 15:48:21'),(3,1,9,'novo comentário','2020-12-14 16:29:53',NULL);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments_answers`
--

DROP TABLE IF EXISTS `comments_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comments_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answer` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments_answers`
--

LOCK TABLES `comments_answers` WRITE;
/*!40000 ALTER TABLE `comments_answers` DISABLE KEYS */;
INSERT INTO `comments_answers` VALUES (1,1,1,'Resposta comentário postando','2020-12-14 15:48:21','2020-12-14 15:48:21'),(2,1,1,'outra resposta ao comentário postando','2020-12-14 15:48:21','2020-12-14 15:48:21'),(3,3,1,'resposta','2020-12-14 16:32:42',NULL);
/*!40000 ALTER TABLE `comments_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `icon1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description1` text COLLATE utf8_unicode_ci NOT NULL,
  `icon2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description2` text COLLATE utf8_unicode_ci NOT NULL,
  `icon3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description3` text COLLATE utf8_unicode_ci NOT NULL,
  `title1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,'Projeto 01 - Backend','Rodinei de Jesus Santos','<p>Short ribs sh<strong>oulder consequat tongue deserunt ribeye minim ut magna fatback dolore anim duis quis. Occaecat in drumstick officia enim. Turducken reprehenderit duis picanha id enim beef voluptate esse in buffalo pariatur fatback jerky. Cupidatat strip steak t-bone pork loin ribeye, bresaola brisket pariatur excepteur prosciutto laboris. Alcatra dolore short loin nisi frankfurter flank strip steak eu salami beef fugiat. Ullamco ball tip eu fi</strong>let mignon. Pork loin cupidatat est, excepteur turducken elit shoulder hamburger ball tip. Tongue elit adipisicing dolor, burgdoggen landjaeger ground round ham hock shank. Leberkas reprehenderit b<em>oudin consectetur ex veniam drumstick corned beef short ribs ipsum. Ullamco turducken pastrami exercitation, shankle officia cupidatat salami id lab</em>orum.</p>','fa fa-home','<p>Venison meatball ut ham swine t-bone. Strip steak filet mignon magna, cow ut tongue doner rump velit chicken burgdoggen. Sausage enim swine b<em>eef ribs eu. Shank jowl do exercitation ullamco burgdoggen ea landjaeger velit consectetur cupidatat meatloaf in. Venison meatball ut ham swine t-bone. Strip steak filet mignon magna, cow ut tongue doner rump velit chicken burgdoggen. Sausage enim swine beef ribs eu. Shank jowl do</em> exercitation ullamco burgdoggen ea landjaeger velit consectetur cupidatat meatloaf in.</p>','fa fa-users','<p>Shoulder turducken tempor porchetta commodo dolore nostrud anim hamburger sausage swine qui incididunt ipsum. Occaecat pork loin fatback shan<strong>kle. Landjaeger reprehenderit hamburger sed flank. Ham voluptate burgdoggen strip steak bacon magna, sirloin porchetta in. Biltong veniam dolore, fla</strong>nk doner bacon quis consectetur sausage.</p>','fa fa-times','<p>Swine <strong>leberkas</strong> venison sed in ut sint in nulla pork chop burgdoggen boudin tempor consectetur non. Cupidatat consectetur eu cillum. Consectetur co<em>mmodo alcatra turkey ham hock ea. Elit fatback frankfurter hamburger bresaola filet mignon. Sausage cupim chicken laborum sint incididunt. Cillum pork shoulder cupidatat pastrami filet mignon velit jowl minim shankle short loin culpa enim. Quis consectetur nulla, fugiat jerky te</em>nderloin doner veniam beef swine labore velit enim.</p>','HTML','CSS','JAVASCRIPT','2020-11-30 17:14:17','2020-12-01 20:05:08');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_student`
--

DROP TABLE IF EXISTS `courses_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `courses_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_student`
--

LOCK TABLES `courses_student` WRITE;
/*!40000 ALTER TABLE `courses_student` DISABLE KEYS */;
INSERT INTO `courses_student` VALUES (2,2,'2020-12-14 21:17:51','2020-12-14 21:17:51');
/*!40000 ALTER TABLE `courses_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `depositions`
--

DROP TABLE IF EXISTS `depositions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `depositions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `order_id` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `depositions`
--

LOCK TABLES `depositions` WRITE;
/*!40000 ALTER TABLE `depositions` DISABLE KEYS */;
INSERT INTO `depositions` VALUES (8,'Rodinei Teixeira','Meatloaf chuck fugiat occaecat frankfurter, voluptate nulla. Quis eu lorem ad filet mignon. Beef ribs nulla swine, short loin labore duis consequat magna. Ball tip bacon corned beef pariatur. Incididunt eu pancetta voluptate duis. update','1','2020-11-30 17:01:19','2020-11-30 17:01:19'),(15,'Fulano de tal','<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1<strong>500s, when an unkno</strong>wn printer took a galley of type and scrambled it to make a type specimen book.</p>',NULL,'2020-12-01 16:50:46','2020-12-01 16:53:37');
/*!40000 ALTER TABLE `depositions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feeds`
--

DROP TABLE IF EXISTS `feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feeds`
--

LOCK TABLES `feeds` WRITE;
/*!40000 ALTER TABLE `feeds` DISABLE KEYS */;
INSERT INTO `feeds` VALUES (1,2,'hello world','2020-12-14 21:47:18','2020-12-14 21:47:18'),(2,2,'asndfjasldf','2020-12-14 21:47:18','2020-12-14 21:47:18'),(3,1,'asdfasçdkf','2020-12-14 21:47:18','2020-12-14 21:47:18'),(4,1,'teste','2020-12-15 13:10:51',NULL),(5,1,'outro teste','2020-12-15 13:10:58',NULL);
/*!40000 ALTER TABLE `feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finances`
--

DROP TABLE IF EXISTS `finances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `finances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` decimal(16,2) NOT NULL,
  `expiration` date NOT NULL,
  `paid` date DEFAULT NULL,
  `portion` char(2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `order_id` char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finances`
--

LOCK TABLES `finances` WRITE;
/*!40000 ALTER TABLE `finances` DISABLE KEYS */;
INSERT INTO `finances` VALUES (1,15,'teste',150.00,'2021-01-21','2020-12-09','1',1,'99','2020-12-09 21:53:35','2020-12-09 22:19:06'),(2,15,'teste',150.00,'2021-01-24','2020-12-09','2',1,'99','2020-12-09 21:53:35','2020-12-09 22:19:06'),(3,16,'mercado',250.00,'2020-12-09','2020-12-09','1',1,'99','2020-12-09 21:55:45','2020-12-09 22:19:06');
/*!40000 ALTER TABLE `finances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_posts`
--

DROP TABLE IF EXISTS `forum_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_posts`
--

LOCK TABLES `forum_posts` WRITE;
/*!40000 ALTER TABLE `forum_posts` DISABLE KEYS */;
INSERT INTO `forum_posts` VALUES (1,2,1,'hello','2020-12-14 17:26:52','2020-12-14 17:26:52'),(2,2,2,'outro teste','2020-12-14 17:26:52','2020-12-14 17:26:52'),(3,2,1,'teste','2020-12-14 17:44:16',NULL),(4,2,1,'ajshdfahdslf','2020-12-14 17:44:20',NULL),(5,4,1,'asdfasdf','2020-12-14 18:08:32',NULL);
/*!40000 ALTER TABLE `forum_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_topics`
--

DROP TABLE IF EXISTS `forum_topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_topics`
--

LOCK TABLES `forum_topics` WRITE;
/*!40000 ALTER TABLE `forum_topics` DISABLE KEYS */;
INSERT INTO `forum_topics` VALUES (1,3,'topico mobile 1','2020-12-14 17:26:52','2020-12-14 17:26:52'),(2,4,'topico 1 marketing','2020-12-14 17:26:52','2020-12-14 17:26:52'),(3,4,'topico 2 marketing','2020-12-14 17:26:52','2020-12-14 17:26:52'),(4,5,'tópico 1 do fullstack','2020-12-14 18:08:17',NULL);
/*!40000 ALTER TABLE `forum_topics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forums`
--

DROP TABLE IF EXISTS `forums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forums`
--

LOCK TABLES `forums` WRITE;
/*!40000 ALTER TABLE `forums` DISABLE KEYS */;
INSERT INTO `forums` VALUES (1,'Games','2020-12-14 17:26:52','2020-12-14 17:26:52'),(2,'Web','2020-12-14 17:26:52','2020-12-14 17:26:52'),(3,'Mobile','2020-12-14 17:26:52','2020-12-14 17:26:52'),(4,'Marketing','2020-12-14 17:26:52','2020-12-14 17:26:52'),(5,'fullstack','2020-12-14 18:00:47',NULL);
/*!40000 ALTER TABLE `forums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lessons`
--

DROP TABLE IF EXISTS `lessons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lessons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module_id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lessons`
--

LOCK TABLES `lessons` WRITE;
/*!40000 ALTER TABLE `lessons` DISABLE KEYS */;
INSERT INTO `lessons` VALUES (2,'Aula 1',3,'https://www.youtube.com/embed/Gz3Q_c67ZH0','2020-12-14 20:37:50','2020-12-14 20:37:50'),(3,'Aula 2',3,'https://www.youtube.com/embed/Gz3Q_c67ZH0','2020-12-14 20:37:50','2020-12-14 21:36:16'),(4,'Aula 1',4,'https://www.youtube.com/embed/Gz3Q_c67ZH0','2020-12-14 20:37:50','2020-12-14 21:36:16'),(5,'Aula 2',4,'https://www.youtube.com/embed/Gz3Q_c67ZH0','2020-12-14 20:37:50','2020-12-14 21:36:16'),(6,'Aula 3',4,'https://www.youtube.com/embed/Gz3Q_c67ZH0','2020-12-14 20:37:50','2020-12-14 21:36:16');
/*!40000 ALTER TABLE `lessons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (3,'Introdução e Conceitos','2020-12-14 20:37:50','2020-12-14 20:37:50'),(4,'Ambiente de desenvolvimento','2020-12-14 20:37:50','2020-12-14 20:37:50');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateNews` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (8,3,'Aprenda HTML','<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></p>\r\n<p><strong><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></strong></p>\r\n<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></p>\r\n<p><strong><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></strong></p>\r\n<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></p>','8','5d164db52e054.jpg','aprenda-html','2019-06-28','2020-11-30 20:31:31',NULL),(9,4,'Aprenda CSS','Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.\r\nAliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.\r\nAliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.\r\nAliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.\r\nAliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.','9','5d164dd81bf0b.jpg','aprenda-css','2019-06-28','2020-11-30 20:31:31','2020-11-30 21:30:13'),(10,6,'Aprenda JAVASCRIPT','<p>Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder volupta<strong>te frankfurter. Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter. Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter. Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bres</strong>aola. Drumstick excepteur shoulder voluptate frankfurter. Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</p>','0','5d164dfcaf0ed.jpg','aprenda-javascript','2019-06-28','2020-11-30 20:31:31',NULL);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online`
--

DROP TABLE IF EXISTS `online`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `online` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_action` datetime DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online`
--

LOCK TABLES `online` WRITE;
/*!40000 ALTER TABLE `online` DISABLE KEYS */;
INSERT INTO `online` VALUES (2,'172.19.0.1','2020-12-15 11:28:48','5fd8a783b7e62','2020-12-15 14:26:37',NULL);
/*!40000 ALTER TABLE `online` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_images`
--

LOCK TABLES `product_images` WRITE;
/*!40000 ALTER TABLE `product_images` DISABLE KEYS */;
INSERT INTO `product_images` VALUES (5,4,'5d1d00104570d.jpg','2020-12-10 18:25:39','2020-12-10 18:25:39'),(6,4,'5d1d005e315f2.jpg','2020-12-10 18:25:39','2020-12-10 18:25:39'),(7,4,'5d1d006ad8ec3.png','2020-12-10 18:25:39','2020-12-10 18:25:39'),(8,4,'5d1d006adb945.png','2020-12-10 18:25:39','2020-12-10 18:25:39'),(9,4,'5d1d006add7f9.png','2020-12-10 18:25:39','2020-12-10 18:25:39'),(11,4,'5d1d00ea2bf53.png','2020-12-10 18:25:39','2020-12-10 18:25:39'),(12,5,'5d23398636dd9.jpg','2020-12-10 18:25:39','2020-12-10 18:25:39'),(13,8,'fb12f1abf967989b23f32e92f742873a.jpg','2020-12-10 20:21:09',NULL),(14,8,'d69e45c30b3c3a3cc18136ac667e29ac.jpg','2020-12-10 20:21:09',NULL),(16,9,'b1831f3ff5b5143cd013cc0dad0e27a2.jpg','2020-12-10 20:22:06',NULL),(17,9,'bb877e6635354190405fd4d665de1bbf.jpg','2020-12-10 20:41:27',NULL);
/*!40000 ALTER TABLE `product_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `weight` int(11) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `value` decimal(16,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (4,'Avengers update 2 sdgsdfg','<p>Avengers update jdskaldsf bafslhdafs</p>',5,10,15,20,30,100.00,'2020-12-10 18:12:23','2020-12-10 18:12:23');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `properties`
--

DROP TABLE IF EXISTS `properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` decimal(16,2) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `properties`
--

LOCK TABLES `properties` WRITE;
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
INSERT INTO `properties` VALUES (1,'Meus empreendimentos','<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n','A',1000000.00,'ffcec28ccbe9a999311d5c498a9ca83d.jpg','meus-empreendimentos',NULL,'2020-12-11 17:01:17',NULL),(2,'Outros Empreendimentos','<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n','C',5000000.00,'88627b19bbd1658ba6e61e5f4feddcb5.jpg','outros-empreendimentos',NULL,'2020-12-11 17:05:41',NULL);
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `properties_home_images`
--

DROP TABLE IF EXISTS `properties_home_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `properties_home_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `properties_home_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `properties_home_images`
--

LOCK TABLES `properties_home_images` WRITE;
/*!40000 ALTER TABLE `properties_home_images` DISABLE KEYS */;
INSERT INTO `properties_home_images` VALUES (19,7,'5d1f4c5de3fdb.jpg','2020-12-10 21:24:14','2020-12-10 21:24:14'),(20,7,'5d1f4c5dea544.jpg','2020-12-10 21:24:14','2020-12-10 21:24:14'),(21,7,'5d1f4c5deef8a.jpg','2020-12-10 21:24:14','2020-12-10 21:24:14'),(22,7,'5d1f4c5df3303.jpg','2020-12-10 21:24:14','2020-12-10 21:24:14'),(31,11,'3dbe1730a86c70f266707f25a237d68a.jpg','2020-12-11 14:47:05',NULL),(32,8,'5a2e34da1382bee4ec18c3b13ddd638c.jpg','2020-12-11 17:01:47',NULL),(33,8,'c178240c2dcb830c41bbff39eb35823c.jpg','2020-12-11 17:01:47',NULL),(34,8,'8ea1ccf2d2800712de0cb92f37324a39.jpg','2020-12-11 17:01:48',NULL),(35,9,'20084969557b4b8083872beeb3b725a2.jpg','2020-12-11 17:02:15',NULL),(36,9,'0246791226353de30393b71055b84582.jpg','2020-12-11 17:02:16',NULL),(37,9,'ca08f72abdad5749368301d1db06f000.jpg','2020-12-11 17:02:17',NULL),(38,9,'274f46db5754b5e7045e309c660f8b78.jpg','2020-12-11 17:02:18',NULL),(39,10,'5fa402a2077484ef6415a70eaba99c4a.jpg','2020-12-11 17:06:10',NULL),(40,10,'eb9c067b94d2ba8a03431d6ab141e036.jpg','2020-12-11 17:06:10',NULL),(41,10,'b82585f23fc14c7cd509b477f2d96531.jpg','2020-12-11 17:06:11',NULL),(42,10,'6ae523e3a5914193471e1d0878e78077.jpg','2020-12-11 17:06:12',NULL);
/*!40000 ALTER TABLE `properties_home_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `properties_homes`
--

DROP TABLE IF EXISTS `properties_homes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `properties_homes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` decimal(16,2) NOT NULL,
  `area` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `properties_homes`
--

LOCK TABLES `properties_homes` WRITE;
/*!40000 ALTER TABLE `properties_homes` DISABLE KEYS */;
INSERT INTO `properties_homes` VALUES (7,9,'Imóvel 1 Empreendimento 2',500000.00,80,'2020-12-10 21:21:34','2020-12-10 21:21:34'),(8,1,'Apartamento 1',250000.00,100,'2020-12-11 17:01:46',NULL),(9,1,'Apartamento 2',500000.00,150,'2020-12-11 17:02:14',NULL),(10,2,'Casa 1',705000.00,500,'2020-12-11 17:06:08',NULL);
/*!40000 ALTER TABLE `properties_homes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_from` int(11) NOT NULL,
  `id_to` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requests`
--

LOCK TABLES `requests` WRITE;
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
INSERT INTO `requests` VALUES (8,1,2,1,'2020-12-15 14:14:27','2020-12-15 14:14:49'),(9,1,3,1,'2020-12-15 14:14:28','2020-12-15 14:14:49'),(10,1,4,1,'2020-12-15 14:14:29','2020-12-15 14:14:49'),(11,1,5,1,'2020-12-15 14:14:30','2020-12-15 14:14:49');
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'Websites','<p>Pig corned beef meatball, velit voluptate burgdoggen in cupim aute incididunt magna. Doner burgdoggen ham bacon. Kevin nostrud turducken,<strong> jowl ut boudin chicken biltong bresaola id nulla reprehenderit velit pork chop. Ball tip incididunt jowl bresaola drumstick duis. Strip steak ball tip veniam excepteur ut, pi</strong>canha shoulder kevin alcatra ad leberkas nostrud.</p>',3,'2020-11-30 17:01:20','2020-11-30 17:01:20'),(2,'Aplicativos','Pastrami pig pork loin ex turkey pork belly incididunt in officia. Id officia nisi, sausage in occaecat cow. Jerky kevin laboris sirloin. Cupidatat strip steak tongue nisi laborum id boudin landjaeger et. Kielbasa t-bone adipisicing short loin magna do velit.',1,'2020-11-30 17:01:20','2020-11-30 17:01:20'),(3,'Sistemas','Nisi do meatloaf aliqua reprehenderit labore venison mollit. Aute doner chicken shankle corned beef cupim. Ham hock filet mignon fatback cillum tri-tip. Capicola chicken jowl, sed officia pork belly beef aute venison aliqua duis brisket landjaeger kevin ipsum. Sint shankle non corned beef bacon tongue in ground round excepteur reprehenderit fugiat voluptate boudin chuck. Beef ribs in officia jerky, proident burgdoggen velit enim leberkas short ribs nulla pancetta picanha.',2,'2020-11-30 17:01:20','2020-11-30 17:01:20'),(4,'Marketing','Aute pig venison voluptate kielbasa. Non anim corned beef chuck. Velit ut quis capicola frankfurter cupim minim in tri-tip pork loin. Cupim deserunt beef ribs leberkas, incididunt officia hamburger consequat.',4,'2020-11-30 17:01:20','2020-11-30 17:01:20');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slides`
--

DROP TABLE IF EXISTS `slides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slide` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slides`
--

LOCK TABLES `slides` WRITE;
/*!40000 ALTER TABLE `slides` DISABLE KEYS */;
INSERT INTO `slides` VALUES (6,'Slide 1','5d165dacc5e28.jpg',6,'2020-11-30 17:01:20','2020-11-30 17:01:20'),(7,'Slide 2','5d165dba6cfb2.jpg',7,'2020-11-30 17:01:20','2020-11-30 17:01:20'),(8,'Slide 3','5d165dc8e0c9d.jpg',8,'2020-11-30 17:01:20','2020-11-30 17:01:20'),(9,'Slide 4','5de181c5f77e8b5ee9b6265b87022e62.jpg',NULL,'2020-12-01 17:29:26','2020-12-01 17:33:38');
/*!40000 ALTER TABLE `slides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (2,'Aluno 1','aluno1@teste.com','$2y$10$8r8UQiSj/5Sau32sAnmqP.60cOZQFxXVtXOpNhvwqnoEXVhg6oQlK','2020-12-14 20:37:50','2020-12-14 20:37:50');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `todo`
--

DROP TABLE IF EXISTS `todo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `todo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `todo`
--

LOCK TABLES `todo` WRITE;
/*!40000 ALTER TABLE `todo` DISABLE KEYS */;
INSERT INTO `todo` VALUES (3,'Tarefa 1','2019-07-01','2020-12-11 20:44:19','2020-12-11 20:44:19'),(4,'Tarefa 2','2019-07-06','2020-12-11 20:44:19','2020-12-11 20:44:19'),(5,'tarefa 3','2019-07-07','2020-12-11 20:44:19','2020-12-11 20:44:19'),(6,'tarefa 4','2019-07-08','2020-12-11 20:44:19','2020-12-11 20:44:19'),(7,'tarefa 5','2019-07-06','2020-12-11 20:44:19','2020-12-11 20:44:19'),(8,'asdfsd','2019-07-06','2020-12-11 20:44:19','2020-12-11 20:44:19'),(9,'asfsd','2019-07-09','2020-12-11 20:44:19','2020-12-11 20:44:19'),(10,'asdfasdf','2019-07-06','2020-12-11 20:44:19','2020-12-11 20:44:19'),(11,'outro teste','2019-07-06','2020-12-11 20:44:19','2020-12-11 20:44:19'),(12,'2341234','2019-07-06','2020-12-11 20:44:19','2020-12-11 20:44:19'),(13,'9796789','2019-07-06','2020-12-11 20:44:19','2020-12-11 20:44:19'),(14,'teste','2020-12-11','2020-12-11 21:08:33',NULL),(15,'oturo','2020-12-11','2020-12-11 21:08:40',NULL);
/*!40000 ALTER TABLE `todo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  FULLTEXT KEY `name_email_FULLTEXT` (`name`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'user','user@user.com','$2y$10$JxsGcZCAPf6pl8BvnppKtONxdVxkJW1pKjWzlr67oSBFS5/nD/oVm','5d1bced7a9c9a.png','2020-08-14 15:16:17','2020-12-15 12:47:11'),(2,'user2','user2@user.com','$2y$10$X.G.uRBaMPeXzlUSFolcje2I4HNdYiAWbDwN6lWBA7TRLImZTmu2W','f8a9952791ab3c8e9a7ed0ab8df439d8.jpg','2020-08-14 15:47:21','2020-12-15 12:47:11'),(3,'User 3','user3@user.com','$2y$10$vHWoNhjTDqNWhBqEGZ3HAuMbUryS0WWC5Xol/VJdHaS41fOZQ3rPi','869b8e7f36b5a0d7cec93cb3946c0368.jpg','2020-12-15 13:02:08','2020-12-15 13:34:06'),(4,'User 4','user4@user.com','$2y$10$fTjzAV6DR0CwXoJ4N.Isv.DxVh16Ec1IScMqUUSsQGxydFvNMZRDO',NULL,'2020-12-15 13:28:18',NULL),(5,'User 5','user5@user.com','$2y$10$kqMs4rFh8SyocM.z2uLv9./Gt1FR4JYokFqyQJUrqBoD4gfvENTOi',NULL,'2020-12-15 13:28:29',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visits`
--

DROP TABLE IF EXISTS `visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `visits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `day` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visits`
--

LOCK TABLES `visits` WRITE;
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
INSERT INTO `visits` VALUES (1,'172.19.0.1','2020-12-01','2020-12-01 14:10:14',NULL),(2,'172.19.0.1','2020-12-02','2020-12-02 20:18:04',NULL),(3,'172.26.0.1','2020-12-11','2020-12-11 16:52:24',NULL);
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-15 11:33:06
