<?php

use Src\Router\Route;

Route::get([
    'set' => '/projeto07',
    'as' => 'projeto07.login',
    'namespace' => "Src\\Controllers\\Projeto07\\"
], 'HomeController@login');

Route::post([
    'set' => 'projeto07',
    'as' => 'projeto07.login',
    'namespace' => "Src\\Controllers\\Projeto07\\"
], 'AuthController@login');

Route::get([
    'set' => '/projeto07/home',
    'as' => 'projeto07.home',
    'namespace' => "Src\\Controllers\\Projeto07\\"
], 'HomeController@home');

Route::get([
    'set' => '/projeto07/comprar',
    'as' => 'projeto07.comprar',
    'namespace' => "Src\\Controllers\\Projeto07\\"
], 'HomeController@comprar');

Route::get([
    'set' => '/projeto07/aula/{id}',
    'as' => 'projeto07.aula',
    'namespace' => "Src\\Controllers\\Projeto07\\"
], 'HomeController@aula');
