<?php

use Src\Router\Route;

Route::get([
    'set' => '/projeto06',
    'as' => 'projeto06.shop',
    'namespace' => "Src\\Controllers\\Projeto06\\"
], 'ShopController@index');

Route::get([
    'set' => '/projeto06/addToCart/{product_id}',
    'as' => 'projeto06.addToCart',
    'namespace' => "Src\\Controllers\\Projeto06\\"
], 'ShopController@addToCart');

Route::get([
    'set' => '/projeto06/checkout',
    'as' => 'projeto06.checkout',
    'namespace' => "Src\\Controllers\\Projeto06\\"
], 'ShopController@checkout');