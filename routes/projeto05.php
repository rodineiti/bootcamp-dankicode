<?php

use Src\Router\Route;

Route::get([
    'set' => '/projeto05/{slug}',
    'as' => 'projeto05.forum',
    'namespace' => "Src\\Controllers\\Projeto05\\"
], 'ForumController@index');

Route::get([
    'set' => '/projeto05/single/{id}',
    'as' => 'projeto05.single',
    'namespace' => "Src\\Controllers\\Projeto05\\"
], 'ForumController@single');

Route::post([
    'set' => '/projeto05/post/store',
    'as' => 'projeto05.post.store',
    'namespace' => "Src\\Controllers\\Projeto05\\"
], 'ForumController@store');

Route::post([
    'set' => '/projeto05/forum/store',
    'as' => 'projeto05.forum.store',
    'namespace' => "Src\\Controllers\\Projeto05\\"
], 'ForumController@storeForum');

Route::post([
    'set' => '/projeto05/topic/store',
    'as' => 'projeto05.topic.store',
    'namespace' => "Src\\Controllers\\Projeto05\\"
], 'ForumController@storeTopic');