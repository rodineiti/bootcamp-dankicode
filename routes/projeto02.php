<?php

use Src\Router\Route;

Route::get([
    'set' => '/projeto02/',
    'as' => 'projeto02.home',
    'namespace' => "Src\\Controllers\\Projeto02\\"
], 'HomeController@index');

Route::get([
    'set' => '/projeto02/{slug}/empreendimento',
    'as' => 'projeto02.empreentimento.show',
    'namespace' => "Src\\Controllers\\Projeto02\\"
], 'HomeController@empreentimento');

/**
 * Ajaxs
 */
Route::post([
    'set' => '/projeto02/ajax/send',
    'as' => 'projeto02.ajax.send',
    'namespace' => "Src\\Controllers\\Projeto02\\"
], 'AjaxController@send');