<?php

use Src\Router\Route;

Route::get([
    'set' => '/admin/login',
    'as' => 'admin.login',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AdminController@index');

Route::post([
    'set' => '/admin/login',
    'as' => 'admin.login',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AdminController@login');

Route::get([
    'set' => '/admin/home',
    'as' => 'admin.home',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'HomeController@index');

Route::get([
    'set' => '/admin/profile',
    'as' => 'admin.profile',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AdminController@profile');

Route::post([
    'set' => '/admin/profile',
    'as' => 'admin.profile',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AdminController@update');

Route::get([
    'set' => '/admin/logout',
    'as' => 'admin.logout',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AdminController@logout');

/**
 * Users
 */
Route::get([
    'set' => '/admin/users',
    'as' => 'admin.users.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'UsersController@index');

Route::get([
    'set' => '/admin/users/create',
    'as' => 'admin.users.create',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'UsersController@create');

Route::post([
    'set' => '/admin/users/store',
    'as' => 'admin.users.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'UsersController@store');

Route::get([
    'set' => '/admin/users/edit/{id}',
    'as' => 'admin.users.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'UsersController@edit');

Route::post([
    'set' => '/admin/users/{id}/update',
    'as' => 'admin.users.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'UsersController@update');

Route::get([
    'set' => '/admin/users/{id}/destroy',
    'as' => 'admin.users.destroy',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'UsersController@destroy');

/**
 * Depoimentos
 */
Route::get([
    'set' => '/admin/depoimentos',
    'as' => 'admin.depoimentos.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'DepoimentoController@index');

Route::get([
    'set' => '/admin/depoimentos/create',
    'as' => 'admin.depoimentos.create',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'DepoimentoController@create');

Route::post([
    'set' => '/admin/depoimentos/store',
    'as' => 'admin.depoimentos.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'DepoimentoController@store');

Route::get([
    'set' => '/admin/depoimentos/edit/{id}',
    'as' => 'admin.depoimentos.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'DepoimentoController@edit');

Route::post([
    'set' => '/admin/depoimentos/{id}/update',
    'as' => 'admin.depoimentos.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'DepoimentoController@update');

Route::get([
    'set' => '/admin/depoimentos/{id}/destroy',
    'as' => 'admin.depoimentos.destroy',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'DepoimentoController@destroy');

/**
 * Servicos
 */
Route::get([
    'set' => '/admin/servicos',
    'as' => 'admin.servicos.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ServicoController@index');

Route::get([
    'set' => '/admin/servicos/create',
    'as' => 'admin.servicos.create',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ServicoController@create');

Route::post([
    'set' => '/admin/servicos/store',
    'as' => 'admin.servicos.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ServicoController@store');

Route::get([
    'set' => '/admin/servicos/edit/{id}',
    'as' => 'admin.servicos.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ServicoController@edit');

Route::post([
    'set' => '/admin/servicos/{id}/update',
    'as' => 'admin.servicos.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ServicoController@update');

Route::get([
    'set' => '/admin/servicos/{id}/destroy',
    'as' => 'admin.servicos.destroy',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ServicoController@destroy');

/**
 * Slides
 */
Route::get([
    'set' => '/admin/slides',
    'as' => 'admin.slides.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'SlideController@index');

Route::get([
    'set' => '/admin/slides/create',
    'as' => 'admin.slides.create',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'SlideController@create');

Route::post([
    'set' => '/admin/slides/store',
    'as' => 'admin.slides.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'SlideController@store');

Route::get([
    'set' => '/admin/slides/edit/{id}',
    'as' => 'admin.slides.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'SlideController@edit');

Route::post([
    'set' => '/admin/slides/{id}/update',
    'as' => 'admin.slides.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'SlideController@update');

Route::get([
    'set' => '/admin/slides/{id}/destroy',
    'as' => 'admin.slides.destroy',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'SlideController@destroy');

/**
 * Config
 */
Route::get([
    'set' => '/admin/config',
    'as' => 'admin.config.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ConfigController@edit');

Route::post([
    'set' => '/admin/config/{id}/update',
    'as' => 'admin.config.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ConfigController@update');

/**
 * Clientes
 */
Route::get([
    'set' => '/admin/clientes',
    'as' => 'admin.clientes.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ClienteController@index');

Route::get([
    'set' => '/admin/clientes/create',
    'as' => 'admin.clientes.create',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ClienteController@create');

Route::post([
    'set' => '/admin/clientes/store',
    'as' => 'admin.clientes.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ClienteController@store');

Route::get([
    'set' => '/admin/clientes/edit/{id}',
    'as' => 'admin.clientes.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ClienteController@edit');

Route::post([
    'set' => '/admin/clientes/{id}/update',
    'as' => 'admin.clientes.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ClienteController@update');

Route::post([
    'set' => '/admin/clientes/{id}/addPayment',
    'as' => 'admin.clientes.addPayment',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ClienteController@addPayment');

Route::get([
    'set' => '/admin/clientes/{id}/updateStatusPayment/{finance_id}',
    'as' => 'admin.clientes.updateStatusPayment',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ClienteController@updateStatusPayment');

Route::get([
    'set' => '/admin/clientes/{id}/destroy',
    'as' => 'admin.clientes.destroy',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ClienteController@destroy');

/**
 * Categorias
 */
Route::get([
    'set' => '/admin/categorias',
    'as' => 'admin.categorias.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'CategoriaController@index');

Route::get([
    'set' => '/admin/categorias/create',
    'as' => 'admin.categorias.create',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'CategoriaController@create');

Route::post([
    'set' => '/admin/categorias/store',
    'as' => 'admin.categorias.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'CategoriaController@store');

Route::get([
    'set' => '/admin/categorias/edit/{id}',
    'as' => 'admin.categorias.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'CategoriaController@edit');

Route::post([
    'set' => '/admin/categorias/{id}/update',
    'as' => 'admin.categorias.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'CategoriaController@update');

Route::get([
    'set' => '/admin/categorias/{id}/destroy',
    'as' => 'admin.categorias.destroy',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'CategoriaController@destroy');

/**
 * Noticias
 */
Route::get([
    'set' => '/admin/noticias',
    'as' => 'admin.noticias.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'NoticiaController@index');

Route::get([
    'set' => '/admin/noticias/create',
    'as' => 'admin.noticias.create',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'NoticiaController@create');

Route::post([
    'set' => '/admin/noticias/store',
    'as' => 'admin.noticias.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'NoticiaController@store');

Route::get([
    'set' => '/admin/noticias/edit/{id}',
    'as' => 'admin.noticias.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'NoticiaController@edit');

Route::post([
    'set' => '/admin/noticias/{id}/update',
    'as' => 'admin.noticias.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'NoticiaController@update');

Route::get([
    'set' => '/admin/noticias/{id}/destroy',
    'as' => 'admin.noticias.destroy',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'NoticiaController@destroy');

/**
 * Noticias Comentários e Respostas
 */
Route::post([
    'set' => '/admin/comentarios/storeComment',
    'as' => 'admin.comentarios.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ComentarioController@storeComment');

Route::post([
    'set' => '/admin/respostas/storeAnswer',
    'as' => 'admin.respostas.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ComentarioController@storeAnswer');

/**
 * Pagamentos
 */
Route::get([
    'set' => '/admin/pagamentos',
    'as' => 'admin.pagamentos.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'PagamentoController@index');

Route::get([
    'set' => '/admin/pagamentos/{client_id}/updateStatusPayment/{finance_id}',
    'as' => 'admin.pagamentos.updateStatusPayment',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'PagamentoController@updateStatusPayment');

Route::get([
    'set' => '/admin/pagamentos/{status}/pdfFinance',
    'as' => 'admin.pagamentos.pdfFinance',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'PagamentoController@pdfFinance');

/**
 * Produtos
 */
Route::get([
    'set' => '/admin/produtos',
    'as' => 'admin.produtos.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ProdutoController@index');

Route::get([
    'set' => '/admin/produtos/create',
    'as' => 'admin.produtos.create',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ProdutoController@create');

Route::post([
    'set' => '/admin/produtos/store',
    'as' => 'admin.produtos.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ProdutoController@store');

Route::get([
    'set' => '/admin/produtos/edit/{id}',
    'as' => 'admin.produtos.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ProdutoController@edit');

Route::post([
    'set' => '/admin/produtos/{id}/update',
    'as' => 'admin.produtos.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ProdutoController@update');

Route::post([
    'set' => '/admin/produtos/{id}/updateQty',
    'as' => 'admin.produtos.updateQty',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ProdutoController@updateQty');

Route::get([
    'set' => '/admin/produtos/{id}/destroy',
    'as' => 'admin.produtos.destroy',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ProdutoController@destroy');

Route::get([
    'set' => '/admin/produtos/{product_id}/destroyImage/{id}',
    'as' => 'admin.produtos.destroyImage',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ProdutoController@destroyImage');

/**
 * Empreendimentos
 */
Route::get([
    'set' => '/admin/empreendimentos',
    'as' => 'admin.empreendimentos.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'EmpreendimentoController@index');

Route::get([
    'set' => '/admin/empreendimentos/create',
    'as' => 'admin.empreendimentos.create',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'EmpreendimentoController@create');

Route::post([
    'set' => '/admin/empreendimentos/store',
    'as' => 'admin.empreendimentos.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'EmpreendimentoController@store');

Route::get([
    'set' => '/admin/empreendimentos/show/{id}',
    'as' => 'admin.empreendimentos.show',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'EmpreendimentoController@show');

Route::get([
    'set' => '/admin/empreendimentos/edit/{id}',
    'as' => 'admin.empreendimentos.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'EmpreendimentoController@edit');

Route::post([
    'set' => '/admin/empreendimentos/{id}/update',
    'as' => 'admin.empreendimentos.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'EmpreendimentoController@update');

Route::get([
    'set' => '/admin/empreendimentos/{id}/destroy',
    'as' => 'admin.empreendimentos.destroy',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'EmpreendimentoController@destroy');

/**
 * Imoveis
 */
Route::get([
    'set' => '/admin/imoveis/create/{property_id}',
    'as' => 'admin.imoveis.create',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ImovelController@create');

Route::post([
    'set' => '/admin/imoveis/store',
    'as' => 'admin.imoveis.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ImovelController@store');

Route::get([
    'set' => '/admin/imoveis/edit/{id}/property/{property_id}',
    'as' => 'admin.imoveis.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ImovelController@edit');

Route::post([
    'set' => '/admin/imoveis/{id}/update',
    'as' => 'admin.imoveis.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ImovelController@update');

Route::get([
    'set' => '/admin/imoveis/{id}/destroy/property/{property_id}',
    'as' => 'admin.imoveis.destroy',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ImovelController@destroy');

Route::get([
    'set' => '/admin/imoveis/{home_id}/destroyImage/{id}/property/{property_id}',
    'as' => 'admin.imoveis.destroyImage',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ImovelController@destroyImage');

/**
 * Ajaxs
 */
Route::post([
    'set' => '/admin/ajax/empreendimentos',
    'as' => 'admin.ajax.empreendimentos',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AjaxController@empreendimentos');

/**
 * Chat
 */
Route::get([
    'set' => '/admin/chat',
    'as' => 'admin.chat.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ChatController@index');

Route::post([
    'set' => '/admin/chat/store',
    'as' => 'admin.chat.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ChatController@store');

Route::post([
    'set' => '/admin/chat/last',
    'as' => 'admin.chat.last',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ChatController@last');

/**
 * Calendário
 */
Route::get([
    'set' => '/admin/calendar',
    'as' => 'admin.calendar.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'CalendarController@index');

Route::post([
    'set' => '/admin/calendar/store',
    'as' => 'admin.calendar.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'CalendarController@store');

Route::post([
    'set' => '/admin/calendar/last',
    'as' => 'admin.calendar.last',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'CalendarController@last');


/**
 * Alunos
 */
Route::get([
    'set' => '/admin/alunos',
    'as' => 'admin.alunos.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AlunoController@index');

Route::get([
    'set' => '/admin/alunos/create',
    'as' => 'admin.alunos.create',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AlunoController@create');

Route::post([
    'set' => '/admin/alunos/store',
    'as' => 'admin.alunos.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AlunoController@store');

Route::get([
    'set' => '/admin/alunos/edit/{id}',
    'as' => 'admin.alunos.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AlunoController@edit');

Route::post([
    'set' => '/admin/alunos/{id}/update',
    'as' => 'admin.alunos.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AlunoController@update');

Route::get([
    'set' => '/admin/alunos/{id}/destroy',
    'as' => 'admin.alunos.destroy',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AlunoController@destroy');

/**
 * Módulos
 */
Route::get([
    'set' => '/admin/modulos',
    'as' => 'admin.modulos.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ModuloController@index');

Route::get([
    'set' => '/admin/modulos/create',
    'as' => 'admin.modulos.create',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ModuloController@create');

Route::post([
    'set' => '/admin/modulos/store',
    'as' => 'admin.modulos.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ModuloController@store');

Route::get([
    'set' => '/admin/modulos/edit/{id}',
    'as' => 'admin.modulos.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ModuloController@edit');

Route::post([
    'set' => '/admin/modulos/{id}/update',
    'as' => 'admin.modulos.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ModuloController@update');

Route::get([
    'set' => '/admin/modulos/{id}/destroy',
    'as' => 'admin.modulos.destroy',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'ModuloController@destroy');

/**
 * Aulas
 */
Route::get([
    'set' => '/admin/aulas',
    'as' => 'admin.aulas.index',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AulaController@index');

Route::get([
    'set' => '/admin/aulas/create',
    'as' => 'admin.aulas.create',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AulaController@create');

Route::post([
    'set' => '/admin/aulas/store',
    'as' => 'admin.aulas.store',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AulaController@store');

Route::get([
    'set' => '/admin/aulas/edit/{id}',
    'as' => 'admin.aulas.edit',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AulaController@edit');

Route::post([
    'set' => '/admin/aulas/{id}/update',
    'as' => 'admin.aulas.update',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AulaController@update');

Route::get([
    'set' => '/admin/aulas/{id}/destroy',
    'as' => 'admin.aulas.destroy',
    'namespace' => "Src\\Controllers\\Admin\\"
], 'AulaController@destroy');