<?php

use Src\Router\Route;

Route::get([
    'set' => '/projeto01/',
    'as' => 'projeto01.home',
    'namespace' => "Src\\Controllers\\Projeto01\\"
], 'HomeController@index');

Route::get([
    'set' => '/projeto01/sobre',
    'as' => 'projeto01.sobre',
    'namespace' => "Src\\Controllers\\Projeto01\\"
], 'HomeController@sobre');

Route::get([
    'set' => '/projeto01/especialidades',
    'as' => 'projeto01.especialidades',
    'namespace' => "Src\\Controllers\\Projeto01\\"
], 'HomeController@especialidades');

Route::get([
    'set' => '/projeto01/servicos',
    'as' => 'projeto01.servicos',
    'namespace' => "Src\\Controllers\\Projeto01\\"
], 'HomeController@servicos');

Route::get([
    'set' => '/projeto01/noticias/{slug}',
    'as' => 'projeto01.noticias',
    'namespace' => "Src\\Controllers\\Projeto01\\"
], 'HomeController@noticias');

Route::get([
    'set' => '/projeto01/noticia_single/{id}',
    'as' => 'projeto01.noticia_single',
    'namespace' => "Src\\Controllers\\Projeto01\\"
], 'HomeController@noticia_single');

Route::get([
    'set' => '/projeto01/contato',
    'as' => 'projeto01.contato',
    'namespace' => "Src\\Controllers\\Projeto01\\"
], 'HomeController@contato');