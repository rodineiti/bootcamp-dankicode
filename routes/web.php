<?php

use Src\Router\Route;

Route::get([
    'set' => '/',
    'as' => 'home'
], 'HomeController@index');

Route::get([
    'set' => '/login',
    'as' => 'login'
], 'AuthController@index');
Route::post([
    'set' => '/login',
    'as' => 'login'
], 'AuthController@login');

Route::get([
    'set' => '/register',
    'as' => 'register'
], 'AuthController@register');
Route::post([
    'set' => '/register',
    'as' => 'register'
], 'AuthController@save');

Route::get([
    'set' => '/profile',
    'as' => 'profile'
], 'AuthController@profile');
Route::post([
    'set' => '/profile',
    'as' => 'profile'
], 'AuthController@update');

Route::get([
    'set' => '/logout',
    'as' => 'logout'
], 'AuthController@logout');
