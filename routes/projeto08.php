<?php

use Src\Router\Route;

Route::get([
    'set' => '/projeto08',
    'as' => 'projeto08.home',
    'namespace' => "Src\\Controllers\\Projeto08\\"
], 'HomeController@home');

Route::post([
    'set' => '/projeto08/login',
    'as' => 'projeto08.login',
    'namespace' => "Src\\Controllers\\Projeto08\\"
], 'AuthController@login');

Route::get([
    'set' => '/projeto08/feed',
    'as' => 'projeto08.feed',
    'namespace' => "Src\\Controllers\\Projeto08\\"
], 'HomeController@feed');

Route::post([
    'set' => '/projeto08/post',
    'as' => 'projeto08.post',
    'namespace' => "Src\\Controllers\\Projeto08\\"
], 'HomeController@post');

Route::get([
    'set' => '/projeto08/solicitacoes',
    'as' => 'projeto08.solicitacoes',
    'namespace' => "Src\\Controllers\\Projeto08\\"
], 'HomeController@solicitacoes');

Route::get([
    'set' => '/projeto08/comunidade/{id}/adicionar',
    'as' => 'projeto08.adicionar',
    'namespace' => "Src\\Controllers\\Projeto08\\"
], 'HomeController@adicionar');

Route::get([
    'set' => '/projeto08/comunidade/{id}/desfazer',
    'as' => 'projeto08.desfazer',
    'namespace' => "Src\\Controllers\\Projeto08\\"
], 'HomeController@desfazer');

Route::get([
    'set' => '/projeto08/solicitacoes/{id}/aceitar',
    'as' => 'projeto08.aceitar',
    'namespace' => "Src\\Controllers\\Projeto08\\"
], 'HomeController@aceitar');

Route::get([
    'set' => '/projeto08/solicitacoes/{id}/recusar',
    'as' => 'projeto08.recusar',
    'namespace' => "Src\\Controllers\\Projeto08\\"
], 'HomeController@recusar');

Route::get([
    'set' => '/projeto08/comunidade',
    'as' => 'projeto08.comunidade',
    'namespace' => "Src\\Controllers\\Projeto08\\"
], 'HomeController@comunidade');

Route::get([
    'set' => '/projeto08/logout',
    'as' => 'projeto08.logout',
    'namespace' => "Src\\Controllers\\Projeto08\\"
], 'AuthController@logout');