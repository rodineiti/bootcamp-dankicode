-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: dankicode
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (3,'HTML','html',3, NOW(), NOW()),(4,'CSS','css',4, NOW(), NOW()),(6,'JAVASCRIPT','javascript',6, NOW(), NOW());
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat`
--

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
INSERT INTO `chat` VALUES (1,1,'olá mundo',NOW(),NOW()),(2,2,'blz',NOW(),NOW()),(3,1,'sim',NOW(),NOW()),(4,1,'olá',NOW(),NOW()),(5,2,'diga',NOW(),NOW());
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `cpf_cnpj` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (15,'Rodinei Teixeira','teste@teste.com.rod','PF','123.456.789-09','5d1bca94d4ec9.png','15', NOW(), NOW()),(16,'Fulano de tal','rodineiguitar@gmail.com','PJ','28.726.608/0001-17','5d1bced7a9c9a.png','16', NOW(), NOW());
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,1,10,'comentário postando', NOW(), NOW()),(2,1,10,'adicionando novo comentário', NOW(), NOW());
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments_answers`
--

DROP TABLE IF EXISTS `comments_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comments_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answer` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments_answers`
--

LOCK TABLES `comments_answers` WRITE;
/*!40000 ALTER TABLE `comments_answers` DISABLE KEYS */;
INSERT INTO `comments_answers` VALUES (1,1,1,'Resposta comentário postando', NOW(), NOW()),(2,1,1,'outra resposta ao comentário postando', NOW(), NOW());
/*!40000 ALTER TABLE `comments_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `icon1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description1` text COLLATE utf8_unicode_ci NOT NULL,
  `icon2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description2` text COLLATE utf8_unicode_ci NOT NULL,
  `icon3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description3` text COLLATE utf8_unicode_ci NOT NULL,
  `title1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,'Projeto 01 - Backend','Rodinei de Jesus Santos','Short ribs shoulder consequat tongue deserunt ribeye minim ut magna fatback dolore anim duis quis. Occaecat in drumstick officia enim. Turducken reprehenderit duis picanha id enim beef voluptate esse in buffalo pariatur fatback jerky. Cupidatat strip steak t-bone pork loin ribeye, bresaola brisket pariatur excepteur prosciutto laboris. Alcatra dolore short loin nisi frankfurter flank strip steak eu salami beef fugiat. Ullamco ball tip eu filet mignon. Pork loin cupidatat est, excepteur turducken elit shoulder hamburger ball tip. Tongue elit adipisicing dolor, burgdoggen landjaeger ground round ham hock shank. Leberkas reprehenderit boudin consectetur ex veniam drumstick corned beef short ribs ipsum. Ullamco turducken pastrami exercitation, shankle officia cupidatat salami id laborum.','fa fa-home','Venison meatball ut ham swine t-bone. Strip steak filet mignon magna, cow ut tongue doner rump velit chicken burgdoggen. Sausage enim swine beef ribs eu. Shank jowl do exercitation ullamco burgdoggen ea landjaeger velit consectetur cupidatat meatloaf in. Venison meatball ut ham swine t-bone. Strip steak filet mignon magna, cow ut tongue doner rump velit chicken burgdoggen. Sausage enim swine beef ribs eu. Shank jowl do exercitation ullamco burgdoggen ea landjaeger velit consectetur cupidatat meatloaf in.','fa fa-users','Shoulder turducken tempor porchetta commodo dolore nostrud anim hamburger sausage swine qui incididunt ipsum. Occaecat pork loin fatback shankle. Landjaeger reprehenderit hamburger sed flank. Ham voluptate burgdoggen strip steak bacon magna, sirloin porchetta in. Biltong veniam dolore, flank doner bacon quis consectetur sausage.','fa fa-times','Swine leberkas venison sed in ut sint in nulla pork chop burgdoggen boudin tempor consectetur non. Cupidatat consectetur eu cillum. Consectetur commodo alcatra turkey ham hock ea. Elit fatback frankfurter hamburger bresaola filet mignon. Sausage cupim chicken laborum sint incididunt. Cillum pork shoulder cupidatat pastrami filet mignon velit jowl minim shankle short loin culpa enim. Quis consectetur nulla, fugiat jerky tenderloin doner veniam beef swine labore velit enim.','HTML','CSS','JAVASCRIPT', NOW(), NOW());
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_contacts`
--

DROP TABLE IF EXISTS `admin_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_contacts`
--

LOCK TABLES `admin_contacts` WRITE;
/*!40000 ALTER TABLE `admin_contacts` DISABLE KEYS */;
INSERT INTO `admin_contacts` VALUES (1,1,'teste@teste.com.rod'),(2,2,'admin@admin.com');
/*!40000 ALTER TABLE `admin_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_student`
--

DROP TABLE IF EXISTS `courses_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `courses_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_student`
--

LOCK TABLES `courses_student` WRITE;
/*!40000 ALTER TABLE `courses_student` DISABLE KEYS */;
INSERT INTO `courses_student` VALUES (2,2,NOW(),NOW());
/*!40000 ALTER TABLE `courses_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `depositions`
--

DROP TABLE IF EXISTS `depositions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `depositions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `order_id` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `depositions`
--

LOCK TABLES `depositions` WRITE;
/*!40000 ALTER TABLE `depositions` DISABLE KEYS */;
INSERT INTO `depositions` VALUES (8,'Rodinei Teixeira','Meatloaf chuck fugiat occaecat frankfurter, voluptate nulla. Quis eu lorem ad filet mignon. Beef ribs nulla swine, short loin labore duis consequat magna. Ball tip bacon corned beef pariatur. Incididunt eu pancetta voluptate duis. update','1', NOW(), NOW()),(12,'Rodinei Teixeira','Culpa consectetur id anim swine commodo, minim burgdoggen pork belly boudin qui pariatur. Tempor in aliquip aute short loin occaecat incididunt, bacon in in irure sunt. Consectetur minim tri-tip pork loin quis in brisket ullamco elit beef alcatra fugiat bresaola ground round non.','2',NOW(), NOW()),(13,'Fulano de tal','Culpa dolor qui do prosciutto aute salami quis. Pork chop commodo spare ribs mollit est picanha qui sirloin ad adipisicing nostrud fugiat. Alcatra occaecat swine hamburger lorem turkey. Ball tip qui in meatloaf leberkas anim aliquip shoulder consectetur velit.','4',NOW(), NOW()),(14,'Ciclano','Nulla non turducken ground round, occaecat id ipsum consectetur est shankle labore culpa doner shoulder. Spare ribs pork loin turkey, velit esse beef ham hock hamburger. Aute tenderloin quis ut, elit veniam cow in spare ribs. Tempor filet mignon ham mollit, excepteur doner in duis pariatur.','3', NOW(), NOW());
/*!40000 ALTER TABLE `depositions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feeds`
--

DROP TABLE IF EXISTS `feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feeds`
--

LOCK TABLES `feeds` WRITE;
/*!40000 ALTER TABLE `feeds` DISABLE KEYS */;
INSERT INTO `feeds` VALUES (1,2,'hello world',NOW(),NOW()),(2,2,'asndfjasldf',NOW(),NOW()),(3,1,'asdfasçdkf',NOW(),NOW());
/*!40000 ALTER TABLE `feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finances`
--

DROP TABLE IF EXISTS `finances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `finances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` decimal(16,2) NOT NULL,
  `expiration` date NOT NULL,
  `paid` date DEFAULT NULL,
  `portion` char(2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `order_id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forum_posts`
--

DROP TABLE IF EXISTS `forum_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_posts`
--

LOCK TABLES `forum_posts` WRITE;
/*!40000 ALTER TABLE `forum_posts` DISABLE KEYS */;
INSERT INTO `forum_posts` VALUES (1,2,1,'hello',NOW(),NOW()),(2,2,2,'outro teste',NOW(),NOW());
/*!40000 ALTER TABLE `forum_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_topics`
--

DROP TABLE IF EXISTS `forum_topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_topics`
--

LOCK TABLES `forum_topics` WRITE;
/*!40000 ALTER TABLE `forum_topics` DISABLE KEYS */;
INSERT INTO `forum_topics` VALUES (1,3,'topico mobile 1',NOW(),NOW()),(2,4,'topico 1 marketing',NOW(),NOW()),(3,4,'topico 2 marketing',NOW(),NOW());
/*!40000 ALTER TABLE `forum_topics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forums`
--

DROP TABLE IF EXISTS `forums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forums`
--

LOCK TABLES `forums` WRITE;
/*!40000 ALTER TABLE `forums` DISABLE KEYS */;
INSERT INTO `forums` VALUES (1,'Games',NOW(),NOW()),(2,'Web',NOW(),NOW()),(3,'Mobile',NOW(),NOW()),(4,'Marketing',NOW(),NOW());
/*!40000 ALTER TABLE `forums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `properties_homes`
--

DROP TABLE IF EXISTS `properties_homes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `properties_homes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` decimal(16,2) NOT NULL,
  `area` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `properties_homes`
--

LOCK TABLES `properties_homes` WRITE;
/*!40000 ALTER TABLE `properties_homes` DISABLE KEYS */;
INSERT INTO `properties_homes` VALUES (6,8,'Imóvel 1 Empreendimento 1',250000.00,60,NOW(),NOW()),(7,9,'Imóvel 1 Empreendimento 2',500000.00,80,NOW(),NOW()),(8,8,'Imóvel 2 Empreendimento 1',50000.00,40,NOW(),NOW());
/*!40000 ALTER TABLE `properties_homes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_images`
--

LOCK TABLES `product_images` WRITE;
/*!40000 ALTER TABLE `product_images` DISABLE KEYS */;
INSERT INTO `product_images` VALUES (5,4,'5d1d00104570d.jpg',NOW(),NOW()),(6,4,'5d1d005e315f2.jpg',NOW(),NOW()),(7,4,'5d1d006ad8ec3.png',NOW(),NOW()),(8,4,'5d1d006adb945.png',NOW(),NOW()),(9,4,'5d1d006add7f9.png',NOW(),NOW()),(11,4,'5d1d00ea2bf53.png',NOW(),NOW()),(12,5,'5d23398636dd9.jpg',NOW(),NOW());
/*!40000 ALTER TABLE `product_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `properties_home_images`
--

DROP TABLE IF EXISTS `properties_home_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `properties_home_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `properties_home_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `properties_home_images`
--

LOCK TABLES `properties_home_images` WRITE;
/*!40000 ALTER TABLE `properties_home_images` DISABLE KEYS */;
INSERT INTO `properties_home_images` VALUES (15,6,'5d1f4c39e533f.jpg',NOW(),NOW()),(16,6,'5d1f4c39ebe24.jpg',NOW(),NOW()),(17,6,'5d1f4c39f0001.jpg',NOW(),NOW()),(18,6,'5d1f4c3a00067.jpg',NOW(),NOW()),(19,7,'5d1f4c5de3fdb.jpg',NOW(),NOW()),(20,7,'5d1f4c5dea544.jpg',NOW(),NOW()),(21,7,'5d1f4c5deef8a.jpg',NOW(),NOW()),(22,7,'5d1f4c5df3303.jpg',NOW(),NOW()),(23,8,'5d1f5d134606d.jpg',NOW(),NOW()),(24,8,'5d1f5d134ccbe.jpg',NOW(),NOW()),(25,8,'5d1f5d134fc1e.jpg',NOW(),NOW()),(26,8,'5d1f5d13525a2.jpg',NOW(),NOW());
/*!40000 ALTER TABLE `properties_home_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lessons`
--

DROP TABLE IF EXISTS `lessons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lessons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module_id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lessons`
--

LOCK TABLES `lessons` WRITE;
/*!40000 ALTER TABLE `lessons` DISABLE KEYS */;
INSERT INTO `lessons` VALUES (2,'Aula 1',3,'https://www.youtube.com/embed/Gz3Q_c67ZH0',NOW(),NOW()),(3,'Aula 2',3,'http://localhost2',NOW(),NOW()),(4,'Aula 1',4,'link',NOW(),NOW()),(5,'Aula 2',4,'link',NOW(),NOW()),(6,'Aula 3',4,'link',NOW(),NOW());
/*!40000 ALTER TABLE `lessons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_mail_list`
--

DROP TABLE IF EXISTS `admin_mail_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_mail_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_mail_list`
--

LOCK TABLES `admin_mail_list` WRITE;
/*!40000 ALTER TABLE `admin_mail_list` DISABLE KEYS */;
INSERT INTO `admin_mail_list` VALUES (1,'Lista 1'),(2,'Lista 2');
/*!40000 ALTER TABLE `admin_mail_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_members`
--

DROP TABLE IF EXISTS `admin_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_members`
--

LOCK TABLES `admin_members` WRITE;
/*!40000 ALTER TABLE `admin_members` DISABLE KEYS */;
INSERT INTO `admin_members` VALUES (1,'Rodinei Teixeira','teste@teste.com.rod','123456','5d23b69c1e727.jpg'),(2,'Fulano de tal','fulano@teste.com','123456','5d23ec048505d.png'),(3,'Ciclano de tal','ciclano@teste.com','123456','5d23ec1a9b703.png');
/*!40000 ALTER TABLE `admin_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (3,'Introdução e Conceitos',NOW(),NOW()),(4,'Ambiente de desenvolvimento',NOW(),NOW());
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateNews` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`id`,`category_id`, `title`, `description`, `order_id`, `image`, `slug`, `dateNews`) VALUES (8,3,'Aprenda HTML','<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></p>\r\n<p><strong><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></strong></p>\r\n<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></p>\r\n<p><strong><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></strong></p>\r\n<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></p>','8','5d164db52e054.jpg','aprenda-html','2019-06-28'),(9,3,'Aprenda CSS','<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></p>\r\n<p><strong><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></strong></p>\r\n<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></p>\r\n<p><strong><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></strong></p>\r\n<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></p>','9','5d164dd81bf0b.jpg','aprenda-css','2019-06-28'),(10,6,'Aprenda JAVASCRIPT','<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></p>\r\n<p><strong><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></strong></p>\r\n<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></p>\r\n<p><strong><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></strong></p>\r\n<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Aliqua ut dolore sint tongue mollit magna turducken commodo shankle. Eu ut aliquip tongue. Burgdoggen porchetta velit fatback. In dolore turducken pork loin dolor veniam picanha ea. Ut veniam pork chop, chuck aliqua sint duis bresaola. Drumstick excepteur shoulder voluptate frankfurter.</span></p>\r\n<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"https://1h4hfe10xz8m3g3xkh2wb9lc-wpengine.netdna-ssl.com/blog/files/2015/08/thestocks-imagem.jpg\" alt=\"\" width=\"960\" height=\"640\" /></span></p>','0','5d164dfcaf0ed.jpg','aprenda-javascript','2019-06-28');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online`
--

DROP TABLE IF EXISTS `online`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `online` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_action` datetime DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online`
--

LOCK TABLES `online` WRITE;
/*!40000 ALTER TABLE `online` DISABLE KEYS */;
INSERT INTO `online` VALUES (1,'172.20.0.1','2020-11-30 13:33:48','5fc50dfe1d291');
/*!40000 ALTER TABLE `online` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `weight` int(11) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `value` decimal(16,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (4,'Avengers update 2 sdgsdfg','<p>Avengers update jdskaldsf bafslhdafs</p>',5,10,15,20,25,100.00,NOW(),NOW()),(5,'Outro Produto','<p>asdfasdf</p>',10,15,15,15,15,520.00,,NOW(),NOW());
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `properties`
--

DROP TABLE IF EXISTS `properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` decimal(16,2) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `properties`
--

LOCK TABLES `properties` WRITE;
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
INSERT INTO `properties` VALUES (8,'Empreendimento 1','<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Officia ham hock duis, incididunt ullamco andouille proident. Ham strip steak shoulder minim. Sunt doner irure in tri-tip magna. Est hamburger proident chuck pariatur venison reprehenderit. Sunt officia shankle aute drumstick, ad hamburger brisket elit burgdoggen turducken voluptate id anim ipsum. Officia laboris ut, drumstick exercitation in porchetta.</span></p>','A',500000.00,'5d1f4be45b6ae.jpg','empreendimento-1','8',NOW(), NOW()),(9,'Empreendimento 2','<p><span style=\"color: #212529; font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, \'Helvetica Neue\', Arial, sans-serif, \'Apple Color Emoji\', \'Segoe UI Emoji\', \'Segoe UI Symbol\', \'Noto Color Emoji\'; font-size: 16px; text-align: justify;\">Pork belly consectetur dolore, jowl aute deserunt do porchetta boudin aliquip minim strip steak pancetta laboris. In boudin frankfurter leberkas cupim, pork salami sausage shoulder ribeye meatball filet mignon tempor ut picanha. Ex ad chicken, sausage occaecat shoulder buffalo flank in adipisicing beef ribs rump elit in. Shank excepteur beef ribs cupim sunt, shankle bresaola exercitation nostrud chuck ipsum corned beef ball tip proident.</span></p>','A',1000000.00,'5d1f4bfeed58a.jpg','empreendimento-2','9',NOW(), NOW());
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_from` int(11) NOT NULL,
  `id_to` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requests`
--

LOCK TABLES `requests` WRITE;
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
INSERT INTO `requests` VALUES (3,1,3,1,NOW(),NOW()),(4,2,3,1,NOW(),NOW());
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'Websites','Pig corned beef meatball, velit voluptate burgdoggen in cupim aute incididunt magna. Doner burgdoggen ham bacon. Kevin nostrud turducken, jowl ut boudin chicken biltong bresaola id nulla reprehenderit velit pork chop. Ball tip incididunt jowl bresaola drumstick duis. Strip steak ball tip veniam excepteur ut, picanha shoulder kevin alcatra ad leberkas nostrud.',3, NOW(), NOW()),(2,'Aplicativos','Pastrami pig pork loin ex turkey pork belly incididunt in officia. Id officia nisi, sausage in occaecat cow. Jerky kevin laboris sirloin. Cupidatat strip steak tongue nisi laborum id boudin landjaeger et. Kielbasa t-bone adipisicing short loin magna do velit.',1, NOW(), NOW()),(3,'Sistemas','Nisi do meatloaf aliqua reprehenderit labore venison mollit. Aute doner chicken shankle corned beef cupim. Ham hock filet mignon fatback cillum tri-tip. Capicola chicken jowl, sed officia pork belly beef aute venison aliqua duis brisket landjaeger kevin ipsum. Sint shankle non corned beef bacon tongue in ground round excepteur reprehenderit fugiat voluptate boudin chuck. Beef ribs in officia jerky, proident burgdoggen velit enim leberkas short ribs nulla pancetta picanha.',2, NOW(), NOW()),(4,'Marketing','Aute pig venison voluptate kielbasa. Non anim corned beef chuck. Velit ut quis capicola frankfurter cupim minim in tri-tip pork loin. Cupim deserunt beef ribs leberkas, incididunt officia hamburger consequat.',4, NOW(), NOW()),(6,'teste','testesssdfs',6, NOW(), NOW()),(7,'serviço','asdfasdfasdf',7, NOW(), NOW()),(8,'Rodinei Teixeira','ftghdfghdfghd',8, NOW(), NOW());
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slides`
--

DROP TABLE IF EXISTS `slides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slide` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slides`
--

LOCK TABLES `slides` WRITE;
/*!40000 ALTER TABLE `slides` DISABLE KEYS */;
INSERT INTO `slides` VALUES (6,'Slide 1','5d165dacc5e28.jpg',6, NOW(), NOW()),(7,'Slide 2','5d165dba6cfb2.jpg',7, NOW(), NOW()),(8,'Slide 3','5d165dc8e0c9d.jpg',8,NOW(), NOW());
/*!40000 ALTER TABLE `slides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (2,'Aluno 1','aluno1@teste.com','123456',NOW(),NOW());
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_tickets`
--

DROP TABLE IF EXISTS `admin_tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tickets`
--

LOCK TABLES `admin_tickets` WRITE;
/*!40000 ALTER TABLE `admin_tickets` DISABLE KEYS */;
INSERT INTO `admin_tickets` VALUES (1,'rodinei.developer@hotmail.com','olá estou com problemas de acesso','9e759b075ea13092cf8ada9d05658440');
/*!40000 ALTER TABLE `admin_tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_tickets_answers`
--

DROP TABLE IF EXISTS `admin_tickets_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_tickets_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `admin` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tickets_answers`
--

LOCK TABLES `admin_tickets_answers` WRITE;
/*!40000 ALTER TABLE `admin_tickets_answers` DISABLE KEYS */;
INSERT INTO `admin_tickets_answers` VALUES (1,'9e759b075ea13092cf8ada9d05658440','Você precisa fazer o seguinte',1,1),(2,'9e759b075ea13092cf8ada9d05658440','ainda não consegui, como proceder?',-1,1),(3,'9e759b075ea13092cf8ada9d05658440','O que não conseguiu?',1,1),(4,'9e759b075ea13092cf8ada9d05658440','certo',-1,0);
/*!40000 ALTER TABLE `admin_tickets_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `todo`
--

DROP TABLE IF EXISTS `todo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `todo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `todo`
--

LOCK TABLES `todo` WRITE;
/*!40000 ALTER TABLE `todo` DISABLE KEYS */;
INSERT INTO `todo` VALUES (3,'Tarefa 1','2019-07-01',NOW(),NOW()),(4,'Tarefa 2','2019-07-06',NOW(),NOW()),(5,'tarefa 3','2019-07-07',NOW(),NOW()),(6,'tarefa 4','2019-07-08',NOW(),NOW()),(7,'tarefa 5','2019-07-06',NOW(),NOW()),(8,'asdfsd','2019-07-06',NOW(),NOW()),(9,'asfsd','2019-07-09',NOW(),NOW()),(10,'asdfasdf','2019-07-06',NOW(),NOW()),(11,'outro teste','2019-07-06',NOW(),NOW()),(12,'2341234','2019-07-06',NOW(),NOW()),(13,'9796789','2019-07-06',NOW(),NOW());
/*!40000 ALTER TABLE `todo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `image` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (1,'admin','admin','2019-06-24 20:42:27','2019-06-26 14:38:28','icone01.png','Rodinei de Jesus','2'),(2,'teste','teste',NULL,NULL,'icone02.png','Teste','2'),(3,'teste2','teste2','2019-06-26 17:39:10','2019-06-26 17:39:10','','Teste2','1'),(4,'teste3','teste3','2019-06-26 17:44:46','2019-06-26 17:44:46','5d13af0ea8024.png','Teste3','0');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visits`
--

DROP TABLE IF EXISTS `visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `visits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `day` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visits`
--

LOCK TABLES `visits` WRITE;
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
INSERT INTO `visits` VALUES (1,'172.21.0.1','2019-06-24'),(2,'172.21.0.1','2019-06-25'),(3,'172.21.0.1','2019-06-26'),(4,'172.21.0.1','2019-06-26'),(5,'172.21.0.1','2019-06-26'),(6,'187.35.127.91','2019-06-28'),(7,'172.21.0.1','2019-07-03'),(8,'172.20.0.1','2019-07-04'),(9,'172.20.0.1','2019-07-05'),(10,'::1','2019-07-06'),(11,'::1','2019-07-07'),(12,'::1','2019-07-07'),(13,'::1','2019-07-08'),(14,'172.20.0.1','2019-07-08'),(15,'172.20.0.1','2019-07-11'),(16,'172.20.0.1','2019-07-18'),(17,'172.20.0.1','2020-11-30');
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categoria` (
  `IdCategoria` int(11) NOT NULL,
  `Nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estoque`
--

DROP TABLE IF EXISTS `estoque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `estoque` (
  `IdProduto` int(11) NOT NULL,
  `Numero` int(11) NOT NULL,
  `Nome` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Quantidade` int(11) DEFAULT NULL,
  `Categoria` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Fornecedor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estoque`
--

LOCK TABLES `estoque` WRITE;
/*!40000 ALTER TABLE `estoque` DISABLE KEYS */;
/*!40000 ALTER TABLE `estoque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornecedor`
--

DROP TABLE IF EXISTS `fornecedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fornecedor` (
  `IdFornecedor` int(11) NOT NULL,
  `Nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedor`
--

LOCK TABLES `fornecedor` WRITE;
/*!40000 ALTER TABLE `fornecedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornecedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbanswers`
--

DROP TABLE IF EXISTS `tbanswers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbanswers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `examId` int(11) NOT NULL,
  `questionnaireId` int(11) NOT NULL,
  `questionId` int(11) NOT NULL,
  `answerId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbanswers`
--

LOCK TABLES `tbanswers` WRITE;
/*!40000 ALTER TABLE `tbanswers` DISABLE KEYS */;
INSERT INTO `tbanswers` VALUES (8,9,2,2,3,13),(9,9,2,2,4,19),(12,8,1,1,1,2),(13,8,1,1,2,6),(14,8,2,2,3,11),(15,8,2,2,4,19),(31,9,1,1,1,2),(32,9,1,1,2,7),(33,9,3,3,5,23);
/*!40000 ALTER TABLE `tbanswers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcities`
--

DROP TABLE IF EXISTS `tbcities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbcities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uf` text COLLATE utf8_unicode_ci,
  `name` text COLLATE utf8_unicode_ci,
  `tbstate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5515 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcities`
--

LOCK TABLES `tbcities` WRITE;
/*!40000 ALTER TABLE `tbcities` DISABLE KEYS */;
INSERT INTO `tbcities` VALUES (1,'AC','Acrelândia',1),(2,'AC','Assis Brasil',1),(3,'AC','Brasiléia',1),(4,'AC','Bujari',1),(5,'AC','Capixaba',1),(6,'AC','Cruzeiro do Sul',1),(7,'AC','Epitaciolândia',1),(8,'AC','Feijó',1),(9,'AC','Jordão',1),(10,'AC','Mâncio Lima',1),(11,'AC','Manoel Urbano',1),(12,'AC','Marechal Thaumaturgo',1),(13,'AC','Plácido de Castro',1),(14,'AC','Porto Acre',1),(15,'AC','Porto Walter',1),(16,'AC','Rio Branco',1),(17,'AC','Rodrigues Alves',1),(18,'AC','Santa Rosa do Purus',1),(19,'AC','Sena Madureira',1),(20,'AC','Senador Guiomard',1),(21,'AC','Tarauacá',1),(22,'AC','Xapuri',1),(23,'AL','Água Branca',2),(24,'AL','Anadia',2),(25,'AL','Arapiraca',2),(26,'AL','Atalaia',2),(27,'AL','Barra de Santo Antônio',2),(28,'AL','Barra de São Miguel',2),(29,'AL','Batalha',2),(30,'AL','Belém',2),(31,'AL','Belo Monte',2),(32,'AL','Boca da Mata',2),(33,'AL','Branquinha',2),(34,'AL','Cacimbinhas',2),(35,'AL','Cajueiro',2),(36,'AL','Campestre',2),(37,'AL','Campo Alegre',2),(38,'AL','Campo Grande',2),(39,'AL','Canapi',2),(40,'AL','Capela',2),(41,'AL','Carneiros',2),(42,'AL','Chã Preta',2),(43,'AL','Coité do Nóia',2),(44,'AL','Colônia Leopoldina',2),(45,'AL','Coqueiro Seco',2),(46,'AL','Coruripe',2),(47,'AL','Craíbas',2),(48,'AL','Delmiro Gouveia',2),(49,'AL','Dois Riachos',2),(50,'AL','Estrela de Alagoas',2),(51,'AL','Feira Grande',2),(52,'AL','Feliz Deserto',2),(53,'AL','Flexeiras',2),(54,'AL','Girau do Ponciano',2),(55,'AL','Ibateguara',2),(56,'AL','Igaci',2),(57,'AL','Igreja Nova',2),(58,'AL','Inhapi',2),(59,'AL','Jacaré dos Homens',2),(60,'AL','Jacuípe',2),(61,'AL','Japaratinga',2),(62,'AL','Jaramataia',2),(63,'AL','Joaquim Gomes',2),(64,'AL','Jundiá',2),(65,'AL','Junqueiro',2),(66,'AL','Lagoa da Canoa',2),(67,'AL','Limoeiro de Anadia',2),(68,'AL','Maceió',2),(69,'AL','Major Isidoro',2),(70,'AL','Mar Vermelho',2),(71,'AL','Maragogi',2),(72,'AL','Maravilha',2),(73,'AL','Marechal Deodoro',2),(74,'AL','Maribondo',2),(75,'AL','Mata Grande',2),(76,'AL','Matriz de Camaragibe',2),(77,'AL','Messias',2),(78,'AL','Minador do Negrão',2),(79,'AL','Monteirópolis',2),(80,'AL','Murici',2),(81,'AL','Novo Lino',2),(82,'AL','Olho dÁgua das Flores',2),(83,'AL','Olho dÁgua do Casado',2),(84,'AL','Olho dÁgua Grande',2),(85,'AL','Olivença',2),(86,'AL','Ouro Branco',2),(87,'AL','Palestina',2),(88,'AL','Palmeira dos Índios',2),(89,'AL','Pão de Açúcar',2),(90,'AL','Pariconha',2),(91,'AL','Paripueira',2),(92,'AL','Passo de Camaragibe',2),(93,'AL','Paulo Jacinto',2),(94,'AL','Penedo',2),(95,'AL','Piaçabuçu',2),(96,'AL','Pilar',2),(97,'AL','Pindoba',2),(98,'AL','Piranhas',2),(99,'AL','Poço das Trincheiras',2),(100,'AL','Porto Calvo',2),(101,'AL','Porto de Pedras',2),(102,'AL','Porto Real do Colégio',2),(103,'AL','Quebrangulo',2),(104,'AL','Rio Largo',2),(105,'AL','Roteiro',2),(106,'AL','Santa Luzia do Norte',2),(107,'AL','Santana do Ipanema',2),(108,'AL','Santana do Mundaú',2),(109,'AL','São Brás',2),(110,'AL','São José da Laje',2),(111,'AL','São José da Tapera',2),(112,'AL','São Luís do Quitunde',2),(113,'AL','São Miguel dos Campos',2),(114,'AL','São Miguel dos Milagres',2),(115,'AL','São Sebastião',2),(116,'AL','Satuba',2),(117,'AL','Senador Rui Palmeira',2),(118,'AL','Tanque dArca',2),(119,'AL','Taquarana',2),(120,'AL','Teotônio Vilela',2),(121,'AL','Traipu',2),(122,'AL','União dos Palmares',2),(123,'AL','Viçosa',2),(124,'AP','Amapá',3),(125,'AP','Calçoene',3),(126,'AP','Cutias',3),(127,'AP','Ferreira Gomes',3),(128,'AP','Itaubal',3),(129,'AP','Laranjal do Jari',3),(130,'AP','Macapá',3),(131,'AP','Mazagão',3),(132,'AP','Oiapoque',3),(133,'AP','Pedra Branca do Amaparí',3),(134,'AP','Porto Grande',3),(135,'AP','Pracuúba',3),(136,'AP','Santana',3),(137,'AP','Serra do Navio',3),(138,'AP','Tartarugalzinho',3),(139,'AP','Vitória do Jari',3),(140,'AM','Alvarães',4),(141,'AM','Amaturá',4),(142,'AM','namã',4),(143,'AM','Anori',4),(144,'AM','Apuí',4),(145,'AM','Atalaia do Norte',4),(146,'AM','Autazes',4),(147,'AM','Barcelos',4),(148,'AM','Barreirinha',4),(149,'AM','Benjamin Constant',4),(150,'AM','Beruri',4),(151,'AM','Boa Vista do Ramos',4),(152,'AM','Boca do Acre',4),(153,'AM','Borba',4),(154,'AM','Caapiranga',4),(155,'AM','Canutama',4),(156,'AM','Carauari',4),(157,'AM','Careiro',4),(158,'AM','Careiro da Várzea',4),(159,'AM','Coari',4),(160,'AM','Codajás',4),(161,'AM','Eirunepé',4),(162,'AM','Envira',4),(163,'AM','Fonte Boa',4),(164,'AM','Guajará',4),(165,'AM','Humaitá',4),(166,'AM','Ipixuna',4),(167,'AM','Iranduba',4),(168,'AM','Itacoatiara',4),(169,'AM','Itamarati',4),(170,'AM','Itapiranga',4),(171,'AM','Japurá',4),(172,'AM','Juruá',4),(173,'AM','Jutaí',4),(174,'AM','Lábrea',4),(175,'AM','Manacapuru',4),(176,'AM','Manaquiri',4),(177,'AM','Manaus',4),(178,'AM','Manicoré',4),(179,'AM','Maraã',4),(180,'AM','Maués',4),(181,'AM','Nhamundá',4),(182,'AM','Nova Olinda do Norte',4),(183,'AM','Novo Airão',4),(184,'AM','Novo Aripuanã',4),(185,'AM','Parintins',4),(186,'AM','Pauini',4),(187,'AM','Presidente Figueiredo',4),(188,'AM','Rio Preto da Eva',4),(189,'AM','Santa Isabel do Rio Negro',4),(190,'AM','Santo Antônio do Içá',4),(191,'AM','São Gabriel da Cachoeira',4),(192,'AM','São Paulo de Olivença',4),(193,'AM','São Sebastião do Uatumã',4),(194,'AM','Silves',4),(195,'AM','Tabatinga',4),(196,'AM','Tapauá',4),(197,'AM','Tefé',4),(198,'AM','Tonantins',4),(199,'AM','Uarini',4),(200,'AM','Urucará',4),(201,'AM','Urucurituba',4),(202,'BA','Abaíra',5),(203,'BA','Abaré',5),(204,'BA','Acajutiba',5),(205,'BA','Adustina',5),(206,'BA','Água Fria',5),(207,'BA','Aiquara',5),(208,'BA','Alagoinhas',5),(209,'BA','Alcobaça',5),(210,'BA','Almadina',5),(211,'BA','Amargosa',5),(212,'BA','Amélia Rodrigues',5),(213,'BA','América Dourada',5),(214,'BA','Anagé',5),(215,'BA','Andaraí',5),(216,'BA','Andorinha',5),(217,'BA','Angical',5),(218,'BA','Anguera',5),(219,'BA','Antas',5),(220,'BA','Antônio Cardoso',5),(221,'BA','Antônio Gonçalves',5),(222,'BA','Aporá',5),(223,'BA','Apuarema',5),(224,'BA','Araças',5),(225,'BA','Aracatu',5),(226,'BA','Araci',5),(227,'BA','Aramari',5),(228,'BA','Arataca',5),(229,'BA','Aratuípe',5),(230,'BA','Aurelino Leal',5),(231,'BA','Baianópolis',5),(232,'BA','Baixa Grande',5),(233,'BA','Banzaê',5),(234,'BA','Barra',5),(235,'BA','Barra da Estiva',5),(236,'BA','Barra do Choça',5),(237,'BA','Barra do Mendes',5),(238,'BA','Barra do Rocha',5),(239,'BA','Barreiras',5),(240,'BA','Barro Alto',5),(241,'BA','Belmonte',5),(242,'BA','Belo Campo',5),(243,'BA','Biritinga',5),(244,'BA','Boa Nova',5),(245,'BA','Boa Vista do Tupim',5),(246,'BA','Bom Jesus da Lapa',5),(247,'BA','Bom Jesus da Serra',5),(248,'BA','Boninal',5),(249,'BA','Bonito',5),(250,'BA','Boquira',5),(251,'BA','Botuporã',5),(252,'BA','Brejões',5),(253,'BA','Brejolândia',5),(254,'BA','Brotas de Macaúbas',5),(255,'BA','Brumado',5),(256,'BA','Buerarema',5),(257,'BA','Buritirama',5),(258,'BA','Caatiba',5),(259,'BA','Cabaceiras do Paraguaçu',5),(260,'BA','Cachoeira',5),(261,'BA','Caculé',5),(262,'BA','Caém',5),(263,'BA','Caetanos',5),(264,'BA','Caetité',5),(265,'BA','Cafarnaum',5),(266,'BA','Cairu',5),(267,'BA','Caldeirão Grande',5),(268,'BA','Camacan',5),(269,'BA','Camaçari',5),(270,'BA','Camamu',5),(271,'BA','Campo Alegre de Lourdes',5),(272,'BA','Campo Formoso',5),(273,'BA','Canápolis',5),(274,'BA','Canarana',5),(275,'BA','Canavieiras',5),(276,'BA','Candeal',5),(277,'BA','Candeias',5),(278,'BA','Candiba',5),(279,'BA','Cândido Sales',5),(280,'BA','Cansanção',5),(281,'BA','Canudos',5),(282,'BA','Capela do Alto Alegre',5),(283,'BA','Capim Grosso',5),(284,'BA','Caraíbas',5),(285,'BA','Caravelas',5),(286,'BA','Cardeal da Silva',5),(287,'BA','Carinhanha',5),(288,'BA','Casa Nova',5),(289,'BA','Castro Alves',5),(290,'BA','Catolândia',5),(291,'BA','Catu',5),(292,'BA','Caturama',5),(293,'BA','Central',5),(294,'BA','Chorrochó',5),(295,'BA','Cícero Dantas',5),(296,'BA','Cipó',5),(297,'BA','Coaraci',5),(298,'BA','Cocos',5),(299,'BA','Conceição da Feira',5),(300,'BA','Conceição do Almeida',5),(301,'BA','Conceição do Coité',5),(302,'BA','Conceição do Jacuípe',5),(303,'BA','Conde',5),(304,'BA','Condeúba',5),(305,'BA','Contendas do Sincorá',5),(306,'BA','Coração de Maria',5),(307,'BA','Cordeiros',5),(308,'BA','Coribe',5),(309,'BA','Coronel João Sá',5),(310,'BA','Correntina',5),(311,'BA','Cotegipe',5),(312,'BA','Cravolândia',5),(313,'BA','Crisópolis',5),(314,'BA','Cristópolis',5),(315,'BA','Cruz das Almas',5),(316,'BA','Curaçá',5),(317,'BA','Dário Meira',5),(318,'BA','Dias dÁvila',5),(319,'BA','Dom Basílio',5),(320,'BA','Dom Macedo Costa',5),(321,'BA','Elísio Medrado',5),(322,'BA','Encruzilhada',5),(323,'BA','Entre Rios',5),(324,'BA','Érico Cardoso',5),(325,'BA','Esplanada',5),(326,'BA','Euclides da Cunha',5),(327,'BA','Eunápolis',5),(328,'BA','Fátima',5),(329,'BA','Feira da Mata',5),(330,'BA','Feira de Santana',5),(331,'BA','Filadélfia',5),(332,'BA','Firmino Alves',5),(333,'BA','Floresta Azul',5),(334,'BA','Formosa do Rio Preto',5),(335,'BA','Gandu',5),(336,'BA','Gavião',5),(337,'BA','Gentio do Ouro',5),(338,'BA','Glória',5),(339,'BA','Gongogi',5),(340,'BA','Governador Lomanto Júnior',5),(341,'BA','Governador Mangabeira',5),(342,'BA','Guajeru',5),(343,'BA','Guanambi',5),(344,'BA','Guaratinga',5),(345,'BA','Heliópolis',5),(346,'BA','Iaçu',5),(347,'BA','Ibiassucê',5),(348,'BA','Ibicaraí',5),(349,'BA','Ibicoara',5),(350,'BA','Ibicuí',5),(351,'BA','Ibipeba',5),(352,'BA','Ibipitanga',5),(353,'BA','Ibiquera',5),(354,'BA','Ibirapitanga',5),(355,'BA','Ibirapuã',5),(356,'BA','Ibirataia',5),(357,'BA','Ibitiara',5),(358,'BA','Ibititá',5),(359,'BA','Ibotirama',5),(360,'BA','Ichu',5),(361,'BA','Igaporã',5),(362,'BA','Igrapiúna',5),(363,'BA','Iguaí',5),(364,'BA','Ilhéus',5),(365,'BA','Inhambupe',5),(366,'BA','Ipecaetá',5),(367,'BA','Ipiaú',5),(368,'BA','Ipirá',5),(369,'BA','Ipupiara',5),(370,'BA','Irajuba',5),(371,'BA','Iramaia',5),(372,'BA','Iraquara',5),(373,'BA','Irará',5),(374,'BA','Irecê',5),(375,'BA','Itabela',5),(376,'BA','Itaberaba',5),(377,'BA','Itabuna',5),(378,'BA','Itacaré',5),(379,'BA','Itaeté',5),(380,'BA','Itagi',5),(381,'BA','Itagibá',5),(382,'BA','Itagimirim',5),(383,'BA','Itaguaçu da Bahia',5),(384,'BA','Itaju do Colônia',5),(385,'BA','Itajuípe',5),(386,'BA','Itamaraju',5),(387,'BA','Itamari',5),(388,'BA','Itambé',5),(389,'BA','Itanagra',5),(390,'BA','Itanhém',5),(391,'BA','Itaparica',5),(392,'BA','Itapé',5),(393,'BA','Itapebi',5),(394,'BA','Itapetinga',5),(395,'BA','Itapicuru',5),(396,'BA','Itapitanga',5),(397,'BA','Itaquara',5),(398,'BA','Itarantim',5),(399,'BA','Itatim',5),(400,'BA','Itiruçu',5),(401,'BA','Itiúba',5),(402,'BA','Itororó',5),(403,'BA','Ituaçu',5),(404,'BA','Ituberá',5),(405,'BA','Iuiú',5),(406,'BA','Jaborandi',5),(407,'BA','Jacaraci',5),(408,'BA','Jacobina',5),(409,'BA','Jaguaquara',5),(410,'BA','Jaguarari',5),(411,'BA','Jaguaripe',5),(412,'BA','Jandaíra',5),(413,'BA','Jequié',5),(414,'BA','Jeremoabo',5),(415,'BA','Jiquiriçá',5),(416,'BA','Jitaúna',5),(417,'BA','João Dourado',5),(418,'BA','Juazeiro',5),(419,'BA','Jucuruçu',5),(420,'BA','Jussara',5),(421,'BA','Jussari',5),(422,'BA','Jussiape',5),(423,'BA','Lafaiete Coutinho',5),(424,'BA','Lagoa Real',5),(425,'BA','Laje',5),(426,'BA','Lajedão',5),(427,'BA','Lajedinho',5),(428,'BA','Lajedo do Tabocal',5),(429,'BA','Lamarão',5),(430,'BA','Lapão',5),(431,'BA','Lauro de Freitas',5),(432,'BA','Lençóis',5),(433,'BA','Licínio de Almeida',5),(434,'BA','Livramento de Nossa Senhora',5),(435,'BA','Macajuba',5),(436,'BA','Macarani',5),(437,'BA','Macaúbas',5),(438,'BA','Macururé',5),(439,'BA','Madre de Deus',5),(440,'BA','Maetinga',5),(441,'BA','Maiquinique',5),(442,'BA','Mairi',5),(443,'BA','Malhada',5),(444,'BA','Malhada de Pedras',5),(445,'BA','Manoel Vitorino',5),(446,'BA','Mansidão',5),(447,'BA','Maracás',5),(448,'BA','Maragogipe',5),(449,'BA','Maraú',5),(450,'BA','Marcionílio Souza',5),(451,'BA','Mascote',5),(452,'BA','Mata de São João',5),(453,'BA','Matina',5),(454,'BA','Medeiros Neto',5),(455,'BA','Miguel Calmon',5),(456,'BA','Milagres',5),(457,'BA','Mirangaba',5),(458,'BA','Mirante',5),(459,'BA','Monte Santo',5),(460,'BA','Morpará',5),(461,'BA','Morro do Chapéu',5),(462,'BA','Mortugaba',5),(463,'BA','Mucugê',5),(464,'BA','Mucuri',5),(465,'BA','Mulungu do Morro',5),(466,'BA','Mundo Novo',5),(467,'BA','Muniz Ferreira',5),(468,'BA','Muquém de São Francisco',5),(469,'BA','Muritiba',5),(470,'BA','Mutuípe',5),(471,'BA','Nazaré',5),(472,'BA','Nilo Peçanha',5),(473,'BA','Nordestina',5),(474,'BA','Nova Canaã',5),(475,'BA','Nova Fátima',5),(476,'BA','Nova Ibiá',5),(477,'BA','Nova Itarana',5),(478,'BA','Nova Redenção',5),(479,'BA','Nova Soure',5),(480,'BA','Nova Viçosa',5),(481,'BA','Novo Horizonte',5),(482,'BA','Novo Triunfo',5),(483,'BA','Olindina',5),(484,'BA','Oliveira dos Brejinhos',5),(485,'BA','Ouriçangas',5),(486,'BA','Ourolândia',5),(487,'BA','Palmas de Monte Alto',5),(488,'BA','Palmeiras',5),(489,'BA','Paramirim',5),(490,'BA','Paratinga',5),(491,'BA','Paripiranga',5),(492,'BA','Pau Brasil',5),(493,'BA','Paulo Afonso',5),(494,'BA','Pé de Serra',5),(495,'BA','Pedrão',5),(496,'BA','Pedro Alexandre',5),(497,'BA','Piatã',5),(498,'BA','Pilão Arcado',5),(499,'BA','Pindaí',5),(500,'BA','Pindobaçu',5),(501,'BA','Pintadas',5),(502,'BA','Piraí do Norte',5),(503,'BA','Piripá',5),(504,'BA','Piritiba',5),(505,'BA','Planaltino',5),(506,'BA','Planalto',5),(507,'BA','Poções',5),(508,'BA','Pojuca',5),(509,'BA','Ponto Novo',5),(510,'BA','Porto Seguro',5),(511,'BA','Potiraguá',5),(512,'BA','Prado',5),(513,'BA','Presidente Dutra',5),(514,'BA','Presidente Jânio Quadros',5),(515,'BA','Presidente Tancredo Neves',5),(516,'BA','Queimadas',5),(517,'BA','Quijingue',5),(518,'BA','Quixabeira',5),(519,'BA','Rafael Jambeiro',5),(520,'BA','Remanso',5),(521,'BA','Retirolândia',5),(522,'BA','Riachão das Neves',5),(523,'BA','Riachão do Jacuípe',5),(524,'BA','Riacho de Santana',5),(525,'BA','Ribeira do Amparo',5),(526,'BA','Ribeira do Pombal',5),(527,'BA','Ribeirão do Largo',5),(528,'BA','Rio de Contas',5),(529,'BA','Rio do Antônio',5),(530,'BA','Rio do Pires',5),(531,'BA','Rio Real',5),(532,'BA','Rodelas',5),(533,'BA','Ruy Barbosa',5),(534,'BA','Salinas da Margarida',5),(535,'BA','Salvador',5),(536,'BA','Santa Bárbara',5),(537,'BA','Santa Brígida',5),(538,'BA','Santa Cruz Cabrália',5),(539,'BA','Santa Cruz da Vitória',5),(540,'BA','Santa Inês',5),(541,'BA','Santa Luzia',5),(542,'BA','Santa Maria da Vitória',5),(543,'BA','Santa Rita de Cássia',5),(544,'BA','Santa Teresinha',5),(545,'BA','Santaluz',5),(546,'BA','Santana',5),(547,'BA','Santanópolis',5),(548,'BA','Santo Amaro',5),(549,'BA','Santo Antônio de Jesus',5),(550,'BA','Santo Estêvão',5),(551,'BA','São Desidério',5),(552,'BA','São Domingos',5),(553,'BA','São Felipe',5),(554,'BA','São Félix',5),(555,'BA','São Félix do Coribe',5),(556,'BA','São Francisco do Conde',5),(557,'BA','São Gabriel',5),(558,'BA','São Gonçalo dos Campos',5),(559,'BA','São José da Vitória',5),(560,'BA','São José do Jacuípe',5),(561,'BA','São Miguel das Matas',5),(562,'BA','São Sebastião do Passé',5),(563,'BA','Sapeaçu',5),(564,'BA','Sátiro Dias',5),(565,'BA','Saubara',5),(566,'BA','Saúde',5),(567,'BA','Seabra',5),(568,'BA','Sebastião Laranjeiras',5),(569,'BA','Senhor do Bonfim',5),(570,'BA','Sento Sé',5),(571,'BA','Serra do Ramalho',5),(572,'BA','Serra Dourada',5),(573,'BA','Serra Preta',5),(574,'BA','Serrinha',5),(575,'BA','Serrolândia',5),(576,'BA','Simões Filho',5),(577,'BA','Sítio do Mato',5),(578,'BA','Sítio do Quinto',5),(579,'BA','Sobradinho',5),(580,'BA','Souto Soares',5),(581,'BA','Tabocas do Brejo Velho',5),(582,'BA','Tanhaçu',5),(583,'BA','Tanque Novo',5),(584,'BA','Tanquinho',5),(585,'BA','Taperoá',5),(586,'BA','Tapiramutá',5),(587,'BA','Teixeira de Freitas',5),(588,'BA','Teodoro Sampaio',5),(589,'BA','Teofilândia',5),(590,'BA','Teolândia',5),(591,'BA','Terra Nova',5),(592,'BA','Tremedal',5),(593,'BA','Tucano',5),(594,'BA','Uauá',5),(595,'BA','Ubaíra',5),(596,'BA','Ubaitaba',5),(597,'BA','Ubatã',5),(598,'BA','Uibaí',5),(599,'BA','Umburanas',5),(600,'BA','Una',5),(601,'BA','Urandi',5),(602,'BA','Uruçuca',5),(603,'BA','Utinga',5),(604,'BA','Valença',5),(605,'BA','Valente',5),(606,'BA','Várzea da Roça',5),(607,'BA','Várzea do Poço',5),(608,'BA','Várzea Nova',5),(609,'BA','Varzedo',5),(610,'BA','Vera Cruz',5),(611,'BA','Vereda',5),(612,'BA','Vitória da Conquista',5),(613,'BA','Wagner',5),(614,'BA','Wanderley',5),(615,'BA','Wenceslau Guimarães',5),(616,'BA','Xique-Xique',5),(617,'CE','Abaiara',6),(618,'CE','Acarapé',6),(619,'CE','Acaraú',6),(620,'CE','Acopiara',6),(621,'CE','Aiuaba',6),(622,'CE','Alcântaras',6),(623,'CE','Altaneira',6),(624,'CE','Alto Santo',6),(625,'CE','Amontada',6),(626,'CE','Antonina do Norte',6),(627,'CE','Apuiarés',6),(628,'CE','Aquiraz',6),(629,'CE','Aracati',6),(630,'CE','Aracoiaba',6),(631,'CE','Ararendá',6),(632,'CE','Araripe',6),(633,'CE','Aratuba',6),(634,'CE','Arneiroz',6),(635,'CE','Assaré',6),(636,'CE','Aurora',6),(637,'CE','Baixio',6),(638,'CE','Banabuiú',6),(639,'CE','Barbalha',6),(640,'CE','Barreira',6),(641,'CE','Barro',6),(642,'CE','Barroquinha',6),(643,'CE','Baturité',6),(644,'CE','Beberibe',6),(645,'CE','Bela Cruz',6),(646,'CE','Boa Viagem',6),(647,'CE','Brejo Santo',6),(648,'CE','Camocim',6),(649,'CE','Campos Sales',6),(650,'CE','Canindé',6),(651,'CE','Capistrano',6),(652,'CE','Caridade',6),(653,'CE','Cariré',6),(654,'CE','Caririaçu',6),(655,'CE','Cariús',6),(656,'CE','Carnaubal',6),(657,'CE','Cascavel',6),(658,'CE','Catarina',6),(659,'CE','Catunda',6),(660,'CE','Caucaia',6),(661,'CE','Cedro',6),(662,'CE','Chaval',6),(663,'CE','Choró',6),(664,'CE','Chorozinho',6),(665,'CE','Coreaú',6),(666,'CE','Crateús',6),(667,'CE','Crato',6),(668,'CE','Croatá',6),(669,'CE','Cruz',6),(670,'CE','Deputado Irapuan Pinheiro',6),(671,'CE','Ererê',6),(672,'CE','Eusébio',6),(673,'CE','Farias Brito',6),(674,'CE','Forquilha',6),(675,'CE','Fortaleza',6),(676,'CE','Fortim',6),(677,'CE','Frecheirinha',6),(678,'CE','General Sampaio',6),(679,'CE','Graça',6),(680,'CE','Granja',6),(681,'CE','Granjeiro',6),(682,'CE','Groaíras',6),(683,'CE','Guaiúba',6),(684,'CE','Guaraciaba do Norte',6),(685,'CE','Guaramiranga',6),(686,'CE','Hidrolândia',6),(687,'CE','Horizonte',6),(688,'CE','Ibaretama',6),(689,'CE','Ibiapina',6),(690,'CE','Ibicuitinga',6),(691,'CE','Icapuí',6),(692,'CE','Icó',6),(693,'CE','Iguatu',6),(694,'CE','Independência',6),(695,'CE','Ipaporanga',6),(696,'CE','Ipaumirim',6),(697,'CE','Ipu',6),(698,'CE','Ipueiras',6),(699,'CE','Iracema',6),(700,'CE','Irauçuba',6),(701,'CE','Itaiçaba',6),(702,'CE','Itaitinga',6),(703,'CE','Itapagé',6),(704,'CE','Itapipoca',6),(705,'CE','Itapiúna',6),(706,'CE','Itarema',6),(707,'CE','Itatira',6),(708,'CE','Jaguaretama',6),(709,'CE','Jaguaribara',6),(710,'CE','Jaguaribe',6),(711,'CE','Jaguaruana',6),(712,'CE','Jardim',6),(713,'CE','Jati',6),(714,'CE','Jijoca de Jericoacoara',6),(715,'CE','Juazeiro do Norte',6),(716,'CE','Jucás',6),(717,'CE','Lavras da Mangabeira',6),(718,'CE','Limoeiro do Norte',6),(719,'CE','Madalena',6),(720,'CE','Maracanaú',6),(721,'CE','Maranguape',6),(722,'CE','Marco',6),(723,'CE','Martinópole',6),(724,'CE','Massapê',6),(725,'CE','Mauriti',6),(726,'CE','Meruoca',6),(727,'CE','Milagres',6),(728,'CE','Milhã',6),(729,'CE','Miraíma',6),(730,'CE','Missão Velha',6),(731,'CE','Mombaça',6),(732,'CE','Monsenhor Tabosa',6),(733,'CE','Morada Nova',6),(734,'CE','Moraújo',6),(735,'CE','Morrinhos',6),(736,'CE','Mucambo',6),(737,'CE','Mulungu',6),(738,'CE','Nova Olinda',6),(739,'CE','Nova Russas',6),(740,'CE','Novo Oriente',6),(741,'CE','Ocara',6),(742,'CE','Orós',6),(743,'CE','Pacajus',6),(744,'CE','Pacatuba',6),(745,'CE','Pacoti',6),(746,'CE','Pacujá',6),(747,'CE','Palhano',6),(748,'CE','Palmácia',6),(749,'CE','Paracuru',6),(750,'CE','Paraipaba',6),(751,'CE','Parambu',6),(752,'CE','Paramoti',6),(753,'CE','Pedra Branca',6),(754,'CE','Penaforte',6),(755,'CE','Pentecoste',6),(756,'CE','Pereiro',6),(757,'CE','Pindoretama',6),(758,'CE','Piquet Carneiro',6),(759,'CE','Pires Ferreira',6),(760,'CE','Poranga',6),(761,'CE','Porteiras',6),(762,'CE','Potengi',6),(763,'CE','Potiretama',6),(764,'CE','Quiterianópolis',6),(765,'CE','Quixadá',6),(766,'CE','Quixelô',6),(767,'CE','Quixeramobim',6),(768,'CE','Quixeré',6),(769,'CE','Redenção',6),(770,'CE','Reriutaba',6),(771,'CE','Russas',6),(772,'CE','Saboeiro',6),(773,'CE','Salitre',6),(774,'CE','Santa Quitéria',6),(775,'CE','Santana do Acaraú',6),(776,'CE','Santana do Cariri',6),(777,'CE','São Benedito',6),(778,'CE','São Gonçalo do Amarante',6),(779,'CE','São João do Jaguaribe',6),(780,'CE','São Luís do Curu',6),(781,'CE','Senador Pompeu',6),(782,'CE','Senador Sá',6),(783,'CE','Sobral',6),(784,'CE','Solonópole',6),(785,'CE','Tabuleiro do Norte',6),(786,'CE','Tamboril',6),(787,'CE','Tarrafas',6),(788,'CE','Tauá',6),(789,'CE','Tejuçuoca',6),(790,'CE','Tianguá',6),(791,'CE','Trairi',6),(792,'CE','Tururu',6),(793,'CE','Ubajara',6),(794,'CE','Umari',6),(795,'CE','Umirim',6),(796,'CE','Uruburetama',6),(797,'CE','Uruoca',6),(798,'CE','Varjota',6),(799,'CE','Várzea Alegre',6),(800,'CE','Viçosa do Ceará',6),(801,'DF','Brasília',7),(802,'ES','Afonso Cláudio',8),(803,'ES','Água Doce do Norte',8),(804,'ES','Águia Branca',8),(805,'ES','Alegre',8),(806,'ES','Alfredo Chaves',8),(807,'ES','Alto Rio Novo',8),(808,'ES','Anchieta',8),(809,'ES','Apiacá',8),(810,'ES','Aracruz',8),(811,'ES','Atilio Vivacqua',8),(812,'ES','Baixo Guandu',8),(813,'ES','Barra de São Francisco',8),(814,'ES','Boa Esperança',8),(815,'ES','Bom Jesus do Norte',8),(816,'ES','Brejetuba',8),(817,'ES','Cachoeiro de Itapemirim',8),(818,'ES','Cariacica',8),(819,'ES','Castelo',8),(820,'ES','Colatina',8),(821,'ES','Conceição da Barra',8),(822,'ES','Conceição do Castelo',8),(823,'ES','Divino de São Lourenço',8),(824,'ES','Domingos Martins',8),(825,'ES','Dores do Rio Preto',8),(826,'ES','Ecoporanga',8),(827,'ES','Fundão',8),(828,'ES','Guaçuí',8),(829,'ES','Guarapari',8),(830,'ES','Ibatiba',8),(831,'ES','Ibiraçu',8),(832,'ES','Ibitirama',8),(833,'ES','Iconha',8),(834,'ES','Irupi',8),(835,'ES','Itaguaçu',8),(836,'ES','Itapemirim',8),(837,'ES','Itarana',8),(838,'ES','Iúna',8),(839,'ES','Jaguaré',8),(840,'ES','Jerônimo Monteiro',8),(841,'ES','João Neiva',8),(842,'ES','Laranja da Terra',8),(843,'ES','Linhares',8),(844,'ES','Mantenópolis',8),(845,'ES','Marataízes',8),(846,'ES','Marechal Floriano',8),(847,'ES','Marilândia',8),(848,'ES','Mimoso do Sul',8),(849,'ES','Montanha',8),(850,'ES','Mucurici',8),(851,'ES','Muniz Freire',8),(852,'ES','Muqui',8),(853,'ES','Nova Venécia',8),(854,'ES','Pancas',8),(855,'ES','Pedro Canário',8),(856,'ES','Pinheiros',8),(857,'ES','Piúma',8),(858,'ES','Ponto Belo',8),(859,'ES','Presidente Kennedy',8),(860,'ES','Rio Bananal',8),(861,'ES','Rio Novo do Sul',8),(862,'ES','Santa Leopoldina',8),(863,'ES','Santa Maria de Jetibá',8),(864,'ES','Santa Teresa',8),(865,'ES','São Domingos do Norte',8),(866,'ES','São Gabriel da Palha',8),(867,'ES','São José do Calçado',8),(868,'ES','São Mateus',8),(869,'ES','São Roque do Canaã',8),(870,'ES','Serra',8),(871,'ES','Sooretama',8),(872,'ES','Vargem Alta',8),(873,'ES','Venda Nova do Imigrante',8),(874,'ES','Viana',8),(875,'ES','Vila Pavão',8),(876,'ES','Vila Valério',8),(877,'ES','Vila Velha',8),(878,'ES','Vitória',8),(879,'GO','Abadia de Goiás',9),(880,'GO','Abadiânia',9),(881,'GO','Acreúna',9),(882,'GO','Adelândia',9),(883,'GO','Água Fria de Goiás',9),(884,'GO','Água Limpa',9),(885,'GO','Águas Lindas de Goiás',9),(886,'GO','Alexânia',9),(887,'GO','Aloândia',9),(888,'GO','Alto Horizonte',9),(889,'GO','Alto Paraíso de Goiás',9),(890,'GO','Alvorada do Norte',9),(891,'GO','Amaralina',9),(892,'GO','Americano do Brasil',9),(893,'GO','Amorinópolis',9),(894,'GO','Anápolis',9),(895,'GO','Anhanguera',9),(896,'GO','Anicuns',9),(897,'GO','Aparecida de Goiânia',9),(898,'GO','Aparecida do Rio Doce',9),(899,'GO','Aporé',9),(900,'GO','Araçu',9),(901,'GO','Aragarças',9),(902,'GO','Aragoiânia',9),(903,'GO','Araguapaz',9),(904,'GO','Arenópolis',9),(905,'GO','Aruanã',9),(906,'GO','Aurilândia',9),(907,'GO','Avelinópolis',9),(908,'GO','Baliza',9),(909,'GO','Barro Alto',9),(910,'GO','Bela Vista de Goiás',9),(911,'GO','Bom Jardim de Goiás',9),(912,'GO','Bom Jesus de Goiás',9),(913,'GO','Bonfinópolis',9),(914,'GO','Bonópolis',9),(915,'GO','Brazabrantes',9),(916,'GO','Britânia',9),(917,'GO','Buriti Alegre',9),(918,'GO','Buriti de Goiás',9),(919,'GO','Buritinópolis',9),(920,'GO','Cabeceiras',9),(921,'GO','Cachoeira Alta',9),(922,'GO','Cachoeira de Goiás',9),(923,'GO','Cachoeira Dourada',9),(924,'GO','Caçu',9),(925,'GO','Caiapônia',9),(926,'GO','Caldas Novas',9),(927,'GO','Caldazinha',9),(928,'GO','Campestre de Goiás',9),(929,'GO','Campinaçu',9),(930,'GO','Campinorte',9),(931,'GO','Campo Alegre de Goiás',9),(932,'GO','Campos Belos',9),(933,'GO','Campos Verdes',9),(934,'GO','Carmo do Rio Verde',9),(935,'GO','Castelândia',9),(936,'GO','Catalão',9),(937,'GO','Caturaí',9),(938,'GO','Cavalcante',9),(939,'GO','Ceres',9),(940,'GO','Cezarina',9),(941,'GO','Chapadão do Céu',9),(942,'GO','Cidade Ocidental',9),(943,'GO','Cocalzinho de Goiás',9),(944,'GO','Colinas do Sul',9),(945,'GO','Córrego do Ouro',9),(946,'GO','Corumbá de Goiás',9),(947,'GO','Corumbaíba',9),(948,'GO','Cristalina',9),(949,'GO','Cristianópolis',9),(950,'GO','Crixás',9),(951,'GO','Cromínia',9),(952,'GO','Cumari',9),(953,'GO','Damianópolis',9),(954,'GO','Damolândia',9),(955,'GO','Davinópolis',9),(956,'GO','Diorama',9),(957,'GO','Divinópolis de Goiás',9),(958,'GO','Doverlândia',9),(959,'GO','Edealina',9),(960,'GO','Edéia',9),(961,'GO','Estrela do Norte',9),(962,'GO','Faina',9),(963,'GO','Fazenda Nova',9),(964,'GO','Firminópolis',9),(965,'GO','Flores de Goiás',9),(966,'GO','Formosa',9),(967,'GO','Formoso',9),(968,'GO','Goianápolis',9),(969,'GO','Goiandira',9),(970,'GO','Goianésia',9),(971,'GO','Goiânia',9),(972,'GO','Goianira',9),(973,'GO','Goiás',9),(974,'GO','Goiatuba',9),(975,'GO','Gouvelândia',9),(976,'GO','Guapó',9),(977,'GO','Guaraíta',9),(978,'GO','Guarani de Goiás',9),(979,'GO','Guarinos',9),(980,'GO','Heitoraí',9),(981,'GO','Hidrolândia',9),(982,'GO','Hidrolina',9),(983,'GO','Iaciara',9),(984,'GO','Inaciolândia',9),(985,'GO','Indiara',9),(986,'GO','Inhumas',9),(987,'GO','Ipameri',9),(988,'GO','Iporá',9),(989,'GO','Israelândia',9),(990,'GO','Itaberaí',9),(991,'GO','Itaguari',9),(992,'GO','Itaguaru',9),(993,'GO','Itajá',9),(994,'GO','Itapaci',9),(995,'GO','Itapirapuã',9),(996,'GO','Itapuranga',9),(997,'GO','Itarumã',9),(998,'GO','Itauçu',9),(999,'GO','Itumbiara',9),(1000,'GO','Ivolândia',9),(1001,'GO','Jandaia',9),(1002,'GO','Jaraguá',9),(1003,'GO','Jataí',9),(1004,'GO','Jaupaci',9),(1005,'GO','Jesúpolis',9),(1006,'GO','Joviânia',9),(1007,'GO','Jussara',9),(1008,'GO','Leopoldo de Bulhões',9),(1009,'GO','Luziânia',9),(1010,'GO','Mairipotaba',9),(1011,'GO','Mambaí',9),(1012,'GO','Mara Rosa',9),(1013,'GO','Marzagão',9),(1014,'GO','Matrinchã',9),(1015,'GO','Maurilândia',9),(1016,'GO','Mimoso de Goiás',9),(1017,'GO','Minaçu',9),(1018,'GO','Mineiros',9),(1019,'GO','Moiporá',9),(1020,'GO','Monte Alegre de Goiás',9),(1021,'GO','Montes Claros de Goiás',9),(1022,'GO','Montividiu',9),(1023,'GO','Montividiu do Norte',9),(1024,'GO','Morrinhos',9),(1025,'GO','Morro Agudo de Goiás',9),(1026,'GO','Mossâmedes',9),(1027,'GO','Mozarlândia',9),(1028,'GO','Mundo Novo',9),(1029,'GO','Mutunópolis',9),(1030,'GO','Nazário',9),(1031,'GO','Nerópolis',9),(1032,'GO','Niquelândia',9),(1033,'GO','Nova América',9),(1034,'GO','Nova Aurora',9),(1035,'GO','Nova Crixás',9),(1036,'GO','Nova Glória',9),(1037,'GO','Nova Iguaçu de Goiás',9),(1038,'GO','Nova Roma',9),(1039,'GO','Nova Veneza',9),(1040,'GO','Novo Brasil',9),(1041,'GO','Novo Gama',9),(1042,'GO','Novo Planalto',9),(1043,'GO','Orizona',9),(1044,'GO','Ouro Verde de Goiás',9),(1045,'GO','Ouvidor',9),(1046,'GO','Padre Bernardo',9),(1047,'GO','Palestina de Goiás',9),(1048,'GO','Palmeiras de Goiás',9),(1049,'GO','Palmelo',9),(1050,'GO','Palminópolis',9),(1051,'GO','Panamá',9),(1052,'GO','Paranaiguara',9),(1053,'GO','Paraúna',9),(1054,'GO','Perolândia',9),(1055,'GO','Petrolina de Goiás',9),(1056,'GO','Pilar de Goiás',9),(1057,'GO','Piracanjuba',9),(1058,'GO','Piranhas',9),(1059,'GO','Pirenópolis',9),(1060,'GO','Pires do Rio',9),(1061,'GO','Planaltina',9),(1062,'GO','Pontalina',9),(1063,'GO','Porangatu',9),(1064,'GO','Porteirão',9),(1065,'GO','Portelândia',9),(1066,'GO','Posse',9),(1067,'GO','Professor Jamil',9),(1068,'GO','Quirinópolis',9),(1069,'GO','Rialma',9),(1070,'GO','Rianápolis',9),(1071,'GO','Rio Quente',9),(1072,'GO','Rio Verde',9),(1073,'GO','Rubiataba',9),(1074,'GO','Sanclerlândia',9),(1075,'GO','Santa Bárbara de Goiás',9),(1076,'GO','Santa Cruz de Goiás',9),(1077,'GO','Santa Fé de Goiás',9),(1078,'GO','Santa Helena de Goiás',9),(1079,'GO','Santa Isabel',9),(1080,'GO','Santa Rita do Araguaia',9),(1081,'GO','Santa Rita do Novo Destino',9),(1082,'GO','Santa Rosa de Goiás',9),(1083,'GO','Santa Tereza de Goiás',9),(1084,'GO','Santa Terezinha de Goiás',9),(1085,'GO','Santo Antônio da Barra',9),(1086,'GO','Santo Antônio de Goiás',9),(1087,'GO','Santo Antônio do Descoberto',9),(1088,'GO','São Domingos',9),(1089,'GO','São Francisco de Goiás',9),(1090,'GO','São João dAliança',9),(1091,'GO','São João da Paraúna',9),(1092,'GO','São Luís de Montes Belos',9),(1093,'GO','São Luíz do Norte',9),(1094,'GO','São Miguel do Araguaia',9),(1095,'GO','São Miguel do Passa Quatro',9),(1096,'GO','São Patrício',9),(1097,'GO','São Simão',9),(1098,'GO','Senador Canedo',9),(1099,'GO','Serranópolis',9),(1100,'GO','Silvânia',9),(1101,'GO','Simolândia',9),(1102,'GO','Sítio dAbadia',9),(1103,'GO','Taquaral de Goiás',9),(1104,'GO','Teresina de Goiás',9),(1105,'GO','Terezópolis de Goiás',9),(1106,'GO','Três Ranchos',9),(1107,'GO','Trindade',9),(1108,'GO','Trombas',9),(1109,'GO','Turvânia',9),(1110,'GO','Turvelândia',9),(1111,'GO','Uirapuru',9),(1112,'GO','Uruaçu',9),(1113,'GO','Uruana',9),(1114,'GO','Urutaí',9),(1115,'GO','Valparaíso de Goiás',9),(1116,'GO','Varjão',9),(1117,'GO','Vianópolis',9),(1118,'GO','Vicentinópolis',9),(1119,'GO','Vila Boa',9),(1120,'GO','Vila Propício',9),(1121,'MA','Açailândia',10),(1122,'MA','Afonso Cunha',10),(1123,'MA','Água Doce do Maranhão',10),(1124,'MA','Alcântara',10),(1125,'MA','Aldeias Altas',10),(1126,'MA','Altamira do Maranhão',10),(1127,'MA','Alto Alegre do Maranhão',10),(1128,'MA','Alto Alegre do Pindaré',10),(1129,'MA','Alto Parnaíba',10),(1130,'MA','Amapá do Maranhão',10),(1131,'MA','Amarante do Maranhão',10),(1132,'MA','Anajatuba',10),(1133,'MA','Anapurus',10),(1134,'MA','Apicum-Açu',10),(1135,'MA','Araguanã',10),(1136,'MA','Araioses',10),(1137,'MA','Arame',10),(1138,'MA','Arari',10),(1139,'MA','Axixá',10),(1140,'MA','Bacabal',10),(1141,'MA','Bacabeira',10),(1142,'MA','Bacuri',10),(1143,'MA','Bacurituba',10),(1144,'MA','Balsas',10),(1145,'MA','Barão de Grajaú',10),(1146,'MA','Barra do Corda',10),(1147,'MA','Barreirinhas',10),(1148,'MA','Bela Vista do Maranhão',10),(1149,'MA','Belágua',10),(1150,'MA','Benedito Leite',10),(1151,'MA','Bequimão',10),(1152,'MA','Bernardo do Mearim',10),(1153,'MA','Boa Vista do Gurupi',10),(1154,'MA','Bom Jardim',10),(1155,'MA','Bom Jesus das Selvas',10),(1156,'MA','Bom Lugar',10),(1157,'MA','Brejo',10),(1158,'MA','Brejo de Areia',10),(1159,'MA','Buriti',10),(1160,'MA','Buriti Bravo',10),(1161,'MA','Buriticupu',10),(1162,'MA','Buritirana',10),(1163,'MA','Cachoeira Grande',10),(1164,'MA','Cajapió',10),(1165,'MA','Cajari',10),(1166,'MA','Campestre do Maranhão',10),(1167,'MA','Cândido Mendes',10),(1168,'MA','Cantanhede',10),(1169,'MA','Capinzal do Norte',10),(1170,'MA','Carolina',10),(1171,'MA','Carutapera',10),(1172,'MA','Caxias',10),(1173,'MA','Cedral',10),(1174,'MA','Central do Maranhão',10),(1175,'MA','Centro do Guilherme',10),(1176,'MA','Centro Novo do Maranhão',10),(1177,'MA','Chapadinha',10),(1178,'MA','Cidelândia',10),(1179,'MA','Codó',10),(1180,'MA','Coelho Neto',10),(1181,'MA','Colinas',10),(1182,'MA','Conceição do Lago-Açu',10),(1183,'MA','Coroatá',10),(1184,'MA','Cururupu',10),(1185,'MA','Davinópolis',10),(1186,'MA','Dom Pedro',10),(1187,'MA','Duque Bacelar',10),(1188,'MA','Esperantinópolis',10),(1189,'MA','Estreito',10),(1190,'MA','Feira Nova do Maranhão',10),(1191,'MA','Fernando Falcão',10),(1192,'MA','Formosa da Serra Negra',10),(1193,'MA','Fortaleza dos Nogueiras',10),(1194,'MA','Fortuna',10),(1195,'MA','Godofredo Viana',10),(1196,'MA','Gonçalves Dias',10),(1197,'MA','Governador Archer',10),(1198,'MA','Governador Edison Lobão',10),(1199,'MA','Governador Eugênio Barros',10),(1200,'MA','Governador Luiz Rocha',10),(1201,'MA','Governador Newton Bello',10),(1202,'MA','Governador Nunes Freire',10),(1203,'MA','Graça Aranha',10),(1204,'MA','Grajaú',10),(1205,'MA','Guimarães',10),(1206,'MA','Humberto de Campos',10),(1207,'MA','Icatu',10),(1208,'MA','Igarapé do Meio',10),(1209,'MA','Igarapé Grande',10),(1210,'MA','Imperatriz',10),(1211,'MA','Itaipava do Grajaú',10),(1212,'MA','Itapecuru Mirim',10),(1213,'MA','Itinga do Maranhão',10),(1214,'MA','Jatobá',10),(1215,'MA','Jenipapo dos Vieiras',10),(1216,'MA','João Lisboa',10),(1217,'MA','Joselândia',10),(1218,'MA','Junco do Maranhão',10),(1219,'MA','Lago da Pedra',10),(1220,'MA','Lago do Junco',10),(1221,'MA','Lago dos Rodrigues',10),(1222,'MA','Lago Verde',10),(1223,'MA','Lagoa do Mato',10),(1224,'MA','Lagoa Grande do Maranhão',10),(1225,'MA','Lajeado Novo',10),(1226,'MA','Lima Campos',10),(1227,'MA','Loreto',10),(1228,'MA','Luís Domingues',10),(1229,'MA','Magalhães de Almeida',10),(1230,'MA','Maracaçumé',10),(1231,'MA','Marajá do Sena',10),(1232,'MA','Maranhãozinho',10),(1233,'MA','Mata Roma',10),(1234,'MA','Matinha',10),(1235,'MA','Matões',10),(1236,'MA','Matões do Norte',10),(1237,'MA','Milagres do Maranhão',10),(1238,'MA','Mirador',10),(1239,'MA','Miranda do Norte',10),(1240,'MA','Mirinzal',10),(1241,'MA','Monção',10),(1242,'MA','Montes Altos',10),(1243,'MA','Morros',10),(1244,'MA','Nina Rodrigues',10),(1245,'MA','Nova Colinas',10),(1246,'MA','Nova Iorque',10),(1247,'MA','Nova Olinda do Maranhão',10),(1248,'MA','Olho dÁgua das Cunhãs',10),(1249,'MA','Olinda Nova do Maranhão',10),(1250,'MA','Paço do Lumiar',10),(1251,'MA','Palmeirândia',10),(1252,'MA','Paraibano',10),(1253,'MA','Parnarama',10),(1254,'MA','Passagem Franca',10),(1255,'MA','Pastos Bons',10),(1256,'MA','Paulino Neves',10),(1257,'MA','Paulo Ramos',10),(1258,'MA','Pedreiras',10),(1259,'MA','Pedro do Rosário',10),(1260,'MA','Penalva',10),(1261,'MA','Peri Mirim',10),(1262,'MA','Peritoró',10),(1263,'MA','Pindaré-Mirim',10),(1264,'MA','Pinheiro',10),(1265,'MA','Pio XII',10),(1266,'MA','Pirapemas',10),(1267,'MA','Poção de Pedras',10),(1268,'MA','Porto Franco',10),(1269,'MA','Porto Rico do Maranhão',10),(1270,'MA','Presidente Dutra',10),(1271,'MA','Presidente Juscelino',10),(1272,'MA','Presidente Médici',10),(1273,'MA','Presidente Sarney',10),(1274,'MA','Presidente Vargas',10),(1275,'MA','Primeira Cruz',10),(1276,'MA','Raposa',10),(1277,'MA','Riachão',10),(1278,'MA','Ribamar Fiquene',10),(1279,'MA','Rosário',10),(1280,'MA','Sambaíba',10),(1281,'MA','Santa Filomena do Maranhão',10),(1282,'MA','Santa Helena',10),(1283,'MA','Santa Inês',10),(1284,'MA','Santa Luzia',10),(1285,'MA','Santa Luzia do Paruá',10),(1286,'MA','Santa Quitéria do Maranhão',10),(1287,'MA','Santa Rita',10),(1288,'MA','Santana do Maranhão',10),(1289,'MA','Santo Amaro do Maranhão',10),(1290,'MA','Santo Antônio dos Lopes',10),(1291,'MA','São Benedito do Rio Preto',10),(1292,'MA','São Bento',10),(1293,'MA','São Bernardo',10),(1294,'MA','São Domingos do Azeitão',10),(1295,'MA','São Domingos do Maranhão',10),(1296,'MA','São Félix de Balsas',10),(1297,'MA','São Francisco do Brejão',10),(1298,'MA','São Francisco do Maranhão',10),(1299,'MA','São João Batista',10),(1300,'MA','São João do Carú',10),(1301,'MA','São João do Paraíso',10),(1302,'MA','São João do Soter',10),(1303,'MA','São João dos Patos',10),(1304,'MA','São José de Ribamar',10),(1305,'MA','São José dos Basílios',10),(1306,'MA','São Luís',10),(1307,'MA','São Luís Gonzaga do Maranhão',10),(1308,'MA','São Mateus do Maranhão',10),(1309,'MA','São Pedro da Água Branca',10),(1310,'MA','São Pedro dos Crentes',10),(1311,'MA','São Raimundo das Mangabeiras',10),(1312,'MA','São Raimundo do Doca Bezerra',10),(1313,'MA','São Roberto',10),(1314,'MA','São Vicente Ferrer',10),(1315,'MA','Satubinha',10),(1316,'MA','Senador Alexandre Costa',10),(1317,'MA','Senador La Rocque',10),(1318,'MA','Serrano do Maranhão',10),(1319,'MA','Sítio Novo',10),(1320,'MA','Sucupira do Norte',10),(1321,'MA','Sucupira do Riachão',10),(1322,'MA','Tasso Fragoso',10),(1323,'MA','Timbiras',10),(1324,'MA','Timon',10),(1325,'MA','Trizidela do Vale',10),(1326,'MA','Tufilândia',10),(1327,'MA','Tuntum',10),(1328,'MA','Turiaçu',10),(1329,'MA','Turilândia',10),(1330,'MA','Tutóia',10),(1331,'MA','Urbano Santos',10),(1332,'MA','Vargem Grande',10),(1333,'MA','Viana',10),(1334,'MA','Vila Nova dos Martírios',10),(1335,'MA','Vitória do Mearim',10),(1336,'MA','Vitorino Freire',10),(1337,'MA','Zé Doca',10),(1338,'MT','Acorizal',11),(1339,'MT','Água Boa',11),(1340,'MT','Alta Floresta',11),(1341,'MT','Alto Araguaia',11),(1342,'MT','Alto Boa Vista',11),(1343,'MT','Alto Garças',11),(1344,'MT','Alto Paraguai',11),(1345,'MT','Alto Taquari',11),(1346,'MT','Apiacás',11),(1347,'MT','Araguaiana',11),(1348,'MT','Araguainha',11),(1349,'MT','Araputanga',11),(1350,'MT','Arenápolis',11),(1351,'MT','Aripuanã',11),(1352,'MT','Barão de Melgaço',11),(1353,'MT','Barra do Bugres',11),(1354,'MT','Barra do Garças',11),(1355,'MT','Brasnorte',11),(1356,'MT','Cáceres',11),(1357,'MT','Campinápolis',11),(1358,'MT','Campo Novo do Parecis',11),(1359,'MT','Campo Verde',11),(1360,'MT','Campos de Júlio',11),(1361,'MT','Canabrava do Norte',11),(1362,'MT','Canarana',11),(1363,'MT','Carlinda',11),(1364,'MT','Castanheira',11),(1365,'MT','Chapada dos Guimarães',11),(1366,'MT','Cláudia',11),(1367,'MT','Cocalinho',11),(1368,'MT','Colíder',11),(1369,'MT','Comodoro',11),(1370,'MT','Confresa',11),(1371,'MT','Cotriguaçu',11),(1372,'MT','Cuiabá',11),(1373,'MT','Denise',11),(1374,'MT','Diamantino',11),(1375,'MT','Dom Aquino',11),(1376,'MT','Feliz Natal',11),(1377,'MT','Figueirópolis dOeste',11),(1378,'MT','Gaúcha do Norte',11),(1379,'MT','General Carneiro',11),(1380,'MT','Glória dOeste',11),(1381,'MT','Guarantã do Norte',11),(1382,'MT','Guiratinga',11),(1383,'MT','Indiavaí',11),(1384,'MT','Itaúba',11),(1385,'MT','Itiquira',11),(1386,'MT','Jaciara',11),(1387,'MT','Jangada',11),(1388,'MT','Jauru',11),(1389,'MT','Juara',11),(1390,'MT','Juína',11),(1391,'MT','Juruena',11),(1392,'MT','Juscimeira',11),(1393,'MT','Lambari dOeste',11),(1394,'MT','Lucas do Rio Verde',11),(1395,'MT','Luciára',11),(1396,'MT','Marcelândia',11),(1397,'MT','Matupá',11),(1398,'MT','Mirassol dOeste',11),(1399,'MT','Nobres',11),(1400,'MT','Nortelândia',11),(1401,'MT','Nossa Senhora do Livramento',11),(1402,'MT','Nova Bandeirantes',11),(1403,'MT','Nova Brasilândia',11),(1404,'MT','Nova Canaã do Norte',11),(1405,'MT','Nova Guarita',11),(1406,'MT','Nova Lacerda',11),(1407,'MT','Nova Marilândia',11),(1408,'MT','Nova Maringá',11),(1409,'MT','Nova Monte Verde',11),(1410,'MT','Nova Mutum',11),(1411,'MT','Nova Olímpia',11),(1412,'MT','Nova Ubiratã',11),(1413,'MT','Nova Xavantina',11),(1414,'MT','Novo Horizonte do Norte',11),(1415,'MT','Novo Mundo',11),(1416,'MT','Novo São Joaquim',11),(1417,'MT','Paranaíta',11),(1418,'MT','Paranatinga',11),(1419,'MT','Pedra Preta',11),(1420,'MT','Peixoto de Azevedo',11),(1421,'MT','Planalto da Serra',11),(1422,'MT','Poconé',11),(1423,'MT','Pontal do Araguaia',11),(1424,'MT','Ponte Branca',11),(1425,'MT','Pontes e Lacerda',11),(1426,'MT','Porto Alegre do Norte',11),(1427,'MT','Porto dos Gaúchos',11),(1428,'MT','Porto Esperidião',11),(1429,'MT','Porto Estrela',11),(1430,'MT','Poxoréo',11),(1431,'MT','Primavera do Leste',11),(1432,'MT','Querência',11),(1433,'MT','Reserva do Cabaçal',11),(1434,'MT','Ribeirão Cascalheira',11),(1435,'MT','Ribeirãozinho',11),(1436,'MT','Rio Branco',11),(1437,'MT','Rondonópolis',11),(1438,'MT','Rosário Oeste',11),(1439,'MT','Salto do Céu',11),(1440,'MT','Santa Carmem',11),(1441,'MT','Santa Terezinha',11),(1442,'MT','Santo Afonso',11),(1443,'MT','Santo Antônio do Leverger',11),(1444,'MT','São Félix do Araguaia',11),(1445,'MT','São José do Povo',11),(1446,'MT','São José do Rio Claro',11),(1447,'MT','São José do Xingu',11),(1448,'MT','São José dos Quatro Marcos',11),(1449,'MT','São Pedro da Cipa',11),(1450,'MT','Sapezal',11),(1451,'MT','Sinop',11),(1452,'MT','Sorriso',11),(1453,'MT','Tabaporã',11),(1454,'MT','Tangará da Serra',11),(1455,'MT','Tapurah',11),(1456,'MT','Terra Nova do Norte',11),(1457,'MT','Tesouro',11),(1458,'MT','Torixoréu',11),(1459,'MT','União do Sul',11),(1460,'MT','Várzea Grande',11),(1461,'MT','Vera',11),(1462,'MT','Vila Bela da Santíssima Trindade',11),(1463,'MT','Vila Rica',11),(1464,'MS','Água Clara',12),(1465,'MS','Alcinópolis',12),(1466,'MS','Amambaí',12),(1467,'MS','Anastácio',12),(1468,'MS','Anaurilândia',12),(1469,'MS','Angélica',12),(1470,'MS','Antônio João',12),(1471,'MS','Aparecida do Taboado',12),(1472,'MS','Aquidauana',12),(1473,'MS','Aral Moreira',12),(1474,'MS','Bandeirantes',12),(1475,'MS','Bataguassu',12),(1476,'MS','Bataiporã',12),(1477,'MS','Bela Vista',12),(1478,'MS','Bodoquena',12),(1479,'MS','Bonito',12),(1480,'MS','Brasilândia',12),(1481,'MS','Caarapó',12),(1482,'MS','Camapuã',12),(1483,'MS','Campo Grande',12),(1484,'MS','Caracol',12),(1485,'MS','Cassilândia',12),(1486,'MS','Chapadão do Sul',12),(1487,'MS','Corguinho',12),(1488,'MS','Coronel Sapucaia',12),(1489,'MS','Corumbá',12),(1490,'MS','Costa Rica',12),(1491,'MS','Coxim',12),(1492,'MS','Deodápolis',12),(1493,'MS','Dois Irmãos do Buriti',12),(1494,'MS','Douradina',12),(1495,'MS','Dourados',12),(1496,'MS','Eldorado',12),(1497,'MS','Fátima do Sul',12),(1498,'MS','Glória de Dourados',12),(1499,'MS','Guia Lopes da Laguna',12),(1500,'MS','Iguatemi',12),(1501,'MS','Inocência',12),(1502,'MS','Itaporã',12),(1503,'MS','Itaquiraí',12),(1504,'MS','Ivinhema',12),(1505,'MS','Japorã',12),(1506,'MS','Jaraguari',12),(1507,'MS','Jardim',12),(1508,'MS','Jateí',12),(1509,'MS','Juti',12),(1510,'MS','Ladário',12),(1511,'MS','Laguna Carapã',12),(1512,'MS','Maracaju',12),(1513,'MS','Miranda',12),(1514,'MS','Mundo Novo',12),(1515,'MS','Naviraí',12),(1516,'MS','Nioaque',12),(1517,'MS','Nova Alvorada do Sul',12),(1518,'MS','Nova Andradina',12),(1519,'MS','Novo Horizonte do Sul',12),(1520,'MS','Paranaíba',12),(1521,'MS','Paranhos',12),(1522,'MS','Pedro Gomes',12),(1523,'MS','Ponta Porã',12),(1524,'MS','Porto Murtinho',12),(1525,'MS','Ribas do Rio Pardo',12),(1526,'MS','Rio Brilhante',12),(1527,'MS','Rio Negro',12),(1528,'MS','Rio Verde de Mato Grosso',12),(1529,'MS','Rochedo',12),(1530,'MS','Santa Rita do Pardo',12),(1531,'MS','São Gabriel do Oeste',12),(1532,'MS','Selvíria',12),(1533,'MS','Sete Quedas',12),(1534,'MS','Sidrolândia',12),(1535,'MS','Sonora',12),(1536,'MS','Tacuru',12),(1537,'MS','Taquarussu',12),(1538,'MS','Terenos',12),(1539,'MS','Três Lagoas',12),(1540,'MS','Vicentina',12),(1541,'MG','Abadia dos Dourados',13),(1542,'MG','Abaeté',13),(1543,'MG','Abre Campo',13),(1544,'MG','Acaiaca',13),(1545,'MG','Açucena',13),(1546,'MG','Água Boa',13),(1547,'MG','Água Comprida',13),(1548,'MG','Aguanil',13),(1549,'MG','Águas Formosas',13),(1550,'MG','Águas Vermelhas',13),(1551,'MG','Aimorés',13),(1552,'MG','Aiuruoca',13),(1553,'MG','Alagoa',13),(1554,'MG','Albertina',13),(1555,'MG','Além Paraíba',13),(1556,'MG','Alfenas',13),(1557,'MG','Alfredo Vasconcelos',13),(1558,'MG','Almenara',13),(1559,'MG','Alpercata',13),(1560,'MG','Alpinópolis',13),(1561,'MG','Alterosa',13),(1562,'MG','Alto Caparaó',13),(1563,'MG','Alto Jequitibá',13),(1564,'MG','Alto Rio Doce',13),(1565,'MG','Alvarenga',13),(1566,'MG','Alvinópolis',13),(1567,'MG','Alvorada de Minas',13),(1568,'MG','Amparo do Serra',13),(1569,'MG','Andradas',13),(1570,'MG','Andrelândia',13),(1571,'MG','Angelândia',13),(1572,'MG','Antônio Carlos',13),(1573,'MG','Antônio Dias',13),(1574,'MG','Antônio Prado de Minas',13),(1575,'MG','Araçaí',13),(1576,'MG','Aracitaba',13),(1577,'MG','Araçuaí',13),(1578,'MG','Araguari',13),(1579,'MG','Arantina',13),(1580,'MG','Araponga',13),(1581,'MG','Araporã',13),(1582,'MG','Arapuá',13),(1583,'MG','Araújos',13),(1584,'MG','Araxá',13),(1585,'MG','Arceburgo',13),(1586,'MG','Arcos',13),(1587,'MG','Areado',13),(1588,'MG','Argirita',13),(1589,'MG','Aricanduva',13),(1590,'MG','Arinos',13),(1591,'MG','Astolfo Dutra',13),(1592,'MG','Ataléia',13),(1593,'MG','Augusto de Lima',13),(1594,'MG','Baependi',13),(1595,'MG','Baldim',13),(1596,'MG','Bambuí',13),(1597,'MG','Bandeira',13),(1598,'MG','Bandeira do Sul',13),(1599,'MG','Barão de Cocais',13),(1600,'MG','Barão de Monte Alto',13),(1601,'MG','Barbacena',13),(1602,'MG','Barra Longa',13),(1603,'MG','Barroso',13),(1604,'MG','Bela Vista de Minas',13),(1605,'MG','Belmiro Braga',13),(1606,'MG','Belo Horizonte',13),(1607,'MG','Belo Oriente',13),(1608,'MG','Belo Vale',13),(1609,'MG','Berilo',13),(1610,'MG','Berizal',13),(1611,'MG','Bertópolis',13),(1612,'MG','Betim',13),(1613,'MG','Bias Fortes',13),(1614,'MG','Bicas',13),(1615,'MG','Biquinhas',13),(1616,'MG','Boa Esperança',13),(1617,'MG','Bocaina de Minas',13),(1618,'MG','Bocaiúva',13),(1619,'MG','Bom Despacho',13),(1620,'MG','Bom Jardim de Minas',13),(1621,'MG','Bom Jesus da Penha',13),(1622,'MG','Bom Jesus do Amparo',13),(1623,'MG','Bom Jesus do Galho',13),(1624,'MG','Bom Repouso',13),(1625,'MG','Bom Sucesso',13),(1626,'MG','Bonfim',13),(1627,'MG','Bonfinópolis de Minas',13),(1628,'MG','Bonito de Minas',13),(1629,'MG','Borda da Mata',13),(1630,'MG','Botelhos',13),(1631,'MG','Botumirim',13),(1632,'MG','Brás Pires',13),(1633,'MG','Brasilândia de Minas',13),(1634,'MG','Brasília de Minas',13),(1635,'MG','Brasópolis',13),(1636,'MG','Braúnas',13),(1637,'MG','Brumadinho',13),(1638,'MG','Bueno Brandão',13),(1639,'MG','Buenópolis',13),(1640,'MG','Bugre',13),(1641,'MG','Buritis',13),(1642,'MG','Buritizeiro',13),(1643,'MG','Cabeceira Grande',13),(1644,'MG','Cabo Verde',13),(1645,'MG','Cachoeira da Prata',13),(1646,'MG','Cachoeira de Minas',13),(1647,'MG','Cachoeira de Pajeú',13),(1648,'MG','Cachoeira Dourada',13),(1649,'MG','Caetanópolis',13),(1650,'MG','Caeté',13),(1651,'MG','Caiana',13),(1652,'MG','Cajuri',13),(1653,'MG','Caldas',13),(1654,'MG','Camacho',13),(1655,'MG','Camanducaia',13),(1656,'MG','Cambuí',13),(1657,'MG','Cambuquira',13),(1658,'MG','Campanário',13),(1659,'MG','Campanha',13),(1660,'MG','Campestre',13),(1661,'MG','Campina Verde',13),(1662,'MG','Campo Azul',13),(1663,'MG','Campo Belo',13),(1664,'MG','Campo do Meio',13),(1665,'MG','Campo Florido',13),(1666,'MG','Campos Altos',13),(1667,'MG','Campos Gerais',13),(1668,'MG','Cana Verde',13),(1669,'MG','Canaã',13),(1670,'MG','Canápolis',13),(1671,'MG','Candeias',13),(1672,'MG','Cantagalo',13),(1673,'MG','Caparaó',13),(1674,'MG','Capela Nova',13),(1675,'MG','Capelinha',13),(1676,'MG','Capetinga',13),(1677,'MG','Capim Branco',13),(1678,'MG','Capinópolis',13),(1679,'MG','Capitão Andrade',13),(1680,'MG','Capitão Enéas',13),(1681,'MG','Capitólio',13),(1682,'MG','Caputira',13),(1683,'MG','Caraí',13),(1684,'MG','Caranaíba',13),(1685,'MG','Carandaí',13),(1686,'MG','Carangola',13),(1687,'MG','Caratinga',13),(1688,'MG','Carbonita',13),(1689,'MG','Careaçu',13),(1690,'MG','Carlos Chagas',13),(1691,'MG','Carmésia',13),(1692,'MG','Carmo da Cachoeira',13),(1693,'MG','Carmo da Mata',13),(1694,'MG','Carmo de Minas',13),(1695,'MG','Carmo do Cajuru',13),(1696,'MG','Carmo do Paranaíba',13),(1697,'MG','Carmo do Rio Claro',13),(1698,'MG','Carmópolis de Minas',13),(1699,'MG','Carneirinho',13),(1700,'MG','Carrancas',13),(1701,'MG','Carvalhópolis',13),(1702,'MG','Carvalhos',13),(1703,'MG','Casa Grande',13),(1704,'MG','Cascalho Rico',13),(1705,'MG','Cássia',13),(1706,'MG','Cataguases',13),(1707,'MG','Catas Altas',13),(1708,'MG','Catas Altas da Noruega',13),(1709,'MG','Catuji',13),(1710,'MG','Catuti',13),(1711,'MG','Caxambu',13),(1712,'MG','Cedro do Abaeté',13),(1713,'MG','Central de Minas',13),(1714,'MG','Centralina',13),(1715,'MG','Chácara',13),(1716,'MG','Chalé',13),(1717,'MG','Chapada do Norte',13),(1718,'MG','Chapada Gaúcha',13),(1719,'MG','Chiador',13),(1720,'MG','Cipotânea',13),(1721,'MG','Claraval',13),(1722,'MG','Claro dos Poções',13),(1723,'MG','Cláudio',13),(1724,'MG','Coimbra',13),(1725,'MG','Coluna',13),(1726,'MG','Comendador Gomes',13),(1727,'MG','Comercinho',13),(1728,'MG','Conceição da Aparecida',13),(1729,'MG','Conceição da Barra de Minas',13),(1730,'MG','Conceição das Alagoas',13),(1731,'MG','Conceição das Pedras',13),(1732,'MG','Conceição de Ipanema',13),(1733,'MG','Conceição do Mato Dentro',13),(1734,'MG','Conceição do Pará',13),(1735,'MG','Conceição do Rio Verde',13),(1736,'MG','Conceição dos Ouros',13),(1737,'MG','Cônego Marinho',13),(1738,'MG','Confins',13),(1739,'MG','Congonhal',13),(1740,'MG','Congonhas',13),(1741,'MG','Congonhas do Norte',13),(1742,'MG','Conquista',13),(1743,'MG','Conselheiro Lafaiete',13),(1744,'MG','Conselheiro Pena',13),(1745,'MG','Consolação',13),(1746,'MG','Contagem',13),(1747,'MG','Coqueiral',13),(1748,'MG','Coração de Jesus',13),(1749,'MG','Cordisburgo',13),(1750,'MG','Cordislândia',13),(1751,'MG','Corinto',13),(1752,'MG','Coroaci',13),(1753,'MG','Coromandel',13),(1754,'MG','Coronel Fabriciano',13),(1755,'MG','Coronel Murta',13),(1756,'MG','Coronel Pacheco',13),(1757,'MG','Coronel Xavier Chaves',13),(1758,'MG','Córrego Danta',13),(1759,'MG','Córrego do Bom Jesus',13),(1760,'MG','Córrego Fundo',13),(1761,'MG','Córrego Novo',13),(1762,'MG','Couto de Magalhães de Minas',13),(1763,'MG','Crisólita',13),(1764,'MG','Cristais',13),(1765,'MG','Cristália',13),(1766,'MG','Cristiano Otoni',13),(1767,'MG','Cristina',13),(1768,'MG','Crucilândia',13),(1769,'MG','Cruzeiro da Fortaleza',13),(1770,'MG','Cruzília',13),(1771,'MG','Cuparaque',13),(1772,'MG','Curral de Dentro',13),(1773,'MG','Curvelo',13),(1774,'MG','Datas',13),(1775,'MG','Delfim Moreira',13),(1776,'MG','Delfinópolis',13),(1777,'MG','Delta',13),(1778,'MG','Descoberto',13),(1779,'MG','Desterro de Entre Rios',13),(1780,'MG','Desterro do Melo',13),(1781,'MG','Diamantina',13),(1782,'MG','Diogo de Vasconcelos',13),(1783,'MG','Dionísio',13),(1784,'MG','Divinésia',13),(1785,'MG','Divino',13),(1786,'MG','Divino das Laranjeiras',13),(1787,'MG','Divinolândia de Minas',13),(1788,'MG','Divinópolis',13),(1789,'MG','Divisa Alegre',13),(1790,'MG','Divisa Nova',13),(1791,'MG','Divisópolis',13),(1792,'MG','Dom Bosco',13),(1793,'MG','Dom Cavati',13),(1794,'MG','Dom Joaquim',13),(1795,'MG','Dom Silvério',13),(1796,'MG','Dom Viçoso',13),(1797,'MG','Dona Eusébia',13),(1798,'MG','Dores de Campos',13),(1799,'MG','Dores de Guanhães',13),(1800,'MG','Dores do Indaiá',13),(1801,'MG','Dores do Turvo',13),(1802,'MG','Doresópolis',13),(1803,'MG','Douradoquara',13),(1804,'MG','Durandé',13),(1805,'MG','Elói Mendes',13),(1806,'MG','Engenheiro Caldas',13),(1807,'MG','Engenheiro Navarro',13),(1808,'MG','Entre Folhas',13),(1809,'MG','Entre Rios de Minas',13),(1810,'MG','Ervália',13),(1811,'MG','Esmeraldas',13),(1812,'MG','Espera Feliz',13),(1813,'MG','Espinosa',13),(1814,'MG','Espírito Santo do Dourado',13),(1815,'MG','Estiva',13),(1816,'MG','Estrela Dalva',13),(1817,'MG','Estrela do Indaiá',13),(1818,'MG','Estrela do Sul',13),(1819,'MG','Eugenópolis',13),(1820,'MG','Ewbank da Câmara',13),(1821,'MG','Extrema',13),(1822,'MG','Fama',13),(1823,'MG','Faria Lemos',13),(1824,'MG','Felício dos Santos',13),(1825,'MG','Felisburgo',13),(1826,'MG','Felixlândia',13),(1827,'MG','Fernandes Tourinho',13),(1828,'MG','Ferros',13),(1829,'MG','Fervedouro',13),(1830,'MG','Florestal',13),(1831,'MG','Formiga',13),(1832,'MG','Formoso',13),(1833,'MG','Fortaleza de Minas',13),(1834,'MG','Fortuna de Minas',13),(1835,'MG','Francisco Badaró',13),(1836,'MG','Francisco Dumont',13),(1837,'MG','Francisco Sá',13),(1838,'MG','Franciscópolis',13),(1839,'MG','Frei Gaspar',13),(1840,'MG','Frei Inocêncio',13),(1841,'MG','Frei Lagonegro',13),(1842,'MG','Fronteira',13),(1843,'MG','Fronteira dos Vales',13),(1844,'MG','Fruta de Leite',13),(1845,'MG','Frutal',13),(1846,'MG','Funilândia',13),(1847,'MG','Galiléia',13),(1848,'MG','Gameleiras',13),(1849,'MG','Glaucilândia',13),(1850,'MG','Goiabeira',13),(1851,'MG','Goianá',13),(1852,'MG','Gonçalves',13),(1853,'MG','Gonzaga',13),(1854,'MG','Gouveia',13),(1855,'MG','Governador Valadares',13),(1856,'MG','Grão Mogol',13),(1857,'MG','Grupiara',13),(1858,'MG','Guanhães',13),(1859,'MG','Guapé',13),(1860,'MG','Guaraciaba',13),(1861,'MG','Guaraciama',13),(1862,'MG','Guaranésia',13),(1863,'MG','Guarani',13),(1864,'MG','Guarará',13),(1865,'MG','Guarda-Mor',13),(1866,'MG','Guaxupé',13),(1867,'MG','Guidoval',13),(1868,'MG','Guimarânia',13),(1869,'MG','Guiricema',13),(1870,'MG','Gurinhatã',13),(1871,'MG','Heliodora',13),(1872,'MG','Iapu',13),(1873,'MG','Ibertioga',13),(1874,'MG','Ibiá',13),(1875,'MG','Ibiaí',13),(1876,'MG','Ibiracatu',13),(1877,'MG','Ibiraci',13),(1878,'MG','Ibirité',13),(1879,'MG','Ibitiúra de Minas',13),(1880,'MG','Ibituruna',13),(1881,'MG','Icaraí de Minas',13),(1882,'MG','Igarapé',13),(1883,'MG','Igaratinga',13),(1884,'MG','Iguatama',13),(1885,'MG','Ijaci',13),(1886,'MG','Ilicínea',13),(1887,'MG','Imbé de Minas',13),(1888,'MG','Inconfidentes',13),(1889,'MG','Indaiabira',13),(1890,'MG','Indianópolis',13),(1891,'MG','Ingaí',13),(1892,'MG','Inhapim',13),(1893,'MG','Inhaúma',13),(1894,'MG','Inimutaba',13),(1895,'MG','Ipaba',13),(1896,'MG','Ipanema',13),(1897,'MG','Ipatinga',13),(1898,'MG','Ipiaçu',13),(1899,'MG','Ipuiúna',13),(1900,'MG','Iraí de Minas',13),(1901,'MG','Itabira',13),(1902,'MG','Itabirinha de Mantena',13),(1903,'MG','Itabirito',13),(1904,'MG','Itacambira',13),(1905,'MG','Itacarambi',13),(1906,'MG','Itaguara',13),(1907,'MG','Itaipé',13),(1908,'MG','Itajubá',13),(1909,'MG','Itamarandiba',13),(1910,'MG','Itamarati de Minas',13),(1911,'MG','Itambacuri',13),(1912,'MG','Itambé do Mato Dentro',13),(1913,'MG','Itamogi',13),(1914,'MG','Itamonte',13),(1915,'MG','Itanhandu',13),(1916,'MG','Itanhomi',13),(1917,'MG','Itaobim',13),(1918,'MG','Itapagipe',13),(1919,'MG','Itapecerica',13),(1920,'MG','Itapeva',13),(1921,'MG','Itatiaiuçu',13),(1922,'MG','Itaú de Minas',13),(1923,'MG','Itaúna',13),(1924,'MG','Itaverava',13),(1925,'MG','Itinga',13),(1926,'MG','Itueta',13),(1927,'MG','Ituiutaba',13),(1928,'MG','Itumirim',13),(1929,'MG','Iturama',13),(1930,'MG','Itutinga',13),(1931,'MG','Jaboticatubas',13),(1932,'MG','Jacinto',13),(1933,'MG','Jacuí',13),(1934,'MG','Jacutinga',13),(1935,'MG','Jaguaraçu',13),(1936,'MG','Jaíba',13),(1937,'MG','Jampruca',13),(1938,'MG','Janaúba',13),(1939,'MG','Januária',13),(1940,'MG','Japaraíba',13),(1941,'MG','Japonvar',13),(1942,'MG','Jeceaba',13),(1943,'MG','Jenipapo de Minas',13),(1944,'MG','Jequeri',13),(1945,'MG','Jequitaí',13),(1946,'MG','Jequitibá',13),(1947,'MG','Jequitinhonha',13),(1948,'MG','Jesuânia',13),(1949,'MG','Joaíma',13),(1950,'MG','Joanésia',13),(1951,'MG','João Monlevade',13),(1952,'MG','João Pinheiro',13),(1953,'MG','Joaquim Felício',13),(1954,'MG','Jordânia',13),(1955,'MG','José Gonçalves de Minas',13),(1956,'MG','José Raydan',13),(1957,'MG','Josenópolis',13),(1958,'MG','Juatuba',13),(1959,'MG','Juiz de Fora',13),(1960,'MG','Juramento',13),(1961,'MG','Juruaia',13),(1962,'MG','Juvenília',13),(1963,'MG','Ladainha',13),(1964,'MG','Lagamar',13),(1965,'MG','Lagoa da Prata',13),(1966,'MG','Lagoa dos Patos',13),(1967,'MG','Lagoa Dourada',13),(1968,'MG','Lagoa Formosa',13),(1969,'MG','Lagoa Grande',13),(1970,'MG','Lagoa Santa',13),(1971,'MG','Lajinha',13),(1972,'MG','Lambari',13),(1973,'MG','Lamim',13),(1974,'MG','Laranjal',13),(1975,'MG','Lassance',13),(1976,'MG','Lavras',13),(1977,'MG','Leandro Ferreira',13),(1978,'MG','Leme do Prado',13),(1979,'MG','Leopoldina',13),(1980,'MG','Liberdade',13),(1981,'MG','Lima Duarte',13),(1982,'MG','Limeira do Oeste',13),(1983,'MG','Lontra',13),(1984,'MG','Luisburgo',13),(1985,'MG','Luislândia',13),(1986,'MG','Luminárias',13),(1987,'MG','Luz',13),(1988,'MG','Machacalis',13),(1989,'MG','Machado',13),(1990,'MG','Madre de Deus de Minas',13),(1991,'MG','Malacacheta',13),(1992,'MG','Mamonas',13),(1993,'MG','Manga',13),(1994,'MG','Manhuaçu',13),(1995,'MG','Manhumirim',13),(1996,'MG','Mantena',13),(1997,'MG','Mar de Espanha',13),(1998,'MG','Maravilhas',13),(1999,'MG','Maria da Fé',13),(2000,'MG','Mariana',13),(2001,'MG','Marilac',13),(2002,'MG','Mário Campos',13),(2003,'MG','Maripá de Minas',13),(2004,'MG','Marliéria',13),(2005,'MG','Marmelópolis',13),(2006,'MG','Martinho Campos',13),(2007,'MG','Martins Soares',13),(2008,'MG','Mata Verde',13),(2009,'MG','Materlândia',13),(2010,'MG','Mateus Leme',13),(2011,'MG','Mathias Lobato',13),(2012,'MG','Matias Barbosa',13),(2013,'MG','Matias Cardoso',13),(2014,'MG','Matipó',13),(2015,'MG','Mato Verde',13),(2016,'MG','Matozinhos',13),(2017,'MG','Matutina',13),(2018,'MG','Medeiros',13),(2019,'MG','Medina',13),(2020,'MG','Mendes Pimentel',13),(2021,'MG','Mercês',13),(2022,'MG','Mesquita',13),(2023,'MG','Minas Novas',13),(2024,'MG','Minduri',13),(2025,'MG','Mirabela',13),(2026,'MG','Miradouro',13),(2027,'MG','Miraí',13),(2028,'MG','Miravânia',13),(2029,'MG','Moeda',13),(2030,'MG','Moema',13),(2031,'MG','Monjolos',13),(2032,'MG','Monsenhor Paulo',13),(2033,'MG','Montalvânia',13),(2034,'MG','Monte Alegre de Minas',13),(2035,'MG','Monte Azul',13),(2036,'MG','Monte Belo',13),(2037,'MG','Monte Carmelo',13),(2038,'MG','Monte Formoso',13),(2039,'MG','Monte Santo de Minas',13),(2040,'MG','Monte Sião',13),(2041,'MG','Montes Claros',13),(2042,'MG','Montezuma',13),(2043,'MG','Morada Nova de Minas',13),(2044,'MG','Morro da Garça',13),(2045,'MG','Morro do Pilar',13),(2046,'MG','Munhoz',13),(2047,'MG','Muriaé',13),(2048,'MG','Mutum',13),(2049,'MG','Muzambinho',13),(2050,'MG','Nacip Raydan',13),(2051,'MG','Nanuque',13),(2052,'MG','Naque',13),(2053,'MG','Natalândia',13),(2054,'MG','Natércia',13),(2055,'MG','Nazareno',13),(2056,'MG','Nepomuceno',13),(2057,'MG','Ninheira',13),(2058,'MG','Nova Belém',13),(2059,'MG','Nova Era',13),(2060,'MG','Nova Lima',13),(2061,'MG','Nova Módica',13),(2062,'MG','Nova Ponte',13),(2063,'MG','Nova Porteirinha',13),(2064,'MG','Nova Resende',13),(2065,'MG','Nova Serrana',13),(2066,'MG','Nova União',13),(2067,'MG','Novo Cruzeiro',13),(2068,'MG','Novo Oriente de Minas',13),(2069,'MG','Novorizonte',13),(2070,'MG','Olaria',13),(2071,'MG','Olhos-dÁgua',13),(2072,'MG','Olímpio Noronha',13),(2073,'MG','Oliveira',13),(2074,'MG','Oliveira Fortes',13),(2075,'MG','Onça de Pitangui',13),(2076,'MG','Oratórios',13),(2077,'MG','Orizânia',13),(2078,'MG','Ouro Branco',13),(2079,'MG','Ouro Fino',13),(2080,'MG','Ouro Preto',13),(2081,'MG','Ouro Verde de Minas',13),(2082,'MG','Padre Carvalho',13),(2083,'MG','Padre Paraíso',13),(2084,'MG','Pai Pedro',13),(2085,'MG','Paineiras',13),(2086,'MG','Pains',13),(2087,'MG','Paiva',13),(2088,'MG','Palma',13),(2089,'MG','Palmópolis',13),(2090,'MG','Papagaios',13),(2091,'MG','Pará de Minas',13),(2092,'MG','Paracatu',13),(2093,'MG','Paraguaçu',13),(2094,'MG','Paraisópolis',13),(2095,'MG','Paraopeba',13),(2096,'MG','Passa Quatro',13),(2097,'MG','Passa Tempo',13),(2098,'MG','Passa-Vinte',13),(2099,'MG','Passabém',13),(2100,'MG','Passos',13),(2101,'MG','Patis',13),(2102,'MG','Patos de Minas',13),(2103,'MG','Patrocínio',13),(2104,'MG','Patrocínio do Muriaé',13),(2105,'MG','Paula Cândido',13),(2106,'MG','Paulistas',13),(2107,'MG','Pavão',13),(2108,'MG','Peçanha',13),(2109,'MG','Pedra Azul',13),(2110,'MG','Pedra Bonita',13),(2111,'MG','Pedra do Anta',13),(2112,'MG','Pedra do Indaiá',13),(2113,'MG','Pedra Dourada',13),(2114,'MG','Pedralva',13),(2115,'MG','Pedras de Maria da Cruz',13),(2116,'MG','Pedrinópolis',13),(2117,'MG','Pedro Leopoldo',13),(2118,'MG','Pedro Teixeira',13),(2119,'MG','Pequeri',13),(2120,'MG','Pequi',13),(2121,'MG','Perdigão',13),(2122,'MG','Perdizes',13),(2123,'MG','Perdões',13),(2124,'MG','Periquito',13),(2125,'MG','Pescador',13),(2126,'MG','Piau',13),(2127,'MG','Piedade de Caratinga',13),(2128,'MG','Piedade de Ponte Nova',13),(2129,'MG','Piedade do Rio Grande',13),(2130,'MG','Piedade dos Gerais',13),(2131,'MG','Pimenta',13),(2132,'MG','Pingo-dÁgua',13),(2133,'MG','Pintópolis',13),(2134,'MG','Piracema',13),(2135,'MG','Pirajuba',13),(2136,'MG','Piranga',13),(2137,'MG','Piranguçu',13),(2138,'MG','Piranguinho',13),(2139,'MG','Pirapetinga',13),(2140,'MG','Pirapora',13),(2141,'MG','Piraúba',13),(2142,'MG','Pitangui',13),(2143,'MG','Piumhi',13),(2144,'MG','Planura',13),(2145,'MG','Poço Fundo',13),(2146,'MG','Poços de Caldas',13),(2147,'MG','Pocrane',13),(2148,'MG','Pompéu',13),(2149,'MG','Ponte Nova',13),(2150,'MG','Ponto Chique',13),(2151,'MG','Ponto dos Volantes',13),(2152,'MG','Porteirinha',13),(2153,'MG','Porto Firme',13),(2154,'MG','Poté',13),(2155,'MG','Pouso Alegre',13),(2156,'MG','Pouso Alto',13),(2157,'MG','Prados',13),(2158,'MG','Prata',13),(2159,'MG','Pratápolis',13),(2160,'MG','Pratinha',13),(2161,'MG','Presidente Bernardes',13),(2162,'MG','Presidente Juscelino',13),(2163,'MG','Presidente Kubitschek',13),(2164,'MG','Presidente Olegário',13),(2165,'MG','Prudente de Morais',13),(2166,'MG','Quartel Geral',13),(2167,'MG','Queluzito',13),(2168,'MG','Raposos',13),(2169,'MG','Raul Soares',13),(2170,'MG','Recreio',13),(2171,'MG','Reduto',13),(2172,'MG','Resende Costa',13),(2173,'MG','Resplendor',13),(2174,'MG','Ressaquinha',13),(2175,'MG','Riachinho',13),(2176,'MG','Riacho dos Machados',13),(2177,'MG','Ribeirão das Neves',13),(2178,'MG','Ribeirão Vermelho',13),(2179,'MG','Rio Acima',13),(2180,'MG','Rio Casca',13),(2181,'MG','Rio do Prado',13),(2182,'MG','Rio Doce',13),(2183,'MG','Rio Espera',13),(2184,'MG','Rio Manso',13),(2185,'MG','Rio Novo',13),(2186,'MG','Rio Paranaíba',13),(2187,'MG','Rio Pardo de Minas',13),(2188,'MG','Rio Piracicaba',13),(2189,'MG','Rio Pomba',13),(2190,'MG','Rio Preto',13),(2191,'MG','Rio Vermelho',13),(2192,'MG','Ritápolis',13),(2193,'MG','Rochedo de Minas',13),(2194,'MG','Rodeiro',13),(2195,'MG','Romaria',13),(2196,'MG','Rosário da Limeira',13),(2197,'MG','Rubelita',13),(2198,'MG','Rubim',13),(2199,'MG','Sabará',13),(2200,'MG','Sabinópolis',13),(2201,'MG','Sacramento',13),(2202,'MG','Salinas',13),(2203,'MG','Salto da Divisa',13),(2204,'MG','Santa Bárbara',13),(2205,'MG','Santa Bárbara do Leste',13),(2206,'MG','Santa Bárbara do Monte Verde',13),(2207,'MG','Santa Bárbara do Tugúrio',13),(2208,'MG','Santa Cruz de Minas',13),(2209,'MG','Santa Cruz de Salinas',13),(2210,'MG','Santa Cruz do Escalvado',13),(2211,'MG','Santa Efigênia de Minas',13),(2212,'MG','Santa Fé de Minas',13),(2213,'MG','Santa Helena de Minas',13),(2214,'MG','Santa Juliana',13),(2215,'MG','Santa Luzia',13),(2216,'MG','Santa Margarida',13),(2217,'MG','Santa Maria de Itabira',13),(2218,'MG','Santa Maria do Salto',13),(2219,'MG','Santa Maria do Suaçuí',13),(2220,'MG','Santa Rita de Caldas',13),(2221,'MG','Santa Rita de Ibitipoca',13),(2222,'MG','Santa Rita de Jacutinga',13),(2223,'MG','Santa Rita de Minas',13),(2224,'MG','Santa Rita do Itueto',13),(2225,'MG','Santa Rita do Sapucaí',13),(2226,'MG','Santa Rosa da Serra',13),(2227,'MG','Santa Vitória',13),(2228,'MG','Santana da Vargem',13),(2229,'MG','Santana de Cataguases',13),(2230,'MG','Santana de Pirapama',13),(2231,'MG','Santana do Deserto',13),(2232,'MG','Santana do Garambéu',13),(2233,'MG','Santana do Jacaré',13),(2234,'MG','Santana do Manhuaçu',13),(2235,'MG','Santana do Paraíso',13),(2236,'MG','Santana do Riacho',13),(2237,'MG','Santana dos Montes',13),(2238,'MG','Santo Antônio do Amparo',13),(2239,'MG','Santo Antônio do Aventureiro',13),(2240,'MG','Santo Antônio do Grama',13),(2241,'MG','Santo Antônio do Itambé',13),(2242,'MG','Santo Antônio do Jacinto',13),(2243,'MG','Santo Antônio do Monte',13),(2244,'MG','Santo Antônio do Retiro',13),(2245,'MG','Santo Antônio do Rio Abaixo',13),(2246,'MG','Santo Hipólito',13),(2247,'MG','Santos Dumont',13),(2248,'MG','São Bento Abade',13),(2249,'MG','São Brás do Suaçuí',13),(2250,'MG','São Domingos das Dores',13),(2251,'MG','São Domingos do Prata',13),(2252,'MG','São Félix de Minas',13),(2253,'MG','São Francisco',13),(2254,'MG','São Francisco de Paula',13),(2255,'MG','São Francisco de Sales',13),(2256,'MG','São Francisco do Glória',13),(2257,'MG','São Geraldo',13),(2258,'MG','São Geraldo da Piedade',13),(2259,'MG','São Geraldo do Baixio',13),(2260,'MG','São Gonçalo do Abaeté',13),(2261,'MG','São Gonçalo do Pará',13),(2262,'MG','São Gonçalo do Rio Abaixo',13),(2263,'MG','São Gonçalo do Rio Preto',13),(2264,'MG','São Gonçalo do Sapucaí',13),(2265,'MG','São Gotardo',13),(2266,'MG','São João Batista do Glória',13),(2267,'MG','São João da Lagoa',13),(2268,'MG','São João da Mata',13),(2269,'MG','São João da Ponte',13),(2270,'MG','São João das Missões',13),(2271,'MG','São João del Rei',13),(2272,'MG','São João do Manhuaçu',13),(2273,'MG','São João do Manteninha',13),(2274,'MG','São João do Oriente',13),(2275,'MG','São João do Pacuí',13),(2276,'MG','São João do Paraíso',13),(2277,'MG','São João Evangelista',13),(2278,'MG','São João Nepomuceno',13),(2279,'MG','São Joaquim de Bicas',13),(2280,'MG','São José da Barra',13),(2281,'MG','São José da Lapa',13),(2282,'MG','São José da Safira',13),(2283,'MG','São José da Varginha',13),(2284,'MG','São José do Alegre',13),(2285,'MG','São José do Divino',13),(2286,'MG','São José do Goiabal',13),(2287,'MG','São José do Jacuri',13),(2288,'MG','São José do Mantimento',13),(2289,'MG','São Lourenço',13),(2290,'MG','São Miguel do Anta',13),(2291,'MG','São Pedro da União',13),(2292,'MG','São Pedro do Suaçuí',13),(2293,'MG','São Pedro dos Ferros',13),(2294,'MG','São Romão',13),(2295,'MG','São Roque de Minas',13),(2296,'MG','São Sebastião da Bela Vista',13),(2297,'MG','São Sebastião da Vargem Alegre',13),(2298,'MG','São Sebastião do Anta',13),(2299,'MG','São Sebastião do Maranhão',13),(2300,'MG','São Sebastião do Oeste',13),(2301,'MG','São Sebastião do Paraíso',13),(2302,'MG','São Sebastião do Rio Preto',13),(2303,'MG','São Sebastião do Rio Verde',13),(2304,'MG','São Thomé das Letras',13),(2305,'MG','São Tiago',13),(2306,'MG','São Tomás de Aquino',13),(2307,'MG','São Vicente de Minas',13),(2308,'MG','Sapucaí-Mirim',13),(2309,'MG','Sardoá',13),(2310,'MG','Sarzedo',13),(2311,'MG','Sem-Peixe',13),(2312,'MG','Senador Amaral',13),(2313,'MG','Senador Cortes',13),(2314,'MG','Senador Firmino',13),(2315,'MG','Senador José Bento',13),(2316,'MG','Senador Modestino Gonçalves',13),(2317,'MG','Senhora de Oliveira',13),(2318,'MG','Senhora do Porto',13),(2319,'MG','Senhora dos Remédios',13),(2320,'MG','Sericita',13),(2321,'MG','Seritinga',13),(2322,'MG','Serra Azul de Minas',13),(2323,'MG','Serra da Saudade',13),(2324,'MG','Serra do Salitre',13),(2325,'MG','Serra dos Aimorés',13),(2326,'MG','Serrania',13),(2327,'MG','Serranópolis de Minas',13),(2328,'MG','Serranos',13),(2329,'MG','Serro',13),(2330,'MG','Sete Lagoas',13),(2331,'MG','Setubinha',13),(2332,'MG','Silveirânia',13),(2333,'MG','Silvianópolis',13),(2334,'MG','Simão Pereira',13),(2335,'MG','Simonésia',13),(2336,'MG','Sobrália',13),(2337,'MG','Soledade de Minas',13),(2338,'MG','Tabuleiro',13),(2339,'MG','Taiobeiras',13),(2340,'MG','Taparuba',13),(2341,'MG','Tapira',13),(2342,'MG','Tapiraí',13),(2343,'MG','Taquaraçu de Minas',13),(2344,'MG','Tarumirim',13),(2345,'MG','Teixeiras',13),(2346,'MG','Teófilo Otoni',13),(2347,'MG','Timóteo',13),(2348,'MG','Tiradentes',13),(2349,'MG','Tiros',13),(2350,'MG','Tocantins',13),(2351,'MG','Tocos do Moji',13),(2352,'MG','Toledo',13),(2353,'MG','Tombos',13),(2354,'MG','Três Corações',13),(2355,'MG','Três Marias',13),(2356,'MG','Três Pontas',13),(2357,'MG','Tumiritinga',13),(2358,'MG','Tupaciguara',13),(2359,'MG','Turmalina',13),(2360,'MG','Turvolândia',13),(2361,'MG','Ubá',13),(2362,'MG','Ubaí',13),(2363,'MG','Ubaporanga',13),(2364,'MG','Uberaba',13),(2365,'MG','Uberlândia',13),(2366,'MG','Umburatiba',13),(2367,'MG','Unaí',13),(2368,'MG','União de Minas',13),(2369,'MG','Uruana de Minas',13),(2370,'MG','Urucânia',13),(2371,'MG','Urucuia',13),(2372,'MG','Vargem Alegre',13),(2373,'MG','Vargem Bonita',13),(2374,'MG','Vargem Grande do Rio Pardo',13),(2375,'MG','Varginha',13),(2376,'MG','Varjão de Minas',13),(2377,'MG','Várzea da Palma',13),(2378,'MG','Varzelândia',13),(2379,'MG','Vazante',13),(2380,'MG','Verdelândia',13),(2381,'MG','Veredinha',13),(2382,'MG','Veríssimo',13),(2383,'MG','Vermelho Novo',13),(2384,'MG','Vespasiano',13),(2385,'MG','Viçosa',13),(2386,'MG','Vieiras',13),(2387,'MG','Virgem da Lapa',13),(2388,'MG','Virgínia',13),(2389,'MG','Virginópolis',13),(2390,'MG','Virgolândia',13),(2391,'MG','Visconde do Rio Branco',13),(2392,'MG','Volta Grande',13),(2393,'MG','Wenceslau Braz',13),(2394,'PA','Abaetetuba',14),(2395,'PA','Abel Figueiredo',14),(2396,'PA','Acará',14),(2397,'PA','Afuá',14),(2398,'PA','Água Azul do Norte',14),(2399,'PA','Alenquer',14),(2400,'PA','Almeirim',14),(2401,'PA','Altamira',14),(2402,'PA','Anajás',14),(2403,'PA','Ananindeua',14),(2404,'PA','Anapu',14),(2405,'PA','Augusto Corrêa',14),(2406,'PA','Aurora do Pará',14),(2407,'PA','Aveiro',14),(2408,'PA','Bagre',14),(2409,'PA','Baião',14),(2410,'PA','Bannach',14),(2411,'PA','Barcarena',14),(2412,'PA','Belém',14),(2413,'PA','Belterra',14),(2414,'PA','Benevides',14),(2415,'PA','Bom Jesus do Tocantins',14),(2416,'PA','Bonito',14),(2417,'PA','Bragança',14),(2418,'PA','Brasil Novo',14),(2419,'PA','Brejo Grande do Araguaia',14),(2420,'PA','Breu Branco',14),(2421,'PA','Breves',14),(2422,'PA','Bujaru',14),(2423,'PA','Cachoeira do Arari',14),(2424,'PA','Cachoeira do Piriá',14),(2425,'PA','Cametá',14),(2426,'PA','Canaã dos Carajás',14),(2427,'PA','Capanema',14),(2428,'PA','Capitão Poço',14),(2429,'PA','Castanhal',14),(2430,'PA','Chaves',14),(2431,'PA','Colares',14),(2432,'PA','Conceição do Araguaia',14),(2433,'PA','Concórdia do Pará',14),(2434,'PA','Cumaru do Norte',14),(2435,'PA','Curionópolis',14),(2436,'PA','Curralinho',14),(2437,'PA','Curuá',14),(2438,'PA','Curuçá',14),(2439,'PA','Dom Eliseu',14),(2440,'PA','Eldorado dos Carajás',14),(2441,'PA','Faro',14),(2442,'PA','Floresta do Araguaia',14),(2443,'PA','Garrafão do Norte',14),(2444,'PA','Goianésia do Pará',14),(2445,'PA','Gurupá',14),(2446,'PA','Igarapé-Açu',14),(2447,'PA','Igarapé-Miri',14),(2448,'PA','Inhangapi',14),(2449,'PA','Ipixuna do Pará',14),(2450,'PA','Irituia',14),(2451,'PA','Itaituba',14),(2452,'PA','Itupiranga',14),(2453,'PA','Jacareacanga',14),(2454,'PA','Jacundá',14),(2455,'PA','Juruti',14),(2456,'PA','Limoeiro do Ajuru',14),(2457,'PA','Mãe do Rio',14),(2458,'PA','Magalhães Barata',14),(2459,'PA','Marabá',14),(2460,'PA','Maracanã',14),(2461,'PA','Marapanim',14),(2462,'PA','Marituba',14),(2463,'PA','Medicilândia',14),(2464,'PA','Melgaço',14),(2465,'PA','Mocajuba',14),(2466,'PA','Moju',14),(2467,'PA','Monte Alegre',14),(2468,'PA','Muaná',14),(2469,'PA','Nova Esperança do Piriá',14),(2470,'PA','Nova Ipixuna',14),(2471,'PA','Nova Timboteua',14),(2472,'PA','Novo Progresso',14),(2473,'PA','Novo Repartimento',14),(2474,'PA','Óbidos',14),(2475,'PA','Oeiras do Pará',14),(2476,'PA','Oriximiná',14),(2477,'PA','Ourém',14),(2478,'PA','Ourilândia do Norte',14),(2479,'PA','Pacajá',14),(2480,'PA','Palestina do Pará',14),(2481,'PA','Paragominas',14),(2482,'PA','Parauapebas',14),(2483,'PA','Pau dArco',14),(2484,'PA','Peixe-Boi',14),(2485,'PA','Piçarra',14),(2486,'PA','Placas',14),(2487,'PA','Ponta de Pedras',14),(2488,'PA','Portel',14),(2489,'PA','Porto de Moz',14),(2490,'PA','Prainha',14),(2491,'PA','Primavera',14),(2492,'PA','Quatipuru',14),(2493,'PA','Redenção',14),(2494,'PA','Rio Maria',14),(2495,'PA','Rondon do Pará',14),(2496,'PA','Rurópolis',14),(2497,'PA','Salinópolis',14),(2498,'PA','Salvaterra',14),(2499,'PA','Santa Bárbara do Pará',14),(2500,'PA','Santa Cruz do Arari',14),(2501,'PA','Santa Isabel do Pará',14),(2502,'PA','Santa Luzia do Pará',14),(2503,'PA','Santa Maria das Barreiras',14),(2504,'PA','Santa Maria do Pará',14),(2505,'PA','Santana do Araguaia',14),(2506,'PA','Santarém',14),(2507,'PA','Santarém Novo',14),(2508,'PA','Santo Antônio do Tauá',14),(2509,'PA','São Caetano de Odivelas',14),(2510,'PA','São Domingos do Araguaia',14),(2511,'PA','São Domingos do Capim',14),(2512,'PA','São Félix do Xingu',14),(2513,'PA','São Francisco do Pará',14),(2514,'PA','São Geraldo do Araguaia',14),(2515,'PA','São João da Ponta',14),(2516,'PA','São João de Pirabas',14),(2517,'PA','São João do Araguaia',14),(2518,'PA','São Miguel do Guamá',14),(2519,'PA','São Sebastião da Boa Vista',14),(2520,'PA','Sapucaia',14),(2521,'PA','Senador José Porfírio',14),(2522,'PA','Soure',14),(2523,'PA','Tailândia',14),(2524,'PA','Terra Alta',14),(2525,'PA','Terra Santa',14),(2526,'PA','Tomé-Açu',14),(2527,'PA','Tracuateua',14),(2528,'PA','Trairão',14),(2529,'PA','Tucumã',14),(2530,'PA','Tucuruí',14),(2531,'PA','Ulianópolis',14),(2532,'PA','Uruará',14),(2533,'PA','Vigia',14),(2534,'PA','Viseu',14),(2535,'PA','Vitória do Xingu',14),(2536,'PA','Xinguara',14),(2537,'PB','Água Branca',15),(2538,'PB','Aguiar',15),(2539,'PB','Alagoa Grande',15),(2540,'PB','Alagoa Nova',15),(2541,'PB','Alagoinha',15),(2542,'PB','Alcantil',15),(2543,'PB','Algodão de Jandaíra',15),(2544,'PB','Alhandra',15),(2545,'PB','Amparo',15),(2546,'PB','Aparecida',15),(2547,'PB','Araçagi',15),(2548,'PB','Arara',15),(2549,'PB','Araruna',15),(2550,'PB','Areia',15),(2551,'PB','Areia de Baraúnas',15),(2552,'PB','Areial',15),(2553,'PB','Aroeiras',15),(2554,'PB','Assunção',15),(2555,'PB','Baía da Traição',15),(2556,'PB','Bananeiras',15),(2557,'PB','Baraúna',15),(2558,'PB','Barra de Santa Rosa',15),(2559,'PB','Barra de Santana',15),(2560,'PB','Barra de São Miguel',15),(2561,'PB','Bayeux',15),(2562,'PB','Belém',15),(2563,'PB','Belém do Brejo do Cruz',15),(2564,'PB','Bernardino Batista',15),(2565,'PB','Boa Ventura',15),(2566,'PB','Boa Vista',15),(2567,'PB','Bom Jesus',15),(2568,'PB','Bom Sucesso',15),(2569,'PB','Bonito de Santa Fé',15),(2570,'PB','Boqueirão',15),(2571,'PB','Borborema',15),(2572,'PB','Brejo do Cruz',15),(2573,'PB','Brejo dos Santos',15),(2574,'PB','Caaporã',15),(2575,'PB','Cabaceiras',15),(2576,'PB','Cabedelo',15),(2577,'PB','Cachoeira dos Índios',15),(2578,'PB','Cacimba de Areia',15),(2579,'PB','Cacimba de Dentro',15),(2580,'PB','Cacimbas',15),(2581,'PB','Caiçara',15),(2582,'PB','Cajazeiras',15),(2583,'PB','Cajazeirinhas',15),(2584,'PB','Caldas Brandão',15),(2585,'PB','Camalaú',15),(2586,'PB','Campina Grande',15),(2587,'PB','Campo de Santana',15),(2588,'PB','Capim',15),(2589,'PB','Caraúbas',15),(2590,'PB','Carrapateira',15),(2591,'PB','Casserengue',15),(2592,'PB','Catingueira',15),(2593,'PB','Catolé do Rocha',15),(2594,'PB','Caturité',15),(2595,'PB','Conceição',15),(2596,'PB','Condado',15),(2597,'PB','Conde',15),(2598,'PB','Congo',15),(2599,'PB','Coremas',15),(2600,'PB','Coxixola',15),(2601,'PB','Cruz do Espírito Santo',15),(2602,'PB','Cubati',15),(2603,'PB','Cuité',15),(2604,'PB','Cuité de Mamanguape',15),(2605,'PB','Cuitegi',15),(2606,'PB','Curral de Cima',15),(2607,'PB','Curral Velho',15),(2608,'PB','Damião',15),(2609,'PB','Desterro',15),(2610,'PB','Diamante',15),(2611,'PB','Dona Inês',15),(2612,'PB','Duas Estradas',15),(2613,'PB','Emas',15),(2614,'PB','Esperança',15),(2615,'PB','Fagundes',15),(2616,'PB','Frei Martinho',15),(2617,'PB','Gado Bravo',15),(2618,'PB','Guarabira',15),(2619,'PB','Gurinhém',15),(2620,'PB','Gurjão',15),(2621,'PB','Ibiara',15),(2622,'PB','Igaracy',15),(2623,'PB','Imaculada',15),(2624,'PB','Ingá',15),(2625,'PB','Itabaiana',15),(2626,'PB','Itaporanga',15),(2627,'PB','Itapororoca',15),(2628,'PB','Itatuba',15),(2629,'PB','Jacaraú',15),(2630,'PB','Jericó',15),(2631,'PB','João Pessoa',15),(2632,'PB','Juarez Távora',15),(2633,'PB','Juazeirinho',15),(2634,'PB','Junco do Seridó',15),(2635,'PB','Juripiranga',15),(2636,'PB','Juru',15),(2637,'PB','Lagoa',15),(2638,'PB','Lagoa de Dentro',15),(2639,'PB','Lagoa Seca',15),(2640,'PB','Lastro',15),(2641,'PB','Livramento',15),(2642,'PB','Logradouro',15),(2643,'PB','Lucena',15),(2644,'PB','Mãe dÁgua',15),(2645,'PB','Malta',15),(2646,'PB','Mamanguape',15),(2647,'PB','Manaíra',15),(2648,'PB','Marcação',15),(2649,'PB','Mari',15),(2650,'PB','Marizópolis',15),(2651,'PB','Massaranduba',15),(2652,'PB','Mataraca',15),(2653,'PB','Matinhas',15),(2654,'PB','Mato Grosso',15),(2655,'PB','Maturéia',15),(2656,'PB','Mogeiro',15),(2657,'PB','Montadas',15),(2658,'PB','Monte Horebe',15),(2659,'PB','Monteiro',15),(2660,'PB','Mulungu',15),(2661,'PB','Natuba',15),(2662,'PB','Nazarezinho',15),(2663,'PB','Nova Floresta',15),(2664,'PB','Nova Olinda',15),(2665,'PB','Nova Palmeira',15),(2666,'PB','Olho dÁgua',15),(2667,'PB','Olivedos',15),(2668,'PB','Ouro Velho',15),(2669,'PB','Parari',15),(2670,'PB','Passagem',15),(2671,'PB','Patos',15),(2672,'PB','Paulista',15),(2673,'PB','Pedra Branca',15),(2674,'PB','Pedra Lavrada',15),(2675,'PB','Pedras de Fogo',15),(2676,'PB','Pedro Régis',15),(2677,'PB','Piancó',15),(2678,'PB','Picuí',15),(2679,'PB','Pilar',15),(2680,'PB','Pilões',15),(2681,'PB','Pilõezinhos',15),(2682,'PB','Pirpirituba',15),(2683,'PB','Pitimbu',15),(2684,'PB','Pocinhos',15),(2685,'PB','Poço Dantas',15),(2686,'PB','Poço de José de Moura',15),(2687,'PB','Pombal',15),(2688,'PB','Prata',15),(2689,'PB','Princesa Isabel',15),(2690,'PB','Puxinanã',15),(2691,'PB','Queimadas',15),(2692,'PB','Quixabá',15),(2693,'PB','Remígio',15),(2694,'PB','Riachão',15),(2695,'PB','Riachão do Bacamarte',15),(2696,'PB','Riachão do Poço',15),(2697,'PB','Riacho de Santo Antônio',15),(2698,'PB','Riacho dos Cavalos',15),(2699,'PB','Rio Tinto',15),(2700,'PB','Salgadinho',15),(2701,'PB','Salgado de São Félix',15),(2702,'PB','Santa Cecília',15),(2703,'PB','Santa Cruz',15),(2704,'PB','Santa Helena',15),(2705,'PB','Santa Inês',15),(2706,'PB','Santa Luzia',15),(2707,'PB','Santa Rita',15),(2708,'PB','Santa Teresinha',15),(2709,'PB','Santana de Mangueira',15),(2710,'PB','Santana dos Garrotes',15),(2711,'PB','Santarém',15),(2712,'PB','Santo André',15),(2713,'PB','São Bentinho',15),(2714,'PB','São Bento',15),(2715,'PB','São Domingos de Pombal',15),(2716,'PB','São Domingos do Cariri',15),(2717,'PB','São Francisco',15),(2718,'PB','São João do Cariri',15),(2719,'PB','São João do Rio do Peixe',15),(2720,'PB','São João do Tigre',15),(2721,'PB','São José da Lagoa Tapada',15),(2722,'PB','São José de Caiana',15),(2723,'PB','São José de Espinharas',15),(2724,'PB','São José de Piranhas',15),(2725,'PB','São José de Princesa',15),(2726,'PB','São José do Bonfim',15),(2727,'PB','São José do Brejo do Cruz',15),(2728,'PB','São José do Sabugi',15),(2729,'PB','São José dos Cordeiros',15),(2730,'PB','São José dos Ramos',15),(2731,'PB','São Mamede',15),(2732,'PB','São Miguel de Taipu',15),(2733,'PB','São Sebastião de Lagoa de Roça',15),(2734,'PB','São Sebastião do Umbuzeiro',15),(2735,'PB','Sapé',15),(2736,'PB','Seridó',15),(2737,'PB','Serra Branca',15),(2738,'PB','Serra da Raiz',15),(2739,'PB','Serra Grande',15),(2740,'PB','Serra Redonda',15),(2741,'PB','Serraria',15),(2742,'PB','Sertãozinho',15),(2743,'PB','Sobrado',15),(2744,'PB','Solânea',15),(2745,'PB','Soledade',15),(2746,'PB','Sossêgo',15),(2747,'PB','Sousa',15),(2748,'PB','Sumé',15),(2749,'PB','Taperoá',15),(2750,'PB','Tavares',15),(2751,'PB','Teixeira',15),(2752,'PB','Tenório',15),(2753,'PB','Triunfo',15),(2754,'PB','Uiraúna',15),(2755,'PB','Umbuzeiro',15),(2756,'PB','Várzea',15),(2757,'PB','Vieirópolis',15),(2758,'PB','Vista Serrana',15),(2759,'PB','Zabelê',15),(2760,'PR','Abatiá',16),(2761,'PR','Adrianópolis',16),(2762,'PR','Agudos do Sul',16),(2763,'PR','Almirante Tamandaré',16),(2764,'PR','Altamira do Paraná',16),(2765,'PR','Alto Paraná',16),(2766,'PR','Alto Piquiri',16),(2767,'PR','Altônia',16),(2768,'PR','Alvorada do Sul',16),(2769,'PR','Amaporã',16),(2770,'PR','Ampére',16),(2771,'PR','Anahy',16),(2772,'PR','Andirá',16),(2773,'PR','Ângulo',16),(2774,'PR','Antonina',16),(2775,'PR','Antônio Olinto',16),(2776,'PR','Apucarana',16),(2777,'PR','Arapongas',16),(2778,'PR','Arapoti',16),(2779,'PR','Arapuã',16),(2780,'PR','Araruna',16),(2781,'PR','Araucária',16),(2782,'PR','Ariranha do Ivaí',16),(2783,'PR','Assaí',16),(2784,'PR','Assis Chateaubriand',16),(2785,'PR','Astorga',16),(2786,'PR','Atalaia',16),(2787,'PR','Balsa Nova',16),(2788,'PR','Bandeirantes',16),(2789,'PR','Barbosa Ferraz',16),(2790,'PR','Barra do Jacaré',16),(2791,'PR','Barracão',16),(2792,'PR','Bela Vista da Caroba',16),(2793,'PR','Bela Vista do Paraíso',16),(2794,'PR','Bituruna',16),(2795,'PR','Boa Esperança',16),(2796,'PR','Boa Esperança do Iguaçu',16),(2797,'PR','Boa Ventura de São Roque',16),(2798,'PR','Boa Vista da Aparecida',16),(2799,'PR','Bocaiúva do Sul',16),(2800,'PR','Bom Jesus do Sul',16),(2801,'PR','Bom Sucesso',16),(2802,'PR','Bom Sucesso do Sul',16),(2803,'PR','Borrazópolis',16),(2804,'PR','Braganey',16),(2805,'PR','Brasilândia do Sul',16),(2806,'PR','Cafeara',16),(2807,'PR','Cafelândia',16),(2808,'PR','Cafezal do Sul',16),(2809,'PR','Califórnia',16),(2810,'PR','Cambará',16),(2811,'PR','Cambé',16),(2812,'PR','Cambira',16),(2813,'PR','Campina da Lagoa',16),(2814,'PR','Campina do Simão',16),(2815,'PR','Campina Grande do Sul',16),(2816,'PR','Campo Bonito',16),(2817,'PR','Campo do Tenente',16),(2818,'PR','Campo Largo',16),(2819,'PR','Campo Magro',16),(2820,'PR','Campo Mourão',16),(2821,'PR','Cândido de Abreu',16),(2822,'PR','Candói',16),(2823,'PR','Cantagalo',16),(2824,'PR','Capanema',16),(2825,'PR','Capitão Leônidas Marques',16),(2826,'PR','Carambeí',16),(2827,'PR','Carlópolis',16),(2828,'PR','Cascavel',16),(2829,'PR','Castro',16),(2830,'PR','Catanduvas',16),(2831,'PR','Centenário do Sul',16),(2832,'PR','Cerro Azul',16),(2833,'PR','Céu Azul',16),(2834,'PR','Chopinzinho',16),(2835,'PR','Cianorte',16),(2836,'PR','Cidade Gaúcha',16),(2837,'PR','Clevelândia',16),(2838,'PR','Colombo',16),(2839,'PR','Colorado',16),(2840,'PR','Congonhinhas',16),(2841,'PR','Conselheiro Mairinck',16),(2842,'PR','Contenda',16),(2843,'PR','Corbélia',16),(2844,'PR','Cornélio Procópio',16),(2845,'PR','Coronel Domingos Soares',16),(2846,'PR','Coronel Vivida',16),(2847,'PR','Corumbataí do Sul',16),(2848,'PR','Cruz Machado',16),(2849,'PR','Cruzeiro do Iguaçu',16),(2850,'PR','Cruzeiro do Oeste',16),(2851,'PR','Cruzeiro do Sul',16),(2852,'PR','Cruzmaltina',16),(2853,'PR','Curitiba',16),(2854,'PR','Curiúva',16),(2855,'PR','Diamante dOeste',16),(2856,'PR','Diamante do Norte',16),(2857,'PR','Diamante do Sul',16),(2858,'PR','Dois Vizinhos',16),(2859,'PR','Douradina',16),(2860,'PR','Doutor Camargo',16),(2861,'PR','Doutor Ulysses',16),(2862,'PR','Enéas Marques',16),(2863,'PR','Engenheiro Beltrão',16),(2864,'PR','Entre Rios do Oeste',16),(2865,'PR','Esperança Nova',16),(2866,'PR','Espigão Alto do Iguaçu',16),(2867,'PR','Farol',16),(2868,'PR','Faxinal',16),(2869,'PR','Fazenda Rio Grande',16),(2870,'PR','Fênix',16),(2871,'PR','Fernandes Pinheiro',16),(2872,'PR','Figueira',16),(2873,'PR','Flor da Serra do Sul',16),(2874,'PR','Floraí',16),(2875,'PR','Floresta',16),(2876,'PR','Florestópolis',16),(2877,'PR','Flórida',16),(2878,'PR','Formosa do Oeste',16),(2879,'PR','Foz do Iguaçu',16),(2880,'PR','Foz do Jordão',16),(2881,'PR','Francisco Alves',16),(2882,'PR','Francisco Beltrão',16),(2883,'PR','General Carneiro',16),(2884,'PR','Godoy Moreira',16),(2885,'PR','Goioerê',16),(2886,'PR','Goioxim',16),(2887,'PR','Grandes Rios',16),(2888,'PR','Guaíra',16),(2889,'PR','Guairaçá',16),(2890,'PR','Guamiranga',16),(2891,'PR','Guapirama',16),(2892,'PR','Guaporema',16),(2893,'PR','Guaraci',16),(2894,'PR','Guaraniaçu',16),(2895,'PR','Guarapuava',16),(2896,'PR','Guaraqueçaba',16),(2897,'PR','Guaratuba',16),(2898,'PR','Honório Serpa',16),(2899,'PR','Ibaiti',16),(2900,'PR','Ibema',16),(2901,'PR','Ibiporã',16),(2902,'PR','Icaraíma',16),(2903,'PR','Iguaraçu',16),(2904,'PR','Iguatu',16),(2905,'PR','Imbaú',16),(2906,'PR','Imbituva',16),(2907,'PR','Inácio Martins',16),(2908,'PR','Inajá',16),(2909,'PR','Indianópolis',16),(2910,'PR','Ipiranga',16),(2911,'PR','Iporã',16),(2912,'PR','Iracema do Oeste',16),(2913,'PR','Irati',16),(2914,'PR','Iretama',16),(2915,'PR','Itaguajé',16),(2916,'PR','Itaipulândia',16),(2917,'PR','Itambaracá',16),(2918,'PR','Itambé',16),(2919,'PR','Itapejara dOeste',16),(2920,'PR','Itaperuçu',16),(2921,'PR','Itaúna do Sul',16),(2922,'PR','Ivaí',16),(2923,'PR','Ivaiporã',16),(2924,'PR','Ivaté',16),(2925,'PR','Ivatuba',16),(2926,'PR','Jaboti',16),(2927,'PR','Jacarezinho',16),(2928,'PR','Jaguapitã',16),(2929,'PR','Jaguariaíva',16),(2930,'PR','Jandaia do Sul',16),(2931,'PR','Janiópolis',16),(2932,'PR','Japira',16),(2933,'PR','Japurá',16),(2934,'PR','Jardim Alegre',16),(2935,'PR','Jardim Olinda',16),(2936,'PR','Jataizinho',16),(2937,'PR','Jesuítas',16),(2938,'PR','Joaquim Távora',16),(2939,'PR','Jundiaí do Sul',16),(2940,'PR','Juranda',16),(2941,'PR','Jussara',16),(2942,'PR','Kaloré',16),(2943,'PR','Lapa',16),(2944,'PR','Laranjal',16),(2945,'PR','Laranjeiras do Sul',16),(2946,'PR','Leópolis',16),(2947,'PR','Lidianópolis',16),(2948,'PR','Lindoeste',16),(2949,'PR','Loanda',16),(2950,'PR','Lobato',16),(2951,'PR','Londrina',16),(2952,'PR','Luiziana',16),(2953,'PR','Lunardelli',16),(2954,'PR','Lupionópolis',16),(2955,'PR','Mallet',16),(2956,'PR','Mamborê',16),(2957,'PR','Mandaguaçu',16),(2958,'PR','Mandaguari',16),(2959,'PR','Mandirituba',16),(2960,'PR','Manfrinópolis',16),(2961,'PR','Mangueirinha',16),(2962,'PR','Manoel Ribas',16),(2963,'PR','Marechal Cândido Rondon',16),(2964,'PR','Maria Helena',16),(2965,'PR','Marialva',16),(2966,'PR','Marilândia do Sul',16),(2967,'PR','Marilena',16),(2968,'PR','Mariluz',16),(2969,'PR','Maringá',16),(2970,'PR','Mariópolis',16),(2971,'PR','Maripá',16),(2972,'PR','Marmeleiro',16),(2973,'PR','Marquinho',16),(2974,'PR','Marumbi',16),(2975,'PR','Matelândia',16),(2976,'PR','Matinhos',16),(2977,'PR','Mato Rico',16),(2978,'PR','Mauá da Serra',16),(2979,'PR','Medianeira',16),(2980,'PR','Mercedes',16),(2981,'PR','Mirador',16),(2982,'PR','Miraselva',16),(2983,'PR','Missal',16),(2984,'PR','Moreira Sales',16),(2985,'PR','Morretes',16),(2986,'PR','Munhoz de Melo',16),(2987,'PR','Nossa Senhora das Graças',16),(2988,'PR','Nova Aliança do Ivaí',16),(2989,'PR','Nova América da Colina',16),(2990,'PR','Nova Aurora',16),(2991,'PR','Nova Cantu',16),(2992,'PR','Nova Esperança',16),(2993,'PR','Nova Esperança do Sudoeste',16),(2994,'PR','Nova Fátima',16),(2995,'PR','Nova Laranjeiras',16),(2996,'PR','Nova Londrina',16),(2997,'PR','Nova Olímpia',16),(2998,'PR','Nova Prata do Iguaçu',16),(2999,'PR','Nova Santa Bárbara',16),(3000,'PR','Nova Santa Rosa',16),(3001,'PR','Nova Tebas',16),(3002,'PR','Novo Itacolomi',16),(3003,'PR','Ortigueira',16),(3004,'PR','Ourizona',16),(3005,'PR','Ouro Verde do Oeste',16),(3006,'PR','Paiçandu',16),(3007,'PR','Palmas',16),(3008,'PR','Palmeira',16),(3009,'PR','Palmital',16),(3010,'PR','Palotina',16),(3011,'PR','Paraíso do Norte',16),(3012,'PR','Paranacity',16),(3013,'PR','Paranaguá',16),(3014,'PR','Paranapoema',16),(3015,'PR','Paranavaí',16),(3016,'PR','Pato Bragado',16),(3017,'PR','Pato Branco',16),(3018,'PR','Paula Freitas',16),(3019,'PR','Paulo Frontin',16),(3020,'PR','Peabiru',16),(3021,'PR','Perobal',16),(3022,'PR','Pérola',16),(3023,'PR','Pérola dOeste',16),(3024,'PR','Piên',16),(3025,'PR','Pinhais',16),(3026,'PR','Pinhal de São Bento',16),(3027,'PR','Pinhalão',16),(3028,'PR','Pinhão',16),(3029,'PR','Piraí do Sul',16),(3030,'PR','Piraquara',16),(3031,'PR','Pitanga',16),(3032,'PR','Pitangueiras',16),(3033,'PR','Planaltina do Paraná',16),(3034,'PR','Planalto',16),(3035,'PR','Ponta Grossa',16),(3036,'PR','Pontal do Paraná',16),(3037,'PR','Porecatu',16),(3038,'PR','Porto Amazonas',16),(3039,'PR','Porto Barreiro',16),(3040,'PR','Porto Rico',16),(3041,'PR','Porto Vitória',16),(3042,'PR','Prado Ferreira',16),(3043,'PR','Pranchita',16),(3044,'PR','Presidente Castelo Branco',16),(3045,'PR','Primeiro de Maio',16),(3046,'PR','Prudentópolis',16),(3047,'PR','Quarto Centenário',16),(3048,'PR','Quatiguá',16),(3049,'PR','Quatro Barras',16),(3050,'PR','Quatro Pontes',16),(3051,'PR','Quedas do Iguaçu',16),(3052,'PR','Querência do Norte',16),(3053,'PR','Quinta do Sol',16),(3054,'PR','Quitandinha',16),(3055,'PR','Ramilândia',16),(3056,'PR','Rancho Alegre',16),(3057,'PR','Rancho Alegre dOeste',16),(3058,'PR','Realeza',16),(3059,'PR','Rebouças',16),(3060,'PR','Renascença',16),(3061,'PR','Reserva',16),(3062,'PR','Reserva do Iguaçu',16),(3063,'PR','Ribeirão Claro',16),(3064,'PR','Ribeirão do Pinhal',16),(3065,'PR','Rio Azul',16),(3066,'PR','Rio Bom',16),(3067,'PR','Rio Bonito do Iguaçu',16),(3068,'PR','Rio Branco do Ivaí',16),(3069,'PR','Rio Branco do Sul',16),(3070,'PR','Rio Negro',16),(3071,'PR','Rolândia',16),(3072,'PR','Roncador',16),(3073,'PR','Rondon',16),(3074,'PR','Rosário do Ivaí',16),(3075,'PR','Sabáudia',16),(3076,'PR','Salgado Filho',16),(3077,'PR','Salto do Itararé',16),(3078,'PR','Salto do Lontra',16),(3079,'PR','Santa Amélia',16),(3080,'PR','Santa Cecília do Pavão',16),(3081,'PR','Santa Cruz de Monte Castelo',16),(3082,'PR','Santa Fé',16),(3083,'PR','Santa Helena',16),(3084,'PR','Santa Inês',16),(3085,'PR','Santa Isabel do Ivaí',16),(3086,'PR','Santa Izabel do Oeste',16),(3087,'PR','Santa Lúcia',16),(3088,'PR','Santa Maria do Oeste',16),(3089,'PR','Santa Mariana',16),(3090,'PR','Santa Mônica',16),(3091,'PR','Santa Tereza do Oeste',16),(3092,'PR','Santa Terezinha de Itaipu',16),(3093,'PR','Santana do Itararé',16),(3094,'PR','Santo Antônio da Platina',16),(3095,'PR','Santo Antônio do Caiuá',16),(3096,'PR','Santo Antônio do Paraíso',16),(3097,'PR','Santo Antônio do Sudoeste',16),(3098,'PR','Santo Inácio',16),(3099,'PR','São Carlos do Ivaí',16),(3100,'PR','São Jerônimo da Serra',16),(3101,'PR','São João',16),(3102,'PR','São João do Caiuá',16),(3103,'PR','São João do Ivaí',16),(3104,'PR','São João do Triunfo',16),(3105,'PR','São Jorge dOeste',16),(3106,'PR','São Jorge do Ivaí',16),(3107,'PR','São Jorge do Patrocínio',16),(3108,'PR','São José da Boa Vista',16),(3109,'PR','São José das Palmeiras',16),(3110,'PR','São José dos Pinhais',16),(3111,'PR','São Manoel do Paraná',16),(3112,'PR','São Mateus do Sul',16),(3113,'PR','São Miguel do Iguaçu',16),(3114,'PR','São Pedro do Iguaçu',16),(3115,'PR','São Pedro do Ivaí',16),(3116,'PR','São Pedro do Paraná',16),(3117,'PR','São Sebastião da Amoreira',16),(3118,'PR','São Tomé',16),(3119,'PR','Sapopema',16),(3120,'PR','Sarandi',16),(3121,'PR','Saudade do Iguaçu',16),(3122,'PR','Sengés',16),(3123,'PR','Serranópolis do Iguaçu',16),(3124,'PR','Sertaneja',16),(3125,'PR','Sertanópolis',16),(3126,'PR','Siqueira Campos',16),(3127,'PR','Sulina',16),(3128,'PR','Tamarana',16),(3129,'PR','Tamboara',16),(3130,'PR','Tapejara',16),(3131,'PR','Tapira',16),(3132,'PR','Teixeira Soares',16),(3133,'PR','Telêmaco Borba',16),(3134,'PR','Terra Boa',16),(3135,'PR','Terra Rica',16),(3136,'PR','Terra Roxa',16),(3137,'PR','Tibagi',16),(3138,'PR','Tijucas do Sul',16),(3139,'PR','Toledo',16),(3140,'PR','Tomazina',16),(3141,'PR','Três Barras do Paraná',16),(3142,'PR','Tunas do Paraná',16),(3143,'PR','Tuneiras do Oeste',16),(3144,'PR','Tupãssi',16),(3145,'PR','Turvo',16),(3146,'PR','Ubiratã',16),(3147,'PR','Umuarama',16),(3148,'PR','União da Vitória',16),(3149,'PR','Uniflor',16),(3150,'PR','Uraí',16),(3151,'PR','Ventania',16),(3152,'PR','Vera Cruz do Oeste',16),(3153,'PR','Verê',16),(3154,'PR','Vila Alta',16),(3155,'PR','Virmond',16),(3156,'PR','Vitorino',16),(3157,'PR','Wenceslau Braz',16),(3158,'PR','Xambrê',16),(3159,'PE','Abreu e Lima',17),(3160,'PE','Afogados da Ingazeira',17),(3161,'PE','Afrânio',17),(3162,'PE','Agrestina',17),(3163,'PE','Água Preta',17),(3164,'PE','Águas Belas',17),(3165,'PE','Alagoinha',17),(3166,'PE','Aliança',17),(3167,'PE','Altinho',17),(3168,'PE','Amaraji',17),(3169,'PE','Angelim',17),(3170,'PE','Araçoiaba',17),(3171,'PE','Araripina',17),(3172,'PE','Arcoverde',17),(3173,'PE','Barra de Guabiraba',17),(3174,'PE','Barreiros',17),(3175,'PE','Belém de Maria',17),(3176,'PE','Belém de São Francisco',17),(3177,'PE','Belo Jardim',17),(3178,'PE','Betânia',17),(3179,'PE','Bezerros',17),(3180,'PE','Bodocó',17),(3181,'PE','Bom Conselho',17),(3182,'PE','Bom Jardim',17),(3183,'PE','Bonito',17),(3184,'PE','Brejão',17),(3185,'PE','Brejinho',17),(3186,'PE','Brejo da Madre de Deus',17),(3187,'PE','Buenos Aires',17),(3188,'PE','Buíque',17),(3189,'PE','Cabo de Santo Agostinho',17),(3190,'PE','Cabrobó',17),(3191,'PE','Cachoeirinha',17),(3192,'PE','Caetés',17),(3193,'PE','Calçado',17),(3194,'PE','Calumbi',17),(3195,'PE','Camaragibe',17),(3196,'PE','Camocim de São Félix',17),(3197,'PE','Camutanga',17),(3198,'PE','Canhotinho',17),(3199,'PE','Capoeiras',17),(3200,'PE','Carnaíba',17),(3201,'PE','Carnaubeira da Penha',17),(3202,'PE','Carpina',17),(3203,'PE','Caruaru',17),(3204,'PE','Casinhas',17),(3205,'PE','Catende',17),(3206,'PE','Cedro',17),(3207,'PE','Chã de Alegria',17),(3208,'PE','Chã Grande',17),(3209,'PE','Condado',17),(3210,'PE','Correntes',17),(3211,'PE','Cortês',17),(3212,'PE','Cumaru',17),(3213,'PE','Cupira',17),(3214,'PE','Custódia',17),(3215,'PE','Dormentes',17),(3216,'PE','Escada',17),(3217,'PE','Exu',17),(3218,'PE','Feira Nova',17),(3219,'PE','Fernando de Noronha',17),(3220,'PE','Ferreiros',17),(3221,'PE','Flores',17),(3222,'PE','Floresta',17),(3223,'PE','Frei Miguelinho',17),(3224,'PE','Gameleira',17),(3225,'PE','Garanhuns',17),(3226,'PE','Glória do Goitá',17),(3227,'PE','Goiana',17),(3228,'PE','Granito',17),(3229,'PE','Gravatá',17),(3230,'PE','Iati',17),(3231,'PE','Ibimirim',17),(3232,'PE','Ibirajuba',17),(3233,'PE','Igarassu',17),(3234,'PE','Iguaraci',17),(3235,'PE','Inajá',17),(3236,'PE','Ingazeira',17),(3237,'PE','Ipojuca',17),(3238,'PE','Ipubi',17),(3239,'PE','Itacuruba',17),(3240,'PE','Itaíba',17),(3241,'PE','Itamaracá',17),(3242,'PE','Itambé',17),(3243,'PE','Itapetim',17),(3244,'PE','Itapissuma',17),(3245,'PE','Itaquitinga',17),(3246,'PE','Jaboatão dos Guararapes',17),(3247,'PE','Jaqueira',17),(3248,'PE','Jataúba',17),(3249,'PE','Jatobá',17),(3250,'PE','João Alfredo',17),(3251,'PE','Joaquim Nabuco',17),(3252,'PE','Jucati',17),(3253,'PE','Jupi',17),(3254,'PE','Jurema',17),(3255,'PE','Lagoa do Carro',17),(3256,'PE','Lagoa do Itaenga',17),(3257,'PE','Lagoa do Ouro',17),(3258,'PE','Lagoa dos Gatos',17),(3259,'PE','Lagoa Grande',17),(3260,'PE','Lajedo',17),(3261,'PE','Limoeiro',17),(3262,'PE','Macaparana',17),(3263,'PE','Machados',17),(3264,'PE','Manari',17),(3265,'PE','Maraial',17),(3266,'PE','Mirandiba',17),(3267,'PE','Moreilândia',17),(3268,'PE','Moreno',17),(3269,'PE','Nazaré da Mata',17),(3270,'PE','Olinda',17),(3271,'PE','Orobó',17),(3272,'PE','Orocó',17),(3273,'PE','Ouricuri',17),(3274,'PE','Palmares',17),(3275,'PE','Palmeirina',17),(3276,'PE','Panelas',17),(3277,'PE','Paranatama',17),(3278,'PE','Parnamirim',17),(3279,'PE','Passira',17),(3280,'PE','Paudalho',17),(3281,'PE','Paulista',17),(3282,'PE','Pedra',17),(3283,'PE','Pesqueira',17),(3284,'PE','Petrolândia',17),(3285,'PE','Petrolina',17),(3286,'PE','Poção',17),(3287,'PE','Pombos',17),(3288,'PE','Primavera',17),(3289,'PE','Quipapá',17),(3290,'PE','Quixaba',17),(3291,'PE','Recife',17),(3292,'PE','Riacho das Almas',17),(3293,'PE','Ribeirão',17),(3294,'PE','Rio Formoso',17),(3295,'PE','Sairé',17),(3296,'PE','Salgadinho',17),(3297,'PE','Salgueiro',17),(3298,'PE','Saloá',17),(3299,'PE','Sanharó',17),(3300,'PE','Santa Cruz',17),(3301,'PE','Santa Cruz da Baixa Verde',17),(3302,'PE','Santa Cruz do Capibaribe',17),(3303,'PE','Santa Filomena',17),(3304,'PE','Santa Maria da Boa Vista',17),(3305,'PE','Santa Maria do Cambucá',17),(3306,'PE','Santa Terezinha',17),(3307,'PE','São Benedito do Sul',17),(3308,'PE','São Bento do Una',17),(3309,'PE','São Caitano',17),(3310,'PE','São João',17),(3311,'PE','São Joaquim do Monte',17),(3312,'PE','São José da Coroa Grande',17),(3313,'PE','São José do Belmonte',17),(3314,'PE','São José do Egito',17),(3315,'PE','São Lourenço da Mata',17),(3316,'PE','São Vicente Ferrer',17),(3317,'PE','Serra Talhada',17),(3318,'PE','Serrita',17),(3319,'PE','Sertânia',17),(3320,'PE','Sirinhaém',17),(3321,'PE','Solidão',17),(3322,'PE','Surubim',17),(3323,'PE','Tabira',17),(3324,'PE','Tacaimbó',17),(3325,'PE','Tacaratu',17),(3326,'PE','Tamandaré',17),(3327,'PE','Taquaritinga do Norte',17),(3328,'PE','Terezinha',17),(3329,'PE','Terra Nova',17),(3330,'PE','Timbaúba',17),(3331,'PE','Toritama',17),(3332,'PE','Tracunhaém',17),(3333,'PE','Trindade',17),(3334,'PE','Triunfo',17),(3335,'PE','Tupanatinga',17),(3336,'PE','Tuparetama',17),(3337,'PE','Venturosa',17),(3338,'PE','Verdejante',17),(3339,'PE','Vertente do Lério',17),(3340,'PE','Vertentes',17),(3341,'PE','Vicência',17),(3342,'PE','Vitória de Santo Antão',17),(3343,'PE','Xexéu',17),(3344,'PI','Acauã',18),(3345,'PI','Agricolândia',18),(3346,'PI','Água Branca',18),(3347,'PI','Alagoinha do Piauí',18),(3348,'PI','Alegrete do Piauí',18),(3349,'PI','Alto Longá',18),(3350,'PI','Altos',18),(3351,'PI','Alvorada do Gurguéia',18),(3352,'PI','Amarante',18),(3353,'PI','Angical do Piauí',18),(3354,'PI','Anísio de Abreu',18),(3355,'PI','Antônio Almeida',18),(3356,'PI','Aroazes',18),(3357,'PI','Arraial',18),(3358,'PI','Assunção do Piauí',18),(3359,'PI','Avelino Lopes',18),(3360,'PI','Baixa Grande do Ribeiro',18),(3361,'PI','Barra dAlcântara',18),(3362,'PI','Barras',18),(3363,'PI','Barreiras do Piauí',18),(3364,'PI','Barro Duro',18),(3365,'PI','Batalha',18),(3366,'PI','Bela Vista do Piauí',18),(3367,'PI','Belém do Piauí',18),(3368,'PI','Beneditinos',18),(3369,'PI','Bertolínia',18),(3370,'PI','Betânia do Piauí',18),(3371,'PI','Boa Hora',18),(3372,'PI','Bocaina',18),(3373,'PI','Bom Jesus',18),(3374,'PI','Bom Princípio do Piauí',18),(3375,'PI','Bonfim do Piauí',18),(3376,'PI','Boqueirão do Piauí',18),(3377,'PI','Brasileira',18),(3378,'PI','Brejo do Piauí',18),(3379,'PI','Buriti dos Lopes',18),(3380,'PI','Buriti dos Montes',18),(3381,'PI','Cabeceiras do Piauí',18),(3382,'PI','Cajazeiras do Piauí',18),(3383,'PI','Cajueiro da Praia',18),(3384,'PI','Caldeirão Grande do Piauí',18),(3385,'PI','Campinas do Piauí',18),(3386,'PI','Campo Alegre do Fidalgo',18),(3387,'PI','Campo Grande do Piauí',18),(3388,'PI','Campo Largo do Piauí',18),(3389,'PI','Campo Maior',18),(3390,'PI','Canavieira',18),(3391,'PI','Canto do Buriti',18),(3392,'PI','Capitão de Campos',18),(3393,'PI','Capitão Gervásio Oliveira',18),(3394,'PI','Caracol',18),(3395,'PI','Caraúbas do Piauí',18),(3396,'PI','Caridade do Piauí',18),(3397,'PI','Castelo do Piauí',18),(3398,'PI','Caxingó',18),(3399,'PI','Cocal',18),(3400,'PI','Cocal de Telha',18),(3401,'PI','Cocal dos Alves',18),(3402,'PI','Coivaras',18),(3403,'PI','Colônia do Gurguéia',18),(3404,'PI','Colônia do Piauí',18),(3405,'PI','Conceição do Canindé',18),(3406,'PI','Coronel José Dias',18),(3407,'PI','Corrente',18),(3408,'PI','Cristalândia do Piauí',18),(3409,'PI','Cristino Castro',18),(3410,'PI','Curimatá',18),(3411,'PI','Currais',18),(3412,'PI','Curral Novo do Piauí',18),(3413,'PI','Curralinhos',18),(3414,'PI','Demerval Lobão',18),(3415,'PI','Dirceu Arcoverde',18),(3416,'PI','Dom Expedito Lopes',18),(3417,'PI','Dom Inocêncio',18),(3418,'PI','Domingos Mourão',18),(3419,'PI','Elesbão Veloso',18),(3420,'PI','Eliseu Martins',18),(3421,'PI','Esperantina',18),(3422,'PI','Fartura do Piauí',18),(3423,'PI','Flores do Piauí',18),(3424,'PI','Floresta do Piauí',18),(3425,'PI','Floriano',18),(3426,'PI','Francinópolis',18),(3427,'PI','Francisco Ayres',18),(3428,'PI','Francisco Macedo',18),(3429,'PI','Francisco Santos',18),(3430,'PI','Fronteiras',18),(3431,'PI','Geminiano',18),(3432,'PI','Gilbués',18),(3433,'PI','Guadalupe',18),(3434,'PI','Guaribas',18),(3435,'PI','Hugo Napoleão',18),(3436,'PI','Ilha Grande',18),(3437,'PI','Inhuma',18),(3438,'PI','Ipiranga do Piauí',18),(3439,'PI','Isaías Coelho',18),(3440,'PI','Itainópolis',18),(3441,'PI','Itaueira',18),(3442,'PI','Jacobina do Piauí',18),(3443,'PI','Jaicós',18),(3444,'PI','Jardim do Mulato',18),(3445,'PI','Jatobá do Piauí',18),(3446,'PI','Jerumenha',18),(3447,'PI','João Costa',18),(3448,'PI','Joaquim Pires',18),(3449,'PI','Joca Marques',18),(3450,'PI','José de Freitas',18),(3451,'PI','Juazeiro do Piauí',18),(3452,'PI','Júlio Borges',18),(3453,'PI','Jurema',18),(3454,'PI','Lagoa Alegre',18),(3455,'PI','Lagoa de São Francisco',18),(3456,'PI','Lagoa do Barro do Piauí',18),(3457,'PI','Lagoa do Piauí',18),(3458,'PI','Lagoa do Sítio',18),(3459,'PI','Lagoinha do Piauí',18),(3460,'PI','Landri Sales',18),(3461,'PI','Luís Correia',18),(3462,'PI','Luzilândia',18),(3463,'PI','Madeiro',18),(3464,'PI','Manoel Emídio',18),(3465,'PI','Marcolândia',18),(3466,'PI','Marcos Parente',18),(3467,'PI','Massapê do Piauí',18),(3468,'PI','Matias Olímpio',18),(3469,'PI','Miguel Alves',18),(3470,'PI','Miguel Leão',18),(3471,'PI','Milton Brandão',18),(3472,'PI','Monsenhor Gil',18),(3473,'PI','Monsenhor Hipólito',18),(3474,'PI','Monte Alegre do Piauí',18),(3475,'PI','Morro Cabeça no Tempo',18),(3476,'PI','Morro do Chapéu do Piauí',18),(3477,'PI','Murici dos Portelas',18),(3478,'PI','Nazaré do Piauí',18),(3479,'PI','Nossa Senhora de Nazaré',18),(3480,'PI','Nossa Senhora dos Remédios',18),(3481,'PI','Nova Santa Rita',18),(3482,'PI','Novo Oriente do Piauí',18),(3483,'PI','Novo Santo Antônio',18),(3484,'PI','Oeiras',18),(3485,'PI','Olho dÁgua do Piauí',18),(3486,'PI','Padre Marcos',18),(3487,'PI','Paes Landim',18),(3488,'PI','Pajeú do Piauí',18),(3489,'PI','Palmeira do Piauí',18),(3490,'PI','Palmeirais',18),(3491,'PI','Paquetá',18),(3492,'PI','Parnaguá',18),(3493,'PI','Parnaíba',18),(3494,'PI','Passagem Franca do Piauí',18),(3495,'PI','Patos do Piauí',18),(3496,'PI','Paulistana',18),(3497,'PI','Pavussu',18),(3498,'PI','Pedro II',18),(3499,'PI','Pedro Laurentino',18),(3500,'PI','Picos',18),(3501,'PI','Pimenteiras',18),(3502,'PI','Pio IX',18),(3503,'PI','Piracuruca',18),(3504,'PI','Piripiri',18),(3505,'PI','Porto',18),(3506,'PI','Porto Alegre do Piauí',18),(3507,'PI','Prata do Piauí',18),(3508,'PI','Queimada Nova',18),(3509,'PI','Redenção do Gurguéia',18),(3510,'PI','Regeneração',18),(3511,'PI','Riacho Frio',18),(3512,'PI','Ribeira do Piauí',18),(3513,'PI','Ribeiro Gonçalves',18),(3514,'PI','Rio Grande do Piauí',18),(3515,'PI','Santa Cruz do Piauí',18),(3516,'PI','Santa Cruz dos Milagres',18),(3517,'PI','Santa Filomena',18),(3518,'PI','Santa Luz',18),(3519,'PI','Santa Rosa do Piauí',18),(3520,'PI','Santana do Piauí',18),(3521,'PI','Santo Antônio de Lisboa',18),(3522,'PI','Santo Antônio dos Milagres',18),(3523,'PI','Santo Inácio do Piauí',18),(3524,'PI','São Braz do Piauí',18),(3525,'PI','São Félix do Piauí',18),(3526,'PI','São Francisco de Assis do Piauí',18),(3527,'PI','São Francisco do Piauí',18),(3528,'PI','São Gonçalo do Gurguéia',18),(3529,'PI','São Gonçalo do Piauí',18),(3530,'PI','São João da Canabrava',18),(3531,'PI','São João da Fronteira',18),(3532,'PI','São João da Serra',18),(3533,'PI','São João da Varjota',18),(3534,'PI','São João do Arraial',18),(3535,'PI','São João do Piauí',18),(3536,'PI','São José do Divino',18),(3537,'PI','São José do Peixe',18),(3538,'PI','São José do Piauí',18),(3539,'PI','São Julião',18),(3540,'PI','São Lourenço do Piauí',18),(3541,'PI','São Luis do Piauí',18),(3542,'PI','São Miguel da Baixa Grande',18),(3543,'PI','São Miguel do Fidalgo',18),(3544,'PI','São Miguel do Tapuio',18),(3545,'PI','São Pedro do Piauí',18),(3546,'PI','São Raimundo Nonato',18),(3547,'PI','Sebastião Barros',18),(3548,'PI','Sebastião Leal',18),(3549,'PI','Sigefredo Pacheco',18),(3550,'PI','Simões',18),(3551,'PI','Simplício Mendes',18),(3552,'PI','Socorro do Piauí',18),(3553,'PI','Sussuapara',18),(3554,'PI','Tamboril do Piauí',18),(3555,'PI','Tanque do Piauí',18),(3556,'PI','Teresina',18),(3557,'PI','União',18),(3558,'PI','Uruçuí',18),(3559,'PI','Valença do Piauí',18),(3560,'PI','Várzea Branca',18),(3561,'PI','Várzea Grande',18),(3562,'PI','Vera Mendes',18),(3563,'PI','Vila Nova do Piauí',18),(3564,'PI','Wall Ferraz',18),(3565,'RJ','Angra dos Reis',19),(3566,'RJ','Aperibé',19),(3567,'RJ','Araruama',19),(3568,'RJ','Areal',19),(3569,'RJ','Armação dos Búzios',19),(3570,'RJ','Arraial do Cabo',19),(3571,'RJ','Barra do Piraí',19),(3572,'RJ','Barra Mansa',19),(3573,'RJ','Belford Roxo',19),(3574,'RJ','Bom Jardim',19),(3575,'RJ','Bom Jesus do Itabapoana',19),(3576,'RJ','Cabo Frio',19),(3577,'RJ','Cachoeiras de Macacu',19),(3578,'RJ','Cambuci',19),(3579,'RJ','Campos dos Goytacazes',19),(3580,'RJ','Cantagalo',19),(3581,'RJ','Carapebus',19),(3582,'RJ','Cardoso Moreira',19),(3583,'RJ','Carmo',19),(3584,'RJ','Casimiro de Abreu',19),(3585,'RJ','Comendador Levy Gasparian',19),(3586,'RJ','Conceição de Macabu',19),(3587,'RJ','Cordeiro',19),(3588,'RJ','Duas Barras',19),(3589,'RJ','Duque de Caxias',19),(3590,'RJ','Engenheiro Paulo de Frontin',19),(3591,'RJ','Guapimirim',19),(3592,'RJ','Iguaba Grande',19),(3593,'RJ','Itaboraí',19),(3594,'RJ','Itaguaí',19),(3595,'RJ','Italva',19),(3596,'RJ','Itaocara',19),(3597,'RJ','Itaperuna',19),(3598,'RJ','Itatiaia',19),(3599,'RJ','Japeri',19),(3600,'RJ','Laje do Muriaé',19),(3601,'RJ','Macaé',19),(3602,'RJ','Macuco',19),(3603,'RJ','Magé',19),(3604,'RJ','Mangaratiba',19),(3605,'RJ','Maricá',19),(3606,'RJ','Mendes',19),(3607,'RJ','Miguel Pereira',19),(3608,'RJ','Miracema',19),(3609,'RJ','Natividade',19),(3610,'RJ','Nilópolis',19),(3611,'RJ','Niterói',19),(3612,'RJ','Nova Friburgo',19),(3613,'RJ','Nova Iguaçu',19),(3614,'RJ','Paracambi',19),(3615,'RJ','Paraíba do Sul',19),(3616,'RJ','Parati',19),(3617,'RJ','Paty do Alferes',19),(3618,'RJ','Petrópolis',19),(3619,'RJ','Pinheiral',19),(3620,'RJ','Piraí',19),(3621,'RJ','Porciúncula',19),(3622,'RJ','Porto Real',19),(3623,'RJ','Quatis',19),(3624,'RJ','Queimados',19),(3625,'RJ','Quissamã',19),(3626,'RJ','Resende',19),(3627,'RJ','Rio Bonito',19),(3628,'RJ','Rio Claro',19),(3629,'RJ','Rio das Flores',19),(3630,'RJ','Rio das Ostras',19),(3631,'RJ','Rio de Janeiro',19),(3632,'RJ','Santa Maria Madalena',19),(3633,'RJ','Santo Antônio de Pádua',19),(3634,'RJ','São Fidélis',19),(3635,'RJ','São Francisco de Itabapoana',19),(3636,'RJ','São Gonçalo',19),(3637,'RJ','São João da Barra',19),(3638,'RJ','São João de Meriti',19),(3639,'RJ','São José de Ubá',19),(3640,'RJ','São José do Vale do Rio Preto',19),(3641,'RJ','São Pedro da Aldeia',19),(3642,'RJ','São Sebastião do Alto',19),(3643,'RJ','Sapucaia',19),(3644,'RJ','Saquarema',19),(3645,'RJ','Seropédica',19),(3646,'RJ','Silva Jardim',19),(3647,'RJ','Sumidouro',19),(3648,'RJ','Tanguá',19),(3649,'RJ','Teresópolis',19),(3650,'RJ','Trajano de Morais',19),(3651,'RJ','Três Rios',19),(3652,'RJ','Valença',19),(3653,'RJ','Varre-Sai',19),(3654,'RJ','Vassouras',19),(3655,'RJ','Volta Redonda',19),(3656,'RN','Acari',20),(3657,'RN','Açu',20),(3658,'RN','Afonso Bezerra',20),(3659,'RN','Água Nova',20),(3660,'RN','Alexandria',20),(3661,'RN','Almino Afonso',20),(3662,'RN','Alto do Rodrigues',20),(3663,'RN','Angicos',20),(3664,'RN','Antônio Martins',20),(3665,'RN','Apodi',20),(3666,'RN','Areia Branca',20),(3667,'RN','Arês',20),(3668,'RN','Augusto Severo',20),(3669,'RN','Baía Formosa',20),(3670,'RN','Baraúna',20),(3671,'RN','Barcelona',20),(3672,'RN','Bento Fernandes',20),(3673,'RN','Bodó',20),(3674,'RN','Bom Jesus',20),(3675,'RN','Brejinho',20),(3676,'RN','Caiçara do Norte',20),(3677,'RN','Caiçara do Rio do Vento',20),(3678,'RN','Caicó',20),(3679,'RN','Campo Redondo',20),(3680,'RN','Canguaretama',20),(3681,'RN','Caraúbas',20),(3682,'RN','Carnaúba dos Dantas',20),(3683,'RN','Carnaubais',20),(3684,'RN','Ceará-Mirim',20),(3685,'RN','Cerro Corá',20),(3686,'RN','Coronel Ezequiel',20),(3687,'RN','Coronel João Pessoa',20),(3688,'RN','Cruzeta',20),(3689,'RN','Currais Novos',20),(3690,'RN','Doutor Severiano',20),(3691,'RN','Encanto',20),(3692,'RN','Equador',20),(3693,'RN','Espírito Santo',20),(3694,'RN','Extremoz',20),(3695,'RN','Felipe Guerra',20),(3696,'RN','Fernando Pedroza',20),(3697,'RN','Florânia',20),(3698,'RN','Francisco Dantas',20),(3699,'RN','Frutuoso Gomes',20),(3700,'RN','Galinhos',20),(3701,'RN','Goianinha',20),(3702,'RN','Governador Dix-Sept Rosado',20),(3703,'RN','Grossos',20),(3704,'RN','Guamaré',20),(3705,'RN','Ielmo Marinho',20),(3706,'RN','Ipanguaçu',20),(3707,'RN','Ipueira',20),(3708,'RN','Itajá',20),(3709,'RN','Itaú',20),(3710,'RN','Jaçanã',20),(3711,'RN','Jandaíra',20),(3712,'RN','Janduís',20),(3713,'RN','Januário Cicco',20),(3714,'RN','Japi',20),(3715,'RN','Jardim de Angicos',20),(3716,'RN','Jardim de Piranhas',20),(3717,'RN','Jardim do Seridó',20),(3718,'RN','João Câmara',20),(3719,'RN','João Dias',20),(3720,'RN','José da Penha',20),(3721,'RN','Jucurutu',20),(3722,'RN','Lagoa dAnta',20),(3723,'RN','Lagoa de Pedras',20),(3724,'RN','Lagoa de Velhos',20),(3725,'RN','Lagoa Nova',20),(3726,'RN','Lagoa Salgada',20),(3727,'RN','Lajes',20),(3728,'RN','Lajes Pintadas',20),(3729,'RN','Lucrécia',20),(3730,'RN','Luís Gomes',20),(3731,'RN','Macaíba',20),(3732,'RN','Macau',20),(3733,'RN','Major Sales',20),(3734,'RN','Marcelino Vieira',20),(3735,'RN','Martins',20),(3736,'RN','Maxaranguape',20),(3737,'RN','Messias Targino',20),(3738,'RN','Montanhas',20),(3739,'RN','Monte Alegre',20),(3740,'RN','Monte das Gameleiras',20),(3741,'RN','Mossoró',20),(3742,'RN','Natal',20),(3743,'RN','Nísia Floresta',20),(3744,'RN','Nova Cruz',20),(3745,'RN','Olho-dÁgua do Borges',20),(3746,'RN','Ouro Branco',20),(3747,'RN','Paraná',20),(3748,'RN','Paraú',20),(3749,'RN','Parazinho',20),(3750,'RN','Parelhas',20),(3751,'RN','Parnamirim',20),(3752,'RN','Passa e Fica',20),(3753,'RN','Passagem',20),(3754,'RN','Patu',20),(3755,'RN','Pau dos Ferros',20),(3756,'RN','Pedra Grande',20),(3757,'RN','Pedra Preta',20),(3758,'RN','Pedro Avelino',20),(3759,'RN','Pedro Velho',20),(3760,'RN','Pendências',20),(3761,'RN','Pilões',20),(3762,'RN','Poço Branco',20),(3763,'RN','Portalegre',20),(3764,'RN','Porto do Mangue',20),(3765,'RN','Presidente Juscelino',20),(3766,'RN','Pureza',20),(3767,'RN','Rafael Fernandes',20),(3768,'RN','Rafael Godeiro',20),(3769,'RN','Riacho da Cruz',20),(3770,'RN','Riacho de Santana',20),(3771,'RN','Riachuelo',20),(3772,'RN','Rio do Fogo',20),(3773,'RN','Rodolfo Fernandes',20),(3774,'RN','Ruy Barbosa',20),(3775,'RN','Santa Cruz',20),(3776,'RN','Santa Maria',20),(3777,'RN','Santana do Matos',20),(3778,'RN','Santana do Seridó',20),(3779,'RN','Santo Antônio',20),(3780,'RN','São Bento do Norte',20),(3781,'RN','São Bento do Trairí',20),(3782,'RN','São Fernando',20),(3783,'RN','São Francisco do Oeste',20),(3784,'RN','São Gonçalo do Amarante',20),(3785,'RN','São João do Sabugi',20),(3786,'RN','São José de Mipibu',20),(3787,'RN','São José do Campestre',20),(3788,'RN','São José do Seridó',20),(3789,'RN','São Miguel',20),(3790,'RN','São Miguel de Touros',20),(3791,'RN','São Paulo do Potengi',20),(3792,'RN','São Pedro',20),(3793,'RN','São Rafael',20),(3794,'RN','São Tomé',20),(3795,'RN','São Vicente',20),(3796,'RN','Senador Elói de Souza',20),(3797,'RN','Senador Georgino Avelino',20),(3798,'RN','Serra de São Bento',20),(3799,'RN','Serra do Mel',20),(3800,'RN','Serra Negra do Norte',20),(3801,'RN','Serrinha',20),(3802,'RN','Serrinha dos Pintos',20),(3803,'RN','Severiano Melo',20),(3804,'RN','Sítio Novo',20),(3805,'RN','Taboleiro Grande',20),(3806,'RN','Taipu',20),(3807,'RN','Tangará',20),(3808,'RN','Tenente Ananias',20),(3809,'RN','Tenente Laurentino Cruz',20),(3810,'RN','Tibau',20),(3811,'RN','Tibau do Sul',20),(3812,'RN','Timbaúba dos Batistas',20),(3813,'RN','Touros',20),(3814,'RN','Triunfo Potiguar',20),(3815,'RN','Umarizal',20),(3816,'RN','Upanema',20),(3817,'RN','Várzea',20),(3818,'RN','Venha-Ver',20),(3819,'RN','Vera Cruz',20),(3820,'RN','Viçosa',20),(3821,'RN','Vila Flor',20),(3822,'RS','Água Santa',21),(3823,'RS','Agudo',21),(3824,'RS','Ajuricaba',21),(3825,'RS','Alecrim',21),(3826,'RS','Alegrete',21),(3827,'RS','Alegria',21),(3828,'RS','Alpestre',21),(3829,'RS','Alto Alegre',21),(3830,'RS','Alto Feliz',21),(3831,'RS','Alvorada',21),(3832,'RS','Amaral Ferrador',21),(3833,'RS','Ametista do Sul',21),(3834,'RS','André da Rocha',21),(3835,'RS','Anta Gorda',21),(3836,'RS','Antônio Prado',21),(3837,'RS','Arambaré',21),(3838,'RS','Araricá',21),(3839,'RS','Aratiba',21),(3840,'RS','Arroio do Meio',21),(3841,'RS','Arroio do Sal',21),(3842,'RS','Arroio do Tigre',21),(3843,'RS','Arroio dos Ratos',21),(3844,'RS','Arroio Grande',21),(3845,'RS','Arvorezinha',21),(3846,'RS','Augusto Pestana',21),(3847,'RS','Áurea',21),(3848,'RS','Bagé',21),(3849,'RS','Balneário Pinhal',21),(3850,'RS','Barão',21),(3851,'RS','Barão de Cotegipe',21),(3852,'RS','Barão do Triunfo',21),(3853,'RS','Barra do Guarita',21),(3854,'RS','Barra do Quaraí',21),(3855,'RS','Barra do Ribeiro',21),(3856,'RS','Barra do Rio Azul',21),(3857,'RS','Barra Funda',21),(3858,'RS','Barracão',21),(3859,'RS','Barros Cassal',21),(3860,'RS','Benjamin Constant do Sul',21),(3861,'RS','Bento Gonçalves',21),(3862,'RS','Boa Vista das Missões',21),(3863,'RS','Boa Vista do Buricá',21),(3864,'RS','Boa Vista do Sul',21),(3865,'RS','Bom Jesus',21),(3866,'RS','Bom Princípio',21),(3867,'RS','Bom Progresso',21),(3868,'RS','Bom Retiro do Sul',21),(3869,'RS','Boqueirão do Leão',21),(3870,'RS','Bossoroca',21),(3871,'RS','Braga',21),(3872,'RS','Brochier',21),(3873,'RS','Butiá',21),(3874,'RS','Caçapava do Sul',21),(3875,'RS','Cacequi',21),(3876,'RS','Cachoeira do Sul',21),(3877,'RS','Cachoeirinha',21),(3878,'RS','Cacique Doble',21),(3879,'RS','Caibaté',21),(3880,'RS','Caiçara',21),(3881,'RS','Camaquã',21),(3882,'RS','Camargo',21),(3883,'RS','Cambará do Sul',21),(3884,'RS','Campestre da Serra',21),(3885,'RS','Campina das Missões',21),(3886,'RS','Campinas do Sul',21),(3887,'RS','Campo Bom',21),(3888,'RS','Campo Novo',21),(3889,'RS','Campos Borges',21),(3890,'RS','Candelária',21),(3891,'RS','Cândido Godói',21),(3892,'RS','Candiota',21),(3893,'RS','Canela',21),(3894,'RS','Canguçu',21),(3895,'RS','Canoas',21),(3896,'RS','Capão da Canoa',21),(3897,'RS','Capão do Leão',21),(3898,'RS','Capela de Santana',21),(3899,'RS','Capitão',21),(3900,'RS','Capivari do Sul',21),(3901,'RS','Caraá',21),(3902,'RS','Carazinho',21),(3903,'RS','Carlos Barbosa',21),(3904,'RS','Carlos Gomes',21),(3905,'RS','Casca',21),(3906,'RS','Caseiros',21),(3907,'RS','Catuípe',21),(3908,'RS','Caxias do Sul',21),(3909,'RS','Centenário',21),(3910,'RS','Cerrito',21),(3911,'RS','Cerro Branco',21),(3912,'RS','Cerro Grande',21),(3913,'RS','Cerro Grande do Sul',21),(3914,'RS','Cerro Largo',21),(3915,'RS','Chapada',21),(3916,'RS','Charqueadas',21),(3917,'RS','Charrua',21),(3918,'RS','Chiapeta',21),(3919,'RS','Chuí',21),(3920,'RS','Chuvisca',21),(3921,'RS','Cidreira',21),(3922,'RS','Ciríaco',21),(3923,'RS','Colinas',21),(3924,'RS','Colorado',21),(3925,'RS','Condor',21),(3926,'RS','Constantina',21),(3927,'RS','Coqueiros do Sul',21),(3928,'RS','Coronel Barros',21),(3929,'RS','Coronel Bicaco',21),(3930,'RS','Cotiporã',21),(3931,'RS','Coxilha',21),(3932,'RS','Crissiumal',21),(3933,'RS','Cristal',21),(3934,'RS','Cristal do Sul',21),(3935,'RS','Cruz Alta',21),(3936,'RS','Cruzeiro do Sul',21),(3937,'RS','David Canabarro',21),(3938,'RS','Derrubadas',21),(3939,'RS','Dezesseis de Novembro',21),(3940,'RS','Dilermando de Aguiar',21),(3941,'RS','Dois Irmãos',21),(3942,'RS','Dois Irmãos das Missões',21),(3943,'RS','Dois Lajeados',21),(3944,'RS','Dom Feliciano',21),(3945,'RS','Dom Pedrito',21),(3946,'RS','Dom Pedro de Alcântara',21),(3947,'RS','Dona Francisca',21),(3948,'RS','Doutor Maurício Cardoso',21),(3949,'RS','Doutor Ricardo',21),(3950,'RS','Eldorado do Sul',21),(3951,'RS','Encantado',21),(3952,'RS','Encruzilhada do Sul',21),(3953,'RS','Engenho Velho',21),(3954,'RS','Entre Rios do Sul',21),(3955,'RS','Entre-Ijuís',21),(3956,'RS','Erebango',21),(3957,'RS','Erechim',21),(3958,'RS','Ernestina',21),(3959,'RS','Erval Grande',21),(3960,'RS','Erval Seco',21),(3961,'RS','Esmeralda',21),(3962,'RS','Esperança do Sul',21),(3963,'RS','Espumoso',21),(3964,'RS','Estação',21),(3965,'RS','Estância Velha',21),(3966,'RS','Esteio',21),(3967,'RS','Estrela',21),(3968,'RS','Estrela Velha',21),(3969,'RS','Eugênio de Castro',21),(3970,'RS','Fagundes Varela',21),(3971,'RS','Farroupilha',21),(3972,'RS','Faxinal do Soturno',21),(3973,'RS','Faxinalzinho',21),(3974,'RS','Fazenda Vilanova',21),(3975,'RS','Feliz',21),(3976,'RS','Flores da Cunha',21),(3977,'RS','Floriano Peixoto',21),(3978,'RS','Fontoura Xavier',21),(3979,'RS','Formigueiro',21),(3980,'RS','Fortaleza dos Valos',21),(3981,'RS','Frederico Westphalen',21),(3982,'RS','Garibaldi',21),(3983,'RS','Garruchos',21),(3984,'RS','Gaurama',21),(3985,'RS','General Câmara',21),(3986,'RS','Gentil',21),(3987,'RS','Getúlio Vargas',21),(3988,'RS','Giruá',21),(3989,'RS','Glorinha',21),(3990,'RS','Gramado',21),(3991,'RS','Gramado dos Loureiros',21),(3992,'RS','Gramado Xavier',21),(3993,'RS','Gravataí',21),(3994,'RS','Guabiju',21),(3995,'RS','Guaíba',21),(3996,'RS','Guaporé',21),(3997,'RS','Guarani das Missões',21),(3998,'RS','Harmonia',21),(3999,'RS','Herval',21),(4000,'RS','Herveiras',21),(4001,'RS','Horizontina',21),(4002,'RS','Hulha Negra',21),(4003,'RS','Humaitá',21),(4004,'RS','Ibarama',21),(4005,'RS','Ibiaçá',21),(4006,'RS','Ibiraiaras',21),(4007,'RS','Ibirapuitã',21),(4008,'RS','Ibirubá',21),(4009,'RS','Igrejinha',21),(4010,'RS','Ijuí',21),(4011,'RS','Ilópolis',21),(4012,'RS','Imbé',21),(4013,'RS','Imigrante',21),(4014,'RS','Independência',21),(4015,'RS','Inhacorá',21),(4016,'RS','Ipê',21),(4017,'RS','Ipiranga do Sul',21),(4018,'RS','Iraí',21),(4019,'RS','Itaara',21),(4020,'RS','Itacurubi',21),(4021,'RS','Itapuca',21),(4022,'RS','Itaqui',21),(4023,'RS','Itatiba do Sul',21),(4024,'RS','Ivorá',21),(4025,'RS','Ivoti',21),(4026,'RS','Jaboticaba',21),(4027,'RS','Jacutinga',21),(4028,'RS','Jaguarão',21),(4029,'RS','Jaguari',21),(4030,'RS','Jaquirana',21),(4031,'RS','Jari',21),(4032,'RS','Jóia',21),(4033,'RS','Júlio de Castilhos',21),(4034,'RS','Lagoa dos Três Cantos',21),(4035,'RS','Lagoa Vermelha',21),(4036,'RS','Lagoão',21),(4037,'RS','Lajeado',21),(4038,'RS','Lajeado do Bugre',21),(4039,'RS','Lavras do Sul',21),(4040,'RS','Liberato Salzano',21),(4041,'RS','Lindolfo Collor',21),(4042,'RS','Linha Nova',21),(4043,'RS','Maçambara',21),(4044,'RS','Machadinho',21),(4045,'RS','Mampituba',21),(4046,'RS','Manoel Viana',21),(4047,'RS','Maquiné',21),(4048,'RS','Maratá',21),(4049,'RS','Marau',21),(4050,'RS','Marcelino Ramos',21),(4051,'RS','Mariana Pimentel',21),(4052,'RS','Mariano Moro',21),(4053,'RS','Marques de Souza',21),(4054,'RS','Mata',21),(4055,'RS','Mato Castelhano',21),(4056,'RS','Mato Leitão',21),(4057,'RS','Maximiliano de Almeida',21),(4058,'RS','Minas do Leão',21),(4059,'RS','Miraguaí',21),(4060,'RS','Montauri',21),(4061,'RS','Monte Alegre dos Campos',21),(4062,'RS','Monte Belo do Sul',21),(4063,'RS','Montenegro',21),(4064,'RS','Mormaço',21),(4065,'RS','Morrinhos do Sul',21),(4066,'RS','Morro Redondo',21),(4067,'RS','Morro Reuter',21),(4068,'RS','Mostardas',21),(4069,'RS','Muçum',21),(4070,'RS','Muitos Capões',21),(4071,'RS','Muliterno',21),(4072,'RS','Não-Me-Toque',21),(4073,'RS','Nicolau Vergueiro',21),(4074,'RS','Nonoai',21),(4075,'RS','Nova Alvorada',21),(4076,'RS','Nova Araçá',21),(4077,'RS','Nova Bassano',21),(4078,'RS','Nova Boa Vista',21),(4079,'RS','Nova Bréscia',21),(4080,'RS','Nova Candelária',21),(4081,'RS','Nova Esperança do Sul',21),(4082,'RS','Nova Hartz',21),(4083,'RS','Nova Pádua',21),(4084,'RS','Nova Palma',21),(4085,'RS','Nova Petrópolis',21),(4086,'RS','Nova Prata',21),(4087,'RS','Nova Ramada',21),(4088,'RS','Nova Roma do Sul',21),(4089,'RS','Nova Santa Rita',21),(4090,'RS','Novo Barreiro',21),(4091,'RS','Novo Cabrais',21),(4092,'RS','Novo Hamburgo',21),(4093,'RS','Novo Machado',21),(4094,'RS','Novo Tiradentes',21),(4095,'RS','Osório',21),(4096,'RS','Paim Filho',21),(4097,'RS','Palmares do Sul',21),(4098,'RS','Palmeira das Missões',21),(4099,'RS','Palmitinho',21),(4100,'RS','Panambi',21),(4101,'RS','Pantano Grande',21),(4102,'RS','Paraí',21),(4103,'RS','Paraíso do Sul',21),(4104,'RS','Pareci Novo',21),(4105,'RS','Parobé',21),(4106,'RS','Passa Sete',21),(4107,'RS','Passo do Sobrado',21),(4108,'RS','Passo Fundo',21),(4109,'RS','Paverama',21),(4110,'RS','Pedro Osório',21),(4111,'RS','Pejuçara',21),(4112,'RS','Pelotas',21),(4113,'RS','Picada Café',21),(4114,'RS','Pinhal',21),(4115,'RS','Pinhal Grande',21),(4116,'RS','Pinheirinho do Vale',21),(4117,'RS','Pinheiro Machado',21),(4118,'RS','Pirapó',21),(4119,'RS','Piratini',21),(4120,'RS','Planalto',21),(4121,'RS','Poço das Antas',21),(4122,'RS','Pontão',21),(4123,'RS','Ponte Preta',21),(4124,'RS','Portão',21),(4125,'RS','Porto Alegre',21),(4126,'RS','Porto Lucena',21),(4127,'RS','Porto Mauá',21),(4128,'RS','Porto Vera Cruz',21),(4129,'RS','Porto Xavier',21),(4130,'RS','Pouso Novo',21),(4131,'RS','Presidente Lucena',21),(4132,'RS','Progresso',21),(4133,'RS','Protásio Alves',21),(4134,'RS','Putinga',21),(4135,'RS','Quaraí',21),(4136,'RS','Quevedos',21),(4137,'RS','Quinze de Novembro',21),(4138,'RS','Redentora',21),(4139,'RS','Relvado',21),(4140,'RS','Restinga Seca',21),(4141,'RS','Rio dos Índios',21),(4142,'RS','Rio Grande',21),(4143,'RS','Rio Pardo',21),(4144,'RS','Riozinho',21),(4145,'RS','Roca Sales',21),(4146,'RS','Rodeio Bonito',21),(4147,'RS','Rolante',21),(4148,'RS','Ronda Alta',21),(4149,'RS','Rondinha',21),(4150,'RS','Roque Gonzales',21),(4151,'RS','Rosário do Sul',21),(4152,'RS','Sagrada Família',21),(4153,'RS','Saldanha Marinho',21),(4154,'RS','Salto do Jacuí',21),(4155,'RS','Salvador das Missões',21),(4156,'RS','Salvador do Sul',21),(4157,'RS','Sananduva',21),(4158,'RS','Santa Bárbara do Sul',21),(4159,'RS','Santa Clara do Sul',21),(4160,'RS','Santa Cruz do Sul',21),(4161,'RS','Santa Maria',21),(4162,'RS','Santa Maria do Herval',21),(4163,'RS','Santa Rosa',21),(4164,'RS','Santa Tereza',21),(4165,'RS','Santa Vitória do Palmar',21),(4166,'RS','Santana da Boa Vista',21),(4167,'RS','Santana do Livramento',21),(4168,'RS','Santiago',21),(4169,'RS','Santo Ângelo',21),(4170,'RS','Santo Antônio da Patrulha',21),(4171,'RS','Santo Antônio das Missões',21),(4172,'RS','Santo Antônio do Palma',21),(4173,'RS','Santo Antônio do Planalto',21),(4174,'RS','Santo Augusto',21),(4175,'RS','Santo Cristo',21),(4176,'RS','Santo Expedito do Sul',21),(4177,'RS','São Borja',21),(4178,'RS','São Domingos do Sul',21),(4179,'RS','São Francisco de Assis',21),(4180,'RS','São Francisco de Paula',21),(4181,'RS','São Gabriel',21),(4182,'RS','São Jerônimo',21),(4183,'RS','São João da Urtiga',21),(4184,'RS','São João do Polêsine',21),(4185,'RS','São Jorge',21),(4186,'RS','São José das Missões',21),(4187,'RS','São José do Herval',21),(4188,'RS','São José do Hortêncio',21),(4189,'RS','São José do Inhacorá',21),(4190,'RS','São José do Norte',21),(4191,'RS','São José do Ouro',21),(4192,'RS','São José dos Ausentes',21),(4193,'RS','São Leopoldo',21),(4194,'RS','São Lourenço do Sul',21),(4195,'RS','São Luiz Gonzaga',21),(4196,'RS','São Marcos',21),(4197,'RS','São Martinho',21),(4198,'RS','São Martinho da Serra',21),(4199,'RS','São Miguel das Missões',21),(4200,'RS','São Nicolau',21),(4201,'RS','São Paulo das Missões',21),(4202,'RS','São Pedro da Serra',21),(4203,'RS','São Pedro do Butiá',21),(4204,'RS','São Pedro do Sul',21),(4205,'RS','São Sebastião do Caí',21),(4206,'RS','São Sepé',21),(4207,'RS','São Valentim',21),(4208,'RS','São Valentim do Sul',21),(4209,'RS','São Valério do Sul',21),(4210,'RS','São Vendelino',21),(4211,'RS','São Vicente do Sul',21),(4212,'RS','Sapiranga',21),(4213,'RS','Sapucaia do Sul',21),(4214,'RS','Sarandi',21),(4215,'RS','Seberi',21),(4216,'RS','Sede Nova',21),(4217,'RS','Segredo',21),(4218,'RS','Selbach',21),(4219,'RS','Senador Salgado Filho',21),(4220,'RS','Sentinela do Sul',21),(4221,'RS','Serafina Corrêa',21),(4222,'RS','Sério',21),(4223,'RS','Sertão',21),(4224,'RS','Sertão Santana',21),(4225,'RS','Sete de Setembro',21),(4226,'RS','Severiano de Almeida',21),(4227,'RS','Silveira Martins',21),(4228,'RS','Sinimbu',21),(4229,'RS','Sobradinho',21),(4230,'RS','Soledade',21),(4231,'RS','Tabaí',21),(4232,'RS','Tapejara',21),(4233,'RS','Tapera',21),(4234,'RS','Tapes',21),(4235,'RS','Taquara',21),(4236,'RS','Taquari',21),(4237,'RS','Taquaruçu do Sul',21),(4238,'RS','Tavares',21),(4239,'RS','Tenente Portela',21),(4240,'RS','Terra de Areia',21),(4241,'RS','Teutônia',21),(4242,'RS','Tiradentes do Sul',21),(4243,'RS','Toropi',21),(4244,'RS','Torres',21),(4245,'RS','Tramandaí',21),(4246,'RS','Travesseiro',21),(4247,'RS','Três Arroios',21),(4248,'RS','Três Cachoeiras',21),(4249,'RS','Três Coroas',21),(4250,'RS','Três de Maio',21),(4251,'RS','Três Forquilhas',21),(4252,'RS','Três Palmeiras',21),(4253,'RS','Três Passos',21),(4254,'RS','Trindade do Sul',21),(4255,'RS','Triunfo',21),(4256,'RS','Tucunduva',21),(4257,'RS','Tunas',21),(4258,'RS','Tupanci do Sul',21),(4259,'RS','Tupanciretã',21),(4260,'RS','Tupandi',21),(4261,'RS','Tuparendi',21),(4262,'RS','Turuçu',21),(4263,'RS','Ubiretama',21),(4264,'RS','União da Serra',21),(4265,'RS','Unistalda',21),(4266,'RS','Uruguaiana',21),(4267,'RS','Vacaria',21),(4268,'RS','Vale do Sol',21),(4269,'RS','Vale Real',21),(4270,'RS','Vale Verde',21),(4271,'RS','Vanini',21),(4272,'RS','Venâncio Aires',21),(4273,'RS','Vera Cruz',21),(4274,'RS','Veranópolis',21),(4275,'RS','Vespasiano Correa',21),(4276,'RS','Viadutos',21),(4277,'RS','Viamão',21),(4278,'RS','Vicente Dutra',21),(4279,'RS','Victor Graeff',21),(4280,'RS','Vila Flores',21),(4281,'RS','Vila Lângaro',21),(4282,'RS','Vila Maria',21),(4283,'RS','Vila Nova do Sul',21),(4284,'RS','Vista Alegre',21),(4285,'RS','Vista Alegre do Prata',21),(4286,'RS','Vista Gaúcha',21),(4287,'RS','Vitória das Missões',21),(4288,'RS','Xangri-lá',21),(4289,'RO','Alta Floresta dOeste',22),(4290,'RO','Alto Alegre dos Parecis',22),(4291,'RO','Alto Paraíso',22),(4292,'RO','Alvorada dOeste',22),(4293,'RO','Ariquemes',22),(4294,'RO','Buritis',22),(4295,'RO','Cabixi',22),(4296,'RO','Cacaulândia',22),(4297,'RO','Cacoal',22),(4298,'RO','Campo Novo de Rondônia',22),(4299,'RO','Candeias do Jamari',22),(4300,'RO','Castanheiras',22),(4301,'RO','Cerejeiras',22),(4302,'RO','Chupinguaia',22),(4303,'RO','Colorado do Oeste',22),(4304,'RO','Corumbiara',22),(4305,'RO','Costa Marques',22),(4306,'RO','Cujubim',22),(4307,'RO','Espigão dOeste',22),(4308,'RO','Governador Jorge Teixeira',22),(4309,'RO','Guajará-Mirim',22),(4310,'RO','Itapuã do Oeste',22),(4311,'RO','Jaru',22),(4312,'RO','Ji-Paraná',22),(4313,'RO','Machadinho dOeste',22),(4314,'RO','Ministro Andreazza',22),(4315,'RO','Mirante da Serra',22),(4316,'RO','Monte Negro',22),(4317,'RO','Nova Brasilândia dOeste',22),(4318,'RO','Nova Mamoré',22),(4319,'RO','Nova União',22),(4320,'RO','Novo Horizonte do Oeste',22),(4321,'RO','Ouro Preto do Oeste',22),(4322,'RO','Parecis',22),(4323,'RO','Pimenta Bueno',22),(4324,'RO','Pimenteiras do Oeste',22),(4325,'RO','Porto Velho',22),(4326,'RO','Presidente Médici',22),(4327,'RO','Primavera de Rondônia',22),(4328,'RO','Rio Crespo',22),(4329,'RO','Rolim de Moura',22),(4330,'RO','Santa Luzia dOeste',22),(4331,'RO','São Felipe dOeste',22),(4332,'RO','São Francisco do Guaporé',22),(4333,'RO','São Miguel do Guaporé',22),(4334,'RO','Seringueiras',22),(4335,'RO','Teixeirópolis',22),(4336,'RO','Theobroma',22),(4337,'RO','Urupá',22),(4338,'RO','Vale do Anari',22),(4339,'RO','Vale do Paraíso',22),(4340,'RO','Vilhena',22),(4341,'RR','Alto Alegre',23),(4342,'RR','Amajari',23),(4343,'RR','Boa Vista',23),(4344,'RR','Bonfim',23),(4345,'RR','Cantá',23),(4346,'RR','Caracaraí',23),(4347,'RR','Caroebe',23),(4348,'RR','Iracema',23),(4349,'RR','Mucajaí',23),(4350,'RR','Normandia',23),(4351,'RR','Pacaraima',23),(4352,'RR','Rorainópolis',23),(4353,'RR','São João da Baliza',23),(4354,'RR','São Luiz',23),(4355,'RR','Uiramutã',23),(4356,'SC','Abdon Batista',24),(4357,'SC','Abelardo Luz',24),(4358,'SC','Agrolândia',24),(4359,'SC','Agronômica',24),(4360,'SC','Água Doce',24),(4361,'SC','Águas de Chapecó',24),(4362,'SC','Águas Frias',24),(4363,'SC','Águas Mornas',24),(4364,'SC','Alfredo Wagner',24),(4365,'SC','Alto Bela Vista',24),(4366,'SC','Anchieta',24),(4367,'SC','Angelina',24),(4368,'SC','Anita Garibaldi',24),(4369,'SC','Anitápolis',24),(4370,'SC','Antônio Carlos',24),(4371,'SC','Apiúna',24),(4372,'SC','Arabutã',24),(4373,'SC','Araquari',24),(4374,'SC','Araranguá',24),(4375,'SC','Armazém',24),(4376,'SC','Arroio Trinta',24),(4377,'SC','Arvoredo',24),(4378,'SC','Ascurra',24),(4379,'SC','Atalanta',24),(4380,'SC','Aurora',24),(4381,'SC','Balneário Arroio do Silva',24),(4382,'SC','Balneário Barra do Sul',24),(4383,'SC','Balneário Camboriú',24),(4384,'SC','Balneário Gaivota',24),(4385,'SC','Bandeirante',24),(4386,'SC','Barra Bonita',24),(4387,'SC','Barra Velha',24),(4388,'SC','Bela Vista do Toldo',24),(4389,'SC','Belmonte',24),(4390,'SC','Benedito Novo',24),(4391,'SC','Biguaçu',24),(4392,'SC','Blumenau',24),(4393,'SC','Bocaina do Sul',24),(4394,'SC','Bom Jardim da Serra',24),(4395,'SC','Bom Jesus',24),(4396,'SC','Bom Jesus do Oeste',24),(4397,'SC','Bom Retiro',24),(4398,'SC','Bombinhas',24),(4399,'SC','Botuverá',24),(4400,'SC','Braço do Norte',24),(4401,'SC','Braço do Trombudo',24),(4402,'SC','Brunópolis',24),(4403,'SC','Brusque',24),(4404,'SC','Caçador',24),(4405,'SC','Caibi',24),(4406,'SC','Calmon',24),(4407,'SC','Camboriú',24),(4408,'SC','Campo Alegre',24),(4409,'SC','Campo Belo do Sul',24),(4410,'SC','Campo Erê',24),(4411,'SC','Campos Novos',24),(4412,'SC','Canelinha',24),(4413,'SC','Canoinhas',24),(4414,'SC','Capão Alto',24),(4415,'SC','Capinzal',24),(4416,'SC','Capivari de Baixo',24),(4417,'SC','Catanduvas',24),(4418,'SC','Caxambu do Sul',24),(4419,'SC','Celso Ramos',24),(4420,'SC','Cerro Negro',24),(4421,'SC','Chapadão do Lageado',24),(4422,'SC','Chapecó',24),(4423,'SC','Cocal do Sul',24),(4424,'SC','Concórdia',24),(4425,'SC','Cordilheira Alta',24),(4426,'SC','Coronel Freitas',24),(4427,'SC','Coronel Martins',24),(4428,'SC','Correia Pinto',24),(4429,'SC','Corupá',24),(4430,'SC','Criciúma',24),(4431,'SC','Cunha Porã',24),(4432,'SC','Cunhataí',24),(4433,'SC','Curitibanos',24),(4434,'SC','Descanso',24),(4435,'SC','Dionísio Cerqueira',24),(4436,'SC','Dona Emma',24),(4437,'SC','Doutor Pedrinho',24),(4438,'SC','Entre Rios',24),(4439,'SC','Ermo',24),(4440,'SC','Erval Velho',24),(4441,'SC','Faxinal dos Guedes',24),(4442,'SC','Flor do Sertão',24),(4443,'SC','Florianópolis',24),(4444,'SC','Formosa do Sul',24),(4445,'SC','Forquilhinha',24),(4446,'SC','Fraiburgo',24),(4447,'SC','Frei Rogério',24),(4448,'SC','Galvão',24),(4449,'SC','Garopaba',24),(4450,'SC','Garuva',24),(4451,'SC','Gaspar',24),(4452,'SC','Governador Celso Ramos',24),(4453,'SC','Grão Pará',24),(4454,'SC','Gravatal',24),(4455,'SC','Guabiruba',24),(4456,'SC','Guaraciaba',24),(4457,'SC','Guaramirim',24),(4458,'SC','Guarujá do Sul',24),(4459,'SC','Guatambú',24),(4460,'SC','Herval dOeste',24),(4461,'SC','Ibiam',24),(4462,'SC','Ibicaré',24),(4463,'SC','Ibirama',24),(4464,'SC','Içara',24),(4465,'SC','Ilhota',24),(4466,'SC','Imaruí',24),(4467,'SC','Imbituba',24),(4468,'SC','Imbuia',24),(4469,'SC','Indaial',24),(4470,'SC','Iomerê',24),(4471,'SC','Ipira',24),(4472,'SC','Iporã do Oeste',24),(4473,'SC','Ipuaçu',24),(4474,'SC','Ipumirim',24),(4475,'SC','Iraceminha',24),(4476,'SC','Irani',24),(4477,'SC','Irati',24),(4478,'SC','Irineópolis',24),(4479,'SC','Itá',24),(4480,'SC','Itaiópolis',24),(4481,'SC','Itajaí',24),(4482,'SC','Itapema',24),(4483,'SC','Itapiranga',24),(4484,'SC','Itapoá',24),(4485,'SC','Ituporanga',24),(4486,'SC','Jaborá',24),(4487,'SC','Jacinto Machado',24),(4488,'SC','Jaguaruna',24),(4489,'SC','Jaraguá do Sul',24),(4490,'SC','Jardinópolis',24),(4491,'SC','Joaçaba',24),(4492,'SC','Joinville',24),(4493,'SC','José Boiteux',24),(4494,'SC','Jupiá',24),(4495,'SC','Lacerdópolis',24),(4496,'SC','Lages',24),(4497,'SC','Laguna',24),(4498,'SC','Lajeado Grande',24),(4499,'SC','Laurentino',24),(4500,'SC','Lauro Muller',24),(4501,'SC','Lebon Régis',24),(4502,'SC','Leoberto Leal',24),(4503,'SC','Lindóia do Sul',24),(4504,'SC','Lontras',24),(4505,'SC','Luiz Alves',24),(4506,'SC','Luzerna',24),(4507,'SC','Macieira',24),(4508,'SC','Mafra',24),(4509,'SC','Major Gercino',24),(4510,'SC','Major Vieira',24),(4511,'SC','Maracajá',24),(4512,'SC','Maravilha',24),(4513,'SC','Marema',24),(4514,'SC','Massaranduba',24),(4515,'SC','Matos Costa',24),(4516,'SC','Meleiro',24),(4517,'SC','Mirim Doce',24),(4518,'SC','Modelo',24),(4519,'SC','Mondaí',24),(4520,'SC','Monte Carlo',24),(4521,'SC','Monte Castelo',24),(4522,'SC','Morro da Fumaça',24),(4523,'SC','Morro Grande',24),(4524,'SC','Navegantes',24),(4525,'SC','Nova Erechim',24),(4526,'SC','Nova Itaberaba',24),(4527,'SC','Nova Trento',24),(4528,'SC','Nova Veneza',24),(4529,'SC','Novo Horizonte',24),(4530,'SC','Orleans',24),(4531,'SC','Otacílio Costa',24),(4532,'SC','Ouro',24),(4533,'SC','Ouro Verde',24),(4534,'SC','Paial',24),(4535,'SC','Painel',24),(4536,'SC','Palhoça',24),(4537,'SC','Palma Sola',24),(4538,'SC','Palmeira',24),(4539,'SC','Palmitos',24),(4540,'SC','Papanduva',24),(4541,'SC','Paraíso',24),(4542,'SC','Passo de Torres',24),(4543,'SC','Passos Maia',24),(4544,'SC','Paulo Lopes',24),(4545,'SC','Pedras Grandes',24),(4546,'SC','Penha',24),(4547,'SC','Peritiba',24),(4548,'SC','Petrolândia',24),(4549,'SC','Piçarras',24),(4550,'SC','Pinhalzinho',24),(4551,'SC','Pinheiro Preto',24),(4552,'SC','Piratuba',24),(4553,'SC','Planalto Alegre',24),(4554,'SC','Pomerode',24),(4555,'SC','Ponte Alta',24),(4556,'SC','Ponte Alta do Norte',24),(4557,'SC','Ponte Serrada',24),(4558,'SC','Porto Belo',24),(4559,'SC','Porto União',24),(4560,'SC','Pouso Redondo',24),(4561,'SC','Praia Grande',24),(4562,'SC','Presidente Castelo Branco',24),(4563,'SC','Presidente Getúlio',24),(4564,'SC','Presidente Nereu',24),(4565,'SC','Princesa',24),(4566,'SC','Quilombo',24),(4567,'SC','Rancho Queimado',24),(4568,'SC','Rio das Antas',24),(4569,'SC','Rio do Campo',24),(4570,'SC','Rio do Oeste',24),(4571,'SC','Rio do Sul',24),(4572,'SC','Rio dos Cedros',24),(4573,'SC','Rio Fortuna',24),(4574,'SC','Rio Negrinho',24),(4575,'SC','Rio Rufino',24),(4576,'SC','Riqueza',24),(4577,'SC','Rodeio',24),(4578,'SC','Romelândia',24),(4579,'SC','Salete',24),(4580,'SC','Saltinho',24),(4581,'SC','Salto Veloso',24),(4582,'SC','Sangão',24),(4583,'SC','Santa Cecília',24),(4584,'SC','Santa Helena',24),(4585,'SC','Santa Rosa de Lima',24),(4586,'SC','Santa Rosa do Sul',24),(4587,'SC','Santa Terezinha',24),(4588,'SC','Santa Terezinha do Progresso',24),(4589,'SC','Santiago do Sul',24),(4590,'SC','Santo Amaro da Imperatriz',24),(4591,'SC','São Bento do Sul',24),(4592,'SC','São Bernardino',24),(4593,'SC','São Bonifácio',24),(4594,'SC','São Carlos',24),(4595,'SC','São Cristovão do Sul',24),(4596,'SC','São Domingos',24),(4597,'SC','São Francisco do Sul',24),(4598,'SC','São João Batista',24),(4599,'SC','São João do Itaperiú',24),(4600,'SC','São João do Oeste',24),(4601,'SC','São João do Sul',24),(4602,'SC','São Joaquim',24),(4603,'SC','São José',24),(4604,'SC','São José do Cedro',24),(4605,'SC','São José do Cerrito',24),(4606,'SC','São Lourenço do Oeste',24),(4607,'SC','São Ludgero',24),(4608,'SC','São Martinho',24),(4609,'SC','São Miguel da Boa Vista',24),(4610,'SC','São Miguel do Oeste',24),(4611,'SC','São Pedro de Alcântara',24),(4612,'SC','Saudades',24),(4613,'SC','Schroeder',24),(4614,'SC','Seara',24),(4615,'SC','Serra Alta',24),(4616,'SC','Siderópolis',24),(4617,'SC','Sombrio',24),(4618,'SC','Sul Brasil',24),(4619,'SC','Taió',24),(4620,'SC','Tangará',24),(4621,'SC','Tigrinhos',24),(4622,'SC','Tijucas',24),(4623,'SC','Timbé do Sul',24),(4624,'SC','Timbó',24),(4625,'SC','Timbó Grande',24),(4626,'SC','Três Barras',24),(4627,'SC','Treviso',24),(4628,'SC','Treze de Maio',24),(4629,'SC','Treze Tílias',24),(4630,'SC','Trombudo Central',24),(4631,'SC','Tubarão',24),(4632,'SC','Tunápolis',24),(4633,'SC','Turvo',24),(4634,'SC','União do Oeste',24),(4635,'SC','Urubici',24),(4636,'SC','Urupema',24),(4637,'SC','Urussanga',24),(4638,'SC','Vargeão',24),(4639,'SC','Vargem',24),(4640,'SC','Vargem Bonita',24),(4641,'SC','Vidal Ramos',24),(4642,'SC','Videira',24),(4643,'SC','Vitor Meireles',24),(4644,'SC','Witmarsum',24),(4645,'SC','Xanxerê',24),(4646,'SC','Xavantina',24),(4647,'SC','Xaxim',24),(4648,'SC','Zortéa',24),(4649,'SP','Adamantina',25),(4650,'SP','Adolfo',25),(4651,'SP','Aguaí',25),(4652,'SP','Águas da Prata',25),(4653,'SP','Águas de Lindóia',25),(4654,'SP','Águas de Santa Bárbara',25),(4655,'SP','Águas de São Pedro',25),(4656,'SP','Agudos',25),(4657,'SP','Alambari',25),(4658,'SP','Alfredo Marcondes',25),(4659,'SP','Altair',25),(4660,'SP','Altinópolis',25),(4661,'SP','Alto Alegre',25),(4662,'SP','Alumínio',25),(4663,'SP','Álvares Florence',25),(4664,'SP','Álvares Machado',25),(4665,'SP','Álvaro de Carvalho',25),(4666,'SP','Alvinlândia',25),(4667,'SP','Americana',25),(4668,'SP','Américo Brasiliense',25),(4669,'SP','Américo de Campos',25),(4670,'SP','Amparo',25),(4671,'SP','Analândia',25),(4672,'SP','Andradina',25),(4673,'SP','Angatuba',25),(4674,'SP','Anhembi',25),(4675,'SP','Anhumas',25),(4676,'SP','Aparecida',25),(4677,'SP','Aparecida dOeste',25),(4678,'SP','Apiaí',25),(4679,'SP','Araçariguama',25),(4680,'SP','Araçatuba',25),(4681,'SP','Araçoiaba da Serra',25),(4682,'SP','Aramina',25),(4683,'SP','Arandu',25),(4684,'SP','Arapeí',25),(4685,'SP','Araraquara',25),(4686,'SP','Araras',25),(4687,'SP','Arco-Íris',25),(4688,'SP','Arealva',25),(4689,'SP','Areias',25),(4690,'SP','Areiópolis',25),(4691,'SP','Ariranha',25),(4692,'SP','Artur Nogueira',25),(4693,'SP','Arujá',25),(4694,'SP','Aspásia',25),(4695,'SP','Assis',25),(4696,'SP','Atibaia',25),(4697,'SP','Auriflama',25),(4698,'SP','Avaí',25),(4699,'SP','Avanhandava',25),(4700,'SP','Avaré',25),(4701,'SP','Bady Bassitt',25),(4702,'SP','Balbinos',25),(4703,'SP','Bálsamo',25),(4704,'SP','Bananal',25),(4705,'SP','Barão de Antonina',25),(4706,'SP','Barbosa',25),(4707,'SP','Bariri',25),(4708,'SP','Barra Bonita',25),(4709,'SP','Barra do Chapéu',25),(4710,'SP','Barra do Turvo',25),(4711,'SP','Barretos',25),(4712,'SP','Barrinha',25),(4713,'SP','Barueri',25),(4714,'SP','Bastos',25),(4715,'SP','Batatais',25),(4716,'SP','Bauru',25),(4717,'SP','Bebedouro',25),(4718,'SP','Bento de Abreu',25),(4719,'SP','Bernardino de Campos',25),(4720,'SP','Bertioga',25),(4721,'SP','Bilac',25),(4722,'SP','Birigui',25),(4723,'SP','Biritiba-Mirim',25),(4724,'SP','Boa Esperança do Sul',25),(4725,'SP','Bocaina',25),(4726,'SP','Bofete',25),(4727,'SP','Boituva',25),(4728,'SP','Bom Jesus dos Perdões',25),(4729,'SP','Bom Sucesso de Itararé',25),(4730,'SP','Borá',25),(4731,'SP','Boracéia',25),(4732,'SP','Borborema',25),(4733,'SP','Borebi',25),(4734,'SP','Botucatu',25),(4735,'SP','Bragança Paulista',25),(4736,'SP','Braúna',25),(4737,'SP','Brejo Alegre',25),(4738,'SP','Brodowski',25),(4739,'SP','Brotas',25),(4740,'SP','Buri',25),(4741,'SP','Buritama',25),(4742,'SP','Buritizal',25),(4743,'SP','Cabrália Paulista',25),(4744,'SP','Cabreúva',25),(4745,'SP','Caçapava',25),(4746,'SP','Cachoeira Paulista',25),(4747,'SP','Caconde',25),(4748,'SP','Cafelândia',25),(4749,'SP','Caiabu',25),(4750,'SP','Caieiras',25),(4751,'SP','Caiuá',25),(4752,'SP','Cajamar',25),(4753,'SP','Cajati',25),(4754,'SP','Cajobi',25),(4755,'SP','Cajuru',25),(4756,'SP','Campina do Monte Alegre',25),(4757,'SP','Campinas',25),(4758,'SP','Campo Limpo Paulista',25),(4759,'SP','Campos do Jordão',25),(4760,'SP','Campos Novos Paulista',25),(4761,'SP','Cananéia',25),(4762,'SP','Canas',25),(4763,'SP','Cândido Mota',25),(4764,'SP','Cândido Rodrigues',25),(4765,'SP','Canitar',25),(4766,'SP','Capão Bonito',25),(4767,'SP','Capela do Alto',25),(4768,'SP','Capivari',25),(4769,'SP','Caraguatatuba',25),(4770,'SP','Carapicuíba',25),(4771,'SP','Cardoso',25),(4772,'SP','Casa Branca',25),(4773,'SP','Cássia dos Coqueiros',25),(4774,'SP','Castilho',25),(4775,'SP','Catanduva',25),(4776,'SP','Catiguá',25),(4777,'SP','Cedral',25),(4778,'SP','Cerqueira César',25),(4779,'SP','Cerquilho',25),(4780,'SP','Cesário Lange',25),(4781,'SP','Charqueada',25),(4782,'SP','Chavantes',25),(4783,'SP','Clementina',25),(4784,'SP','Colina',25),(4785,'SP','Colômbia',25),(4786,'SP','Conchal',25),(4787,'SP','Conchas',25),(4788,'SP','Cordeirópolis',25),(4789,'SP','Coroados',25),(4790,'SP','Coronel Macedo',25),(4791,'SP','Corumbataí',25),(4792,'SP','Cosmópolis',25),(4793,'SP','Cosmorama',25),(4794,'SP','Cotia',25),(4795,'SP','Cravinhos',25),(4796,'SP','Cristais Paulista',25),(4797,'SP','Cruzália',25),(4798,'SP','Cruzeiro',25),(4799,'SP','Cubatão',25),(4800,'SP','Cunha',25),(4801,'SP','Descalvado',25),(4802,'SP','Diadema',25),(4803,'SP','Dirce Reis',25),(4804,'SP','Divinolândia',25),(4805,'SP','Dobrada',25),(4806,'SP','Dois Córregos',25),(4807,'SP','Dolcinópolis',25),(4808,'SP','Dourado',25),(4809,'SP','Dracena',25),(4810,'SP','Duartina',25),(4811,'SP','Dumont',25),(4812,'SP','Echaporã',25),(4813,'SP','Eldorado',25),(4814,'SP','Elias Fausto',25),(4815,'SP','Elisiário',25),(4816,'SP','Embaúba',25),(4817,'SP','Embu',25),(4818,'SP','Embu-Guaçu',25),(4819,'SP','Emilianópolis',25),(4820,'SP','Engenheiro Coelho',25),(4821,'SP','Espírito Santo do Pinhal',25),(4822,'SP','Espírito Santo do Turvo',25),(4823,'SP','Estiva Gerbi',25),(4824,'SP','Estrela dOeste',25),(4825,'SP','Estrela do Norte',25),(4826,'SP','Euclides da Cunha Paulista',25),(4827,'SP','Fartura',25),(4828,'SP','Fernando Prestes',25),(4829,'SP','Fernandópolis',25),(4830,'SP','Fernão',25),(4831,'SP','Ferraz de Vasconcelos',25),(4832,'SP','Flora Rica',25),(4833,'SP','Floreal',25),(4834,'SP','Florínia',25),(4835,'SP','Flórida Paulista',25),(4836,'SP','Franca',25),(4837,'SP','Francisco Morato',25),(4838,'SP','Franco da Rocha',25),(4839,'SP','Gabriel Monteiro',25),(4840,'SP','Gália',25),(4841,'SP','Garça',25),(4842,'SP','Gastão Vidigal',25),(4843,'SP','Gavião Peixoto',25),(4844,'SP','General Salgado',25),(4845,'SP','Getulina',25),(4846,'SP','Glicério',25),(4847,'SP','Guaiçara',25),(4848,'SP','Guaimbê',25),(4849,'SP','Guaíra',25),(4850,'SP','Guapiaçu',25),(4851,'SP','Guapiara',25),(4852,'SP','Guará',25),(4853,'SP','Guaraçaí',25),(4854,'SP','Guaraci',25),(4855,'SP','Guarani dOeste',25),(4856,'SP','Guarantã',25),(4857,'SP','Guararapes',25),(4858,'SP','Guararema',25),(4859,'SP','Guaratinguetá',25),(4860,'SP','Guareí',25),(4861,'SP','Guariba',25),(4862,'SP','Guarujá',25),(4863,'SP','Guarulhos',25),(4864,'SP','Guatapará',25),(4865,'SP','Guzolândia',25),(4866,'SP','Herculândia',25),(4867,'SP','Holambra',25),(4868,'SP','Hortolândia',25),(4869,'SP','Iacanga',25),(4870,'SP','Iacri',25),(4871,'SP','Iaras',25),(4872,'SP','Ibaté',25),(4873,'SP','Ibirá',25),(4874,'SP','Ibirarema',25),(4875,'SP','Ibitinga',25),(4876,'SP','Ibiúna',25),(4877,'SP','Icém',25),(4878,'SP','Iepê',25),(4879,'SP','Igaraçu do Tietê',25),(4880,'SP','Igarapava',25),(4881,'SP','Igaratá',25),(4882,'SP','Iguape',25),(4883,'SP','Ilha Comprida',25),(4884,'SP','Ilha Solteira',25),(4885,'SP','Ilhabela',25),(4886,'SP','Indaiatuba',25),(4887,'SP','Indiana',25),(4888,'SP','Indiaporã',25),(4889,'SP','Inúbia Paulista',25),(4890,'SP','Ipauçu',25),(4891,'SP','Iperó',25),(4892,'SP','Ipeúna',25),(4893,'SP','Ipiguá',25),(4894,'SP','Iporanga',25),(4895,'SP','Ipuã',25),(4896,'SP','Iracemápolis',25),(4897,'SP','Irapuã',25),(4898,'SP','Irapuru',25),(4899,'SP','Itaberá',25),(4900,'SP','Itaí',25),(4901,'SP','Itajobi',25),(4902,'SP','Itaju',25),(4903,'SP','Itanhaém',25),(4904,'SP','Itaóca',25),(4905,'SP','Itapecerica da Serra',25),(4906,'SP','Itapetininga',25),(4907,'SP','Itapeva',25),(4908,'SP','Itapevi',25),(4909,'SP','Itapira',25),(4910,'SP','Itapirapuã Paulista',25),(4911,'SP','Itápolis',25),(4912,'SP','Itaporanga',25),(4913,'SP','Itapuí',25),(4914,'SP','Itapura',25),(4915,'SP','Itaquaquecetuba',25),(4916,'SP','Itararé',25),(4917,'SP','Itariri',25),(4918,'SP','Itatiba',25),(4919,'SP','Itatinga',25),(4920,'SP','Itirapina',25),(4921,'SP','Itirapuã',25),(4922,'SP','Itobi',25),(4923,'SP','Itu',25),(4924,'SP','Itupeva',25),(4925,'SP','Ituverava',25),(4926,'SP','Jaborandi',25),(4927,'SP','Jaboticabal',25),(4928,'SP','Jacareí',25),(4929,'SP','Jaci',25),(4930,'SP','Jacupiranga',25),(4931,'SP','Jaguariúna',25),(4932,'SP','Jales',25),(4933,'SP','Jambeiro',25),(4934,'SP','Jandira',25),(4935,'SP','Jardinópolis',25),(4936,'SP','Jarinu',25),(4937,'SP','Jaú',25),(4938,'SP','Jeriquara',25),(4939,'SP','Joanópolis',25),(4940,'SP','João Ramalho',25),(4941,'SP','José Bonifácio',25),(4942,'SP','Júlio Mesquita',25),(4943,'SP','Jumirim',25),(4944,'SP','Jundiaí',25),(4945,'SP','Junqueirópolis',25),(4946,'SP','Juquiá',25),(4947,'SP','Juquitiba',25),(4948,'SP','Lagoinha',25),(4949,'SP','Laranjal Paulista',25),(4950,'SP','Lavínia',25),(4951,'SP','Lavrinhas',25),(4952,'SP','Leme',25),(4953,'SP','Lençóis Paulista',25),(4954,'SP','Limeira',25),(4955,'SP','Lindóia',25),(4956,'SP','Lins',25),(4957,'SP','Lorena',25),(4958,'SP','Lourdes',25),(4959,'SP','Louveira',25),(4960,'SP','Lucélia',25),(4961,'SP','Lucianópolis',25),(4962,'SP','Luís Antônio',25),(4963,'SP','Luiziânia',25),(4964,'SP','Lupércio',25),(4965,'SP','Lutécia',25),(4966,'SP','Macatuba',25),(4967,'SP','Macaubal',25),(4968,'SP','Macedônia',25),(4969,'SP','Magda',25),(4970,'SP','Mairinque',25),(4971,'SP','Mairiporã',25),(4972,'SP','Manduri',25),(4973,'SP','Marabá Paulista',25),(4974,'SP','Maracaí',25),(4975,'SP','Marapoama',25),(4976,'SP','Mariápolis',25),(4977,'SP','Marília',25),(4978,'SP','Marinópolis',25),(4979,'SP','Martinópolis',25),(4980,'SP','Matão',25),(4981,'SP','Mauá',25),(4982,'SP','Mendonça',25),(4983,'SP','Meridiano',25),(4984,'SP','Mesópolis',25),(4985,'SP','Miguelópolis',25),(4986,'SP','Mineiros do Tietê',25),(4987,'SP','Mira Estrela',25),(4988,'SP','Miracatu',25),(4989,'SP','Mirandópolis',25),(4990,'SP','Mirante do Paranapanema',25),(4991,'SP','Mirassol',25),(4992,'SP','Mirassolândia',25),(4993,'SP','Mococa',25),(4994,'SP','Mogi das Cruzes',25),(4995,'SP','Mogi Guaçu',25),(4996,'SP','Mogi-Mirim',25),(4997,'SP','Mombuca',25),(4998,'SP','Monções',25),(4999,'SP','Mongaguá',25),(5000,'SP','Monte Alegre do Sul',25),(5001,'SP','Monte Alto',25),(5002,'SP','Monte Aprazível',25),(5003,'SP','Monte Azul Paulista',25),(5004,'SP','Monte Castelo',25),(5005,'SP','Monte Mor',25),(5006,'SP','Monteiro Lobato',25),(5007,'SP','Morro Agudo',25),(5008,'SP','Morungaba',25),(5009,'SP','Motuca',25),(5010,'SP','Murutinga do Sul',25),(5011,'SP','Nantes',25),(5012,'SP','Narandiba',25),(5013,'SP','Natividade da Serra',25),(5014,'SP','Nazaré Paulista',25),(5015,'SP','Neves Paulista',25),(5016,'SP','Nhandeara',25),(5017,'SP','Nipoã',25),(5018,'SP','Nova Aliança',25),(5019,'SP','Nova Campina',25),(5020,'SP','Nova Canaã Paulista',25),(5021,'SP','Nova Castilho',25),(5022,'SP','Nova Europa',25),(5023,'SP','Nova Granada',25),(5024,'SP','Nova Guataporanga',25),(5025,'SP','Nova Independência',25),(5026,'SP','Nova Luzitânia',25),(5027,'SP','Nova Odessa',25),(5028,'SP','Novais',25),(5029,'SP','Novo Horizonte',25),(5030,'SP','Nuporanga',25),(5031,'SP','Ocauçu',25),(5032,'SP','Óleo',25),(5033,'SP','Olímpia',25),(5034,'SP','Onda Verde',25),(5035,'SP','Oriente',25),(5036,'SP','Orindiúva',25),(5037,'SP','Orlândia',25),(5038,'SP','Osasco',25),(5039,'SP','Oscar Bressane',25),(5040,'SP','Osvaldo Cruz',25),(5041,'SP','Ourinhos',25),(5042,'SP','Ouro Verde',25),(5043,'SP','Ouroeste',25),(5044,'SP','Pacaembu',25),(5045,'SP','Palestina',25),(5046,'SP','Palmares Paulista',25),(5047,'SP','Palmeira dOeste',25),(5048,'SP','Palmital',25),(5049,'SP','Panorama',25),(5050,'SP','Paraguaçu Paulista',25),(5051,'SP','Paraibuna',25),(5052,'SP','Paraíso',25),(5053,'SP','Paranapanema',25),(5054,'SP','Paranapuã',25),(5055,'SP','Parapuã',25),(5056,'SP','Pardinho',25),(5057,'SP','Pariquera-Açu',25),(5058,'SP','Parisi',25),(5059,'SP','Patrocínio Paulista',25),(5060,'SP','Paulicéia',25),(5061,'SP','Paulínia',25),(5062,'SP','Paulistânia',25),(5063,'SP','Paulo de Faria',25),(5064,'SP','Pederneiras',25),(5065,'SP','Pedra Bela',25),(5066,'SP','Pedranópolis',25),(5067,'SP','Pedregulho',25),(5068,'SP','Pedreira',25),(5069,'SP','Pedrinhas Paulista',25),(5070,'SP','Pedro de Toledo',25),(5071,'SP','Penápolis',25),(5072,'SP','Pereira Barreto',25),(5073,'SP','Pereiras',25),(5074,'SP','Peruíbe',25),(5075,'SP','Piacatu',25),(5076,'SP','Piedade',25),(5077,'SP','Pilar do Sul',25),(5078,'SP','Pindamonhangaba',25),(5079,'SP','Pindorama',25),(5080,'SP','Pinhalzinho',25),(5081,'SP','Piquerobi',25),(5082,'SP','Piquete',25),(5083,'SP','Piracaia',25),(5084,'SP','Piracicaba',25),(5085,'SP','Piraju',25),(5086,'SP','Pirajuí',25),(5087,'SP','Pirangi',25),(5088,'SP','Pirapora do Bom Jesus',25),(5089,'SP','Pirapozinho',25),(5090,'SP','Pirassununga',25),(5091,'SP','Piratininga',25),(5092,'SP','Pitangueiras',25),(5093,'SP','Planalto',25),(5094,'SP','Platina',25),(5095,'SP','Poá',25),(5096,'SP','Poloni',25),(5097,'SP','Pompéia',25),(5098,'SP','Pongaí',25),(5099,'SP','Pontal',25),(5100,'SP','Pontalinda',25),(5101,'SP','Pontes Gestal',25),(5102,'SP','Populina',25),(5103,'SP','Porangaba',25),(5104,'SP','Porto Feliz',25),(5105,'SP','Porto Ferreira',25),(5106,'SP','Potim',25),(5107,'SP','Potirendaba',25),(5108,'SP','Pracinha',25),(5109,'SP','Pradópolis',25),(5110,'SP','Praia Grande',25),(5111,'SP','Pratânia',25),(5112,'SP','Presidente Alves',25),(5113,'SP','Presidente Bernardes',25),(5114,'SP','Presidente Epitácio',25),(5115,'SP','Presidente Prudente',25),(5116,'SP','Presidente Venceslau',25),(5117,'SP','Promissão',25),(5118,'SP','Quadra',25),(5119,'SP','Quatá',25),(5120,'SP','Queiroz',25),(5121,'SP','Queluz',25),(5122,'SP','Quintana',25),(5123,'SP','Rafard',25),(5124,'SP','Rancharia',25),(5125,'SP','Redenção da Serra',25),(5126,'SP','Regente Feijó',25),(5127,'SP','Reginópolis',25),(5128,'SP','Registro',25),(5129,'SP','Restinga',25),(5130,'SP','Ribeira',25),(5131,'SP','Ribeirão Bonito',25),(5132,'SP','Ribeirão Branco',25),(5133,'SP','Ribeirão Corrente',25),(5134,'SP','Ribeirão do Sul',25),(5135,'SP','Ribeirão dos Índios',25),(5136,'SP','Ribeirão Grande',25),(5137,'SP','Ribeirão Pires',25),(5138,'SP','Ribeirão Preto',25),(5139,'SP','Rifaina',25),(5140,'SP','Rincão',25),(5141,'SP','Rinópolis',25),(5142,'SP','Rio Claro',25),(5143,'SP','Rio das Pedras',25),(5144,'SP','Rio Grande da Serra',25),(5145,'SP','Riolândia',25),(5146,'SP','Riversul',25),(5147,'SP','Rosana',25),(5148,'SP','Roseira',25),(5149,'SP','Rubiácea',25),(5150,'SP','Rubinéia',25),(5151,'SP','Sabino',25),(5152,'SP','Sagres',25),(5153,'SP','Sales',25),(5154,'SP','Sales Oliveira',25),(5155,'SP','Salesópolis',25),(5156,'SP','Salmourão',25),(5157,'SP','Saltinho',25),(5158,'SP','Salto',25),(5159,'SP','Salto de Pirapora',25),(5160,'SP','Salto Grande',25),(5161,'SP','Sandovalina',25),(5162,'SP','Santa Adélia',25),(5163,'SP','Santa Albertina',25),(5164,'SP','Santa Bárbara dOeste',25),(5165,'SP','Santa Branca',25),(5166,'SP','Santa Clara dOeste',25),(5167,'SP','Santa Cruz da Conceição',25),(5168,'SP','Santa Cruz da Esperança',25),(5169,'SP','Santa Cruz das Palmeiras',25),(5170,'SP','Santa Cruz do Rio Pardo',25),(5171,'SP','Santa Ernestina',25),(5172,'SP','Santa Fé do Sul',25),(5173,'SP','Santa Gertrudes',25),(5174,'SP','Santa Isabel',25),(5175,'SP','Santa Lúcia',25),(5176,'SP','Santa Maria da Serra',25),(5177,'SP','Santa Mercedes',25),(5178,'SP','Santa Rita dOeste',25),(5179,'SP','Santa Rita do Passa Quatro',25),(5180,'SP','Santa Rosa de Viterbo',25),(5181,'SP','Santa Salete',25),(5182,'SP','Santana da Ponte Pensa',25),(5183,'SP','Santana de Parnaíba',25),(5184,'SP','Santo Anastácio',25),(5185,'SP','Santo André',25),(5186,'SP','Santo Antônio da Alegria',25),(5187,'SP','Santo Antônio de Posse',25),(5188,'SP','Santo Antônio do Aracanguá',25),(5189,'SP','Santo Antônio do Jardim',25),(5190,'SP','Santo Antônio do Pinhal',25),(5191,'SP','Santo Expedito',25),(5192,'SP','Santópolis do Aguapeí',25),(5193,'SP','Santos',25),(5194,'SP','São Bento do Sapucaí',25),(5195,'SP','São Bernardo do Campo',25),(5196,'SP','São Caetano do Sul',25),(5197,'SP','São Carlos',25),(5198,'SP','São Francisco',25),(5199,'SP','São João da Boa Vista',25),(5200,'SP','São João das Duas Pontes',25),(5201,'SP','São João de Iracema',25),(5202,'SP','São João do Pau dAlho',25),(5203,'SP','São Joaquim da Barra',25),(5204,'SP','São José da Bela Vista',25),(5205,'SP','São José do Barreiro',25),(5206,'SP','São José do Rio Pardo',25),(5207,'SP','São José do Rio Preto',25),(5208,'SP','São José dos Campos',25),(5209,'SP','São Lourenço da Serra',25),(5210,'SP','São Luís do Paraitinga',25),(5211,'SP','São Manuel',25),(5212,'SP','São Miguel Arcanjo',25),(5213,'SP','São Paulo',25),(5214,'SP','São Pedro',25),(5215,'SP','São Pedro do Turvo',25),(5216,'SP','São Roque',25),(5217,'SP','São Sebastião',25),(5218,'SP','São Sebastião da Grama',25),(5219,'SP','São Simão',25),(5220,'SP','São Vicente',25),(5221,'SP','Sarapuí',25),(5222,'SP','Sarutaiá',25),(5223,'SP','Sebastianópolis do Sul',25),(5224,'SP','Serra Azul',25),(5225,'SP','Serra Negra',25),(5226,'SP','Serrana',25),(5227,'SP','Sertãozinho',25),(5228,'SP','Sete Barras',25),(5229,'SP','Severínia',25),(5230,'SP','Silveiras',25),(5231,'SP','Socorro',25),(5232,'SP','Sorocaba',25),(5233,'SP','Sud Mennucci',25),(5234,'SP','Sumaré',25),(5235,'SP','Suzanápolis',25),(5236,'SP','Suzano',25),(5237,'SP','Tabapuã',25),(5238,'SP','Tabatinga',25),(5239,'SP','Taboão da Serra',25),(5240,'SP','Taciba',25),(5241,'SP','Taguaí',25),(5242,'SP','Taiaçu',25),(5243,'SP','Taiúva',25),(5244,'SP','Tambaú',25),(5245,'SP','Tanabi',25),(5246,'SP','Tapiraí',25),(5247,'SP','Tapiratiba',25),(5248,'SP','Taquaral',25),(5249,'SP','Taquaritinga',25),(5250,'SP','Taquarituba',25),(5251,'SP','Taquarivaí',25),(5252,'SP','Tarabai',25),(5253,'SP','Tarumã',25),(5254,'SP','Tatuí',25),(5255,'SP','Taubaté',25),(5256,'SP','Tejupá',25),(5257,'SP','Teodoro Sampaio',25),(5258,'SP','Terra Roxa',25),(5259,'SP','Tietê',25),(5260,'SP','Timburi',25),(5261,'SP','Torre de Pedra',25),(5262,'SP','Torrinha',25),(5263,'SP','Trabiju',25),(5264,'SP','Tremembé',25),(5265,'SP','Três Fronteiras',25),(5266,'SP','Tuiuti',25),(5267,'SP','Tupã',25),(5268,'SP','Tupi Paulista',25),(5269,'SP','Turiúba',25),(5270,'SP','Turmalina',25),(5271,'SP','Ubarana',25),(5272,'SP','Ubatuba',25),(5273,'SP','Ubirajara',25),(5274,'SP','Uchoa',25),(5275,'SP','União Paulista',25),(5276,'SP','Urânia',25),(5277,'SP','Uru',25),(5278,'SP','Urupês',25),(5279,'SP','Valentim Gentil',25),(5280,'SP','Valinhos',25),(5281,'SP','Valparaíso',25),(5282,'SP','Vargem',25),(5283,'SP','Vargem Grande do Sul',25),(5284,'SP','Vargem Grande Paulista',25),(5285,'SP','Várzea Paulista',25),(5286,'SP','Vera Cruz',25),(5287,'SP','Vinhedo',25),(5288,'SP','Viradouro',25),(5289,'SP','Vista Alegre do Alto',25),(5290,'SP','Vitória Brasil',25),(5291,'SP','Votorantim',25),(5292,'SP','Votuporanga',25),(5293,'SP','Zacarias',25),(5294,'SE','Amparo de São Francisco',26),(5295,'SE','Aquidabã',26),(5296,'SE','Aracaju',26),(5297,'SE','Arauá',26),(5298,'SE','Areia Branca',26),(5299,'SE','Barra dos Coqueiros',26),(5300,'SE','Boquim',26),(5301,'SE','Brejo Grande',26),(5302,'SE','Campo do Brito',26),(5303,'SE','Canhoba',26),(5304,'SE','Canindé de São Francisco',26),(5305,'SE','Capela',26),(5306,'SE','Carira',26),(5307,'SE','Carmópolis',26),(5308,'SE','Cedro de São João',26),(5309,'SE','Cristinápolis',26),(5310,'SE','Cumbe',26),(5311,'SE','Divina Pastora',26),(5312,'SE','Estância',26),(5313,'SE','Feira Nova',26),(5314,'SE','Frei Paulo',26),(5315,'SE','Gararu',26),(5316,'SE','General Maynard',26),(5317,'SE','Gracho Cardoso',26),(5318,'SE','Ilha das Flores',26),(5319,'SE','Indiaroba',26),(5320,'SE','Itabaiana',26),(5321,'SE','Itabaianinha',26),(5322,'SE','Itabi',26),(5323,'SE','Itaporanga dAjuda',26),(5324,'SE','Japaratuba',26),(5325,'SE','Japoatã',26),(5326,'SE','Lagarto',26),(5327,'SE','Laranjeiras',26),(5328,'SE','Macambira',26),(5329,'SE','Malhada dos Bois',26),(5330,'SE','Malhador',26),(5331,'SE','Maruim',26),(5332,'SE','Moita Bonita',26),(5333,'SE','Monte Alegre de Sergipe',26),(5334,'SE','Muribeca',26),(5335,'SE','Neópolis',26),(5336,'SE','Nossa Senhora Aparecida',26),(5337,'SE','Nossa Senhora da Glória',26),(5338,'SE','Nossa Senhora das Dores',26),(5339,'SE','Nossa Senhora de Lourdes',26),(5340,'SE','Nossa Senhora do Socorro',26),(5341,'SE','Pacatuba',26),(5342,'SE','Pedra Mole',26),(5343,'SE','Pedrinhas',26),(5344,'SE','Pinhão',26),(5345,'SE','Pirambu',26),(5346,'SE','Poço Redondo',26),(5347,'SE','Poço Verde',26),(5348,'SE','Porto da Folha',26),(5349,'SE','Propriá',26),(5350,'SE','Riachão do Dantas',26),(5351,'SE','Riachuelo',26),(5352,'SE','Ribeirópolis',26),(5353,'SE','Rosário do Catete',26),(5354,'SE','Salgado',26),(5355,'SE','Santa Luzia do Itanhy',26),(5356,'SE','Santa Rosa de Lima',26),(5357,'SE','Santana do São Francisco',26),(5358,'SE','Santo Amaro das Brotas',26),(5359,'SE','São Cristóvão',26),(5360,'SE','São Domingos',26),(5361,'SE','São Francisco',26),(5362,'SE','São Miguel do Aleixo',26),(5363,'SE','Simão Dias',26),(5364,'SE','Siriri',26),(5365,'SE','Telha',26),(5366,'SE','Tobias Barreto',26),(5367,'SE','Tomar do Geru',26),(5368,'SE','Umbaúba',26),(5369,'TO','Abreulândia',27),(5370,'TO','Aguiarnópolis',27),(5371,'TO','Aliança do Tocantins',27),(5372,'TO','Almas',27),(5373,'TO','Alvorada',27),(5374,'TO','Ananás',27),(5375,'TO','Angico',27),(5376,'TO','Aparecida do Rio Negro',27),(5377,'TO','Aragominas',27),(5378,'TO','Araguacema',27),(5379,'TO','Araguaçu',27),(5380,'TO','Araguaína',27),(5381,'TO','Araguanã',27),(5382,'TO','Araguatins',27),(5383,'TO','Arapoema',27),(5384,'TO','Arraias',27),(5385,'TO','Augustinópolis',27),(5386,'TO','Aurora do Tocantins',27),(5387,'TO','Axixá do Tocantins',27),(5388,'TO','Babaçulândia',27),(5389,'TO','Bandeirantes do Tocantins',27),(5390,'TO','Barra do Ouro',27),(5391,'TO','Barrolândia',27),(5392,'TO','Bernardo Sayão',27),(5393,'TO','Bom Jesus do Tocantins',27),(5394,'TO','Brasilândia do Tocantins',27),(5395,'TO','Brejinho de Nazaré',27),(5396,'TO','Buriti do Tocantins',27),(5397,'TO','Cachoeirinha',27),(5398,'TO','Campos Lindos',27),(5399,'TO','Cariri do Tocantins',27),(5400,'TO','Carmolândia',27),(5401,'TO','Carrasco Bonito',27),(5402,'TO','Caseara',27),(5403,'TO','Centenário',27),(5404,'TO','Chapada da Natividade',27),(5405,'TO','Chapada de Areia',27),(5406,'TO','Colinas do Tocantins',27),(5407,'TO','Colméia',27),(5408,'TO','Combinado',27),(5409,'TO','Conceição do Tocantins',27),(5410,'TO','Couto de Magalhães',27),(5411,'TO','Cristalândia',27),(5412,'TO','Crixás do Tocantins',27),(5413,'TO','Darcinópolis',27),(5414,'TO','Dianópolis',27),(5415,'TO','Divinópolis do Tocantins',27),(5416,'TO','Dois Irmãos do Tocantins',27),(5417,'TO','Dueré',27),(5418,'TO','Esperantina',27),(5419,'TO','Fátima',27),(5420,'TO','Figueirópolis',27),(5421,'TO','Filadélfia',27),(5422,'TO','Formoso do Araguaia',27),(5423,'TO','Fortaleza do Tabocão',27),(5424,'TO','Goianorte',27),(5425,'TO','Goiatins',27),(5426,'TO','Guaraí',27),(5427,'TO','Gurupi',27),(5428,'TO','Ipueiras',27),(5429,'TO','Itacajá',27),(5430,'TO','Itaguatins',27),(5431,'TO','Itapiratins',27),(5432,'TO','Itaporã do Tocantins',27),(5433,'TO','Jaú do Tocantins',27),(5434,'TO','Juarina',27),(5435,'TO','Lagoa da Confusão',27),(5436,'TO','Lagoa do Tocantins',27),(5437,'TO','Lajeado',27),(5438,'TO','Lavandeira',27),(5439,'TO','Lizarda',27),(5440,'TO','Luzinópolis',27),(5441,'TO','Marianópolis do Tocantins',27),(5442,'TO','Mateiros',27),(5443,'TO','Maurilândia do Tocantins',27),(5444,'TO','Miracema do Tocantins',27),(5445,'TO','Miranorte',27),(5446,'TO','Monte do Carmo',27),(5447,'TO','Monte Santo do Tocantins',27),(5448,'TO','Muricilândia',27),(5449,'TO','Natividade',27),(5450,'TO','Nazaré',27),(5451,'TO','Nova Olinda',27),(5452,'TO','Nova Rosalândia',27),(5453,'TO','Novo Acordo',27),(5454,'TO','Novo Alegre',27),(5455,'TO','Novo Jardim',27),(5456,'TO','Oliveira de Fátima',27),(5457,'TO','Palmas',27),(5458,'TO','Palmeirante',27),(5459,'TO','Palmeiras do Tocantins',27),(5460,'TO','Palmeirópolis',27),(5461,'TO','Paraíso do Tocantins',27),(5462,'TO','Paranã',27),(5463,'TO','Pau dArco',27),(5464,'TO','Pedro Afonso',27),(5465,'TO','Peixe',27),(5466,'TO','Pequizeiro',27),(5467,'TO','Pindorama do Tocantins',27),(5468,'TO','Piraquê',27),(5469,'TO','Pium',27),(5470,'TO','Ponte Alta do Bom Jesus',27),(5471,'TO','Ponte Alta do Tocantins',27),(5472,'TO','Porto Alegre do Tocantins',27),(5473,'TO','Porto Nacional',27),(5474,'TO','Praia Norte',27),(5475,'TO','Presidente Kennedy',27),(5476,'TO','Pugmil',27),(5477,'TO','Recursolândia',27),(5478,'TO','Riachinho',27),(5479,'TO','Rio da Conceição',27),(5480,'TO','Rio dos Bois',27),(5481,'TO','Rio Sono',27),(5482,'TO','Sampaio',27),(5483,'TO','Sandolândia',27),(5484,'TO','Santa Fé do Araguaia',27),(5485,'TO','Santa Maria do Tocantins',27),(5486,'TO','Santa Rita do Tocantins',27),(5487,'TO','Santa Rosa do Tocantins',27),(5488,'TO','Santa Tereza do Tocantins',27),(5489,'TO','Santa Terezinha do Tocantins',27),(5490,'TO','São Bento do Tocantins',27),(5491,'TO','São Félix do Tocantins',27),(5492,'TO','São Miguel do Tocantins',27),(5493,'TO','São Salvador do Tocantins',27),(5494,'TO','São Sebastião do Tocantins',27),(5495,'TO','São Valério da Natividade',27),(5496,'TO','Silvanópolis',27),(5497,'TO','Sítio Novo do Tocantins',27),(5498,'TO','Sucupira',27),(5499,'TO','Taguatinga',27),(5500,'TO','Taipas do Tocantins',27),(5501,'TO','Talismã',27),(5502,'TO','Tocantínia',27),(5503,'TO','Tocantinópolis',27),(5504,'TO','Tupirama',27),(5505,'TO','Tupiratins',27),(5506,'TO','Wanderlândia',27),(5507,'TO','Xambioá',27),(5508,'DF','Ceilândia',7),(5509,'DF','Águas Claras',7),(5510,'DF','Guará',7),(5511,'DF','Taguatinga',7),(5512,'DF','Samambaia',7),(5513,'DF','Gama',7),(5514,'DF','Sobradinho',7);
/*!40000 ALTER TABLE `tbcities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcountrys`
--

DROP TABLE IF EXISTS `tbcountrys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbcountrys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=198 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcountrys`
--

LOCK TABLES `tbcountrys` WRITE;
/*!40000 ALTER TABLE `tbcountrys` DISABLE KEYS */;
INSERT INTO `tbcountrys` VALUES (1,'Afeganistão'),(2,'África do Sul'),(3,'Albânia'),(4,'Alemanha'),(5,'Andorra'),(6,'Angola'),(7,'Antiga e Barbuda'),(8,'Arábia Saudita'),(9,'Argélia'),(10,'Argentina'),(11,'Arménia'),(12,'Austrália'),(13,'Áustria'),(14,'Azerbaijão'),(15,'Bahamas'),(16,'Bangladexe'),(17,'Barbados'),(18,'Barém'),(19,'Bélgica'),(20,'Belize'),(21,'Benim'),(22,'Bielorrússia'),(23,'Bolívia'),(24,'Bósnia e Herzegovina'),(25,'Botsuana'),(26,'Brasil'),(27,'Brunei'),(28,'Bulgária'),(29,'Burquina Faso'),(30,'Burúndi'),(31,'Butão'),(32,'Cabo Verde'),(33,'Camarões'),(34,'Camboja'),(35,'Canadá'),(36,'Catar'),(37,'Cazaquistão'),(38,'Chade'),(39,'Chile'),(40,'China'),(41,'Chipre'),(42,'Colômbia'),(43,'Comores'),(44,'Congo-Brazzaville'),(45,'Coreia do Norte'),(46,'Coreia do Sul'),(47,'Cosovo'),(48,'Costa do Marfim'),(49,'Costa Rica'),(50,'Croácia'),(51,'Cuaite'),(52,'Cuba'),(53,'Dinamarca'),(54,'Dominica'),(55,'Egito'),(56,'Emirados Árabes Unidos'),(57,'Equador'),(58,'Eritreia'),(59,'Eslováquia'),(60,'Eslovénia'),(61,'Espanha'),(62,'Estado da Palestina'),(63,'Estados Unidos'),(64,'Estónia'),(65,'Etiópia'),(66,'Fiji'),(67,'Filipinas'),(68,'Finlândia'),(69,'França'),(70,'Gabão'),(71,'Gâmbia'),(72,'Gana'),(73,'Geórgia'),(74,'Granada'),(75,'Grécia'),(76,'Guatemala'),(77,'Guiana'),(78,'Guiné'),(79,'Guiné Equatorial'),(80,'Guiné-Bissau'),(81,'Haiti'),(82,'Honduras'),(83,'Hungria'),(84,'Iémen'),(85,'Ilhas Marechal'),(86,'Índia'),(87,'Indonésia'),(88,'Irão'),(89,'Iraque'),(90,'Irlanda'),(91,'Islândia'),(92,'Israel'),(93,'Itália'),(94,'Jamaica'),(95,'Japão'),(96,'Jibuti'),(97,'Jordânia'),(98,'Laus'),(99,'Lesoto'),(100,'Letónia'),(101,'Líbano'),(102,'Libéria'),(103,'Líbia'),(104,'Listenstaine'),(105,'Lituânia'),(106,'Luxemburgo'),(107,'Macedónia'),(108,'Madagáscar'),(109,'Malásia'),(110,'Maláui'),(111,'Maldivas'),(112,'Mali'),(113,'Malta'),(114,'Marrocos'),(115,'Maurícia'),(116,'Mauritânia'),(117,'México'),(118,'Mianmar'),(119,'Micronésia'),(120,'Moçambique'),(121,'Moldávia'),(122,'Mónaco'),(123,'Mongólia'),(124,'Montenegro'),(125,'Namíbia'),(126,'Nauru'),(127,'Nepal'),(128,'Nicarágua'),(129,'Níger'),(130,'Nigéria'),(131,'Noruega'),(132,'Nova Zelândia'),(133,'Omã'),(134,'Países Baixos'),(135,'Palau'),(136,'Panamá'),(137,'Papua Nova Guiné'),(138,'Paquistão'),(139,'Paraguai'),(140,'Peru'),(141,'Polónia'),(142,'Portugal'),(143,'Quénia'),(144,'Quirguistão'),(145,'Quiribáti'),(146,'Reino Unido'),(147,'República Centro-Africana'),(148,'República Checa'),(149,'República Democrática do Congo'),(150,'República Dominicana'),(151,'Roménia'),(152,'Ruanda'),(153,'Rússia'),(154,'Salomão'),(155,'Salvador'),(156,'Samoa'),(157,'Santa Lúcia'),(158,'São Cristóvão e Neves'),(159,'São Marinho'),(160,'São Tomé e Príncipe'),(161,'São Vicente e Granadinas'),(162,'Seicheles'),(163,'Senegal'),(164,'Serra Leoa'),(165,'Sérvia'),(166,'Singapura'),(167,'Síria'),(168,'Somália'),(169,'Sri Lanca'),(170,'Suazilândia'),(171,'Sudão'),(172,'Sudão do Sul'),(173,'Suécia'),(174,'Suíça'),(175,'Suriname'),(176,'Tailândia'),(177,'Taiuã'),(178,'Tajiquistão'),(179,'Tanzânia'),(180,'Timor-Leste'),(181,'Togo'),(182,'Tonga'),(183,'Trindade e Tobago'),(184,'Tunísia'),(185,'Turcomenistão'),(186,'Turquia'),(187,'Tuvalu'),(188,'Ucrânia'),(189,'Uganda'),(190,'Uruguai'),(191,'Usbequistão'),(192,'Vanuatu'),(193,'Vaticano'),(194,'Venezuela'),(195,'Vietname'),(196,'Zâmbia'),(197,'Zimbábue');
/*!40000 ALTER TABLE `tbcountrys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbstates`
--

DROP TABLE IF EXISTS `tbstates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbstates` (
  `pais` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `sigla` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbstates`
--

LOCK TABLES `tbstates` WRITE;
/*!40000 ALTER TABLE `tbstates` DISABLE KEYS */;
INSERT INTO `tbstates` VALUES (26,1,'Acre','AC'),(26,2,'Alagoas','AL'),(26,3,'Amapá','AP'),(26,4,'Amazonas','AM'),(26,5,'Bahia','BA'),(26,6,'Ceará','CE'),(26,7,'Distrito Federal','DF'),(26,8,'Espírito Santo','ES'),(26,9,'Goiás','GO'),(26,10,'Maranhão','MA'),(26,11,'Mato Grosso','MT'),(26,12,'Mato Grosso do Sul','MS'),(26,13,'Minas Gerais','MG'),(26,14,'Pará','PA'),(26,15,'Paraíba','PB'),(26,16,'Paraná','PR'),(26,17,'Pernambuco','PE'),(26,18,'Piauí','PI'),(26,19,'Rio de Janeiro','RJ'),(26,20,'Rio Grande do Norte','RN'),(26,21,'Rio Grande do Sul','RS'),(26,22,'Rondônia','RO'),(26,23,'Roraima','RR'),(26,24,'Santa Catarina','SC'),(26,25,'São Paulo','SP'),(26,26,'Sergipe','SE'),(26,27,'Tocantins','TO'),(26,28,'Exterior','EX');
/*!40000 ALTER TABLE `tbstates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbtestenode`
--

DROP TABLE IF EXISTS `tbtestenode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbtestenode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbtestenode`
--

LOCK TABLES `tbtestenode` WRITE;
/*!40000 ALTER TABLE `tbtestenode` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbtestenode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tinder_likes`
--

DROP TABLE IF EXISTS `tinder_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tinder_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_from` int(11) NOT NULL,
  `id_to` int(11) NOT NULL,
  `action` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tinder_likes`
--

LOCK TABLES `tinder_likes` WRITE;
/*!40000 ALTER TABLE `tinder_likes` DISABLE KEYS */;
INSERT INTO `tinder_likes` VALUES (1,1,21,1),(2,1,17,0),(3,1,17,0),(4,1,17,0),(5,21,1,1),(6,1,17,0),(8,1,17,0),(9,1,5,1),(10,1,3,1),(11,1,19,1),(12,1,15,1),(13,1,11,1),(14,11,2,1),(15,11,12,1),(16,11,6,1),(17,11,16,1),(18,11,14,1),(19,11,12,1),(20,11,12,1),(21,11,20,0),(22,11,14,0),(23,11,16,0),(24,11,6,0),(25,11,1,1),(26,1,9,0);
/*!40000 ALTER TABLE `tinder_likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tinder_users`
--

DROP TABLE IF EXISTS `tinder_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tinder_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lgt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `senha` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sexo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tinder_users`
--

LOCK TABLES `tinder_users` WRITE;
/*!40000 ALTER TABLE `tinder_users` DISABLE KEYS */;
INSERT INTO `tinder_users` VALUES (1,'Rodinei','São Paulo','-3.6441405','-6.7188515','teste@teste.com','123456','Masculino','https://picsum.photos/200'),(2,'Fulano','Rio de Janeiro',NULL,NULL,'fulano@teste.com','123456','Masculino','https://picsum.photos/201'),(3,'Ciclana','Bahia',NULL,NULL,'ciclana@teste.com','123456','Feminino','https://picsum.photos/202'),(4,'Outro usuario','Paraná',NULL,NULL,'outro@teste.com','123456','Feminino','https://picsum.photos/210'),(5,'Mais um','São Paulo',NULL,NULL,'mais@teste.com','123456','Feminino','https://picsum.photos/211'),(6,'Usuário 1','São Paulo',NULL,NULL,'usuario1@teste.com','123456','Masculino','https://picsum.photos/200'),(7,'Usuário 2','São Paulo',NULL,NULL,'usuario2@teste.com','123456','Feminino','https://picsum.photos/201'),(8,'Usuário 3','São Paulo',NULL,NULL,'usuario3@teste.com','123456','Masculino','https://picsum.photos/202'),(9,'Usuário 4','São Paulo',NULL,NULL,'usuario4@teste.com','123456','Feminino','https://picsum.photos/203'),(10,'Usuário 5','São Paulo',NULL,NULL,'usuario5@teste.com','123456','Masculino','https://picsum.photos/204'),(11,'Usuário 6','São Paulo','-23.6441405','-47.7188515','usuario6@teste.com','123456','Feminino','https://picsum.photos/205'),(12,'Usuário 7','São Paulo',NULL,NULL,'usuario7@teste.com','123456','Masculino','https://picsum.photos/206'),(13,'Usuário 8','São Paulo',NULL,NULL,'usuario8@teste.com','123456','Feminino','https://picsum.photos/207'),(14,'Usuário 9','São Paulo',NULL,NULL,'usuario9@teste.com','123456','Masculino','https://picsum.photos/208'),(15,'Usuário 10','São Paulo',NULL,NULL,'usuario10@teste.com','123456','Feminino','https://picsum.photos/209'),(16,'Usuário 11','São Paulo',NULL,NULL,'usuario11@teste.com','123456','Masculino','https://picsum.photos/210'),(17,'Usuário 12','São Paulo',NULL,NULL,'usuario12@teste.com','123456','Feminino','https://picsum.photos/211'),(18,'Usuário 13','São Paulo',NULL,NULL,'usuario13@teste.com','123456','Masculino','https://picsum.photos/212'),(19,'Usuário 14','São Paulo',NULL,NULL,'usuario14@teste.com','123456','Feminino','https://picsum.photos/213'),(20,'Usuário 15','São Paulo',NULL,NULL,'usuario15@teste.com','123456','Masculino','https://picsum.photos/214'),(21,'Usuário 16','São Paulo','-23.6441405','-46.7188515','usuario16@teste.com','123456','Feminino','https://picsum.photos/215');
/*!40000 ALTER TABLE `tinder_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tinder_views`
--

DROP TABLE IF EXISTS `tinder_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tinder_views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_from` int(11) NOT NULL,
  `id_to` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tinder_views`
--

LOCK TABLES `tinder_views` WRITE;
/*!40000 ALTER TABLE `tinder_views` DISABLE KEYS */;
/*!40000 ALTER TABLE `tinder_views` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cnpj` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profissao` varchar(45) COLLATE utf8_unicode_ci DEFAULT '-1',
  `sexo` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag1` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag2` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag3` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checks` longtext COLLATE utf8_unicode_ci,
  `mailing` tinyint(4) DEFAULT '0',
  `team` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `district` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regional` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom4` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom5` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom6` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom7` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom8` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom9` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` tinyint(4) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `estado` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cidade` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmacaoSenha` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (8,'William','william@teste.com','$2a$10$DrB77Rj68OAg/BERLbLBner2sS1NSpEQ4h1wxLHX1z3HMSs80WZ7q',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'SP','Itapevi',NULL),(9,'Testando cadastro','rodinei@teste.com','$2a$10$M/n03FEcpZmoCGFjRReBeO7vohy8sxCzQQAuomTRbqSB9dA/MXn2C','30150690800','','16','M','1983-06-21','12341234','12347896','1','1','1','[]',1,'time','departamento','empresa','cargo','distrito','regional','custom1','custom2','custom3','custom4','custom15','custom6','custom7','custom8','custom9',1,NULL,NULL,NULL,'SP','Itapevi',NULL),(13,'123','asdf@a.com','$2a$10$rAeFqcvbdm.PUr61SLFiXuiJXeGBNPnXI7ercQjQAY/T9nBHd.20q','','','2','M',NULL,'','',NULL,NULL,NULL,'[\"1\",\"3\"]',0,'','','','','','','','','','','','','','','',0,NULL,NULL,NULL,'','',NULL),(14,'William','william@teste.com','$2a$10$Eg.ccTGN6iTuqFgky0q2wuOX/5DKySHUal.zvNmGg/CO/..KPq2Aq',NULL,'','3','M','1988-06-25','1100000000','11000000000','0','0','1','[\"1\"]',0,'Sei la','Sei la','SeiLa','Sei la','Sei la','Sei la','Sei la','','','','','','','','',1,NULL,NULL,NULL,'AP','Calçoene',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `IdUsuario` int(11) NOT NULL,
  `Nome` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `Sobrenome` varchar(90) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Senha` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `NivelUsuario` int(11) NOT NULL,
  `Status` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (7,'Thalles','Rangel','rangelthr@gmail.com','6347e0278295370b504aca25bf8a80e011ae22da',1,'Ativo');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-30 13:47:40
