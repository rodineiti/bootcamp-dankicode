# RDNBASE

    this is a base template for your frontend and backend applications using PHP, MYSQL and Bootstrap in the MVC standard, using good practices to 
    create your applications in an organized manner.

Clone the repository

    git clone git@gitlab.com:rodineiti/bootcamp-dankicode.git

Switch to the repo folder

    cd base
    cp config-example.php config.php
    cp htaccess-example.txt .htaccess
    
Edit file config.php, and set connection mysql

    $config["dbname"] = "database_base";
    $config["dbhost"] = "mysql";
    $config["dbuser"] = "root";
    $config["dbpass"] = "root";
    
Dump file database_base.sql into database

Run server php or your server (Wamp, Mamp, Xamp), and open in the browser localhost
  
    php -S localhost

Url ADMIN:

    Admin login: http://localhost/base/admin/login

    login: admin@admin.com
    password: 123456
    
LOGIN USER:

    login: http://localhost/base/login

    login: user@user.com
    password: 123456
    

#### Selects
```php
<?php
use Src\Models\User;

// get one with where - alternative find one
$model = new User();
$user = $model->select(["id", "name"])->where("name", "=", "Fulano")->fisrt();

var_dump($user);

// get all with get - alternative find all
$model = new User();
$users = $model->select()->all();

var_dump($users);
```

#### findById

```php
<?php
use Src\Models\User;

$model = new User();
$user = $model->getById(2);
echo $user->name;
```

#### count

```php
<?php
use Src\Models\User;
$model = new User();

$count = $model->select()->count();
```

#### wheres
```php
<?php
use Src\Models\User;
$model = new User();

// find with where
$users = $model
    ->select()
    ->where("id", ">", 1)
    ->where("name", "=", "teste")
    ->all();

var_dump($users);

$model = new User();
// find with whereRaw
$users = $model
    ->select()
    ->whereRaw("name LIKE '%fulano%' ")
    ->all();

var_dump($users);

$model = new User();
// find with whereIn
$users = $model
    ->select()
    ->whereIn("id", [1,2])
    ->all();

var_dump($users);
```

#### joins
```php
<?php
use Src\Models\User;

$model = new User();
// find with join address
$users = $model
    ->select()
    ->join("address", "user_id", "id")
    ->all();

var_dump($users);

$model = new User();
// find with left join address
$users = $model
    ->select()
    ->leftJoin("address", "user_id", "id")
    ->all();

var_dump($users);

$model = new User();
// find with right join address
$users = $model
    ->select()
    ->rightJoin("address", "user_id", "id")
    ->all();

var_dump($users);
```



Prints:

![image](https://user-images.githubusercontent.com/25492122/102231863-35659680-3ecd-11eb-8683-e6d979ab64b8.png)

![image](https://user-images.githubusercontent.com/25492122/102229046-17e2fd80-3eca-11eb-870e-35069c7bfadb.png)

![image](https://user-images.githubusercontent.com/25492122/102229091-24ffec80-3eca-11eb-95ce-38a28e8b09ae.png)

![image](https://user-images.githubusercontent.com/25492122/102229120-30531800-3eca-11eb-8e61-6080c0322945.png)

![image](https://user-images.githubusercontent.com/25492122/102229176-3d700700-3eca-11eb-8adc-0d39869f777b.png)

![image](https://user-images.githubusercontent.com/25492122/102229209-452fab80-3eca-11eb-989e-4c71ee02358f.png)

![image](https://user-images.githubusercontent.com/25492122/102229233-4d87e680-3eca-11eb-8ded-694eef4a5080.png)

![image](https://user-images.githubusercontent.com/25492122/102229268-58427b80-3eca-11eb-8196-ffb533db1bf9.png)

![image](https://user-images.githubusercontent.com/25492122/102229319-685a5b00-3eca-11eb-86b1-423cf35e56e5.png)
