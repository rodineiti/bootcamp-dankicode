<?php

namespace Src\Models\Projeto08;

use Src\Core\Model;
use Src\Models\User;

class Request extends Model
{
    public function __construct()
    {
        parent::__construct("requests");
    }

    public function getFriends()
    {
        $results = $this->select()->where('status', '=', 1)
            ->whereRaw('id_from = '. auth()->id . ' OR id_to = '. auth()->id . '')
            ->all() ?? [];

        $whereIn = [];
        foreach ($results as $result) {
            if ($result->id_from === auth()->id) {
                $whereIn[] = $result->id_to;
            } else {
                $whereIn[] = $result->id_from;
            }
        }

        return $whereIn;
    }

    public function userFrom()
    {
        return ($this->id_from) ? (new User())->findById($this->id_from) : null;
    }

    public function userTo()
    {
        return ($this->id_to) ? (new User())->findById($this->id_to) : null;
    }
}

?>