<?php

namespace Src\Models\Projeto08;

use Src\Core\Model;
use Src\Models\User;

class Feed extends Model
{
    public function __construct()
    {
        parent::__construct("feeds");
    }

    public function user()
    {
        return ($this->user_id) ? (new User())->findById($this->user_id) : null;
    }
}

?>