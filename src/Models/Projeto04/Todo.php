<?php

namespace Src\Models\Projeto04;

use Src\Core\Model;

class Todo extends Model
{
    public function __construct()
    {
        parent::__construct("todo");
    }
}