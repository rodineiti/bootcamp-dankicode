<?php

namespace Src\Models\Projeto05;

use Src\Core\Model;
use Src\Models\Admin;

class ForumPost extends Model
{
    public function __construct()
    {
        parent::__construct("forum_posts");
    }

    public function user()
    {
        return ($this->user_id) ? (new Admin())->findById($this->user_id) : null;
    }
}