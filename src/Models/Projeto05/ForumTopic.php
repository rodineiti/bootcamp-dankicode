<?php

namespace Src\Models\Projeto05;

use Src\Core\Model;

class ForumTopic extends Model
{
    public function __construct()
    {
        parent::__construct("forum_topics");
    }

    public function posts()
    {
        if ($this->id) {
            return (new ForumPost())->select()->where('topic_id', '=', $this->id);
        }
        return null;
    }

    public function getTopics($limit, $offset, $search, $forum)
    {
        $results = $this->select()->order('created_at', 'DESC');

        if ($search) {
            $results = $results->where('title', 'like', $search);
        }

        if ($forum) {
            $results = $results->where('forum_id', '=', $forum->id);
        }

        $results = $results->limit($limit)->offset($offset)->all();

        return $results ?? [];
    }

    public function getCount($search, $forum)
    {
        $news = $this->select();

        if ($search) {
            $news = $news->where('title', 'like', $search);
        }

        if ($forum) {
            $news = $news->where('forum_id', '=', $forum->id);
        }

        return $news->count();
    }
}