<?php

namespace Src\Models\Projeto05;

use Src\Core\Model;

class Forum extends Model
{
    public function __construct()
    {
        parent::__construct("forums");
    }

    public function topics()
    {
        if ($this->id) {
            return (new ForumTopic())->select()->where('forum_id', '=', $this->id);
        }
        return null;
    }
}