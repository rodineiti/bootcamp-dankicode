<?php

namespace Src\Models\Projeto07;

use Src\Core\Model;

/**
 * Class Module
 * @package Src\Models\Projeto07
 */
class Module extends Model
{
    /**
     * Module constructor.
     */
    public function __construct()
    {
        parent::__construct("modules");
    }

    public function lessons()
    {
        if ($this->id) {
            return (new Lesson())->select()->where('module_id', '=', $this->id);
        }
        return null;
    }
}

?>