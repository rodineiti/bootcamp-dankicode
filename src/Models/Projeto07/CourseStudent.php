<?php

namespace Src\Models\Projeto07;

use Src\Core\Model;

/**
 * Class CourseStudent
 * @package Src\Models\Projeto07
 */
class CourseStudent extends Model
{
    /**
     * CourseStudent constructor.
     */
    public function __construct()
    {
        parent::__construct("courses_student");
    }
}

?>