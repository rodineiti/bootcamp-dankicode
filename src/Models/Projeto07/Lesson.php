<?php

namespace Src\Models\Projeto07;

use Src\Core\Model;

/**
 * Class Lesson
 * @package Src\Models\Projeto07
 */
class Lesson extends Model
{
    /**
     * Lesson constructor.
     */
    public function __construct()
    {
        parent::__construct("lessons");
    }

    public function module()
    {
        return ($this->module_id) ? (new Module())->findById($this->module_id) : null;
    }
}

?>