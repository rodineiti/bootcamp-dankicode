<?php

namespace Src\Models\Projeto03;

use Src\Core\Model;
use Src\Models\Admin;

class Chat extends Model
{
    public function __construct()
    {
        parent::__construct("chat");
    }

    public function user()
    {
        if ($this->user_id) {
            return (new Admin())->findById($this->user_id);
        }

        return null;
    }
}