<?php

namespace Src\Models\Projeto01;

use Src\Core\Model;

class Visit extends Model
{
    public function __construct()
    {
        parent::__construct("visits");
    }

    public function getCount($date = null)
    {
        $visits = $this->select();

        if ($date) {
            $visits = $visits->where('day', '=', $date);
        }

        return $visits->count();
    }
}

?>