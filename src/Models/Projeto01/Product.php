<?php

namespace Src\Models\Projeto01;

use Src\Core\Model;

class Product extends Model
{
    public function __construct()
    {
        parent::__construct("products");
    }

    public function images()
    {
        if ($this->id) {
            return (new ProductImage())->select()->where('product_id', '=', $this->id);
        }
        return null;
    }
}

?>