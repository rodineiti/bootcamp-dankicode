<?php

namespace Src\Models\Projeto01;

use Src\Core\Model;

class Client extends Model
{
    public function __construct()
    {
        parent::__construct("clients");
    }

    public function finances()
    {
        if ($this->id) {
            return (new Finance())->select()->where('client_id', '=', $this->id);
        }
        return null;
    }
}

?>