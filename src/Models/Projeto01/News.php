<?php

namespace Src\Models\Projeto01;

use Src\Core\Model;

class News extends Model
{
    public function __construct()
    {
        parent::__construct("news");
    }

    public function getNews($limit, $offset, $search, $category)
    {
        $news = $this->select()->order('order_id', 'DESC');

        if ($search) {
            $news = $news->where('title', 'like', $search);
        }

        if ($category) {
            $news = $news->where('category_id', '=', $category->id);
        }

        $news = $news->limit($limit)->offset($offset)->all();

        return $news ?? [];
    }

    public function getCount($search, $category)
    {
        $news = $this->select();

        if ($search) {
            $news = $news->where('title', 'like', $search);
        }

        if ($category) {
            $news = $news->where('category_id', '=', $category->id);
        }

        return $news->count();
    }

    public function category()
    {
        return ($this->category_id) ? (new Category())->findById($this->category_id) : null;
    }

    public function comments()
    {
        if ($this->id) {
            return (new Comment())->select()->where('news_id', '=', $this->id);
        }
        return null;
    }
}

?>