<?php

namespace Src\Models\Projeto01;

use Src\Core\Model;

class Properties extends Model
{
    public function __construct()
    {
        parent::__construct("properties");
    }

    public function imoveis()
    {
        if ($this->id) {
            return (new PropertieHome())->select()->where('property_id', '=', $this->id);
        }
        return null;
    }
}

?>