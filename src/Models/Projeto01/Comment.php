<?php

namespace Src\Models\Projeto01;

use Src\Core\Model;
use Src\Models\Admin;

class Comment extends Model
{
    public function __construct()
    {
        parent::__construct("comments");
    }

    public function answers()
    {
        if ($this->id) {
            return (new CommentAnswer())->select()->where('comment_id', '=', $this->id);
        }
        return null;
    }

    public function user()
    {
        return ($this->user_id) ? (new Admin())->findById($this->user_id) : null;
    }
}

?>