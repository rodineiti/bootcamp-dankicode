<?php

namespace Src\Models\Projeto01;

use Src\Core\Model;

class Finance extends Model
{
    public function __construct()
    {
        parent::__construct("finances");
    }

    public function client()
    {
        if ($this->client_id) {
            return (new Client())->findById($this->client_id);
        }
        return null;
    }
}

?>