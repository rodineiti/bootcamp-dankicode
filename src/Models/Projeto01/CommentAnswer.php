<?php

namespace Src\Models\Projeto01;

use Src\Core\Model;
use Src\Models\Admin;

class CommentAnswer extends Model
{
    public function __construct()
    {
        parent::__construct("comments_answers");
    }

    public function user()
    {
        return ($this->user_id) ? (new Admin())->findById($this->user_id) : null;
    }
}

?>