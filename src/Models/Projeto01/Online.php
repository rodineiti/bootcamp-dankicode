<?php

namespace Src\Models\Projeto01;

use Src\Core\Model;

class Online extends Model
{
    public function __construct()
    {
        parent::__construct("online");
    }

    public function getCount()
    {
        return $this->select()->count();
    }

    public function clear()
    {
        $date = date('Y-m-d H:i:s');
        $onlines = $this->select()->whereRaw("last_action < '{$date}' - INTERVAL 10 MINUTE")->all() ?? [];
        foreach ($onlines as $online) {
            $this->delete(['id' => $online->id]);
        }
    }
}

?>