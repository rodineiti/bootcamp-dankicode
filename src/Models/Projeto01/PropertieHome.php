<?php

namespace Src\Models\Projeto01;

use Src\Core\Model;

class PropertieHome extends Model
{
    public function __construct()
    {
        parent::__construct("properties_homes");
    }

    public function images()
    {
        if ($this->id) {
            return (new PropertiesHomeImage())->select()->where('properties_home_id', '=', $this->id);
        }
        return null;
    }

    public function getFilter($filter = [])
    {
        $results = $this->select();

        if (isset($filter['name']) && !empty($filter['name'])) {
            $results = $results->where('name','LIKE', $filter['name']);
        }

        if (isset($filter['area_min']) && floatval($filter['area_min']) > 0
            && isset($filter['area_max']) && floatval($filter['area_max']) > 0) {
            $results = $results
                ->where('area','>=', $filter['area_min'])
                ->where('area','<=', $filter['area_max']);
        }

        if (isset($filter['value_min']) && floatval($filter['value_min']) > 0
            && isset($filter['value_max']) && floatval($filter['value_max']) > 0) {
            $results = $results
                ->where('value','>=', $filter['value_min'])
                ->where('value','<=', $filter['value_max']);
        }

        return $results->all() ?? [];
    }
}

?>