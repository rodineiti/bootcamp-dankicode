<?php

namespace Src\Models;

use Src\Models\Projeto07\Student;
use Src\Support\Session;

/**
 * Class Auth
 * @package Src\Models
 */
class Auth
{
    public static function admin(): ?Admin
    {
        if (!Session::has("admin")) {
            return null;
        }

        return (new Admin())->findById(Session::get("admin"));
    }

    public static function user(): ?User
    {
        if (!Session::has("user")) {
            return null;
        }

        return (new User())->findById(Session::get("user"));
    }

    public static function student(): ?Student
    {
        if (!Session::has("student")) {
            return null;
        }

        return (new Student())->findById(Session::get("student"));
    }
}

?>