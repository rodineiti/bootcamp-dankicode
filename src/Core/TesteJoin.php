<?php
 
class SelectJoin
{
 
    protected $ons = [];
 
    public function on($localKey, $operator, $referenceKey, $expression = 'and')
    {
        $this->ons[] = [$expression, $localKey, $operator, $referenceKey];
        return $this;
    }
 
    public function orOn($localKey, $operator, $referenceKey)
    {
        $this->on($localKey, $operator, $referenceKey, 'or');
        return $this;
    }
 
    public function getOns()
    {
        return $this->ons;
    }
}
 
class Select
{
 
    protected $joins = [];
 
    public function innerJoin($table, $localKey, $operator = null, $referenceKey = null)
    {
        return $this->join($table, $localKey, $operator, $referenceKey, 'inner');
    }
 
 
    public function join($table, $localKey, $operator = null, $referenceKey = null, $expression = 'left')
    {
 
        if (!in_array($expression, array('inner', 'left', 'right', 'outer'))) {
            throw new Exception('Invalid join expression "' . $expression . '" given. Available expression: inner, left, right, outer');
        }
 
 
        if (is_object($localKey) && ($localKey instanceof \Closure)) {
 
            $subquery = new SelectJoin;
 
            call_user_func_array($localKey, array(&$subquery));
 
            $this->joins[] = array($expression, $table, $subquery);
            return $this;
        }
 
        $this->joins[] = array($expression, $table, $localKey, $operator, $referenceKey);
        return $this;
    }
 
    public function getJoins()
    {
        return $this->joins;
    }
}
 
$user = new Select;
 
$user->innerJoin('companies as c', function ($query) {
    $query->on('c.user_id', '=', 'users.id');
    $query->orOn('c.user_id', '<>', 'users.id');
});
 
 
echo '<pre>';
var_dump($user->getJoins());