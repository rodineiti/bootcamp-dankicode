<?php

namespace Src\Controllers\Projeto06;

use Src\Core\Controller;
use Src\Models\Projeto01\Product;

class ShopController extends Controller
{
    protected $data;

    public function __construct()
    {
        parent::__construct("projeto06/template");
        $this->data = array();
    }

    public function index()
    {
        $products = (new Product())->select()->all() ?? [];
        $this->data['products'] = $products;
        $this->template("projeto06/shop", $this->data);
    }

    public function addToCart($product_id)
    {
        if (!$product = (new Product())->findById($product_id)) {
            setFlashMessage("danger", ["Produto não encontrado."]);
        }

        cart()->add($product, 1);

        back_route(back());
    }

    public function checkout()
    {
        $this->template("projeto06/checkout", $this->data);
    }
}