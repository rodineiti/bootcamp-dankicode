<?php

namespace Src\Controllers\Projeto02;

use Src\Core\Controller;

class NotfoundController extends Controller
{
    public function __construct()
    {
        parent::__construct("projeto02/template");
    }

    public function index()
    {
        $this->template("projeto02/error/404");
    }
}