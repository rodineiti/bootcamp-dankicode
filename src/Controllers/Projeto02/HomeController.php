<?php

namespace Src\Controllers\Projeto02;

use Src\Core\Controller;
use Src\Models\Projeto01\Properties;

class HomeController extends Controller
{
    protected $data;

    public function __construct()
    {
        parent::__construct("projeto02/template");
        $this->data = array();
    }

    public function index()
    {
        $this->template("projeto02/home", $this->data);
    }

    public function empreentimento($slug)
    {
        $item = (new Properties())->findById($slug);

        $this->data['item'] = $item;
        $this->template("projeto02/empreendimento", $this->data);
    }
}