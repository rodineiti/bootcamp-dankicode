<?php

namespace Src\Controllers\Projeto02;

use Src\Core\Controller;
use Src\Models\Projeto01\PropertieHome;

class AjaxController extends Controller
{
    public function __construct()
    {
        parent::__construct("projeto02/template");
    }

    public function send()
    {
        if ($this->method() !== 'POST') {
            $this->json(['error' => true, 'message' => 'Method not allowed']);
        }

        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        $search_imovel = isset($data['name']) ? trim($data['name']) : '';
        $area_min = isset($data['area_min']) ? trim($data['area_min']) : 0;
        $area_max = isset($data['area_max']) ? trim($data['area_max']) : 0;
        $value_min = isset($data['value_min']) ? trim(str_replace(['R$',' '], '', $data['value_min'])) : 0;
        $value_max = isset($data['value_max']) ? trim(str_replace(['R$',' '], '', $data['value_max'])) : 0;
        $value_min = str_price_db($value_min);
        $value_max = str_price_db($value_max);

        $filters = [
            'name' => $search_imovel,
            'area_min' => $area_min,
            'area_max' => $area_max,
            'value_min' => $value_min,
            'value_max' => $value_max,
        ];

        $imoveis = (new PropertieHome())->getFilter($filters);

        //        $response = [];
        //        foreach ($imoveis as $item) {
        //            $response[] = $item->data();
        //        }

        $html = '<h2 class="card-title">Listando <b>'.count($imoveis).' imóveis</b></h2>';

        foreach($imoveis as $value) {
            $image = $value->images()->first();
            $html .= '<div class="row-imoveis">
				<div class="r1">
					<img src="' . media('uploads/' . $image->image) . '" alt="img" />
				</div>
				<div class="r2">
					<p><i class="fa fa-info"></i> Nome do Imóvel: <b>' . $value->name . '</b></p>
					<p><i class="fa fa-info"></i> Área do Imóvel: <b>' . $value->area . '</b></p>
					<p><i class="fa fa-info"></i> Valor do Imóvel: <b>' . str_price($value->value) . '</b></p>
				</div>
			</div>';
        }

        $this->json(['error' => false, 'message' => 'Resposta com sucesso', 'html' => $html]);
    }
}