<?php

namespace Src\Controllers\Projeto05;

use Src\Core\Controller;
use Src\Models\Projeto05\Forum;
use Src\Models\Projeto05\ForumPost;
use Src\Models\Projeto05\ForumTopic;

class ForumController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("projeto05/template");
        $this->data = array();
    }

    public function index($slug)
    {
        $request = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        $forum = null;
        $limit = 10;
        $page = !empty($request["page"]) ? intval($request["page"]) : 1;
        $offset = (($page * $limit) - $limit) ?? 0;
        $search = $request["search"] ?? null;

        if ($slug !== 'all') {
            $forum = (new Forum())->select()->where('id', '=', $slug)->first();
        }

        $topics = (new ForumTopic())->getTopics($limit, $offset, $search, $forum);
        $count = (new ForumTopic())->getCount($search, $forum);

        $pages = ceil($count / $limit);

        $this->data['forums'] = (new Forum())->select()->order('created_at', 'DESC')->all() ?? [];
        $this->data['slug'] = $slug;
        $this->data['forum'] = $forum;
        $this->data['topics'] = $topics;
        $this->data["total"] = count($topics);
        $this->data["pages"] = $pages;
        $this->data["page"] = $page;

        $this->template("projeto05/forum", $this->data);
    }

    public function single($id)
    {
        $topic = (new ForumTopic())->findById($id);

        $this->data['topic'] = $topic;

        $this->template("projeto05/topico", $this->data);
    }

    public function store()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        $this->required = ["topic_id", "message"];
        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("projeto05.forum", ["slug" => "all"]));
        }

        if (!$topic = (new ForumTopic())->findById($data['topic_id'])) {
            setFlashMessage("danger", ["Tópico não encontrado."]);
            back_route(route("projeto05.forum", ["slug" => "all"]));
        }

        $model = new ForumPost();
        $model->topic_id = $topic->id;
        $model->message = $data['message'];
        $model->user_id = auth("admins")->id;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("projeto05.single", ["id" => $topic->id]));
        }

        setFlashMessage("success", ["Publicação adicionada com sucesso"]);
        back_route(route("projeto05.single", ["id" => $topic->id]));
    }

    public function storeForum()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        $this->required = ["title"];
        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("projeto05.forum", ["slug" => "all"]));
        }

        $model = new Forum();
        $model->title = $data['title'];

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("projeto05.forum", ["slug" => "all"]));
        }

        setFlashMessage("success", ["Fórum adicionado com sucesso"]);
        back_route(route("projeto05.forum", ["slug" => "all"]));
    }

    public function storeTopic()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        $this->required = ["forum_id","title"];
        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("projeto05.forum", ["slug" => "all"]));
        }

        if (!$forum = (new Forum())->findById($data['forum_id'])) {
            setFlashMessage("danger", ["Fórum não encontrado."]);
            back_route(route("projeto05.forum", ["slug" => "all"]));
        }

        $model = new ForumTopic();
        $model->forum_id = $forum->id;
        $model->title = $data['title'];

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("projeto05.forum", ["slug" => "all"]));
        }

        setFlashMessage("success", ["Tópico adicionado com sucesso"]);
        back_route(route("projeto05.forum", ["slug" => "all"]));
    }
}