<?php

namespace Src\Controllers\Projeto01;

use Src\Core\Controller;

class AjaxController extends Controller
{
    public function __construct()
    {
        parent::__construct("projeto01/template");
    }
}