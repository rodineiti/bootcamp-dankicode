<?php

namespace Src\Controllers\Projeto01;

use Src\Core\Controller;
use Src\Models\Projeto01\Category;
use Src\Models\Projeto01\Deposition;
use Src\Models\Projeto01\News;
use Src\Models\Projeto01\Service;
use Src\Models\Projeto01\Slide;

class HomeController extends Controller
{
    protected $data;

    public function __construct()
    {
        parent::__construct("projeto01/template");
        $this->data = array();
        online_projeto01();
        visit_projeto01();
    }

    public function index()
    {
        $this->setData();

        $this->template("projeto01/home", $this->data);
    }

    public function sobre()
    {
        $this->setData();

        $this->data['target'] = 'sobre';
        $this->template("projeto01/home", $this->data);
    }

    public function especialidades()
    {
        $this->setData();

        $this->data['target'] = 'especialidades';
        $this->template("projeto01/home", $this->data);
    }

    public function servicos()
    {
        $this->setData();

        $this->data['target'] = 'servicos';
        $this->template("projeto01/home", $this->data);
    }

    public function noticias($slug)
    {
        $request = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        $categoria = null;
        $limit = 10;
        $page = !empty($request["page"]) ? intval($request["page"]) : 1;
        $offset = (($page * $limit) - $limit) ?? 0;
        $search = $request["search"] ?? null;

        if ($slug !== 'all') {
            $categoria = (new Category())->select()->where('slug', '=', $slug)->first();
        }

        $news = (new News())->getNews($limit, $offset, $search, $categoria);
        $count = (new News())->getCount($search, $categoria);

        $pages = ceil($count / $limit);

        $this->data['categorias'] = (new Category())->select()->order('order_id', 'DESC')->all();
        $this->data['slug'] = $slug;
        $this->data['categoria'] = $categoria;
        $this->data['noticias'] = $news;
        $this->data["total"] = count($news);
        $this->data["pages"] = $pages;
        $this->data["page"] = $page;

        $this->template("projeto01/noticias", $this->data);
    }

    public function noticia_single($id)
    {
        $new = (new News())->findById($id);

        $this->data['post'] = $new;

        $this->template("projeto01/noticia", $this->data);
    }

    public function contato()
    {
        $this->template("projeto01/contato", $this->data);
    }

    private function setData()
    {
        $depoimentos = (new Deposition())->select()->order('order_id', 'DESC')->limit(3)->all();
        $services = (new Service())->select()->order('order_id', 'DESC')->limit(4)->all();
        $slides = (new Slide())->select()->all();

        $this->data['depoimentos'] = $depoimentos;
        $this->data['servicos'] = $services;
        $this->data['slides'] = $slides;
    }
}