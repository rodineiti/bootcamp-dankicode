<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;

class NotfoundController extends Controller
{
    public function __construct()
    {
        parent::__construct("projeto01/template");
    }

    public function index()
    {
        $this->template("projeto01/error/404");
    }
}