<?php

namespace Src\Controllers\Projeto08;

use Src\Core\Controller;
use Src\Models\User;

class AuthController extends Controller
{
    protected $user;
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct();
        $this->user = new User();
        $this->data = array();
    }

    public function login()
    {
        $request = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        $this->required = ["email", "password"];
        if (!$this->required($request)) {
            setFlashMessage("danger", ["Favor, informar seu e-mail e senha"]);
            back_route(route("projeto08.login"));
        }

        $email = $request["email"];
        $password = $request["password"];

        $user = $this->user->attempt($email, $password);

        if (!$user) {
            setFlashMessage("danger", ["Usuário e/ou Senha errados!"]);
            back_route(route("projeto08.login"));
        }

        $this->user->setSession($user);

        setFlashMessage("success", ["Bem vindo " . auth()->name]);
        back_route(route("projeto08.feed"));
    }

    public function logout()
    {
        $this->user->destroySession();
        back_route(route("projeto08.home"));
    }
}