<?php

namespace Src\Controllers\Projeto08;

use Src\Core\Controller;
use Src\Models\Projeto08\Feed;
use Src\Models\Projeto08\Request;
use Src\Models\User;

class HomeController extends Controller
{
    protected $data;

    public function __construct()
    {
        parent::__construct("projeto08/template");
        $this->data = array();
    }

    public function home()
    {
        if (check()) {
            back_route(route("projeto08.feed"));
        }

        $this->template("projeto08/home", $this->data);
    }

    public function feed()
    {
        $amigos = (new User())->select()->whereIn('id', (new Request())->getFriends())->all() ?? [];
        $this->data['amigos'] = $amigos;
        $this->data['posts'] = (new Feed())->select()->all() ?? [];
        $this->template("projeto08/feed", $this->data);
    }

    public function post()
    {
        $request = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        $this->required = ["post"];
        if (!$this->required($request)) {
            setFlashMessage("danger", ["Favor, informar o texto do post"]);
            back_route(route("projeto08.feed"));
        }

        $model = new Feed();
        $model->post = $request['post'];
        $model->user_id = auth()->id;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("projeto08.feed"));
        }

        setFlashMessage("success", ["Post adicionado com sucesso"]);
        back_route(route("projeto08.feed"));
    }

    public function solicitacoes()
    {
        $this->data['solicitacoes'] = getSolicitacoes()->all() ?? [];
        $this->template("projeto08/solicitacoes", $this->data);
    }

    public function comunidade()
    {
        $this->data['members'] = (new User())->select()->where('id', '<>', auth()->id)->all() ?? [];
        $this->template("projeto08/comunidade", $this->data);
    }

    public function adicionar($id)
    {
        if (!$model = (new User())->findById($id)) {
            setFlashMessage("danger", ["Usuário não encontrado"]);
            back_route(route("projeto08.comunidade"));
        }

        $r = new Request();
        $r->id_from = auth()->id;
        $r->id_to = $model->id;
        $r->status = 0;
        $r->save();

        setFlashMessage("success", ["Solicitação enviada com sucesso"]);
        back_route(route("projeto08.comunidade"));
    }

    public function desfazer($id)
    {
        if (!$model = (new User())->findById($id)) {
            setFlashMessage("danger", ["Usuário não encontrado"]);
            back_route(route("projeto08.comunidade"));
        }

        $r = (new Request())->select()
            ->where('id_from', '=', auth()->id)
            ->where('id_to', '=', $model->id)->first();

        if ($r) {
            $r->destroy();
        }

        $r = (new Request())->select()
            ->where('id_from', '=', $model->id)
            ->where('id_to', '=', auth()->id)->first();

        if ($r) {
            $r->destroy();
        }

        setFlashMessage("success", ["Solicitação defeita com sucesso"]);
        back_route(back());
    }

    public function aceitar($id)
    {
        if (!$model = (new Request())->findById($id)) {
            setFlashMessage("danger", ["Solicitação não encontrada"]);
            back_route(route("projeto08.solicitacoes"));
        }

        $model->status = 1;
        $model->save();

        setFlashMessage("success", ["Solicitação aceita com sucesso"]);
        back_route(route("projeto08.solicitacoes"));
    }

    public function recusar($id)
    {
        if (!$model = (new Request())->findById($id)) {
            setFlashMessage("danger", ["Solicitação não encontrada"]);
            back_route(back());
        }

        $model->destroy();

        setFlashMessage("success", ["Solicitação recusada com sucesso"]);
        back_route(back());
    }
}