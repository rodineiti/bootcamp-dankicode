<?php

namespace Src\Controllers\Projeto07;

use Src\Core\Controller;
use Src\Models\Projeto07\CourseStudent;
use Src\Models\Projeto07\Lesson;
use Src\Models\Projeto07\Module;

class HomeController extends Controller
{
    protected $data;

    public function __construct()
    {
        parent::__construct("projeto07/template");
        $this->data = array();
    }

    public function login()
    {
        if (check("students")) {
            back_route(route('projeto07.home'));
        }
        $this->template("projeto07/login", $this->data);
    }

    public function home()
    {
        if (!check("students")) {
            back_route(route('projeto07.login'));
        }

        $this->data['access'] = (new CourseStudent())->select()
            ->where('student_id', '=', auth("students")->id)->count();
        $this->data['modules'] = (new Module())->select()->all() ?? [];
        $this->template("projeto07/home", $this->data);
    }

    public function comprar()
    {
        $add = new CourseStudent();
        $add->student_id = auth("students")->id;
        $add->save();

        back_route(back());
    }

    public function aula($id)
    {
        if (!$aula = (new Lesson())->findById($id)) {
            setFlashMessage("danger", ["Aula não encontrada."]);
            back_route(route("projeto07.home"));
        }

        $this->data['aula'] = $aula;
        $this->template("projeto07/aula", $this->data);
    }
}