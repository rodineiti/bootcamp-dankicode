<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\User;

class HomeController extends Controller
{
    protected $data;

    public function __construct()
    {
        parent::__construct();
        $this->data = array();
    }

    public function index()
    {
        // $user = new User();
        // $user = $user->select();

        // dd($user);

        $this->template("home", $this->data);
    }
}