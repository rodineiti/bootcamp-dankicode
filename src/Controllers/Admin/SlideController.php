<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto01\Slide;

class SlideController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["name"];
    }

    public function index()
    {
        $this->data["list"] = (new Slide())->select()->all();
        $this->template("admin/slides/index", $this->data);
    }

    public function create()
    {
        $this->template("admin/slides/create");
    }

    public function store()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.slides.create"));
        }

        if ($this->request()->hasFile('image')) {
            $image = $this->request()->file('image');
            if (!$image["error"]) {
                $data["image"] = $image;
            }
        }

        if (isset($data["image"]) && count($data["image"])) {
            if (in_array($data["image"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $data["slide"] = cutImage(
                    $data["image"],
                    1440,
                    800,
                    CONF_UPLOAD_FILE_UPLOADS
                );
            }
        }

        $model = new Slide();
        $model->name = $data['name'];
        $model->slide = $data['slide'];

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.slides.create"));
        }

        setFlashMessage("success", ["Slide adicionado com sucesso"]);
        back_route(route("admin.slides.index"));
    }

    public function edit($id)
    {
        if (!$model = (new Slide())->findById($id)) {
            setFlashMessage("danger", ["Slide não encontrado."]);
            back_route(route("admin.slides.index"));
        }

        $this->data = array();
        $this->data["item"] = $model;
        $this->template("admin/slides/edit", $this->data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$model = (new Slide())->findById($id)) {
            setFlashMessage("danger", ["Slide não encontrado."]);
            back_route(route("admin.slides.index"));
        }

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.slides.edit", ["id" => $id]));
        }

        if ($this->request()->hasFile('image')) {
            $image = $this->request()->file('image');
            if (!$image["error"]) {
                $data["image"] = $image;
            }
        }

        if (isset($data["image"]) && count($data["image"])) {
            if (in_array($data["image"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $data["slide"] = cutImage(
                    $data["image"],
                    1440,
                    800,
                    CONF_UPLOAD_FILE_UPLOADS
                );
                removeFile(CONF_UPLOAD_FILE_UPLOADS, $model->slide);
            }
        }

        $model->name = $data['name'];
        $model->slide = $data['slide'];

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.slides.edit", ["id" => $id]));
        }

        setFlashMessage("success", ["Slide atualizado com sucesso"]);
        back_route(route("admin.slides.edit", ["id" => $id]));
    }

    public function destroy($id)
    {
        if (!$model = (new Slide())->findById($id)) {
            setFlashMessage("danger", ["Slide não encontrado."]);
            back_route(route("admin.slides.index"));
        }

        removeFile(CONF_UPLOAD_FILE_UPLOADS, $model->slide);

        $model->destroy();

        setFlashMessage("success", ["Slide deletado com sucesso"]);
        back_route(route("admin.slides.index"));
    }
}