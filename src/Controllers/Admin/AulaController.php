<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto07\Lesson;
use Src\Models\Projeto07\Module;

class AulaController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["module_id", "name", "link"];
    }

    public function index()
    {
        $this->data["list"] = (new Lesson())->select()->all();
        $this->template("admin/aulas/index", $this->data);
    }

    public function create()
    {
        $this->data['modules'] = (new Module())->select()->all() ?? [];
        $this->template("admin/aulas/create", $this->data);
    }

    public function store()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.aulas.create"));
        }

        $model = new Lesson();
        $model->module_id = $data['module_id'];
        $model->name = $data['name'];
        $model->link = $data['link'];

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.aulas.create"));
        }

        setFlashMessage("success", ["Aula adicionado com sucesso"]);
        back_route(route("admin.aulas.index"));
    }

    public function edit($id)
    {
        if (!$find = (new Lesson())->findById($id)) {
            setFlashMessage("danger", ["Aula não encontrado."]);
            back_route(route("admin.aulas.index"));
        }

        $this->data = array();
        $this->data["item"] = $find;
        $this->data['modules'] = (new Module())->select()->all() ?? [];
        $this->template("admin/aulas/edit", $this->data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$model = (new Lesson())->findById($id)) {
            setFlashMessage("danger", ["Aula não encontrado."]);
            back_route(route("admin.aulas.index"));
        }

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.aulas.edit", ["id" => $id]));
        }

        $model->module_id = $data['module_id'];
        $model->name = $data['name'];
        $model->link = $data['link'];

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.aulas.edit", ["id" => $id]));
        }

        setFlashMessage("success", ["Aula atualizado com sucesso"]);
        back_route(route("admin.aulas.edit", ["id" => $id]));
    }

    public function destroy($id)
    {
        if (!$model = (new Lesson())->findById($id)) {
            setFlashMessage("danger", ["Aula não encontrado."]);
            back_route(route("admin.aulas.index"));
        }

        $model->destroy();

        setFlashMessage("success", ["Aula deletado com sucesso"]);
        back_route(route("admin.aulas.index"));
    }
}