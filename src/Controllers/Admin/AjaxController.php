<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;

class AjaxController extends Controller
{
    public function __construct()
    {
        parent::__construct("admin/template");
    }

    public function empreendimentos()
    {
        if ($this->method() !== 'POST') {
            $this->json(['error' => true, 'message' => 'Method not allowed']);
        }

        $this->json(['error' => false, 'message' => 'Resposta com sucesso']);
    }
}