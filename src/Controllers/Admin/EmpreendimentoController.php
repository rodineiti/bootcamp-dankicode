<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto01\Properties;

class EmpreendimentoController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["name","description","type","value"];
    }

    public function index()
    {
        $this->data["list"] = (new Properties())->select()->all() ?? [];
        $this->template("admin/empreendimentos/index", $this->data);
    }

    public function create()
    {
        $this->template("admin/empreendimentos/create", $this->data);
    }

    public function store()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);
        $description = $this->request()->description;

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.empreendimentos.create"));
        }

        $data['slug'] = str_slug($data['name']);

        if ((new Properties())->select()->where('slug', '=', $data['slug'])->first()) {
            setFlashMessage("danger", ["Já existe um empreendimento com este slug como chave"]);
            back_route(route("admin.empreendimentos.create"));
        }

        if ($this->request()->hasFile('image')) {
            $image = $this->request()->file('image');
            if (!$image["error"]) {
                $data["image"] = $image;
            }
        }

        if (isset($data["image"]) && count($data["image"])) {
            if (in_array($data["image"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $data["image"] = cutImage(
                    $data["image"],
                    1440,
                    800,
                    CONF_UPLOAD_FILE_UPLOADS
                );
            }
        }

        $model = new Properties();
        $model->name = $data["name"];
        $model->type = $data["type"];
        $model->slug = $data["slug"];
        $model->value = str_price_db($data["value"]) ?? 0;
        $model->description = $description;
        $model->image = $data['image'] ?? "default.png";

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.empreendimentos.create"));
        }

        setFlashMessage("success", ["Empreendimento adicionado com sucesso"]);
        back_route(route("admin.empreendimentos.index"));
    }

    public function edit($id)
    {
        if (!$find = (new Properties())->findById($id)) {
            setFlashMessage("danger", ["Empreendimento não encontrado."]);
            back_route(route("admin.empreendimentos.index"));
        }

        $this->data = array();
        $this->data["item"] = $find;
        $this->template("admin/empreendimentos/edit", $this->data);
    }

    public function show($id)
    {
        if (!$find = (new Properties())->findById($id)) {
            setFlashMessage("danger", ["Empreendimento não encontrado."]);
            back_route(route("admin.empreendimentos.index"));
        }

        $this->data = array();
        $this->data["item"] = $find;
        $this->data["list"] = $find->imoveis()->all() ?? [];
        $this->template("admin/empreendimentos/imoveis/index", $this->data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);
        $description = $this->request()->description;

        if (!$model = (new Properties())->findById($id)) {
            setFlashMessage("danger", ["Empreendimento não encontrado."]);
            back_route(route("admin.empreendimentos.index"));
        }

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.empreendimentos.edit", ["id" => $id]));
        }

        $data['slug'] = str_slug($data['name']);

        $checkSlug = (new Properties())->select()
            ->where('slug', '=', $data['slug'])
            ->where('id', '<>', $id)
            ->first();

        if ($checkSlug) {
            setFlashMessage("danger", ["Já existe uma notícia com este slug como chave"]);
            back_route(route("admin.noticias.edit", ["id" => $id]));
        }

        if ($this->request()->hasFile('image')) {
            $image = $this->request()->file('image');
            if (!$image["error"]) {
                $data["image"] = $image;
            }
        }

        if (isset($data["image"]) && count($data["image"])) {
            if (in_array($data["image"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $data["image"] = cutImage(
                    $data["image"],
                    1440,
                    800,
                    CONF_UPLOAD_FILE_UPLOADS
                );
                removeFile(CONF_UPLOAD_FILE_UPLOADS, $model->image);
            }
        }

        $model->name = $data["name"];
        $model->type = $data["type"];
        $model->slug = $data["slug"];
        $model->value = str_price_db($data["value"]) ?? 0;
        $model->description = $description;
        if ($data['image'] && $data['image'] !== "") {
            $model->image = $data['image'];
        }

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.empreendimentos.edit", ["id" => $id]));
        }

        setFlashMessage("success", ["Empreendimento atualizado com sucesso"]);
        back_route(route("admin.empreendimentos.edit", ["id" => $id]));
    }

    public function destroy($id)
    {
        if (!$model = (new Properties())->findById($id)) {
            setFlashMessage("danger", ["Empreendimento não encontrado."]);
            back_route(route("admin.empreendimentos.index"));
        }

        $imoveis = $model->imoveis()->all() ?? [];
        foreach ($imoveis as $imovel) {
            $images = $imovel->images()->all() ?? [];

            foreach ($images as $image) {
                removeFile(CONF_UPLOAD_FILE_UPLOADS, $image->image);
                $image->destroy();
            }

            $imovel->destroy();
        }

        removeFile(CONF_UPLOAD_FILE_UPLOADS, $model->image);

        $model->destroy();

        setFlashMessage("success", ["Empreendimento deletado com sucesso"]);
        back_route(route("admin.empreendimentos.index"));
    }
}