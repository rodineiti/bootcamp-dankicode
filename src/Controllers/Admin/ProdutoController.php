<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto01\Product;
use Src\Models\Projeto01\ProductImage;

class ProdutoController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["name","description","width","height","weight","qty","value","length"];
    }

    public function index()
    {
        $this->data["list"] = (new Product())->select()->all();
        $this->template("admin/produtos/index", $this->data);
    }

    public function create()
    {
        $this->template("admin/produtos/create", $this->data);
    }

    public function store()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);
        $description = $this->request()->description;

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.produtos.create"));
        }

        if ($this->request()->hasFile('images')) {
            $images = $this->request()->file('images');
            if (!in_array(true, $images["error"])) {
                $data["images"] = $images;
            }
        }

        $model = new Product();
        $model->name = $data["name"];
        $model->width = $data["width"] ?? 0;
        $model->height = $data["height"] ?? 0;
        $model->weight = $data["weight"] ?? 0;
        $model->length = $data["length"] ?? 0;
        $model->qty = $data["qty"] ?? 0;
        $model->value = str_price_db($data["value"]) ?? 0;
        $model->description = $description;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.produtos.create"));
        }

        $errors = [];
        if (isset($data["images"]) && count($data["images"])) {
            for ($i = 0; $i < count($data["images"]['name']); $i++) {
                if (in_array($data["images"]['type'][$i], ["image/jpeg", "image/jpg", "image/png"])) {
                    $file = [
                        'tmp_name' => $data["images"]['tmp_name'][$i],
                        'name' => $data["images"]['name'][$i],
                        'type' => $data["images"]['type'][$i],
                        'size' => $data["images"]['size'][$i]
                    ];
                    $image = cutImage(
                        $file,
                        1440,
                        800,
                        CONF_UPLOAD_FILE_UPLOADS
                    );
                    $productImage = new ProductImage();
                    $productImage->image = $image;
                    $productImage->product_id = $model->id;
                    if (!$productImage->save()) {
                        $errors[] = $productImage->error()->getMessage();
                    }
                }
            }
        }

        if (count($errors)) {
            setFlashMessage("danger", $errors);
        }

        setFlashMessage("success", ["Produto adicionado com sucesso"]);
        back_route(route("admin.produtos.index"));
    }

    public function edit($id)
    {
        if (!$find = (new Product())->findById($id)) {
            setFlashMessage("danger", ["Produto não encontrado."]);
            back_route(route("admin.produtos.index"));
        }

        $this->data = array();
        $this->data["item"] = $find;
        $this->template("admin/produtos/edit", $this->data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);
        $description = $this->request()->description;

        if (!$model = (new Product())->findById($id)) {
            setFlashMessage("danger", ["Produto não encontrado."]);
            back_route(route("admin.produtos.index"));
        }

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.produtos.edit", ["id" => $id]));
        }

        if ($this->request()->hasFile('images')) {
            $images = $this->request()->file('images');
            if (!in_array(true, $images["error"])) {
                $data["images"] = $images;
            }
        }

        $model->name = $data["name"];
        $model->width = $data["width"] ?? 0;
        $model->height = $data["height"] ?? 0;
        $model->weight = $data["weight"] ?? 0;
        $model->length = $data["length"] ?? 0;
        $model->qty = $data["qty"] ?? 0;
        $model->value = str_price_db($data["value"]) ?? 0;
        $model->description = $description;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.produtos.edit", ["id" => $id]));
        }

        $errors = [];
        if (isset($data["images"]) && count($data["images"])) {
            for ($i = 0; $i < count($data["images"]['name']); $i++) {
                if (in_array($data["images"]['type'][$i], ["image/jpeg", "image/jpg", "image/png"])) {
                    $file = [
                        'tmp_name' => $data["images"]['tmp_name'][$i],
                        'name' => $data["images"]['name'][$i],
                        'type' => $data["images"]['type'][$i],
                        'size' => $data["images"]['size'][$i]
                    ];
                    $image = cutImage(
                        $file,
                        1440,
                        800,
                        CONF_UPLOAD_FILE_UPLOADS
                    );
                    $productImage = new ProductImage();
                    $productImage->image = $image;
                    $productImage->product_id = $model->id;
                    if (!$productImage->save()) {
                        $errors[] = $productImage->error()->getMessage();
                    }
                }
            }
        }

        if (count($errors)) {
            setFlashMessage("danger", $errors);
        }

        setFlashMessage("success", ["Produto atualizado com sucesso"]);
        back_route(route("admin.produtos.edit", ["id" => $id]));
    }

    public function updateQty($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_VALIDATE_INT);

        if (!$model = (new Product())->findById($id)) {
            setFlashMessage("danger", ["Produto não encontrado."]);
            back_route(route("admin.produtos.index"));
        }

        $this->required = ["qty"];
        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.produtos.index"));
        }

        $model->qty = $data['qty'];

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.produtos.index"));
        }

        setFlashMessage("success", ["Quantidade do produto atualizado com sucesso"]);
        back_route(route("admin.produtos.index"));
    }

    public function destroy($id)
    {
        if (!$model = (new Product())->findById($id)) {
            setFlashMessage("danger", ["Produto não encontrado."]);
            back_route(route("admin.produtos.index"));
        }

        $images = $model->images()->all() ?? [];

        foreach ($images as $image) {
            removeFile(CONF_UPLOAD_FILE_UPLOADS, $image->image);
        }

        $model->destroy();

        setFlashMessage("success", ["Produto deletado com sucesso"]);
        back_route(route("admin.produtos.index"));
    }

    public function destroyImage($product_id, $id)
    {
        if (!$model = (new Product())->findById($product_id)) {
            setFlashMessage("danger", ["Produto não encontrado."]);
            back_route(route("admin.produtos.index"));
        }

        $image = $model->images()->where('id', '=', $id)->first();

        if ($image) {
            removeFile(CONF_UPLOAD_FILE_UPLOADS, $image->image);
        }

        $image->destroy();

        setFlashMessage("success", ["Imagem do produto deletado com sucesso"]);
        back_route(route("admin.produtos.edit", ["id" => $model->id]));
    }
}