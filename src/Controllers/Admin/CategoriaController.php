<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto01\Category;

class CategoriaController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["title"];
    }

    public function index()
    {
        $this->data["list"] = (new Category())->select()->all();
        $this->template("admin/categorias/index", $this->data);
    }

    public function create()
    {
        $this->template("admin/categorias/create");
    }

    public function store()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.categorias.create"));
        }

        $data['slug'] = str_slug($data['title']);

        if ((new Category())->select()->where('slug', '=', $data['slug'])->first()) {
            setFlashMessage("danger", ["Já existe uma categoria com este slug como chave"]);
            back_route(route("admin.categorias.create"));
        }

        $model = new Category();
        $model->title = $data['title'];
        $model->slug = $data['slug'];        

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.categorias.create"));
        }

        setFlashMessage("success", ["Categoria adicionado com sucesso"]);
        back_route(route("admin.categorias.index"));
    }

    public function edit($id)
    {
        if (!$find = (new Category())->findById($id)) {
            setFlashMessage("danger", ["Categoria não encontrado."]);
            back_route(route("admin.categorias.index"));
        }

        $this->data = array();
        $this->data["item"] = $find;
        $this->template("admin/categorias/edit", $this->data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$model = (new Category())->findById($id)) {
            setFlashMessage("danger", ["Categoria não encontrado."]);
            back_route(route("admin.categorias.index"));
        }

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.categorias.edit", ["id" => $id]));
        }

        $data['slug'] = str_slug($data['title']);

        $checkSlug = (new Category())->select()
            ->where('slug', '=', $data['slug'])
            ->where('id', '<>', $id)
            ->first();

        if ($checkSlug) {
            setFlashMessage("danger", ["Já existe uma categoria com este slug como chave"]);
            back_route(route("admin.categorias.edit", ["id" => $id]));
        }

        $model->title = $data['title'];
        $model->slug = $data['slug'];

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.categorias.edit", ["id" => $id]));
        }

        setFlashMessage("success", ["Categoria atualizado com sucesso"]);
        back_route(route("admin.categorias.edit", ["id" => $id]));
    }

    public function destroy($id)
    {
        if (!$model = (new Category())->findById($id)) {
            setFlashMessage("danger", ["Categoria não encontrado."]);
            back_route(route("admin.categorias.index"));
        }

        $model->destroy();

        setFlashMessage("success", ["Categoria deletado com sucesso"]);
        back_route(route("admin.categorias.index"));
    }
}