<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto01\Comment;
use Src\Models\Projeto01\CommentAnswer;
use Src\Models\Projeto01\News;

class ComentarioController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["news_id","comment"];
    }

    public function storeComment()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("projeto01.noticias", ["slug" => "all"]));
        }

        if (!$post = (new News())->findById($data['news_id'])) {
            setFlashMessage("danger", ["Notícia não encontrada."]);
            back_route(route("projeto01.noticias", ["slug" => "all"]));
        }

        $model = new Comment();
        $model->news_id = $post->id;
        $model->comment = $data['comment'];
        $model->user_id = auth("admins")->id;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("projeto01.noticia_single", ["id" => $post->id]));
        }

        setFlashMessage("success", ["Comentário adicionado com sucesso"]);
        back_route(route("projeto01.noticia_single", ["id" => $post->id]));
    }

    public function storeAnswer()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        $this->required = ["news_id", "comment_id","answer"];
        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("projeto01.noticias", ["slug" => "all"]));
        }

        if (!$post = (new News())->findById($data['news_id'])) {
            setFlashMessage("danger", ["Notícia não encontrada."]);
            back_route(route("projeto01.noticias", ["slug" => "all"]));
        }

        if (!$comment = (new Comment())->findById($data['comment_id'])) {
            setFlashMessage("danger", ["Comentário não encontrada."]);
            back_route(route("projeto01.noticias", ["slug" => "all"]));
        }

        $model = new CommentAnswer();
        $model->comment_id = $comment->id;
        $model->answer = $data['answer'];
        $model->user_id = auth("admins")->id;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("projeto01.noticia_single", ["id" => $post->id]));
        }

        setFlashMessage("success", ["Resposta adicionada com sucesso"]);
        back_route(route("projeto01.noticia_single", ["id" => $post->id]));
    }
}