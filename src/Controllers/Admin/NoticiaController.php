<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto01\Category;
use Src\Models\Projeto01\News;

class NoticiaController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["title","description", "category_id"];
    }

    public function index()
    {
        $this->data["list"] = (new News())->select()->all();
        $this->template("admin/noticias/index", $this->data);
    }

    public function create()
    {
        $this->data["categorias"] = (new Category())->select()->all();
        $this->template("admin/noticias/create", $this->data);
    }

    public function store()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);
        $description = $this->request()->description;

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.noticias.create"));
        }

        $data['slug'] = str_slug($data['title']);

        if ((new News())->select()->where('slug', '=', $data['slug'])->first()) {
            setFlashMessage("danger", ["Já existe uma notícias com este slug como chave"]);
            back_route(route("admin.noticias.create"));
        }

        if ($this->request()->hasFile('image')) {
            $image = $this->request()->file('image');
            if (!$image["error"]) {
                $data["image"] = $image;
            }
        }

        if (isset($data["image"]) && count($data["image"])) {
            if (in_array($data["image"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $data["image"] = cutImage(
                    $data["image"],
                    1440,
                    800,
                    CONF_UPLOAD_FILE_UPLOADS
                );
            }
        }

        $model = new News();
        $model->category_id = $data['category_id'];
        $model->title = $data['title'];
        $model->slug = $data['slug'];
        $model->description = $description;
        $model->image = $data['image'] ?? null;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.noticias.create"));
        }

        setFlashMessage("success", ["Notícia adicionado com sucesso"]);
        back_route(route("admin.noticias.index"));
    }

    public function edit($id)
    {
        if (!$find = (new News())->findById($id)) {
            setFlashMessage("danger", ["Notícia não encontrado."]);
            back_route(route("admin.noticias.index"));
        }

        $this->data = array();
        $this->data["item"] = $find;
        $this->data["categorias"] = (new Category())->select()->all();
        $this->template("admin/noticias/edit", $this->data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);
        $description = $this->request()->description;

        if (!$model = (new News())->findById($id)) {
            setFlashMessage("danger", ["Notícia não encontrado."]);
            back_route(route("admin.noticias.index"));
        }

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.noticias.edit", ["id" => $id]));
        }

        $data['slug'] = str_slug($data['title']);

        $checkSlug = (new News())->select()
            ->where('slug', '=', $data['slug'])
            ->where('id', '<>', $id)
            ->first();

        if ($checkSlug) {
            setFlashMessage("danger", ["Já existe uma notícia com este slug como chave"]);
            back_route(route("admin.noticias.edit", ["id" => $id]));
        }

        if ($this->request()->hasFile('image')) {
            $image = $this->request()->file('image');
            if (!$image["error"]) {
                $data["image"] = $image;
            }
        }

        if (isset($data["image"]) && count($data["image"])) {
            if (in_array($data["image"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $data["image"] = cutImage(
                    $data["image"],
                    1440,
                    800,
                    CONF_UPLOAD_FILE_UPLOADS
                );
                removeFile(CONF_UPLOAD_FILE_UPLOADS, $model->image);
            }
        }

        $model->category_id = $data['category_id'];
        $model->title = $data['title'];
        $model->slug = $data['slug'];
        $model->description = $description;
        if ($data['image'] && $data['image'] !== "") {
            $model->image = $data['image'];
        }

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.noticias.edit", ["id" => $id]));
        }

        setFlashMessage("success", ["Notícia atualizado com sucesso"]);
        back_route(route("admin.noticias.edit", ["id" => $id]));
    }

    public function destroy($id)
    {
        if (!$model = (new News())->findById($id)) {
            setFlashMessage("danger", ["Notícia não encontrado."]);
            back_route(route("admin.noticias.index"));
        }

        removeFile(CONF_UPLOAD_FILE_UPLOADS, $model->image);

        $model->destroy();

        setFlashMessage("success", ["Notícia deletado com sucesso"]);
        back_route(route("admin.noticias.index"));
    }
}