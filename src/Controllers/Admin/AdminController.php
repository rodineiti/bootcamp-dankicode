<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Admin as User;

class AdminController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->data = array();
        $this->required = ["email", "password"];
    }

    public function index()
    {
        if (check("admins")) {
            back_route(route("admin.home"));
        }

        $this->template("admin/login");
    }

    public function profile()
    {
        $this->template("admin/profile");
    }

    public function login()
    {
        $request = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($request)) {
            setFlashMessage("danger", ["Favor, informar seu e-mail e senha"]);
            back_route();
        }

        $email = $request["email"];
        $password = $request["password"];

        $user = (new User())->attempt($email, $password);

        if (!$user) {
            setFlashMessage("danger", ["Usuário e/ou Senha errados!"]);
            back_route();
        }

        (new User())->setSession($user);

        setFlashMessage("success", ["Bem vindo " . auth("admins")->name]);
        back_route(route("admin.home"));
    }

    public function logout()
    {
        (new User())->destroySession();
        back_route(route("admin.login"));
    }
}