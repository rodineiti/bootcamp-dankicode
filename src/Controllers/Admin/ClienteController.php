<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto01\Client;
use Src\Models\Projeto01\Finance;

class ClienteController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["name","email","type","cpf_cnpj"];
    }

    public function index()
    {
        $this->data["list"] = (new Client())->select()->all();
        $this->template("admin/clientes/index", $this->data);
    }

    public function create()
    {
        $this->template("admin/clientes/create");
    }

    public function store()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.clientes.create"));
        }

        if ($this->request()->hasFile('image')) {
            $image = $this->request()->file('image');
            if (!$image["error"]) {
                $data["image"] = $image;
            }
        }

        if (isset($data["image"]) && count($data["image"])) {
            if (in_array($data["image"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $data["image"] = cutImage(
                    $data["image"],
                    200,
                    200,
                    CONF_UPLOAD_FILE_AVATARS
                );
            }
        }

        $model = new Client();
        $model->name = $data['name'];
        $model->email = $data['email'];
        $model->type = $data['type'];
        $model->cpf_cnpj = $data['cpf_cnpj'];
        $model->image = $data['image'];

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.clientes.create"));
        }

        setFlashMessage("success", ["Cliente adicionado com sucesso"]);
        back_route(route("admin.clientes.index"));
    }

    public function edit($id)
    {
        if (!$model = (new Client())->findById($id)) {
            setFlashMessage("danger", ["Cliente não encontrado."]);
            back_route(route("admin.clientes.index"));
        }

        $this->data = array();
        $this->data["item"] = $model;
        $this->data['financesP'] = $model->finances()->where('status', '=', 0)->all() ?? [];
        $this->data['financesC'] = $model->finances()->where('status', '=', 1)->all() ?? [];
        $this->template("admin/clientes/edit", $this->data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$model = (new Client())->findById($id)) {
            setFlashMessage("danger", ["Cliente não encontrado."]);
            back_route(route("admin.clientes.index"));
        }

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.clientes.edit", ["id" => $id]));
        }

        if ($this->request()->hasFile('image')) {
            $image = $this->request()->file('image');
            if (!$image["error"]) {
                $data["image"] = $image;
            }
        }

        if (isset($data["image"]) && count($data["image"])) {
            if (in_array($data["image"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $data["image"] = cutImage(
                    $data["image"],
                    200,
                    200,
                    CONF_UPLOAD_FILE_AVATARS
                );
                removeFile(CONF_UPLOAD_FILE_AVATARS, $model->image);
            }
        }

        $model->name = $data['name'];
        $model->email = $data['email'];
        $model->type = $data['type'];
        $model->cpf_cnpj = $data['cpf_cnpj'];
        if ($data['image'] && $data['image'] !== "") {
            $model->image = $data['image'];
        }

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.clientes.edit", ["id" => $id]));
        }

        setFlashMessage("success", ["Cliente atualizado com sucesso"]);
        back_route(route("admin.clientes.edit", ["id" => $id]));
    }

    public function addPayment($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$model = (new Client())->findById($id)) {
            setFlashMessage("danger", ["Cliente não encontrado."]);
            back_route(route("admin.clientes.index"));
        }

        $this->required = ["name","value","portion","interval","expiration"];
        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.clientes.edit", ["id" => $id]));
        }

        $checkData = checkdate(
            explode('-', $data['expiration'])[1],
            explode('-', $data['expiration'])[2],
            explode('-', $data['expiration'])[0]);

        if (!$checkData) {
            setFlashMessage("danger", ["Formato de data de vencimento inválida, favor corrigir"]);
            back_route(route("admin.clientes.edit", ["id" => $id]));
        }

        if (strtotime($data['expiration']) < strtotime(date('Y-m-d'))) {
            setFlashMessage("danger", ["Data inferior ao permitido."]);
            back_route(route("admin.clientes.edit", ["id" => $id]));
        }

        $portion = $data['portion'] && is_numeric($data['portion']) ? $data['portion'] : 0;
        $interval = $data['interval'] && is_numeric($data['interval']) ? $data['interval'] : 0;
        $error = array();

        for ($i = 0; $i < $portion; $i++) {
            $expiration = strtotime($data['expiration']) + (($i * $interval)*(60*60*24));

            $finance = new Finance();
            $finance->client_id = $model->id;
            $finance->name = $data['name'];
            $finance->value = str_price_db($data['value']);
            $finance->portion = ($i + 1);
            $finance->expiration = date('Y-m-d', $expiration);
            $finance->paid = date('Y-m-d');
            $finance->status = 0;
            $finance->order_id = 99;
            if (!$finance->save()) {
                $error[] = $finance->error()->getMessage();
            }
        }

        if (count($error)) {
            setFlashMessage("danger", $error);
            back_route(route("admin.clientes.edit", ["id" => $id]));
        }

        setFlashMessage("success", ["Pagamento(s) adicionado(s) com sucesso"]);
        back_route(route("admin.clientes.edit", ["id" => $id]));
    }

    public function updateStatusPayment($id, $finance_id)
    {
        if (!$model = (new Client())->findById($id)) {
            setFlashMessage("danger", ["Cliente não encontrado."]);
            back_route(route("admin.clientes.index"));
        }

        $finace = $model->finances()->where('id', '=', $finance_id)->first();

        if (!$finace) {
            setFlashMessage("danger", ["Pagamento não encontrado."]);
            back_route(route("admin.clientes.edit", ["id" => $id]));
        }

        $finace->status = 1;

        if (!$finace->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.clientes.edit", ["id" => $id]));
        }

        setFlashMessage("success", ["Pagamento atualizado para pago com sucesso"]);
        back_route(route("admin.clientes.edit", ["id" => $id]));
    }

    public function destroy($id)
    {
        if (!$model = (new Client())->findById($id)) {
            setFlashMessage("danger", ["Cliente não encontrado."]);
            back_route(route("admin.clientes.index"));
        }

        removeFile(CONF_UPLOAD_FILE_AVATARS, $model->image);

        $model->destroy();

        setFlashMessage("success", ["Cliente deletado com sucesso"]);
        back_route(route("admin.clientes.index"));
    }
}