<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto01\Online;
use Src\Models\Projeto01\Visit;

class HomeController extends Controller
{
    protected $data;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->data = array();
        $this->auth("admins");
    }

    public function index()
    {
        (new Online())->clear();

        $this->data['usersOnlineCount'] = (new Online())->select()->count();
        $this->data['usersOnline'] = (new Online())->select()->all() ?? [];
        $this->data['visitsCount'] = (new Visit())->getCount();
        $this->data['visitsCountDay'] = (new Visit())->getCount(date('Y-m-d'));
        $this->template("admin/home", $this->data);
    }
}