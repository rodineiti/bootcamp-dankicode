<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto01\Properties;
use Src\Models\Projeto01\PropertieHome;
use Src\Models\Projeto01\PropertiesHomeImage;

class ImovelController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["name","area","value","property_id"];
    }

    public function create($property_id)
    {
        if (!$property = (new Properties())->findById($property_id)) {
            setFlashMessage("danger", ["Empreendimento não encontrado."]);
            back_route(route("admin.empreendimentos.index"));
        }

        $this->data['property'] = $property;
        $this->template("admin/empreendimentos/imoveis/create", $this->data);
    }

    public function store()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos para cadastrar o imóvel"]);
            back_route(route("admin.empreendimentos.index"));
        }

        if (!$property = (new Properties())->findById($data['property_id'])) {
            setFlashMessage("danger", ["Empreendimento não encontrado."]);
            back_route(route("admin.empreendimentos.index"));
        }

        if ($this->request()->hasFile('images')) {
            $images = $this->request()->file('images');
            if (!in_array(true, $images["error"])) {
                $data["images"] = $images;
            }
        }

        $model = new PropertieHome();
        $model->property_id = $property->id;
        $model->name = $data["name"];
        $model->area = $data["area"] ?? 0;
        $model->value = str_price_db($data["value"]) ?? 0;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.imoveis.create", ["property_id" => $property->id]));
        }

        $errors = [];
        if (isset($data["images"]) && count($data["images"])) {
            for ($i = 0; $i < count($data["images"]['name']); $i++) {
                if (in_array($data["images"]['type'][$i], ["image/jpeg", "image/jpg", "image/png"])) {
                    $file = [
                        'tmp_name' => $data["images"]['tmp_name'][$i],
                        'name' => $data["images"]['name'][$i],
                        'type' => $data["images"]['type'][$i],
                        'size' => $data["images"]['size'][$i]
                    ];
                    $image = cutImage(
                        $file,
                        1440,
                        800,
                        CONF_UPLOAD_FILE_UPLOADS
                    );
                    $modelImage = new PropertiesHomeImage();
                    $modelImage->image = $image;
                    $modelImage->properties_home_id = $model->id;
                    if (!$modelImage->save()) {
                        $errors[] = $modelImage->error()->getMessage();
                    }
                }
            }
        }

        if (count($errors)) {
            setFlashMessage("danger", $errors);
        }

        setFlashMessage("success", ["Imóvel adicionado com sucesso"]);
        back_route(route("admin.empreendimentos.show", ["id" => $property->id]));
    }

    public function edit($id, $property_id)
    {
        if (!$property = (new Properties())->findById($property_id)) {
            setFlashMessage("danger", ["Empreendimento não encontrado."]);
            back_route(route("admin.empreendimentos.index"));
        }

        if (!$find = $property->imoveis()->where('id', '=', $id)->first()) {
            setFlashMessage("danger", ["Imóvel não encontrado."]);
            back_route(route("admin.empreendimentos.show", ["id" => $property->id]));
        }

        $this->data = array();
        $this->data['property'] = $property;
        $this->data['item'] = $find;
        $this->template("admin/empreendimentos/imoveis/edit", $this->data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.imoveis.edit", ["id" => $id]));
        }

        if (!$property = (new Properties())->findById($data['property_id'])) {
            setFlashMessage("danger", ["Empreendimento não encontrado."]);
            back_route(route("admin.empreendimentos.index"));
        }

        if (!$model = $property->imoveis()->where('id', '=', $id)->first()) {
            setFlashMessage("danger", ["Imóvel não encontrado."]);
            back_route(route("admin.empreendimentos.show", ["id" => $property->id]));
        }

        if ($this->request()->hasFile('images')) {
            $images = $this->request()->file('images');
            if (!in_array(true, $images["error"])) {
                $data["images"] = $images;
            }
        }

        $model->property_id = $property->id;
        $model->name = $data["name"];
        $model->area = $data["area"] ?? 0;
        $model->value = str_price_db($data["value"]) ?? 0;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.imoveis.edit", ["id" => $id, "property_id" => $property->id]));
        }

        $errors = [];
        if (isset($data["images"]) && count($data["images"])) {
            for ($i = 0; $i < count($data["images"]['name']); $i++) {
                if (in_array($data["images"]['type'][$i], ["image/jpeg", "image/jpg", "image/png"])) {
                    $file = [
                        'tmp_name' => $data["images"]['tmp_name'][$i],
                        'name' => $data["images"]['name'][$i],
                        'type' => $data["images"]['type'][$i],
                        'size' => $data["images"]['size'][$i]
                    ];
                    $image = cutImage(
                        $file,
                        1440,
                        800,
                        CONF_UPLOAD_FILE_UPLOADS
                    );
                    $modelImage = new PropertiesHomeImage();
                    $modelImage->image = $image;
                    $modelImage->properties_home_id = $model->id;
                    if (!$modelImage->save()) {
                        $errors[] = $modelImage->error()->getMessage();
                    }
                }
            }
        }

        if (count($errors)) {
            setFlashMessage("danger", $errors);
        }

        setFlashMessage("success", ["Imóvel atualizado com sucesso"]);
        back_route(route("admin.empreendimentos.show", ["id" => $property->id]));
    }

    public function destroy($id, $property_id)
    {
        if (!$property = (new Properties())->findById($property_id)) {
            setFlashMessage("danger", ["Empreendimento não encontrado."]);
            back_route(route("admin.empreendimentos.index"));
        }

        if (!$model = $property->imoveis()->where('id', '=', $id)->first()) {
            setFlashMessage("danger", ["Imóvel não encontrado."]);
            back_route(route("admin.empreendimentos.show", ["id" => $property->id]));
        }

        $images = $model->images()->all() ?? [];

        foreach ($images as $image) {
            removeFile(CONF_UPLOAD_FILE_UPLOADS, $image->image);
        }

        $model->destroy();

        setFlashMessage("success", ["Imóvel deletado com sucesso"]);
        back_route(route("admin.empreendimentos.show", ["id" => $property->id]));
    }

    public function destroyImage($home_id, $id, $property_id)
    {
        if (!$property = (new Properties())->findById($property_id)) {
            setFlashMessage("danger", ["Empreendimento não encontrado."]);
            back_route(route("admin.empreendimentos.index"));
        }

        if (!$home = $property->imoveis()->where('id', '=', $home_id)->first()) {
            setFlashMessage("danger", ["Imóvel não encontrado."]);
            back_route(route("admin.empreendimentos.show", ["id" => $property->id]));
        }

        if (!$image = $home->images()->where('id', '=', $id)->first()) {
            setFlashMessage("danger", ["Imagem do imóvel não encontrado."]);
            back_route(route("admin.imoveis.edit", ["id" => $home_id, "property_id" => $property->id]));
        }

        removeFile(CONF_UPLOAD_FILE_UPLOADS, $image->image);

        $image->destroy();

        setFlashMessage("success", ["Imagem do imóvel deletado com sucesso"]);
        back_route(route("admin.imoveis.edit", ["id" => $home_id, "property_id" => $property->id]));
    }
}