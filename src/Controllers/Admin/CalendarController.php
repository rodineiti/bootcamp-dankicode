<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto04\Todo;

class CalendarController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["title","date"];
    }

    public function index()
    {
        $mes = date('m', time());
        $ano = date('Y', time());

        if ($mes > 12) {
            $mes = 12;
        }
        if ($mes < 1) {
            $mes = 1;
        }

        $numeroDias = days_in_month($mes, $ano);
        $diaInicialdoMes = date('m', strtotime("$ano-$mes-01"));
        $diaAtual = date('d', time());
        $dataAtual = "$ano-$mes-$diaAtual";


        $tasks = (new Todo())->select()->where('date', '=', $dataAtual)->all() ?? [];

        $this->data['mes'] = $mes;
        $this->data['ano'] = $ano;
        $this->data['numeroDias'] = $numeroDias;
        $this->data['diaInicialdoMes'] = $diaInicialdoMes;
        $this->data['diaAtual'] = $diaAtual;
        $this->data['dataAtual'] = $dataAtual;
        $this->data['tasks'] = $tasks;

        $this->template("admin/calendario/index", $this->data);
    }

    public function store()
    {
        if ($this->method() !== 'POST') {
            $this->json(['error' => true, 'message' => 'Method not allowed']);
        }

        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            $this->json(['error' => true, 'message' => 'Favor, informe a data e o título']);
        }

        $model = new Todo();
        $model->title = $data['title'];
        $model->date = $data['date'];

        if (!$model->save()) {
            $this->json(['error' => true, 'message' => $model->error()->getMessage()]);
        }

        $this->json(['error' => false, 'message' => 'Tarefa adicionada com sucesso', 'title' => $model->title]);
    }

    public function last()
    {
        if ($this->method() !== 'POST') {
            $this->json(['error' => true, 'message' => 'Method not allowed']);
        }

        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);
        
        $this->required = ["date"];
        if (!$this->required($data)) {
            $this->json(['error' => true, 'message' => 'Favor, informe a data']);
        }

        $results = (new Todo())->select()->where('date', '=', $data['date'])->all() ?? [];
        $data = [];
        foreach ($results as $value) {
            $data[] = $value->data();
        }

        $this->json(['error' => false, 'todos' => $data]);
    }
}