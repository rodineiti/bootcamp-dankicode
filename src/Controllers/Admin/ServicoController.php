<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto01\Service;

class ServicoController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["name", "description"];
    }

    public function index()
    {
        $this->data["list"] = (new Service())->select()->all();
        $this->template("admin/servicos/index", $this->data);
    }

    public function create()
    {
        $this->template("admin/servicos/create");
    }

    public function store()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);
        $description = $this->request()->description;

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.servicos.create"));
        }

        $model = new Service();
        $model->name = $data['name'];
        $model->description = $description;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.servicos.create"));
        }

        setFlashMessage("success", ["Serviço adicionado com sucesso"]);
        back_route(route("admin.servicos.index"));
    }

    public function edit($id)
    {
        if (!$model = (new Service())->findById($id)) {
            setFlashMessage("danger", ["Serviço não encontrado."]);
            back_route(route("admin.servicos.index"));
        }

        $this->data = array();
        $this->data["item"] = $model;
        $this->template("admin/servicos/edit", $this->data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);
        $description = $this->request()->description;

        if (!$model = (new Service())->findById($id)) {
            setFlashMessage("danger", ["Serviço não encontrado."]);
            back_route(route("admin.servicos.index"));
        }

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.servicos.edit", ["id" => $id]));
        }

        $model->name = $data['name'];
        $model->description = $description;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.servicos.edit", ["id" => $id]));
        }

        setFlashMessage("success", ["Serviço atualizado com sucesso"]);
        back_route(route("admin.servicos.edit", ["id" => $id]));
    }

    public function destroy($id)
    {
        if (!$model = (new Service())->findById($id)) {
            setFlashMessage("danger", ["Serviço não encontrado."]);
            back_route(route("admin.servicos.index"));
        }

        $model->destroy();

        setFlashMessage("success", ["Serviço deletado com sucesso"]);
        back_route(route("admin.servicos.index"));
    }
}