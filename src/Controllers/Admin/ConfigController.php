<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto01\Config;

class ConfigController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = [
            "title",
            "author",
            "description",
            "icon1",
            "title1",
            "description1",
            "icon2",
            "title2",
            "description2",
            "icon3",
            "title3",
            "description3"
        ];
    }

    public function edit()
    {
        if (!$model = (new Config())->select()->first()) {
            setFlashMessage("danger", ["Configuração não encontrada."]);
            back_route(route("admin.config.index"));
        }

        $this->data = array();
        $this->data["item"] = $model;
        $this->template("admin/config/edit", $this->data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$model = (new Config())->findById($id)) {
            setFlashMessage("danger", ["Configuração não encontrada."]);
            back_route(route("admin.config.index"));
        }

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.config.edit", ["id" => $id]));
        }

        $model->title = $data['title'];
        $model->author = $data['author'];
        $model->description = $this->request()->description;
        $model->icon1 = $data['icon1'];
        $model->title1 = $data['title1'];
        $model->description1 = $this->request()->description1;
        $model->icon2 = $data['icon2'];
        $model->title2 = $data['title2'];
        $model->description2 = $this->request()->description2;
        $model->icon3 = $data['icon3'];
        $model->title3 = $data['title3'];
        $model->description3 = $this->request()->description3;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
        } else {
            setFlashMessage("success", ["Dados atualizados com sucesso"]);
        }

        back_route(route("admin.config.edit", ["id" => $id]));
    }
}