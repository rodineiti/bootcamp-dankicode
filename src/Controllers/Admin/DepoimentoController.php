<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto01\Deposition;

class DepoimentoController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["name", "description"];
    }

    public function index()
    {
        $this->data["list"] = (new Deposition())->select()->all();
        $this->template("admin/depoimentos/index", $this->data);
    }

    public function create()
    {
        $this->template("admin/depoimentos/create");
    }

    public function store()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);
        $description = $this->request()->description;

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.depoimentos.create"));
        }

        $model = new Deposition();
        $model->name = $data['name'];
        $model->description = $description;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.depoimentos.create"));
        }

        setFlashMessage("success", ["Depoimento adicionado com sucesso"]);
        back_route(route("admin.depoimentos.index"));
    }

    public function edit($id)
    {
        if (!$model = (new Deposition())->findById($id)) {
            setFlashMessage("danger", ["Depoimento não encontrado."]);
            back_route(route("admin.depoimentos.index"));
        }

        $this->data = array();
        $this->data["item"] = $model;
        $this->template("admin/depoimentos/edit", $this->data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);
        $description = $this->request()->description;

        if (!$model = (new Deposition())->findById($id)) {
            setFlashMessage("danger", ["Depoimento não encontrado."]);
            back_route(route("admin.depoimentos.index"));
        }

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.depoimentos.edit", ["id" => $id]));
        }

        $model->name = $data['name'];
        $model->description = $description;

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.depoimentos.edit", ["id" => $id]));
        }

        setFlashMessage("success", ["Depoimento atualizado com sucesso"]);
        back_route(route("admin.depoimentos.edit", ["id" => $id]));
    }

    public function destroy($id)
    {
        if (!$model = (new Deposition())->findById($id)) {
            setFlashMessage("danger", ["Depoimento não encontrado."]);
            back_route(route("admin.depoimentos.index"));
        }

        $model->destroy();

        setFlashMessage("success", ["Depoimento deletado com sucesso"]);
        back_route(route("admin.depoimentos.index"));
    }
}