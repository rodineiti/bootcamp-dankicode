<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto03\Chat;
use Src\Support\Session;

class ChatController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["message"];
    }

    public function index()
    {
        $chats = (new Chat())->select()->all() ?? [];
        $lastId = 0;

        $this->data['chats'] = $chats;
        $this->data['lastId'] = $lastId;
        $this->template("admin/chat/index", $this->data);
    }

    public function store()
    {
        if ($this->method() !== 'POST') {
            $this->json(['error' => true, 'message' => 'Method not allowed']);
        }

        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            $this->json(['error' => true, 'message' => 'Favor, informe a mensagem']);
        }

        $model = new Chat();
        $model->user_id = auth("admins")->id;
        $model->message = $data['message'];

        if (!$model->save()) {
            $this->json(['error' => true, 'message' => $model->error()->getMessage()]);
        }

        Session::set('lastId', $model->id);

        $this->json(['error' => false, 'message' => $model->message, 'user' => auth("admins")->name]);
    }

    public function last()
    {
        if ($this->method() !== 'POST') {
            $this->json(['error' => true, 'message' => 'Method not allowed']);
        }

        $results = (new Chat())->select()->where('id', '>', Session::get('lastId'))->all() ?? [];
        $chats = [];
        foreach ($results as $value) {
            $data = $value->data();
            $data->user = $value->user()->data();
            $chats[] = $data;
        }

        $this->json(['error' => false, 'chats' => $chats, 'lastId' => Session::get('lastId')]);
    }
}