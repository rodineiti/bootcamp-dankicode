<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto07\Module;

class ModuloController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["name"];
    }

    public function index()
    {
        $this->data["list"] = (new Module())->select()->all();
        $this->template("admin/modulos/index", $this->data);
    }

    public function create()
    {
        $this->template("admin/modulos/create");
    }

    public function store()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.modulos.create"));
        }

        $model = new Module();
        $model->name = $data['name'];

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.modulos.create"));
        }

        setFlashMessage("success", ["Módulo adicionado com sucesso"]);
        back_route(route("admin.modulos.index"));
    }

    public function edit($id)
    {
        if (!$find = (new Module())->findById($id)) {
            setFlashMessage("danger", ["Módulo não encontrado."]);
            back_route(route("admin.modulos.index"));
        }

        $this->data = array();
        $this->data["item"] = $find;
        $this->template("admin/modulos/edit", $this->data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$model = (new Module())->findById($id)) {
            setFlashMessage("danger", ["Módulo não encontrado."]);
            back_route(route("admin.modulos.index"));
        }

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.modulos.edit", ["id" => $id]));
        }

        $model->name = $data['name'];

        if (!$model->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.modulos.edit", ["id" => $id]));
        }

        setFlashMessage("success", ["Módulo atualizado com sucesso"]);
        back_route(route("admin.modulos.edit", ["id" => $id]));
    }

    public function destroy($id)
    {
        if (!$model = (new Module())->findById($id)) {
            setFlashMessage("danger", ["Módulo não encontrado."]);
            back_route(route("admin.modulos.index"));
        }

        foreach ($model->lessons()->all() ?? [] as $value) {
            $value->destroy();
        }

        $model->destroy();

        setFlashMessage("success", ["Módulo deletado com sucesso"]);
        back_route(route("admin.modulos.index"));
    }
}