<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto01\Client;
use Src\Models\Projeto01\Finance;
use Src\Support\Projeto01\Pdf;

class PagamentoController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
    }

    public function index()
    {
        $this->data["financesP"] = (new Finance())->select()->where('status', '=', 0)->all() ?? [];
        $this->data["financesC"] = (new Finance())->select()->where('status', '=', 1)->all() ?? [];
        $this->template("admin/pagamentos/index", $this->data);
    }

    public function updateStatusPayment($client_id, $finance_id)
    {
        if (!$model = (new Client())->findById($client_id)) {
            setFlashMessage("danger", ["Cliente não encontrado."]);
            back_route(route("admin.pagamentos.index"));
        }

        $finace = $model->finances()->where('id', '=', $finance_id)->first();

        if (!$finace) {
            setFlashMessage("danger", ["Pagamento não encontrado."]);
            back_route(route("admin.pagamentos.index"));
        }

        $finace->status = 1;

        if (!$finace->save()) {
            setFlashMessage("danger", [$model->error()->getMessage()]);
            back_route(route("admin.pagamentos.index"));
        }

        setFlashMessage("success", ["Pagamento atualizado para pago com sucesso"]);
        back_route(route("admin.pagamentos.index"));
    }

    public function pdfFinance($status)
    {
        $finance = (new Finance())->select()
                ->where('status', '=', (strtolower($status) === 'pendente' ? 0 : 1))
                ->all() ?? [];

        $pdf = new Pdf("templateFinanceiro.php");
        $pdf->render(['list' => $finance, 'status' => $status]);
    }
}