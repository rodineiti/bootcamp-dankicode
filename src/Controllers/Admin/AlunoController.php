<?php

namespace Src\Controllers\Admin;

use Src\Core\Controller;
use Src\Models\Projeto07\Student;

class AlunoController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct("admin/template");
        $this->auth("admins");

        $this->data = array();
        $this->required = ["name", "email", "password"];
    }

    public function index()
    {
        $this->data["users"] = (new Student())->select()->all();
        $this->template("admin/alunos/index", $this->data);
    }

    public function create()
    {
        $this->template("admin/alunos/create");
    }

    public function store()
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.alunos.create"));
        }

        $user = new Student();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = $data['password'];

        if (!$user->save()) {
            setFlashMessage("danger", [$user->error()->getMessage()]);
            back_route(route("admin.alunos.create"));
        }

        setFlashMessage("success", ["Aluno adicionado com sucesso"]);
        back_route(route("admin.alunos.index"));
    }

    public function edit($id)
    {
        if (!$user = (new Student())->findById($id)) {
            setFlashMessage("danger", ["Aluno não encontrado."]);
            back_route(route("admin.alunos.index"));
        }

        $this->data = array();
        $this->data["user"] = $user;
        $this->template("admin/alunos/edit", $this->data);
    }

    public function update($id)
    {
        $data = filter_var_array($this->request()->all(), FILTER_SANITIZE_STRIPPED);

        if (!$user = (new Student())->findById($id)) {
            setFlashMessage("danger", ["Aluno não encontrado."]);
            back_route(route("admin.alunos.index"));
        }

        $this->required = ["name", "email"];
        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor, preencher todos os campos"]);
            back_route(route("admin.alunos.edit", ["id" => $id]));
        }

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = (!empty($data["password"]) ? $data["password"] : $user->password);

        if (!$user->save()) {
            setFlashMessage("danger", [$user->error()->getMessage()]);
            back_route(route("admin.alunos.edit", ["id" => $id]));
        }

        back_route(route("admin.alunos.index"));
    }

    public function destroy($id)
    {
        if (!$user = (new Student())->findById($id)) {
            setFlashMessage("danger", ["Aluno não encontrado."]);
            back_route(route("admin.alunos.index"));
        }

        $user->destroy();

        setFlashMessage("success", ["Aluno deletado com sucesso"]);
        back_route(route("admin.alunos.index"));
    }
}