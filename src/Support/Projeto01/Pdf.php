<?php

namespace Src\Support\Projeto01;

use Mpdf\Mpdf;
use Mpdf\MpdfException;

class Pdf
{
    protected $mpdf;
    protected $file;

    public function __construct($file, $orientation = 'L')
    {
        $this->file = $file;
        try {
            $this->mpdf = new Mpdf(['orientation', $orientation]);
        } catch (MpdfException $e) {
            dd($e->getMessage());
        }
    }

    public function render($data = [])
    {
        try {
            ob_start();
            extract($data);
            require __DIR__ . "/../../../" . CONF_PDF_DIR . "/" . $this->file;
            $this->mpdf->WriteHTML(ob_get_clean());
            $this->mpdf->Output();
        } catch (MpdfException $e) {
            dd($e->getMessage(), "FILE: " . __FILE__
                . " \n CLASS: " . __CLASS__
                .  " \n FUNCTION: " . __FUNCTION__
                . " \n LINE: " . __LINE__
                . " \n file: " . __DIR__ . "/../../../" . CONF_PDF_DIR . "/" . $this->file);
        }
    }
}