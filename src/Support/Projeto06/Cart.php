<?php

namespace Src\Support\Projeto06;

use Src\Models\Projeto01\Product;

class Cart
{
    public function add($product, $qty)
    {
        if (!isset($_SESSION["cart"])) {
            $_SESSION["cart"] = array();
        }

        if (isset($_SESSION["cart"][$product->id])) {
            $_SESSION["cart"][$product->id] += $qty;
        } else {
            $_SESSION["cart"][$product->id] = $qty;
        }
    }

    public function get()
    {
        $data = array();
        if (isset($_SESSION["cart"])) {
            foreach ($_SESSION["cart"] as $id => $qty) {
                $product = (new Product())->findById($id);
                $image = $product->images()->first();
                $data[] = [
                    "id" => $id,
                    "qty" => $qty,
                    "value" => $product->value,
                    "name" => $product->name,
                    "image" => $image->image,
                ];
            }
        }
        return $data;
    }

    public function del($product)
    {
        if (isset($_SESSION["cart"][$product->id])) {
            unset($_SESSION["cart"][$product->id]);
        }
        return true;
    }

    public function inc($product)
    {
        if (isset($_SESSION["cart"][$product->id])) {
            $_SESSION["cart"][$product->id] += 1;
        }
        return true;
    }

    public function dec($product)
    {
        if (isset($_SESSION["cart"][$product->id])) {
            if ($_SESSION["cart"][$product->id] > 1) {
                $_SESSION["cart"][$product->id] -= 1;
            } else {
                unset($_SESSION["cart"][$product->id]);
            }
        }
        return true;
    }

    public function count()
    {
        $qty = 0;

        $cart = $this->get();
        foreach ($cart as $item) {
            $qty += intval($item["qty"]);
        }

        return $qty;
    }

    public function subtotal()
    {
        $subtotal = 0;

        $cart = $this->get();
        foreach ($cart as $item) {
            $subtotal += (floatval($item["value"]) * intval($item["qty"]));
        }

        return $subtotal;
    }

    public function total()
    {
        return $this->subtotal();
    }

    public function destroy()
    {
        unset($_SESSION["cart"]);
        unset($_SESSION["shipping"]);
        return true;
    }

}